/**
 * @file drivingExtensionalShear.h
 * @author David Head
 * @brief Extensional shear, *i.e.* contracts along the y-axis and expands along the x-axes such that the volume is preserved.
 */
 

#ifndef FIBRE_NETWORK_HI_DRIVING_EXTENSIONAL_SHEAR_H
#define FIBRE_NETWORK_HI_DRIVING_EXTENSIONAL_SHEAR_H


//
// Includes.
//

// Standard includes.
#include <complex>

// Parent class.
#include "drivingBase.h"


/**
 * @brief Extensional shear, *i.e.* contracts along the y-axis and expands along the x-axes such that the volume is preserved.
 */
class drivingExtensionalShear : public drivingBase
{

private:

protected:

public:

    // Constructors / destructor.
    drivingExtensionalShear( double X, double Y, const XMLParameters *params, ostream *log )
        : drivingBase(X,Y,params,log) {}
    virtual ~drivingExtensionalShear() {}

    /// Textual representation of the driving type.
    virtual std::string typeLabel() const override { return "drivingExtensionalShear"; }

    /**
     * @brief Strain tensor for extensional strain.
     * 
     * The strain tensor for extensional shear, expanding in the x-direction and contracting in the
     * y-direction, normalised such that the overall magnitude \gamma=1.
     * 
     * \f[
     *   \gamma = \left(
     *         \begin{array}{cc}
     *              1 & 0 \\
     *              0 & -1 
     *          \end{array}
     *    \right)
     * \f]
     */
    virtual std::array< std::array<double,2>, 2 > strainTensor() const override { return {{ {1.0,0.0}, {0.0,-1.0} }}; }

    /**
     * @brief Returns an ordered std::map with one labels and corresponding network extensional modulus.
     */
    virtual moduliMap networkModuli( tensor2D sigma_ij ) const override
        { return {{"G",sigma_ij[0][0]}}; }

    /**
     * @brief Returns an ordered std::map with labels and the corresponding fluid extensional modulus.
     */
    virtual moduliMap fluidModuli( double omega, double viscosity ) const override
        { return {{"G",{0.0,2.0*omega*viscosity}}}; }
};



#endif


