/**
 * @file drivingSimpleShear.h
 * @author David Head
 * @brief Simple shear, *i.e.* only the x-components are non-zero.
 */


#ifndef FIBRE_NETWORK_HI_DRIVING_SIMPLE_SHEAR_H
#define FIBRE_NETWORK_HI_DRIVING_SIMPLE_SHEAR_H


//
// Includes.
//

// Standard includes.
#include <complex>

// Parent class.
#include "drivingBase.h"


/**
 * @brief Simple shear, *i.e.* only the x-components are non-zero.
 */
class drivingSimpleShear : public drivingBase
{

private:

protected:

public:

    // Constructors / destructor.
    drivingSimpleShear( double X, double Y, const XMLParameters *params, ostream *log )
        : drivingBase(X,Y,params,log) {}
    virtual ~drivingSimpleShear() {}

    /// Textual representation of the driving type.
    virtual std::string typeLabel() const override { return "drivingSimpleShear"; }

    /**
     * @brief Strain tensor for simple shear.
     * 
     * The strain tensor for simple shear, normalised such that the overall magnitude \gamma=1.
     * This is oriented as normally visualised, *i.e.*
     * 
     * \f[
     *   \gamma = \left(
     *         \begin{array}{cc}
     *              0 & 1 \\
     *              0 & 0 
     *          \end{array}
     *    \right)
     * \f]
     */
    virtual std::array< std::array<double,2>, 2 > strainTensor() const override { return {{ {0.0,1.0}, {0.0,0.0} }}; }

    /*
     * @brief Map of moduli extracted from the given strain.
     *
     * Returns an ordered std::map with one label ('G') and the corresponding network shear modulus.
     */
    virtual moduliMap networkModuli( tensor2D sigma_ij ) const override
        { return {{"G",sigma_ij[0][1]}}; }

    /**
     * @brief Returns an ordered std::map with one label ('G') and the corresponding fluid shear modulus.
     */
    virtual moduliMap fluidModuli( double omega, double viscosity ) const override
        { return {{"G",{0.0,omega*viscosity}}}; }
};



#endif


