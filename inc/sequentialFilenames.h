//
// Small class to help generating indexed filenames in sequential order, such as "state_0000.xml",
// "state_0001.xml" etc., and determining the current highest such index in a directory.
//
// Provided as static routines that do the work, and class that has instance function wrappers
// that insert instance variables. In this way, the static methods are available prior to construction.
//
// Typical usage:
// =============
//
// sequentialFilenames seq( numDigits, prefix, suffix );
// 
// - seq.filename() will return "prefix_<nn>suffix", where <nn> is the current index with leading zeros
//  to make exactly numDigits digits.
//
// - seq.index( "prefix_<nn>suffix" ) will return the index using the stored predix and suffix.
//
// - seq.largestFileIndex() will return the largest index, without breaks, starting from zero, of
//   all files in the current directory (can specify subdir).
//
// Note that UNIX filenaming is assumed throughout, i.e. directories are denoted '/'. May be possible to
// generalise to Windows.
//


#ifndef _UD_SEQUENTIAL_FILENAMES_H
#define _UD_SEQUENTIAL_FILENAMES_H


//
// Includes
//
#include <string>
#include <sstream>
#include <stdexcept>
#include <fstream>


class sequentialFilenames
{
private:

protected:

	// The current index and the maximum no. of digits
	int _currentIndex;
	int _numDigits;
	
	// The prefix and suffix strings
	std::string _prefix;
	std::string _suffix;
	
	// Returns i as string, with leading zeros to required length.
	static std::string _fixedZeros( int i, int numDigits );

	// Maximum index given the number of digits
	static int _maxIndex( int nDigits );

public:

	//
	// Constructors and destructor
	//
	sequentialFilenames() : _currentIndex(0), _numDigits(4) {}
	sequentialFilenames( int numDigits, std::string prefix="", std::string suffix="" );
	virtual ~sequentialFilenames() {}
	
	//
	// Accessors
	//
	inline int index    () const noexcept { return _currentIndex; }
	inline int numDigits() const noexcept { return _numDigits;    }
	
	inline std::string filename()                    const { return indexedFilename(_currentIndex,_numDigits,_prefix,_suffix); }
	inline int         indexOf ( std::string fname ) const { return indexOfFilename(fname,_prefix,_suffix); }

	inline int largestFileIndex( std::string dir="" ) const { return largestFileIndex(_numDigits,dir,_prefix,_suffix); }

	//
	// Modifiers
	//
	       void setIndex( int index );
	inline void setIndex( std::string filename ) { setIndex( indexOf(filename) ); }

	inline void incrementIndex() { setIndex(_currentIndex+1); }
	inline void decrementIndex() { setIndex(_currentIndex-1); }

	inline sequentialFilenames& operator++()      { incrementIndex(); return *this; }		// ++obj
	inline sequentialFilenames  operator++( int ) { incrementIndex(); return *this; }		// obj++
	inline sequentialFilenames& operator--()      { decrementIndex(); return *this; }		// --obj
	inline sequentialFilenames  operator--( int ) { decrementIndex(); return *this; }		// obj--

	//
	// Static methods; the instance methods are essentially wrappers around these.
	// Provided as sometimes useful to access these prior to construction.
	//
	static std::string indexedFilename ( int index, int numDigits, std::string prefix, std::string suffix );
	static int         indexOfFilename ( std::string fname , std::string prefix="", std::string suffix="" );
	static int         largestFileIndex( int numDigits, std::string dir="", std::string prefix="", std::string suffix="" );

};

#endif


