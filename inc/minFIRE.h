//
// Implementation of the FIRE algorithm of Bitzek et al., Phys. Rev. Lett. 97, 170201 (2006).
// Uses double precision and C++11 explicit multi-threading where possible.
//
// Usage:
// =====
// minFIRE min(nDOF);                   // Can also set no. of threads and MD integrator.
//
// int nIters = min.minimise(           // Example of minimisation. See also the test code.
//    x,            // Solution vector; double *,
//    [](const double *U, double *F) { fillAccnAndEnergy(U,nullptr,F); },
//    1e-6,         // Accn tolerance.
//    0.01,         // Initial dt.
//    0.1,          // Initial alpha.
//    100           // Max. iters per DOF.
// );
//
// Notes:
// =====
// Treats the input vector as acceleration rather than force, so may need to rescale by some mass.
//
//
// Parallelism:
// ===========
// Currently just have multi-threaded loops for each vector operation, but since these are fast anyway
// you would need large problem sizes for the benefits to outweight the overheads. This can effectively
// be switched off by setting nThreads=1 in the constructor.
//
// Scome scope for task parallelism in stages 1 and 2 of the FIRE algorithm - the two magnitudes and
// the power can be calculated simultaneously.
//


#ifndef UD_MIN_FIRE_H
#define UD_MIN_FIRE_H


//
// Includes.
//

// Standard includes.
#include <iostream>
#include <sstream>
#include <cmath>
#include <string>
#include <stdexcept>

#include <vector>
#include <functional>
#include <numeric>

#include <thread>
#include <future>


//
// Scoped enumrated types.
//
enum class fireMDIntegrator { fEuler, vVerlet };


class minFIRE
{

private:

    // The factor by which \Delta t_{\rm max} is larger than the initial \Delta t; 10.0 used in the paper.
    constexpr static auto __defaultDeltaTMaxFactor = 10.0;

protected:

    // Genetal parameters.
    int _nDOF;

    // MD integrator.
    fireMDIntegrator _MDType;

    // FIRE-specific parameters.
    int    _nMin;
    double _fInc;
    double _fAlpha;
    double _fDec;
    double _dtMax;
    double _dtLast;

    // Number of threads for parallel methods (can be less than hardware concurrency), and pre-calculated partitions.
    int _nThreads;
    std::vector<int> _threadStarts;
    std::vector<int> _threadSizes ;

    // Vectors.
    std::vector<double> _v;
    std::vector<double> _A;
 
    // Performs a single MD step using the chosen integrator. Fills the velocity vector _v.
    void _singleMDStep( double *x, const double *F, double dt );
    void _singleMDStep( double *x, const std::vector<double> &F, double dt ) { _singleMDStep(x,F.data(),dt); }
 
    void _singleMDStep_post( double *x, const double *F, double dt );
    void _singleMDStep_post( double *x, const std::vector<double> &F, double dt ) { _singleMDStep_post(x,F.data(),dt); }

    // Common vector operations. All assume a total vector size of _nDOF.
    double _dot      ( const double *x, const double *y ) const;                                // x \cdot y
    void   _daxpy    ( double *x, const double *y, double a ) const;                            // x = x + a y
    void   _daxpy_mod( double *x, const double *y, double a, double b ) const;                  // x = a x + b y
    void   _dabxyz   ( double *x, const double *y, const double *z, double a, double b ) const; // x = x + a y + b z
    void   _clear    ( double *x ) const;                                                       // x = 0

    // Wrappers for the vector operations. Can add to as required.
    inline double _dot( const double *x ) const { return _dot(x,x); }
    inline double _dot( const std::vector<double> &x, const std::vector<double> &y ) const { return _dot(x.data(),y.data()); }
    inline double _dot( const std::vector<double> &x ) const { return _dot(x.data()); }

    template< typename T >    
    inline double _mag( const T &x ) const { return std::sqrt(_dot(x)); }
   
    inline void _daxpy( std::vector<double> &x, const double *y, double a ) const { _daxpy(x.data(),y,a); }
    inline void _daxpy( double *x, const std::vector<double> &y, double a ) const { _daxpy(x,y.data(),a); }
    inline void _daxpy( std::vector<double> &x, const std::vector<double> &y, double a ) const { _daxpy(x.data(),y.data(),a); }

    inline void _dabxyz( double *x, const std::vector<double> &y, const double *z, double a, double b ) const
        { _dabxyz(x,y.data(),z,a,b); }

    inline void _daxpy_mod( std::vector<double> &x, const std::vector<double> &y, double a, double b ) const
        { _daxpy_mod(x.data(),y.data(),a,b); }
    
    inline void _clear( std::vector<double> &x ) const { _clear(x.data()); }

 public:

    // Constructors / destructor.
    minFIRE( int numDOF, int numThreads=std::thread::hardware_concurrency(), fireMDIntegrator MDType = fireMDIntegrator::vVerlet );
    virtual ~minFIRE() {}

    //
    // Setters/getters for FIRE parameters, except the timestep and initial alpha which are set when minimise() is called.
    //
    inline void set_nMin  ( int n    ) noexcept { _nMin   = n; }
    inline void set_fInc  ( double f ) noexcept { _fInc   = f; }
    inline void set_fDec  ( double f ) noexcept { _fDec   = f; }
    inline void set_fAlpha( double f ) noexcept { _fAlpha = f; }
    inline void set_dtMax ( double t ) noexcept { _dtMax  = t; }

    inline int    get_nMIn  () const noexcept { return _nMin  ; }
    inline double get_fInc  () const noexcept { return _fInc  ; }
    inline double get_fDec  () const noexcept { return _fDec  ; }
    inline double get_fAlpha() const noexcept { return _fAlpha; }
    inline double get_dtMax () const noexcept { return _dtMax ; }
    inline double get_dtLast() const noexcept { return _dtLast; }

    //
    // Main minimisation routine.
    //
    int minimise(
        double *x,                                                          // Solution vector; updated in-place.
        std::function<void(const double*,double*)> fillAccn,                // Acceleration given current x.
        double accnTol,                                                     // Magnitude of the acceleration vector.
        double init_dt,                                                     // The initial time step.
        double init_alpha = 0.1,                                            // The initial value of \alpha.
        int maxItersPerDOF = 100                                            // Max. iterations is scaled to the number of DOF.
    );

    //
    // Debugging.
    //
    
    // Similar to ::minimise() but with overdamped MD: x \rightarrow x + \gamma * F
    int debugOverdampedMD(
        double *x,
        std::function<void(const double*,double*)> fillAccn,
        double accnTol,
        double gamma,
        int maxItersPerDOF = 1000
    );

    //
    // Custom exceptions.
    //
    template< typename T > class BadParameterError;
    class ExceededMaxIterations;
};




#pragma mark -
#pragma mark Templated Custom exceptions (non-templated in .cpp)
#pragma mark -

template< typename T >
class minFIRE::BadParameterError : public std::runtime_error
{
private:
    std::string makeWhat( std::string label, T value, std::string reason, std::string file, int line )
    {
        std::ostringstream out;
        out << "Invalid value " << value << " for FIRE parameter '" << label << "': " << reason
            << " [in file " << file << ", line " << line << "]" << std::endl;
        return out.str();
    }

public:
    BadParameterError( std::string label, T value, std::string reason, std::string file, int line )
        : std::runtime_error( makeWhat(label,value,reason,file,line) ) {}
};



#endif


