/**
 * @file networkNone.h
 * @author David Head
 * @brief Surely the simplest possible network - none! Useful for developing/debugging the fluid solver in isolation.
 */
 

#ifndef FIBRE_NETWORK_HI_NETWORK_NONE_H
#define FIBRE_NETWORK_HI_NETWORK_NONE_H


//
// Includes
//

// Standard includes
#include <ostream>
#include <string>

// Project includes
#include "networkBase.h"			// Parent (base) class


/**
 * @brief Surely the simplest possible network - none! Useful for developing/debugging the fluid solver in isolation.
 */
class networkNone : public networkBase
{

private:

protected:

	/// Zero volume.
	virtual double _volume() const override { return 0.0; }

	/// The sum of r_{i} f_{j} due to the network is always zero here.
	virtual std::array< std::array< std::complex<double>, 2 >, 2 > _sum_ri_fj() const override
		{ return {{ {0.0,0.0}, {0.0,0.0} }}; }

	/// String label for class type.
	virtual std::string _typeLabel() const noexcept override { return "networkNone"; }

public:
	networkNone() {}
	networkNone( double X, double Y, const XMLParameters *params, ostream *log, const drivingBase* driving )
		: networkBase(X,Y,params,log,driving) {}
	virtual ~networkNone() {}

	//
	// Analysis
	//
	virtual std::map< std::string, std::complex<double> > complexModuli() const override { return {{ }}; }
	virtual double coordinationNumber() const override { return 0.0; }
	virtual std::array< std::complex<double>, 2 > meanDisplacement() const override
		{ return {0.0,0.0}; }
	virtual std::vector< std::complex<double> > forceMoments( int maxMoment ) const override
		{ std::vector< std::complex<double> > fMoms(maxMoment+1,{0.0,0.0}); return fMoms; };

	//
	// I/O
	//
	virtual void saveSolution( ostream &out ) const override { out << _typeLabel() << std::endl; }

	//
	// Debugging
	//
	virtual double debugNodeResidual( double zeta ) const override { return 0.0; }
};


#endif
