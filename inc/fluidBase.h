/**
 * @file fluidBase.h
 * @author David Head
 * @brief Base class for the fluid. Provides the basic access methods.
 */


#ifndef FIBRE_NET_HI_FLUID_BASE_H
#define FIBRE_NET_HI_FLUID_BASE_H


//
// Includes.
//

// Standard includes.
#include <string>
#include <sstream>
#include <complex>
#include <array>
#include <set>

// Local includes.
#include "XMLParameters.h"

// Project includes.
#include "drivingBase.h"


//
// Constants; used for parsing the parameters file.
//
namespace parameterParsing
{
	const std::string fluid_viscosity = "viscosity";
	const std::string fluid_dampCoefficient = "dampCoefficient";
}


/**
 * @brief Base class for the fluid. Provides the basic access methods.
 * 
 * All fluid class ust derive from this abstract base.
 * 
 * The viscosity is defined here after being parsed from the parameters file, although is only really
 * used here for calling complexModuli() - child classes will typically make more use of this parameter.
 */
class fluidBase
{

private:

protected:

	// Shorthand.
    using moduliMap = std::map< std::string, std::complex<double> >;
 
	// Pointers to parameters and the log file; const where possible
	const XMLParameters *_params;
	      ostream       *_log;

	/// Type of strain control.
	const drivingBase *_driving;

	// Box dimensions.
	double _X;
	double _Y;

	/// Dynamic viscosity.
	double _eta;

	/// Drag coefficient; couples fluid to network.
	double _zeta;

	/// Frequency for the current solution; set in 'initialiseForSolution'.
	double _omega;

public:

	fluidBase() : _omega(0.0) {}
	fluidBase( double X, double Y, const XMLParameters *params, ostream *log, const drivingBase *driving );
	virtual ~fluidBase() {}

	//
	// Nature of the fluid implementation.
	//
	virtual bool canDrift() const noexcept = 0;
	virtual bool incompressible() const noexcept = 0;			///< Must preserve volume for this fluid type.

	//
	// Accessors.
	//
	inline double getZeta() const noexcept { return _zeta; }

	//
	// Solution routines
	//
	virtual void initialiseForSolution( double omega ) { _omega = omega; }
	virtual void tearDownAfterSolution() {}
	virtual void offsetVelocities( std::array< std::complex<double>,2 > delta_v ) {}
	virtual std::array<double,2> imagAffineOffset( double omega ) const { return { {0.0,0.0} }; }
	virtual std::array< std::complex<double>,2 > meanVelocity() const { return {0.0,0.0}; }

	//
	// Analysis
	//
	virtual unsigned int numCells() const noexcept { return 0; }
	virtual double nonAffinity() const { return 0.0; }
	virtual moduliMap complexModuli() const { return _driving->fluidModuli(_omega,_eta); }
	virtual std::array< std::complex<double>, 2 > velocityAt( double x, double y ) const = 0;
	
	//
	// I/O
	//
	virtual void saveSolution( ostream &out ) const = 0;

	//
	// Debugging
	//
	virtual double debugPressureResidual() const = 0;
	virtual double debugVelocityResidual( std::set<int> N, std::vector<double> P, const std::complex<double>* U ) const = 0;

	//
	// Exceptions
	//
	class BadParameter;
	class SystemError;
};



#pragma mark -
#pragma mark Exceptions
#pragma mark -

/**
 * @brief Custom exception for a bad parameter value.
 */
class fluidBase::BadParameter : public std::runtime_error
{
private:
        std::string makeWhat( std::string param, std::string why, std::string file, int line )
        {
                std::ostringstream out;
                out << "Bad fluid parameter '" << param << "'; " << why
                        << " [in file " << file << ", line " << line << "]" << std::endl;
                return out.str();
        }

public:
        BadParameter( std::string param, std::string why, std::string file, int line ) : std::runtime_error( makeWhat(param,why,file,line) ) {}
};

/**
 * @brief Custom exception for a general system error.
 */
class fluidBase::SystemError : public std::runtime_error
{
private:
        std::string makeWhat( std::string where, std::string why, std::string file, int line )
        {
                std::ostringstream out;
                out << "Fluid system error during " << where << "'; " << why
                        << " [in file " << file << ", line " << line << "]" << std::endl;
                return out.str();
        }

public:
        SystemError( std::string where, std::string why, std::string file, int line ) : std::runtime_error( makeWhat(where,why,file,line) ) {}
};


#endif
