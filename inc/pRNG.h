/**
 * @file pRNG.h
 * @author David Head
 * @brief Pseudo-random number generator; global but in its own namespace.
 */
 

#ifndef FIBRE_NET_HI_PRNG_H
#define FIBRE_NET_HI_PRNG_H


// Includes
#include <random>

/// Pseudo-random number generator.
namespace pRNG
{
	/// The random device ensures that each invocation of the executable has a different seed.
	extern std::random_device randomDevice;

	/// The pRNG engine itself. Can call with `.seed()` to re-initialise at any time.
	extern std::mt19937_64 engine;
	
	/// Distributions. Will extend list as required.
	extern std::uniform_real_distribution<double> dist_unitUniform;
	extern std::normal_distribution<double>       dist_unitNormal ;
	
	// Shorthand accessors
	double uniform();						///< *i.e.* `double x = pRNG::uniform();`
	double normal();
}


#endif
