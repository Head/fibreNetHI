/**
 * @file matrixNetwork.h
 * @author David Head
 * @brief Matrix with contributions from the network only, templated to the scalar type.
 */


#ifndef FIBRE_NET_HI_MATRIX_NETWORK_H
#define FIBRE_NET_HI_MATRIX_NETWORK_H


//
// Includes.
//

// Standard includes.
#include <vector>
#include <array>

// Local includes.
#include "smallSquareMatrix.h"

// Parent class.
#include "matrixBase.h"


/**
 * @brief Matrix with network nodes only. Supports a node indexer. Not an abstract class.
 */
template< typename T >
class matrixNetwork : public matrixBase<T>
{

private:

protected:

	// Matrix and RHS vectors declared in base class; also the total number of scalars.
	
	/// Size of the network mesh.
	unsigned int _numNetNodes;
	
	// Starting points for the block of displacement scalars.
	unsigned int _startDisps_x;
	unsigned int _startDisps_y;

	/// Indexer maps from network indices to matrix indices, typically skipping 'redundant' nodes.
	const std::vector<int> *_netNodeIndexer;

	/// Reduced node index given the network index. Throws an exception if not in the set of reduced nodes.
	int _matrixNodeIndex( int networkNode ) const;

public:

    //
    // Constructor and destructor.
    //
    matrixNetwork( unsigned int numNodes, unsigned int totalDOF, std::ostream *log=nullptr, const std::vector<int> *indexer=nullptr );
    virtual ~matrixNetwork() {};

	//
	// Accessors.
	//
	virtual const T* disps() { return &this->_sol[_startDisps_x]; }
	inline unsigned int numNetworkRows() const { return 2*_numNetNodes; }

	//
	// Matrix contributions. 
	//

	/**
	 * @brief Adds same value to x and y diagonal terms for the given node.
	 * 
	 * Adds the val to the diagonal term corresponding to the x-component of n,
	 * and again for the diaginal term corresponding to the y-component of n.
	 * 
	 * @param n The index of the node. Will be mapped to the reduced index.
	 * @param val The value added to the x and y diaginal terms.
	 */ 
	virtual void addDispsDiag_xy( unsigned int n, T val );

	/**
	 * @brief Adds local matrix to the global one.
	 * 
	 * Only applies to node n1, i.e. the (n1,n1) diagional block and one of the (n1,n2) off-diagonal blocks.
	 * To get all terms, i.e. also those on n2, would need to call twice.
	 * 
	 * @param local_M The local Hessian as a 2x2 matrix.
	 * @param n1 The node to which the contributions are added.
	 * @param n2 The node 'from' the forces derive; contributes to one off-diagonal block, with a sign flip.
	 * @param weight Optional weighting factor applied to all terms; defaults to 1.0.
	 */ 
	void addDispsLocalHessian( smallSquareMatrix<2,double> local_M, unsigned int n1, unsigned int n2, double weight=1.0 );
	
	//
	// RHS contributions.
	//

	/**
	 * @brief Add 2-vector to the RHS for node n.
	 * 
	 * Adds the x-component of the passed vector to the RHS of the equation for the x-component
	 * of the displacement for node n, and the same for the corresponding y-components.
	 * 
	 * @param n The node index. Will be mapped to reduced index.
	 * @param twoVec The 2-vector whose components are added to the RHS.
	 * @param weight An optional weighting factor that is equally applied to both contributions.
	 */
	void addDispsRHS_xy( unsigned int n, std::array<T,2> twoVec, double weight=1.0 );

	/**
	 * @brief Add from an array to the RHS for node n.
	 * 
	 * Adds the 0-component of the passed array to the x-component of the RHS of the equations
	 * for node n, and the 1-component to the corresponding y-component.
	 * 
	 * @param n The node index. Will be mapped to reduced index.
	 * @param array The array vector whose [0] and [1] components are added to the RHS.
	 * @param weight An optional weighting factor that is equally applied to both contributions.
	 */
	void addDispsRHS_xy( unsigned int n, T* array, double weight=1.0 );

	/**
	 * @brief Adds terms to the right-hand side vector given local matrix and offset vector.
	 * 
	 * @param local_M The local 2x2 matrix as a type lf smallSquareMatrix<2,...>.
	 * @param n The node index to which this offset applies.
	 * @param shift The 2-vector to which lodal_M is applied; normally a shift after moving to the RHS.
	 * @param weight Optional weight factor applied to all contributions.
	 */
	void addDispsRHS_xy( smallSquareMatrix<2,double> local_M, unsigned int n, double shift[2], double weight=1.0 );
    
    //
	// Exceptions.
	//
	class NodeNotIndexedError;							///< Network node does not correspond to a matrix node.
};


#pragma mark -
#pragma mark Exceptions
#pragma mark -

/**
 * @brief Custon exception for a bad node index.
 */
template< typename T >
class matrixNetwork<T>::NodeNotIndexedError : public std::runtime_error
{
private:
        std::string makeWhat( int n, std::string file, int line )
        {
                std::ostringstream out;
                out << "Bad network node " << n << "; does not correspond to a matrix node according to the provided indexer"
                    << " [in file " << file << ", line " << line << "]" << std::endl;
                return out.str();
        }

public:
        NodeNotIndexedError( int nodeIndex, std::string file, int line ) : std::runtime_error( makeWhat(nodeIndex,file,line) ) {}
};


#pragma mark -
#pragma mark Implementation in-file
#pragma mark -

// Constructor.
template< typename T >
matrixNetwork<T>::matrixNetwork(
	unsigned int netNodes,
    unsigned int totalDOF,
	std::ostream *log,
	const std::vector<int> *indexer )
	: matrixBase<T>(totalDOF,log), _numNetNodes(netNodes), _netNodeIndexer(indexer)
{
	// Starting points for each block of scalars.
	_startDisps_x = 0u;						// Redundant, but makes the code more readable.
	_startDisps_y = _numNetNodes;
}


#pragma mark -
#pragma mark Indexing
#pragma mark -

// Matrix node index given the network node index and indexer. Throws an exdeption if the index was -1.
template< typename T >
int matrixNetwork<T>::_matrixNodeIndex( int networkNode ) const
{
	if( networkNode<0 || networkNode>=int(_netNodeIndexer->size()) || _netNodeIndexer->at(networkNode)==-1 )
		throw NodeNotIndexedError(networkNode,__FILE__,__LINE__);

	return _netNodeIndexer->at(networkNode);
}



#pragma mark -
#pragma mark Matrix terms
#pragma mark -

// Adds same value to x and y diagonal terms for the given node.
template< typename T >
void matrixNetwork<T>::addDispsDiag_xy( unsigned int n, T val )
{
	int m = _matrixNodeIndex(n);

	this->_addToMatrix(_startDisps_x+m,_startDisps_x+m,val);
	this->_addToMatrix(_startDisps_y+m,_startDisps_y+m,val);
}

// Adds a (real) local matrix to the global matrix, similar to addDispsProjBlock.
template< typename T >
void matrixNetwork<T>::addDispsLocalHessian(
		smallSquareMatrix<2,double> local_M,
		unsigned int n1,
		unsigned int n2,
		double factor )
{
	int m1 = _matrixNodeIndex(n1), m2 = _matrixNodeIndex(n2);

	// Diagonal block (n1,n1).
	this->_addToMatrix( _startDisps_x+m1, _startDisps_x+m1, factor*local_M.get(0,0) );
	this->_addToMatrix( _startDisps_x+m1, _startDisps_y+m1, factor*local_M.get(0,1) );
	this->_addToMatrix( _startDisps_y+m1, _startDisps_x+m1, factor*local_M.get(1,0) );
	this->_addToMatrix( _startDisps_y+m1, _startDisps_y+m1, factor*local_M.get(1,1) );

	// Off-diagonal block (n1,n2) with sign flip.
	this->_addToMatrix( _startDisps_x+m1, _startDisps_x+m2, -factor*local_M.get(0,0) );
	this->_addToMatrix( _startDisps_x+m1, _startDisps_y+m2, -factor*local_M.get(0,1) );
	this->_addToMatrix( _startDisps_y+m1, _startDisps_x+m2, -factor*local_M.get(1,0) );
	this->_addToMatrix( _startDisps_y+m1, _startDisps_y+m2, -factor*local_M.get(1,1) );
}


#pragma mark -
#pragma mark RHS terms
#pragma mark -

// Adds contributions from a local matrix to the RHS vector elements corresponding to node n.
template< typename T >
void matrixNetwork<T>::addDispsRHS_xy( smallSquareMatrix<2,double> local_M, unsigned int n, double shift[2], double factor )
{
	int m = _matrixNodeIndex(n);

	this->_addToRHS( _startDisps_x+m, factor*( local_M.get(0,0)*shift[0] + local_M.get(0,1)*shift[1] ) );
	this->_addToRHS( _startDisps_y+m, factor*( local_M.get(1,0)*shift[0] + local_M.get(1,1)*shift[1] ) );
}

// From a normal C-array rather than a std::array.
template< typename T >
void matrixNetwork<T>::addDispsRHS_xy( unsigned int n, T *vals, double weight )
{
	int m = _matrixNodeIndex(n);

	this->_addToRHS(_startDisps_x+m,weight*vals[0]);
	this->_addToRHS(_startDisps_y+m,weight*vals[1]);
}

// Add 2-vector to the RHS for node n.
template< typename T >
void matrixNetwork<T>::addDispsRHS_xy( unsigned int n, std::array<T,2> twoVec, double weight )
{
	addDispsRHS_xy(n,twoVec.data(),weight);
}



#endif

 