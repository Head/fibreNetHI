/**
 * @file fluidDragOnly.h
 * @author David Head
 * @brief A very simple (the simplest?) fluid - just drag at the nodes.
 */
 

#ifndef FIBRE_NETWORK_HI_FLUID_DRAG_ONLY_H
#define FIBRE_NETWORK_HI_FLUID_DRAG_ONLY_H


//
// Includes
//

// Standard includes
#include <array>
#include <complex>
#include <string>
#include <ostream>
#include <set>

// Local includes
#include "XMLParameters.h"

// Project includes
#include "fluidBase.h" 								// The parent (base) class
#include "matrixTriSpringDrag.h"


/**
 * @brief A very simple (the simplest?) fluid - just drag at the nodes.
 *
 * The fluid flow field is assumed to be affine for the typical x-y shear,
 * with zero velocities in the midplane y=Y/2.
 */
class fluidDragOnly : public fluidBase
{
private:

protected:
	
public:

	fluidDragOnly() : fluidBase() {}
	fluidDragOnly( double X, double Y, const XMLParameters *params, ostream *log, const drivingBase *driving );
	virtual ~fluidDragOnly() {}

	//
	// Nature of the fluid implementation.
	//
	virtual bool canDrift() const noexcept override { return false; }
	virtual bool incompressible() const noexcept override { return false; }		///< Allow volume changes (leave interpretation to user).

	//
	// Fill the matrix terms for the drag force, i.e. the coupling to the network.
	//
	virtual void matrix_fillDragTerms( matrixTriSpringDrag *M, double omega, const std::set<int> netNodes, const std::vector<double> nodePosns ) const;

	//
	// Solution routines
	//
	virtual void offsetVelocities( std::array< std::complex<double>,2 >  delta_v ) override
		{ std::cerr << "WARNING: Attempting to offset fluid origin when the fluid field is fixed." << std::endl; }

	//
	// Analysis
	//
	virtual std::array< std::complex<double>, 2 > velocityAt( double x, double y ) const override
		{ return _driving->affineVelocity( x, y, _omega ); }

	//
	// I/O
	//
	virtual void saveSolution( ostream &out ) const override;

	//
	// Debugging
	//
	virtual double debugPressureResidual() const override { return 0.0; }
	virtual double debugVelocityResidual( std::set<int> N, std::vector<double> P, const std::complex<double>* U ) const override { return 0.0; }
};


#endif
