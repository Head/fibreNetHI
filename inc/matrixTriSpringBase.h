/**
 * @file matrixTriSpringBase.h
 * @author David Head
 * @brief Base class for matrix generated for a triangular spring network, but for an unspecified fluid type.
 */
 

#ifndef FIBRE_NET_HI_MATRIX_TRI_SPRING_BASE_H
#define FIBRE_NET_HI_MATRIX_TRI_SPRING_BASE_H


//
// Includes.
//

// Standard includes.
#include <vector>

// Local includes.
#include "smallSquareMatrix.h"

// Base class.
#include "matrixNetwork.h"


/**
 * @brief Base class for matrix generated for a triangular spring network, but for an unspecified fluid type.
 */
class matrixTriSpringBase : public matrixNetwork< std::complex<double> >
{

private:

protected:

public:

	// Constructor and destructor.
	matrixTriSpringBase( unsigned int netNodes, unsigned int numScalars, std::ostream *log=nullptr, const std::vector<int> *indexer=nullptr )
		: matrixNetwork(netNodes,numScalars,log,indexer) {}
	virtual ~matrixTriSpringBase() {}

};


#endif
