/**
 * @file matrixTriSpringDrag.h
 * @author David Head
 * @brief Builds and stores a matrix for a triangular spring lattice.
 */
 


#ifndef FIBRE_NET_HI_MATRIX_TRI_SPRING_DRAG_H
#define FIBRE_NET_HI_MATRIX_TRI_SPRING_DRAG_H


//
// Includes.
//

// Base class.
#include "matrixTriSpringBase.h"


/**
 * @brief Builds and stores a matrix for a triangular spring lattice, where the fluid is represented
 * purely as a drag term (assuming affine flow of the implicit fluid).
 */
class matrixTriSpringDrag : public matrixTriSpringBase
{
private:

protected:

public:

	// Constructor and destructor.
	matrixTriSpringDrag( unsigned int netNodes, std::ostream *log, const std::vector<int> *indexer );
	virtual ~matrixTriSpringDrag() {}

	//
	// Debugging
	//
	double debugResidual( const std::complex<double> *u ) const;
};



#endif

