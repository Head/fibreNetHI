//
// Parser for parameters provided in an XML file; allows some degree of nesting
// and thus supercedes the flat-XML parsing routines. In particular, it allows blocks
// of parameters to be specified and parsed as a block. Uses libxml2.
//
// Two class of parameter: Grouped and Ungrouped. The Ungrouped parameters are essentially
// a list, whereas the grouped types are first grouped according to a string tag, and then
// as a number of parameter blocks corresponding to a single group within that tag.
//
// Parameter type declarations (case insensitive):
// - "float", "scalar" -> scalar (stored as a double)
// - "int", "integer"  -> integer (stored as long [signed])
// - "flag", "bool"    -> flag; "yes"/"true" etc. allowed as values
// - "label", "string" -> label (stored as std::string; kept in original case)
//
// Note that comparison of variable names IS case sensitive, it's just the choice
// of e.g. 'Float' over 'float' that doesn't matter.
//
// Can also add new parameters directly using 'addScalarParam()' etc.
//
// See class definition for actual routines, but in brief, it works something like this:
//
// params = XMLParameters( filename );
//	- will read the file looking for a <Parameters...> block, and then tries to parse the ungrouped
//    (flat) <Parameter.../> objects, and the nested (1 level up) <ParameterGroup...> blocks.
//
// params.addScalar( name, value );
//  - and similar to addInteger, addLabel, addSwitch. Adds only to ungrouped parameters.
//
// params.hasScalarParam(name);	 (etc)
// - true/false if there is an ungrouped scalar of the given name
//
// params.scalarParam(name); (etc)
// - returns scalar variable; raises a custom exception if it does not exist.
//
// params.scalarParamOrDefault(name,def); (etc)
// - sim. to scalarParams(), but returns the supplied default value if it does not exist.
//
// For grouped parameters, there are similar routines with names like scalarParamForGroupWithTag()
// which require a tag and index within the list of groups for that tag. Also, it is possible to determine
// if there is a tag using hasGroupTag(), and the number of groups with numGroupsForTag().
//
// 2015/11/10/DAH: Altered read method to walk the tree, rather than loading in one go. This should help
// speed loading of large files (with a small parameters block embedded within in), as well as lower
// memory requirements.
//
// 2016/5/4/DAH: Changed "switch" to "flag", mostly to avoid confusion with the C/C++ keyword, but in any
// case flag is surely a better name.
//

#ifndef _UD_XMLPARAMETERS_H
#define _UD_XMLPARAMETERS_H

// Headers from libxml2; usually /usr/include/libxml2, but has been on /opt/local/include/libxml2 in the past.
#include <libxml/xmlreader.h>

// Standard includes
#include <iostream>
#include <string>
#include <ios>
#include <sstream>
#include <map>
#include <vector>
#include <stdexcept>



using namespace std;



class XMLParameters
{

private:

	// Returns true if an integer character: numerals 0 to 9 or minus sign (C++ doesn't allow 'e' for ints)
	bool __integer_char( char c ) const
		{ if( ( c>='0' and c<='9' ) || c=='-' ) return true; else return false; };

	// Returns true if an decimal character: same as integer, plus '.' and 'e' in either case
	bool __decimal_char( char c ) const
		{ if( __integer_char(c) || c=='.' || c=='e' || c=='E' ) return true; else return false; };

	// The reader ptr; can be opened in multiple ways
	xmlTextReaderPtr __readerPtr;
	
	// Performs parsing of a successfully-opened __readerPtr object
	void __parse();
		
protected:

	// Use STL objects to store the parameters. Each has a unique integer ID, so not all IDs exist in each of these containers.
	std::map<long,bool> _flags;						// All boolean parameters; keyed by ID
	std::map<long,long> _integers;					// All integer parameters; keyed by ID
	std::map<long,double> _scalars;					// All scalar parameters; keyed by ID
	std::map<long,std::string> _labels;				// All label parameters; keyed by ID
	std::map<long,std::string> _names;				// Names of the variables; keyed by ID
	std::map<std::string, std::vector< std::vector<long> > > _groups;
													// Grouped IDs: < tag, groupIndex, indexInGroup >

	// Returns the unique ID for the given parameter name, or -1 if it was not found
	virtual long _IDForName(const std::string&) const;

	// Grouped and ungrouped parameters
	std::vector<long> _ungroupedIDs;				// All ID's of ungrouped parameters
	
	// Remove a grouped ID from the name map, ID vector and value map
	virtual void _removeUngroupedID( long ID );

	// Used to track when we are in a <Parameters...> block
	bool _inParametersBlock;

	// Unique ID
	int _uniqueID;

	// Basic initialiser; always called; set up variables etc.
	void _init();
	
	// Get all attributes from one node, and return as an associative array
	virtual map<string,string> _getAllAttributes(xmlTextReaderPtr) const;
	
	// Returns the node name as a string
	string _nodeName( xmlTextReaderPtr _readerPtr ) const;

	// Parse a single node; returns 'false' when all parameters have been parsed
	virtual bool _processNode(xmlTextReaderPtr);
	
	// Parses an element node that resides within a <Parameters>...</Parameters> block.
	virtual void _parseElementNode(xmlTextReaderPtr);

	// Parses a single parameter, and stores the i.d. (if parsing was successful)
	virtual int _parseSingleParameter(xmlTextReaderPtr);						// Parses a single node; returns unique ID if successful, -1 if not
	
	// Outputs a single parameter (with no tabs prefixed or eol) to the given output stream
	virtual void _outputSingleParameter(long,std::ostream&) const;
	
	// Convert strings to integers or floats; may be overriden to permit e.g. unit conversion
	virtual long   _to_long ( std::string from ) const { return atol( from.c_str() ); }
	virtual double _to_float( std::string from ) const { return atof( from.c_str() ); }

	// When using the compact declaration <Parameter name="value" />, need to infer number type; standard versions here
	// assume no non-numeric characters (except 'e','.','-')
	virtual bool _looksLikeInteger( std::string value ) const;
	virtual bool _looksLikeFloat  ( std::string value ) const;

public:

	// Constructors and destructor
	XMLParameters() : _inParametersBlock(false), _uniqueID(0) {}		// Bare initialiser
	virtual ~XMLParameters() {};
	
	//
	// Start the parsing for the given filename or memory block
	//
	void parseFile  ( const std::string &name );
	void parseMemory( const char* buffer, int size );
	
	//
	// Output
	//
	friend std::ostream& operator<< (std::ostream&,const XMLParameters&);

	//
	// Add parameters; currently only ungrouped possible
	//
	virtual void addScalarParam ( const std::string &name, const double      &val );
	virtual void addIntegerParam( const std::string &name, const long        &val );
	virtual void addLabelParam  ( const std::string &name, const std::string &val );
	virtual void addFlagParam   ( const std::string &name, const bool        &val );

	// Change values of existing parameters; raise exception if they do not exist
	virtual void changeScalarParam ( const std::string &name, const double      &val );
	virtual void changeIntegerParam( const std::string &name, const long        &val );
	virtual void changeLabelParam  ( const std::string &name, const std::string &val );
	virtual void changeFlagParam   ( const std::string &name, const bool        &val );
	
	// Remove existing parameters; raise exception if they do not exist
	virtual void removeScalarParam ( const std::string &name );
	virtual void removeIntegerParam( const std::string &name );
	virtual void removeLabelParam  ( const std::string &name );
	virtual void removeFlagParam   ( const std::string &name );
	
	// Change or add a new parameter without specifying the type (parameter provided as string).
	virtual void overwriteOrAddParam( std::string name, std::string value );
	
	//
	// Checking for existence of parameters
	//

	// Ungrouped
	virtual bool hasScalarParam (const std::string&) const;
	virtual bool hasIntegerParam(const std::string&) const;
	virtual bool hasLabelParam  (const std::string&) const;
	virtual bool hasFlagParam   (const std::string&) const;
	
	// Grouped; can also get the sizes of the groups
	virtual bool hasGroupTag    (const std::string&) const;
	virtual bool hasParamInGroupWithTag(const std::string &,const long&,const std::string&) const;
	virtual int  numGroupsForTag(const std::string&) const;							// -1 if tag does not exist
	virtual int  numParamsForGroupWithTag(const int&,const std::string&) const;		// -1 if could not be found

	//
	// Accessing parameters
	//
	
	// These ungrouped versions raise an XMLParameters::CouldNotFindParameterError exception if the parameter does not exist
	virtual double      scalarParam (const std::string&) const;
	virtual long        integerParam(const std::string&) const;
	virtual bool        flagParam   (const std::string&) const;
	virtual std::string labelParam  (const std::string&) const;
	
	// These versions have a default option; if the parameter cannot be found, it returns the default. Also ungrouped.
	virtual double      scalarParamOrDefault (const std::string&,const double&     ) const;
	virtual long        integerParamOrDefault(const std::string&,const long&       ) const;
	virtual bool        flagParamOrDefault   (const std::string&,const bool&       ) const;
	virtual std::string labelParamOrDefault  (const std::string&,const std::string&) const;
	
	// Accessing parameters within groups; exception-throwing versions first
	virtual double      scalarParamForGroupWithTag (const std::string&,const int&,const std::string&) const;	// Param name; group no.; tag
	virtual long        integerParamForGroupWithTag(const std::string&,const int&,const std::string&) const;
	virtual bool        flagParamForGroupWithTag   (const std::string&,const int&,const std::string&) const;
	virtual std::string labelParamForGroupWithTag  (const std::string&,const int&,const std::string&) const;

	// ... and the default equivalents.
	virtual double      scalarParamForGroupWithTagOrDefault (const std::string&,const int&,const std::string&,const double&     ) const;
	virtual long        integerParamForGroupWithTagOrDefault(const std::string&,const int&,const std::string&,const long&       ) const;
	virtual bool        flagParamForGroupWithTagOrDefault   (const std::string&,const int&,const std::string&,const bool&       ) const;
	virtual std::string labelParamForGroupWithTagOrDefault  (const std::string&,const int&,const std::string&,const std::string&) const;

	//
	// Custom exceptions
	//
	class CouldNotFindParameterError;
	class ParameterAlreadyExists;
	class CouldNotFindTagError;
	class InvalidGroupNumberError;
};


#pragma mark -
#pragma mark Custom exceptions
#pragma mark -

// - for when a parameter is accessed (in the non-default accessor) but not found
class XMLParameters::CouldNotFindParameterError : public runtime_error {
private:
	string makeWhat( string name ) {
		ostringstream out;
		out << "ERROR: Could not find the requested parameter '" << name << "' in the XMLParameters object" << endl;
		return out.str();
	}
public:
	CouldNotFindParameterError( string paramName ) : runtime_error( makeWhat(paramName) ) {}
};

// - for when trying to reuse an existing name for the same type of parameter
class XMLParameters::ParameterAlreadyExists : public runtime_error {
private:
	string makeWhat( string name ) {
		ostringstream out;
		out << "ERROR: Parameter of the name '" << name << "' already exists; cannot add a second" << endl;
		return out.str();
	}
public:
	ParameterAlreadyExists( string name ) : runtime_error( makeWhat(name) ) {}
};

// - for when an unknown tag is used
class XMLParameters::CouldNotFindTagError : public runtime_error {
private:
	string makeWhat( string tag ) {
		ostringstream out;
		out << "ERROR: Could not find groups with the requested tag '" << tag << "' in the XMLParameters object" << endl;
		return out.str();
	}
public:
	CouldNotFindTagError( string tagName ) : runtime_error( makeWhat(tagName) ) {};
};

// - for when group number outside the valid range is accessed
class XMLParameters::InvalidGroupNumberError : public runtime_error {
private:
	string makeWhat( string tag, int num ) {
		ostringstream out;
		out << "ERROR: There is no group number " << num << " for groups with tag '" << tag << "'" << endl;
		return out.str();
	}
public:
	InvalidGroupNumberError( string tagName, int num ) : runtime_error( makeWhat(tagName,num) ) {};
};


#endif



