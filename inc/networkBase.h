/**
 * @file networkBase.h
 * @author David Head
 * @brief Base class for the discrete network. Provides methods that must be overridden.
 */


#ifndef FIBRE_NETWORK_HI_NETWORK_BASE_H
#define FIBRE_NETWORK_HI_NETWORK_BASE_H


//
// Includes
//

// Standard includes
#include <stdexcept>
#include <sstream>
#include <ostream>
#include <complex>
#include <array>
#include <vector>
#include <functional>
#include <set>

// Local includes
#include "XMLParameters.h"

// Project includes.
#include "drivingBase.h"
#include "matrixNetwork.h"


/**
 * @brief Base class for the discrete network. Provides methods that must be overridden.
 */
class networkBase
{
private:

protected:

	// Pointers to parameters and the log file; const where possible
	const XMLParameters *_params;
	      ostream       *_log;

	// Box dimensions.
	double _X;
	double _Y;
	
	/// Frequency the last time 'initialiseForSolution()' was called
	double _omega;
	
	/// Type of driving, including strain.
	const drivingBase* _driving;

	/// Function that returns the fluid velocity at a given point.
	std::function<std::array<std::complex<double>,2>(double,double)> _fluidVelocityAt;

	/// The sum of r_{i} f_{j}; essentially the stress tensor prior to normalising to the volume.
	virtual std::array< std::array< std::complex<double>, 2 >, 2 > _sum_ri_fj() const = 0;

	/// Volume of the network phase.
	virtual double _volume() const = 0;

	/// String label for class type.
	virtual std::string _typeLabel() const noexcept = 0;

	/// Reduced set of nodes to solve for, based on some rationale defined by the child class.
	std::vector<int> _reducedNodeMap;

	/// Generate the map from network nodes to reduced nodes. Defaults to 1-1.
	virtual void _generateReducedNodeMap();

	/// Copies 2-dimensional vectors from the normal node list [size numNodes()] to a reduced one [size numReducedNodes()].
	virtual void _copyToReduced( const std::vector<double> &from, std::vector<double> &to ) const;

	/// Copies 2-dimensional vectors from the reduced node list [size numReducedNodes()] to a normal sized one [size numNodes()].
	virtual void _copyFromReduced( const std::vector<double> &from, std::vector<double> &to ) const;

public:
	networkBase() {}
	networkBase( double X, double Y, const XMLParameters *params, ostream *log, const drivingBase *driving )
		: _params(params), _log(log), _X(X), _Y(Y), _omega(-1.0), _driving(driving) {}
	virtual ~networkBase() {}
	
	//
	// Initialisation
	//
	virtual void initialise() { *_log << std::endl << "Network type: " << _typeLabel() << std::endl; }

	void setFluidVelocityAt( std::function<std::array<std::complex<double>,2>(double,double)> fn )
		{ _fluidVelocityAt = fn; }

	//
	// Nature of the network implementation.
	//
	virtual bool canDrift() const noexcept { return true; }

	//
	// Extent (bulk geometry) of the network; required by some strain fields.
	//
	virtual double extentX() const noexcept { return _X; }
	virtual double extentY() const noexcept { return _Y; }

	//
	// Base methods for matrix assembly. Templated to type as can be real or complex.
	//
	template< typename T >
	void matrix_fillDisps( matrixNetwork<T> *M, double omega ) const {}

	template< typename T >
	void matrix_copyDisps( const T *sol ) {}

	//
	// Solution routines
	//
	virtual void initialiseForSolution( double omega ) { _omega = omega; };
	virtual void tearDownAfterSolution() {};
	virtual std::array<double,2> realAffineOffset() { return {{0.0,0.0}}; }
	virtual void offsetDisps( std::array< std::complex<double>, 2 > uOffset ) {};
	
	//
	// Child class may define a set of reduced nodes for solvers. Defaults to 1-1 mapping.
	//
	virtual unsigned int numReducedNodes() const { return numNodes(); }				///< No. of nodes whose displacements are solved for.
	virtual const std::set<int> reducedNodes() const;								///< Set of nodes that needs to be solved for.
	virtual const std::vector<int>* reducedNodeMap() { return &_reducedNodeMap; };	///< Reindexes nodes for solvers only.

	//
	// Analysis
	//
	virtual unsigned int numNodes() const { return 0; }
	virtual unsigned int numConnections() const { return 0; }
	virtual unsigned int maxConnections() const { return 0; }
	virtual unsigned short connectionsForNode( int n ) const { return 0; }
	virtual double coordinationNumber() const = 0;	

	virtual const std::complex<double>* nodeDisplacements() const { return nullptr; }
	virtual const std::vector<double> nodePositions() const { std::vector<double> dummy; return dummy; }

	virtual std::array< std::complex<double>, 2 > meanDisplacement() const = 0;
	virtual double nonAffinity() const { return 0.0; }

	virtual bool hasInterface() const noexcept { return false; }
	virtual double interfacialFlux() const { return 0.0; }			///< Only called if hasInterface().

	virtual std::map< std::string, std::complex<double> > complexModuli() const;			///< Labelled modulus/moduli.
	virtual std::vector< std::complex<double> > forceMoments( int maxMoment ) const = 0;		///< Moments of force distribution.

	//
	// I/O
	//
	virtual void saveSolution( ostream &out ) const = 0;
	
	//
	// Debugging
	//
	virtual double debugNodeResidual( double zeta ) const = 0;
        
	//
	// Exceptions
	//
	class BadParameter;
	class SystemError;
	class IterationDiverging;
	
};


#pragma mark -
#pragma mark Exceptions
#pragma mark -

/**
 * @brief Custon exception for a bad network parameter.
 */
class networkBase::BadParameter : public std::runtime_error
{
private:
        std::string makeWhat( std::string param, std::string why, std::string file, int line )
        {
                std::ostringstream out;
                out << "Bad network parameter '" << param << "'; " << why
                    << " [in file " << file << ", line " << line << "]" << std::endl;
                return out.str();
        }

public:
        BadParameter( std::string param, std::string why, std::string file, int line ) : std::runtime_error( makeWhat(param,why,file,line) ) {}
};

/**
 * @brief Custon exception for a network system error.
 */
class networkBase::SystemError : public std::runtime_error
{
private:
        std::string makeWhat( std::string where, std::string why, std::string file, int line )
        {
                std::ostringstream out;
                out << "Network system error during " << where << "; " << why
                    << " [in file " << file << ", line " << line << "]" << std::endl;
                return out.str();
        }

public:
        SystemError( std::string where, std::string why, std::string file, int line ) : std::runtime_error( makeWhat(where,why,file,line) ) {}
};


/**
 * @brief Custon exception for a diverging network iteration.
 */
class networkBase::IterationDiverging : public std::runtime_error
{
	private:
		std::string makeWhat( std::string criterion, std::string file, int line )
		{
			std::ostringstream out;
			out << "Network iteration appears to be diverging, since: " << criterion
				<< " [in file " << file << ", line " << line << "]" << std::endl;
            return out.str();
		}
public:
	IterationDiverging( std::string criterion, std::string file, int line ) : std::runtime_error( makeWhat(criterion,file,line) ) {}
};


#endif
