/**
 * @file matrixBase.h
 * @author David Head
 * @brief Base class for matrices for fully implicit solvers. Templated to real type.
 */
 

#ifndef FIBRE_NET_HI_MATRIX_BASE_H
#define FIBRE_NET_HI_MATRIX_BASE_H


//
// Includes
//

// Standard includes.
#include <ostream>
#include <complex>
#include <chrono>
#include <fstream>

// SuperLU sparse direct solver.
#ifdef SUPERLU
#include "slu_dcomplex.h"
#include "slu_ddefs.h"
#include "slu_zdefs.h"
#endif

// Custom includes.
#include "LUDecomposition.h"



/**
 * @brief Base class for matrices for fully implicit solvers.
 */
template< typename T >
class matrixBase
{

private:

	/// Floating point tolerance used for rejecting matrix/RHS values, and debug checks.
	constexpr static double __tol = 1e-12;

protected:

	/// The log ouptut stream.
	std::ostream *_log;
	
	/// The total number of scalars.
	unsigned int _numScalars;

	/// Once constructed, can re-use the sparse matrix assuming a fixed sparsity pattern.
	bool _reuseCSR;	
	
	// First storage format: Similar to CSR but using multiple std::vector<>'s to calculate in a single sweep.
	std::vector< std::vector<int> > _matrixCols;		///< Column indices for each row.
	std::vector< std::vector<T>   > _matrixVals;		///< The corresponding values.
	
	// CSR format. Constructed the first time _getSCRMatrix() is called and only the values changed until a fullReset().
	std::vector<T>   _CSRMatrixVals;
	std::vector<int> _CSRMatrixCols;
	std::vector<int> _CSRMatrixOffsets;

	/// Convert to a common sparse format (CSR = 'Compressed Sparse Row')
	void _getCSRMatrix( const std::vector<T> **vals, const std::vector<int> **cols, const std::vector<int> **startCol );

	// The RHS and solution vectors are always trated as dense, regarded of the matrix.
	std::vector<T> _RHS;
	std::vector<T> _sol;

	/// Maps global row/column indices to global dense matrix index.
	inline int _index( unsigned int i, unsigned int j ) const noexcept { return i*_numScalars + j; }

	// Add to matrix or RHS vector. No range checking. Do not add values smaller than __tol.
	void _addToMatrix( int i, int j, T val ) noexcept;
	void _addToRHS   ( int i,        T val ) noexcept;
	
public:

	// Construction / destruction.
	matrixBase( int numDOF, std::ostream *log=nullptr );
	virtual ~matrixBase() {}

	//
	// Solvers (once matrix and RHS filled using child class methods). Returns a negative integer if failed.
	//
	virtual int solveDenseDirect ();
	virtual int solveSparseDirect();

	//
	// Modifiers
	//
	virtual void fullReset();							///< Completely clears matrix; resets sizes to constructor.
	virtual void clearSameSparsity();					///< Keeps sparsity pattern but sets values to zero.

	//
	// Accessors
	//
	inline const T* solution() const { return _sol.data(); }
	inline       T* RHSVec  ()       { return _RHS.data(); }

	inline int numDOF    () const { return _numScalars; }
	       int numNonZero() const;	

	std::vector<T> getDenseMatrix() const;				///< Convert matrix format to dense format.
	
	//
	// Debug
	//

	/**
	 * @brief Outputs matrix to file in a format that can be converted to sparsity in Matlab.
	 * 
	 * Outputs the (dense) matrix in a format that can be read and displayed by Matlab:
	 * - `A = load( "matrix.dat" )`
	 * - `spy(A)` to display the sparsity.
	 * - `condest(A)` for an estimate of the condition number.
	 */
	void debugSparsityToMatlab() const;

	/// Check the CSR matrix against the dense format. Slow; only check for small systems.
	void debugCheckCSRAgainstDense() const;

	//
	// Exceptions.
	//
	class CouldNotAssembleMatrixError;					///< General problem when assembling the matrix.
};


#pragma mark -
#pragma mark Exceptions
#pragma mark -

/**
 * @brief Custom exception for some failure when assembling the matrix.
 * 
 * If passed the string `why`, as well as the file name and location, will throw
 * a `std::runtime_error` with the message
 * 
 * `Could not assemble the global matrix; <why>`
 * 
 * followed by the location where the error was thrown.
 */
template< typename T >
class matrixBase<T>::CouldNotAssembleMatrixError : public std::runtime_error
{
private:
        std::string makeWhat( std::string why, std::string file, int line )
        {
                std::ostringstream out;
                out << "Could not assemble the global matrix; " << why
                    << " [in file " << file << ", line " << line << "]" << std::endl;
                return out.str();
        }

public:
        CouldNotAssembleMatrixError( std::string why, std::string file, int line )
			: std::runtime_error( makeWhat(why,file,line) ) {}
};


#pragma mark -
#pragma mark Implementation in-file
#pragma mark -

// Initialise RHS and solution vectors. Nothing is yet assumed about the matrix storage format.
template< typename T >
matrixBase<T>::matrixBase( int numDOF, std::ostream *log )
	: _log(log), _numScalars(numDOF)
{
	fullReset();
}	

#pragma mark -
#pragma mark Matrix storage formats
#pragma mark -

// The number of non-zero entries as determined from the first-pass matrix.
template< typename T >
int matrixBase<T>::numNonZero() const
{
	int total = 0;
	for( auto &v : _matrixCols ) total += v.size();
	return total;
}

// Returns a dense matrix using the index format matrix[i*_numScalars+j] \equiv M[i,j].
// Expect RVO ('Return Value Optimisation').
template< typename T >
std::vector<T> matrixBase<T>::getDenseMatrix() const
{
	// Initialise to full size, all elements zero.
	std::vector<T> matrix( _numScalars*_numScalars, 0.0 );
	matrix.shrink_to_fit();

	// Add values.
	for( auto row=0u; row<_numScalars; row++ )
		for( auto index=0u; index<_matrixCols[row].size(); index++ )
			matrix[ row + _numScalars*_matrixCols[row][index] ] = _matrixVals[row].at(index);
	
	return matrix;
}


// Returns a common compressed sparse format, CSR = 'Compressed Sparse Row.'
template< typename T >
void matrixBase<T>::_getCSRMatrix( const std::vector<T> **vals, const std::vector<int> **cols, const std::vector<int> **offsets )
{
	// Set pointers to the persistent vectors;
	*vals    = &_CSRMatrixVals;
	*cols    = &_CSRMatrixCols;
	*offsets = &_CSRMatrixOffsets;
	
	// If re-using the CSR matrix, values have already been filled, so we are done.
	if( _reuseCSR ) return;

	// Construct the matrix.
	int nnz = numNonZero(), n = numDOF();
	
	// First resize vectors to their known sizes.
	_CSRMatrixVals   .resize( nnz );
	_CSRMatrixCols   .resize( nnz );
	_CSRMatrixOffsets.resize( n+1 );

	_CSRMatrixVals   .shrink_to_fit();
	_CSRMatrixCols   .shrink_to_fit();
	_CSRMatrixOffsets.shrink_to_fit();
	
	// Keep track of the number added so far. Can also insert the first offset.
	int numAdded = 0;
	_CSRMatrixOffsets[0] = 0;
	
	// Loop over all rows and add column indices and values to the contiguous vectors.	
	for( auto row=0; row<n; row++ )
	{
		for( auto index=0u; index<_matrixCols[row].size(); index++ )
		{
			_CSRMatrixCols[numAdded] = _matrixCols[row][index];
			_CSRMatrixVals[numAdded] = _matrixVals[row][index];
			
			numAdded++;
		}

		// Store the offset for the next row.
		_CSRMatrixOffsets[row+1] = numAdded;
	}
	
	// Check: numAdded should match the number of non-zero values.
	if( numAdded != nnz )
	{
		std::cerr << "Failure in CSC-format matrix construction: Number of non-zero values did not match." << std::endl;
		return;
	}

	// Output matrix size and sparsity to the log file.
	if( _log ) *_log << " - first construction of sparse "<< n << "x" << n << " matrix with " << nnz << " non-zero real or complex entries (average " << 1.0*nnz/n << " per row/column)" << std::endl;

	// Next time re-use the sparsity pattern, i.e. just change the values [calling fullReset() cancels this].
	_reuseCSR = true;
}


#pragma mark -
#pragma mark Solvers
#pragma mark -

// A simple direct solve using the custom LUDecomposition class.
// Returns zero for success and negative for failure.
template< typename T >
int matrixBase<T>::solveDenseDirect()
{
	using namespace std::chrono;

	// Get the matrix in dense format, i.e. with many zeroes.
	std::vector<T> denseMatrix = getDenseMatrix();

	// Sanity check that the matrix and vectors are of the correct size.
	if( denseMatrix.size()!=_numScalars*_numScalars || _RHS.size()!=_numScalars || _sol.size()!=_numScalars )
	{
		std::cerr << "Cannot solve: Matrix, RHS and/or solution vectors not correct size (not yet filled?)" << std::endl;
		return -1;
	}
	
	// Solve in a try-except clause, with timing.
	try
	{
		high_resolution_clock::time_point start = high_resolution_clock::now();

		// Initialise the LUDecomposition object and solve for the matrix only.
		LUDecomposition<T> decomp( denseMatrix, _numScalars );

		// Apply to the RHS vector
		decomp.solve( _RHS, &_sol );
		
  		// Message to log file.
		duration<double> durn = duration_cast<duration<double>>( high_resolution_clock::now() - start );
		if( _log ) *_log << " - solved in time " << durn.count() << " seconds." << std::endl;
	}
	catch( std::exception &ex )
	{
		std::cerr << "Cannot solve: " << ex.what() << std::endl;
		return -1;
	}

	return 0;
}

// The sparse direct solver is in the .cpp file as it uses template specialisation.


#pragma mark -
#pragma mark Modifiers
#pragma mark -

// Adds a value to the matrix in Aij format. No range checking.
template< typename T >
void matrixBase<T>::_addToMatrix( int i, int j, T val ) noexcept
{
	// Only add non-zero values (e.g. isolated nodes can give zero vals).
	if( std::abs(val)<__tol ) return;
	
	// If re-using the CSR matrix, add to it efficiently.
	if( _reuseCSR )
	{
		for( auto index=_CSRMatrixOffsets[j]; index<_CSRMatrixOffsets[j+1]; index++ )
			if( _CSRMatrixCols[index] == i )
			{
				_CSRMatrixVals[index] += val;
				return;
			}

		std::cerr << "Failed to find entry in existing CSR matrix." << std::endl;
		return;
	}

	// Add to an existing entry for this row and column, or create a new one.
	for( int index=0; index<static_cast<int>(_matrixCols[j].size()); index++ )
		if( _matrixCols[j][index] == i )
		{
			_matrixVals[j][index] += val;
			return;
		}

	// If still here it did not exist; create a new entry,
	_matrixCols[j].push_back(i  );
	_matrixVals[j].push_back(val);
}

// Adds a value to the RHS vector, checking for non-zero to within tolerance first.
template< typename T >
void matrixBase<T>::_addToRHS( int i, T val ) noexcept
{
	if( std::abs(val)>__tol ) _RHS[i] += val;
}


// Full reset, as per the constructor (i.e. can be used for another problem with a different sparsity pattern).
template< typename T >
void matrixBase<T>::fullReset()
{
	// Initialise the sizes of the (dense) matrix (with bespoke indexing) and the vectors. Also try to optimise memory.
	_RHS.clear();
	_RHS.resize( _numScalars, 0.0 );			// 0.0 is the same as {0.0,0.0} for std::complex.
	_RHS.shrink_to_fit();

	_sol.clear();
	_sol.resize( _numScalars, 0.0 );
	_sol.shrink_to_fit();
	
	// Reset the first storage matrix.
	_matrixVals.clear();
	_matrixCols.clear();

	_matrixVals.resize( _numScalars );			// One vector of column indices and values per row.
	_matrixCols.resize( _numScalars );

	_matrixVals.shrink_to_fit();
	_matrixCols.shrink_to_fit();	

	// (Re-)construct the CSR matrix from scratch next time _getCSRMatrix() is called.
	_reuseCSR = false;
	_CSRMatrixVals   .clear();
	_CSRMatrixCols   .clear();
	_CSRMatrixOffsets.clear();
}

// Keeps sparsity pattern but resets everything else.
template< typename T >
void matrixBase<T>::clearSameSparsity()
{
	for( auto &r : _RHS           ) r = 0.0;
	for( auto &u : _sol           ) u = 0.0;
	for( auto &a : _CSRMatrixVals ) a = 0.0;			// Keep column and offset vectors.

	for( auto &v : _matrixVals )
		for( auto &m : v )
			m = 0.0;
}


#pragma mark -
#pragma mark Debug
#pragma mark -

// Check the CSR matrix against the dense format. Slow; only check for small systems.
template< typename T >
void matrixBase<T>::debugCheckCSRAgainstDense() const
{
	int n = numDOF();
	auto dense = getDenseMatrix();

	for( auto row=0u; row<n; row++ )
		for( auto col=0u; col<n; col++ )
		{
			// Is there a corresponding element in the sparse matrix? If so, do the values match?
			bool alsoInSparse = false;
			for( auto index=_CSRMatrixOffsets[row]; index<_CSRMatrixOffsets[row+1]; index++ )
				if( _CSRMatrixCols[index]==col )
				{
					// Entry exists in the sparse matrix; check the value.
					if( std::abs(dense[row+n*_CSRMatrixCols[index]]-_CSRMatrixVals[index])>__tol )
					{
						std::cerr << "Failure in SCS-format matrix construction: Value in sparse matrix did not match dense matrix." << std::endl;
						return;
					}
					
					// Also exists in sparse, with the correct value (to within tolerance).
					alsoInSparse = true;
					break;
				}
			
			// If not in the sparse matrix, was it zero?
			if( !alsoInSparse && std::abs(dense[col*n+row])>__tol )
			{
				std::cerr << "Failure in SCS-format matrix construction: Non-zero value in dense matrix not in sparse one." << std::endl;
				return;
			}
		}
}

// Outputs the (dense) matrix in a format that can be read and displayed by Matlab:
// - A = load( "matrix.dat" )
// - spy(A)
// - condest(A) for an estimate of the condition number [gave around 1e9 for 30x30 matrix with w=0.01)
template< typename T >
void matrixBase<T>::debugSparsityToMatlab() const
{
	auto matrix = getDenseMatrix();

	ofstream outFile;
 	outFile.open( "matrix.dat" );				// Currently the filename is hard-wired in.

 	for( auto j=0u; j<_numScalars; j++ )
 	{
 		for( auto i=0u; i<_numScalars; i++ ) outFile << std::abs(matrix[i+_numScalars*j]) << "\t";
 		outFile << std::endl;
 	}

 	outFile.close();
}

#endif
