/**
 * @file pebbleGame.h
 * @author David Head
 * @brief Class that implements the pebble game for graph rigidity.
 */


//
// Includes.
//
#include <iostream>
#include <set>
#include <list>
#include <deque>
#include <vector>
#include <stdexcept>
#include <random>
#include <algorithm>
#include <chrono>
#include <map>


/**
 * @brief Class that implements the pebble game for planar graph rigidity.
 * 
 * Implements the pebble game of Jacobs and Thorpe, Phys. Rev. Lett. 75, 4051 (1995), with some
 * clarifications on details coming from later work by Lee, Streinu and Theranl; and separately,
 * de Souza and Harrowell.
 * 
 * Nodes are assumed to be indexed as integers, consecutively, starting from zero.
 */  
class pebbleGame
{

private:

    // Optimisation for post-processing: Leep track of all mutually-rigid nodes to avoid re-application of pebble game for same nodes on different edges.
    std::map< std::pair<int,int>, bool > __mutuallyRigidNodes;

protected:

    // Shorthand for an edge, and for a list of edges. 
    using _edge     = std::pair<int,int>;               // Used for both the directed (first->second) and undirected cases.
    using _edgeList = std::list<_edge>;                 // Container for edges.
 
    // The number of nodes.
    int _nNodes;

    // The edge lists.
    _edgeList _eIndependent;
    _edgeList _eRedundant;

    /// The current numbers of free pebbles on each node.
    std::vector<short> _nFreePebbles;

    /// Keep track of the search status of nodes to avoid loops.
    std::vector<bool> _nodeSearchedFrom;

    /// As an optimisation, keep track of all valid edge iterators during a search, indexed by node for rapid access.
    std::vector< std::vector<_edge*> > _viableSearchEdges;

    /// The container of rigid clusters with at least 2 nodes, determined by post-processing the network after all edges have been added.
    std::list< std::set<int> > _rigidClusters;

    /// The container of isolated nodes, determined by post-processing the network after all edges have been added.
    std::set<int> _isolatedNodes;

    /// Initialise all of the persistent containers given a known number of nodes _nNodes.
    virtual void _initContainers();

    /**
     * @brief Determines if the provided trial edge is redundant or not.
     * 
     * Applies the pebble game method to the given bond, shuffling bonds if possible, to determine
     * if it is redundant - in which case returns 'true' - or independent, returning 'false'.
     * Does not add the edge to either of the persistent edge containers.
     * 
     * Not a const method as may update the locations of free pebbles and the directions of edges,
     * corresponding to the shuffling of anchored pebbles.
     * 
     * @param edge The trial edge.
     * @return 'true' for a redundant edge; 'false' for an independent edge.
     */
    virtual bool _trialEdgeRedundant( _edge edge );

    /**
     * @brief Returns the path from the given node to a node with a free pebble, or an empty vector if no such path could be found.
     * 
     * Essentially a depth-first graph search specialised to the pebble game method. Starting from the given node, searches outwards
     * as per a normal graph search, but only considers paths that go against the direction of independent edges - this is because the
     * pebble will be moved from the end of the path to the start, which _will_ then obey the directionality of the edges. Redundant
     * edges have no anchoring pebble to be shuffled, and are therefore not included.
     * 
     * If a path can be found, the search is terminated immediately, and a vector of successive edges, as iterators to the container for
     * independent edges, is returned, starting from the given startNode.
     * 
     * If a path can not be found, will return an empty container.
     * 
     * @param startNode The node to start the search from; will be the node that receives the pebble if a path is found.
     * @param invalidEndNode This node is not considered to be an end of the path, even if it has a free pebble; usually the other end of the trial edge.
     * @return Ordered list of edges along the path, starting from startNode, or an empty container if no such path could be found.
     */
    virtual std::vector<_edge*> _pathToFreePebble( int startNode, int invalidEndNode );

    /**
     * @brief Returns 'true' if the two nodes are mutually rigid during post-processing.
     * 
     * Determines if the two nodes are mutually rigid, i.e. if adding a trial bond between and applying the pebble game
     * would determine that bond is redundant. Does not actually add the bond. At its most basic level, just calls
     * _trialEdgeRedundant(), but separating off this post-processing functionality allows for optimisation to be considered.
     * 
     * @param n1 One of the nodes (order does not matter)
     * @param n2 The other node index (order does not matter)
     * @return 'true' if the two nodes are mtuually rigid, 'false' otherwise.
     */
    virtual bool _mutuallyRigid( int n1, int n2 );

public:

    // Constructors / destructor.
    pebbleGame( int numNodes );
    virtual ~pebbleGame() {}

    //
    // Pebble game algorithm and relevany accessors.
    //

    /**
     * @brief Classifies all bonds as either redundant or independent using the pebble game algorithm.
     * 
     * Loops through all of the supplied edges/bonds, selected at random, and adds them one at a time to
     * the graph classifyig them as redundant or independent, as per the pebble game algorithm. This updates
     * the existing persistent containers of redundant and independent bonds that can be accessed via
     * accessor methods.
     * 
     * @param edges Set of edges to be classified and added to the graph.
     */
    virtual void addEdges( const _edgeList &edges );

    /**
    * @brief Gives the total number of free pebbles, which equals the number of degrees of freedom of the whole graph.
    */
    virtual int numFreePebbles() const { return std::accumulate(_nFreePebbles.begin(),_nFreePebbles.end(),0); }

    /**
    * @brief Returns thw total number of redundant bonds/edges.
    */
    virtual int numRedundantEdges() const noexcept { return _eRedundant.size(); }

    /**
    * @brief Returns the total number of independent bonds/edges.
    */
    virtual int numIndependentEdges() const noexcept { return _eIndependent.size(); }

    /**
     * @brief Access the list of redundant edges/bonds, i.e. a std::list templated to std::pair<int,int>.
     */
    virtual const _edgeList* redundantEdges() const noexcept { return &_eRedundant; }

    /**
     * @brief Access the list of independent edges/bonds, i.e. a std::list templated to std::pair<int,int>.
     */
    virtual const _edgeList* independentEdges() const noexcept { return &_eRedundant; }


    //
    // Post-processing, i.e. rigid cluster identification, and relevant accessors.
    //

    /**
     * @brief Identify all rigid clusters.
     * 
     * Determines the set of all rigid clusters for the current sets of independent and redundant bonds. Normally called
     * after calling 'addEdges()' with all of the actual edges/bonds in the system. Identification uif rigid clusters ses
     * the pebble game and so may move pebbles and reverse the durections of independent bonds, but will not add any new
     * edges to the system.
     * 
     * Various accessor methods used to extract the rigid clusters after this method returns.
     */
    virtual void postProcess();

    /**
     * @brief Returns the total number of isolated nodes after post-processing.
     */
    inline int numIsolatedNodes() const { return _isolatedNodes.size(); }

    /**
     * @brief Returns all of the isolated nodes in a std::set<int>.
     */
    inline std::set<int> allIsolatedNodes() const { return _isolatedNodes; }

    /**
     * @brief Returns the total number of rigid clusters consisting of at least 2 nodes.
     */
    inline int numRigidClusters() const { return _rigidClusters.size(); }

    /**
     * @brief Returns the set of nodes in the largest rigid cluster.
     */
    virtual std::set<int> largestRigidCluster() const;

   /**
     * @brief Returns all pivot nodes, i.e. nodes that appear in 2 or more rigid clusters.
     */
    virtual std::set<int> allPivotNodes() const;


    //
    // Debugging.
    //

    /**
     * @brief Perform a consistency check on persistent edge containers.
     * 
     * Performs the following consistency checks on persistent containers:
     *   - Number of remaining pebbles is 2 * num.nodes minus the number of independent edges.
     *   - Edges only appear once on each list, i.e. independent or redundant.
     *   - Node indices in each edge are in the valid range and not equal.
     * 
     * @return 'true' if everything looks okay; 'false' if not.
     */
    virtual bool checkEdgeConsistency() const;

    /**
     * @brief Perform a consistency check on the rigid clusters identified after .
     * 
     * Performs the following consistency checks on the persistent container of rigid clusters:
     *   - All nodes appear at least once in a rigid cluster or as an isolated node.
     *   - Isolated nodes do not appear in any rigid clusters.
     *   - All isolated nodes have two remaining degrees of freedom (free pebbles).
     * 
     * @return 'true' if everything looks okay; 'false' if not.
     */
    virtual bool checkClusterConsistency() const;
};




 