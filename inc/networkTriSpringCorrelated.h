/**
 * @file networkTriSpringCorrelated.h
 * @author David Head
 * @brief Specialisation of the triangular spring network that incorporates correlated bond dilution.
 */
 

#ifndef FIBRE_NETWORK_HI_NETWORK_TRI_SPRING_CORRELATED_H
#define FIBRE_NETWORK_HI_NETWORK_TRI_SPRING_CORRELATED_H
 

//
// Includes.
//

// Parent class.
#include "networkTriSpring.h"

// Standard includes.
#include <string>
#include <chrono>

// Project includes.
#include "pRNG.h"


//
// Parameters.
//
namespace parameterParsing
{
    // Parameters required by the algorithm that generates correlations.
	const std::string netTSCorrn_correlationStrength = "correlationStrength";    ///< Parameter c in the article.
    const std::string netTSCorrn_siteOccupation      = "siteOccupation";         ///< Parameter f in the article (not \phi_{1} is not defined nor needed).
}


/**
 * @brief Specialisation of the triangular spring network that allows bonds to be correlated.
 * 
 * This is based on the first correlation algorithm in the article "Correlated Rigidity Percolation
 * and Colloidal Gels", S. Zhang, L. Zhang, M. Bouzid, D. Z. Rockling, E. Del Gado and X. Mao,
 * Phys. Rev. Lett. 123, 058001 (2019), but could be expanded to other types of correlation
 * in the future, including their second algorithm (which is however MD-based and quite compute
 * intensive).
 * 
 * This requires two parameters, called 'c' and 'f' in the article, here referred to as the
 * correlation strength and the (target) site occupation, respectively. These must be defined
 * in the parameters file as e.g.,
 * 
 * ```
 *    <Parameter correlationStrength="0.5"  />
 *    <Parameter siteOccupation     ="0.5"  />
 * ```
 * 
 * Note \phi_{1} from the article is not defined here as it is not immediately a lattice quantity,
 * but as stated in the article, can be found from f using \phi_{1} = \pi f / (2 \sqrt{3}).
 */
class networkTriSpringCorrelated : public networkTriSpring
{

private:

protected:

    // Algorithm parameters.
    double _correlationStrength;            ///< Parameter 'c' in the article that controls the strength of correlations.
    double _siteOccupation;                 ///< Parameter 'f' in the article - the target fraction of sites occupied.

    /// String label for class type.
	virtual std::string _typeLabel() const noexcept override { return "networkTriSpringCorrelated"; }

    /**
     * @brief Adds springs between sites that become occupied following Model 1 of Zhang et al.
     * 
     * Performs the site occupation algorithm of Zhang et al. [Phys. Rev. Lett. 123, 058001 (2019)],
     * in which sites are occupied based on the number of occupied neighbours according to the parameter
     * c = 'correlation strength' as p = (1-c)^{6-Nnn}, with Nnn the number of nearest neighbours.
     * 
     * This addition continues until the target site occupation fraction f = 'site occupation' is reached,
     * where f is the second parameter that needs to be specified for the algorithm.
     * 
     * One all sites have been occupied, springs are added between adjacent occupied sites. The internal
     * storage used to determine which sites are occupied is then released. All other rules will still apply. 
     * 
     * Note normal bond dilution is not currently allowed with this correlated algorithm; an error will be
     * thrown in the constructor if any dilution parameters are not 1.0.
     */
    virtual void _initSprings() override;

public:

    // Constructors / destructor.
    networkTriSpringCorrelated() {}
    networkTriSpringCorrelated( double X, double Y, const XMLParameters *params, ostream *log, const drivingBase *driving );
    virtual ~networkTriSpringCorrelated() {}
};


#endif
