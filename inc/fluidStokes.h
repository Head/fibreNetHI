/**
 * @file fluidStokes.h
 * @author David Head
 * @brief Fluid flow modelled as per Levine-MacKintosh, i.e. Stokes flow coupled to the network nodes.
 */
 

#ifndef FIBRE_NETWORK_HI_FLUID_STOKES_H
#define FIBRE_NETWORK_HI_FLUID_STOKES_H


//
// Includes
//

// Standard includes
#include <complex>
#include <cmath>
#include <vector>
#include <thread>
#include <atomic>
#include <set>
#include <numeric>

// Project includes
#include "fluidBase.h"
#include "matrixTriSpringStokes.h"


//
// Parameters
//
namespace parameterParsing
{
	const std::string fluidS_cellSizeAttr = "cellSize";
}


/**
 * @brief Fluid flow modelled as per Levine-MacKintosh, i.e. Stokes flow coupled to the network nodes.
 */
class fluidStokes : public fluidBase
{

private:

	/**
	 * @brief Interpolation method: Bilinear.
	 * Indexing is as follows, with (x,y) somewhere within the square:
	 *       (0)    (1)
	 *       (2)    (3)
	 */
	void __interpBilinear( double x, double y, std::vector<unsigned int> *indices, std::vector<double> *coeffs,
								std::vector<int> *shifts_x, std::vector<int> *shifts_y ) const;

	/**
	 * @brief Interpolation method: Gaussian.
	 * Indexing is as follows, with (x,y) somewhere within the square (3,4,7,8):
	 *       (0)    (1)
	 * (2)   (3)    (4)    (5)
	 * (6)   (7)    (8)    (9)
	 *       (10)   (11)
	 */
	void __interpGaussian( double x, double y, std::vector<unsigned int> *indices, std::vector<double> *coeffs,
								std::vector<int> *shifts_x, std::vector<int> *shifts_y ) const;

protected:

	// The number of cells in each direction, and the total number of cells
	unsigned int _numX;
	unsigned int _numY;
	unsigned int _numCells;
	
	// The actual cell sizes in each direction; will typically differ from the requested one.
	double _dx;
	double _dy;

	/// The pressure mesh, located at centres of grid rectangles flush to the boundaries.
	std::vector< std::complex<double> > _P;

	/// The velocity mesh, staggered with the pressure mesh to lie on corners of grid rectangles.
	std::vector< std::complex<double> > _v;
	
	/// Global index for given local indices, no periodicity
	inline unsigned int _index( unsigned int i, unsigned int j ) const noexcept { return _numX*j + i; }

	/// Periodically wrap indices, in case e.g. node coordinates outside box (possibly with non-trivial ICs).
	void _wrap( unsigned int &i, unsigned int &j ) const;

	/**
	 * @brief Velocity mesh indices, coefficients, and periodic wrapping, for the given position.
	 * 
	 * Given a location (x,y) - typically a network node - fills in the passed containers for:
	 * - indices of mesh points in the interpolation;
	 * - coefficients (i.e. weightings) of the same mesh points;
	 * - whether or not a periodic copy in the x-direction was used for each mesh point;
	 * - whether or not a periodic copy in the y-direction was used for each mesh point.
	 * The $(x,y)$ coordinates are wrapped to the primary cell prior to interpolation.
	 * 
	 * @param x The x-coordinate of the point
	 * @param y The y-coordinate of the point.
	 * @param indices Pointer to a std::vector of velocity mesh indices.
	 * @param coeffs Pointer to a std::vector of weights/coefficients, order as per `indices`.
	 * @param shifts_x Pointer to a std::vector of booleans for nodes wrapped in the $x$-direction.
	 * @param shifts_y Pointer to a std::vector of booleans for nodes wrapped in the $y$-direction.
	 */
	virtual void _velocityMeshInterp( double x, double y, std::vector<unsigned int> *indices, std::vector<double> *coeffs,
										std::vector<int> *shifts_x, std::vector<int> *shifts_y ) const;

public:

	fluidStokes() {}
	fluidStokes( double X, double Y, const XMLParameters *params, ostream *log, const drivingBase *base );
	virtual ~fluidStokes() {}

	//
	// Nature of the fluid implementation.
	//
	virtual bool canDrift() const noexcept override { return true; }
	virtual bool incompressible() const noexcept override { return true; }

	//
	// Accessors.
	//
	inline  unsigned int numX() const noexcept { return _numX; }
	inline  unsigned int numY() const noexcept { return _numY; }
	virtual unsigned int numCells() const noexcept override { return _numCells; }
	inline  const std::complex<double>* velocities() const noexcept { return _v.data(); }
	inline  const std::complex<double>* pressures () const noexcept { return _P.data(); }

	//
	// Resetting the origin.
	//
	virtual std::array<double,2> imagAffineOffset( double omega ) const override;
	virtual std::array< std::complex<double>,2 > meanVelocity() const override;
	virtual void offsetVelocities( std::array< std::complex<double>,2 > delta_v ) override;

	//
	// Filling the matrix for an implicit solver, and extract the resulting solution vector.
	//
	virtual void matrix_fillDragTerms  ( matrixTriSpringStokes *M, double omega, const std::set<int> netNodes, const std::vector<double> nodePosns ) const;
	virtual void matrix_fillStokesTerms( matrixTriSpringStokes *M, double omega ) const;

	virtual void matrix_copyVels ( const std::complex<double> *sol );
	virtual void matrix_copyPress( const std::complex<double> *sol );

	//
	// Analysis
	//
	virtual double nonAffinity() const override;			///< Return value needs to be divided by some length scale squared.
	virtual std::array< std::complex<double>, 2 > velocityAt( double x, double y ) const override;		///< (vx,vy), both complex

	//
	// I/O
	//
	virtual void saveSolution( ostream &out ) const override;

	//
	// Debugging
	//
	virtual double debugPressureResidual() const override;
	virtual double debugVelocityResidual( std::set<int> N, std::vector<double> P, const std::complex<double>* U ) const override;
};




#endif

