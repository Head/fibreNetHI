/**
 * @file matrixTriSpringStokes.h
 * @author David Head
 * @brief Builds and stores a matrix corresponding to the case of `networkTriSpring` and `fluidStokes`.
 */
 

#ifndef FIBRE_NET_HI_MATRIX_TRI_SPRING_STOKES_H
#define FIBRE_NET_HI_MATRIX_TRI_SPRING_STOKES_H

//
// Includes
//

// Standard includes
#include <vector>
#include <ostream>
#include <complex>
#include <iostream>

// Base class
#include "matrixTriSpringBase.h"


/**
 * @brief Builds and stores a matrix corresponding to the case of `networkTriSpring` and `fluidStokes`.
 */
class matrixTriSpringStokes : public matrixTriSpringBase
{

private:

protected:

	/// Size of the fluid mesh (network displacements handled by the parent class).
	unsigned int _numMeshNodes;
	
	/// Starting points for each block of scalars involving fluid quantities.
	unsigned int _startVels_x;
	unsigned int _startVels_y;
	unsigned int _startPress;
	
public:

	// Constructor and destructor
	matrixTriSpringStokes( unsigned int netNodes, unsigned int meshNodes, std::ostream *log, const std::vector<int> *indexer );
	virtual ~matrixTriSpringStokes() {}

	//
	// Accessors for fluid quantities.
	//
	inline const std::complex<double>* vels () const { return &_sol[_startVels_x]; }
	inline const std::complex<double>* press() const { return &_sol[_startPress ]; }

	//
	// Matrix contributions.
	//

	/**
	 * @brief Adds the same constant to the x and y-components of the given node equations involving the velocity.
	 * 
	 * @param n Network node index.
	 * @param v Velocity mesh point index.
	 * @param val Complex constant added to both terms in the matrix.
	 */
	void addDispsVels_xy( unsigned int n, unsigned int v, std::complex<double> val );

	/**
	 * @brief Adds the same constant to the x and y-components of the given velocity equations involving a network node.
	 * 
	 * @param v Velocity mesh point index.
	 * @param n Network node index.
	 * @param val Complex constant added to both terms in the matrix.
	 */
	void addVelsDisps_xy( unsigned int v, unsigned int n, std::complex<double> val );

	/**
	 * @brief Adds the same constant to the x and y-components of the diaginal terms for the given velocity mesh.
	 * 
	 * @param v Velocity mesh point index.
	 * @param val Complex constant added to both x and y diagonal terms in the matrix.
	 */
	void addVelsDiag_xy( unsigned int v, std::complex<double> val );

	// Fluid velocity rows
	        void addVelsVels_xy( unsigned int v1, unsigned int f2, double val );
	inline  void addVelsPress_x( unsigned int v, unsigned int p, double val )
		{ _addToMatrix(_startVels_x+v,_startPress+p,val); }
	inline  void addVelsPress_y( unsigned int v, unsigned int p, double val )
		{ _addToMatrix(_startVels_y+v,_startPress+p,val); }

	// Fluid pressure rows
	inline void addPressVel_x( unsigned int p, unsigned int v, std::complex<double> val )
		{ _addToMatrix(_startPress+p,_startVels_x+v,val); }
	inline void addPressVel_y( unsigned int p, unsigned int v, std::complex<double> val )
		{ _addToMatrix(_startPress+p,_startVels_y+v,val); }

	//
	// RHS contributions
	//
	inline void addVelsRHS_x( unsigned int v, std::complex<double> val ) { _addToRHS(_startVels_x+v,val); }
	inline void addVelsRHS_y( unsigned int v, std::complex<double> val ) { _addToRHS(_startVels_y+v,val); }
	inline void addPressRHS ( unsigned int p, std::complex<double> val ) { _addToRHS(_startPress +p,val); }

	/**
	 * @brief Add 2-vector to the RHS of rows for given velocity node.
	 * 
	 * Adds the x-component of the given 2-vector to the RHS corresponding to the x-cpt of
	 * the given velocity mesh node, and the same for the y-component. Optional weighting factor
	 * applied equally to both.
	 * 
	 * @param v Node index for the fluid velocity mesh.
	 * @param twoVec The 2-vector to be added to the RHS.
	 * @param weight Optional weighting factor. Same for x and y.
	 */
	void addVelsRHS_xy( unsigned int v, std::array<std::complex<double>,2> twoVec, double weight=1.0 );

	//
	// Debugging
	//
	double debugResidual( const std::complex<double> *u, const std::complex<double> *v, const std::complex<double> *P ) const;
};

#endif