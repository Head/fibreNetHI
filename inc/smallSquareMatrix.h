//
// Square matrix class.
//
// This was devised to perform basic matrix operations, including inversion,
// is a somewhat optimised manner; and also to aid in the construction of
// certain types of preconditioned matrix. Templated to floating point type.
// (Have considered specialising to dimension, if only to get rid of all of
// the switch-case loops, but not yet convinced it would make much difference).
//
// Originally devised to aid preconditioning of square matrices along the
// diagonal of a Hessian (to optimise minimisation); the choice of routines
// provided reflects this, but could easily be generalised.
//
// Currently limited to matrix size d=1, 2 or 3 [defined in static method
// largestDimensionSupported()].
//
// Specialisations in .cpp, partly to aid compiler optimisations (hopefully),
// but also to handle special cases, i.e. for complex types. The list of specialisations
// so far is by no means exhaustive and will be extended as required.
//
// 2017/6/26/DAH: Dimension converted to a template parameter to provide better
// scope for compiler optimisations; also some common template specialisations
// (definitions in .cpp file), in the (vain?) hope this will help the compiler
// optimise the code.
//
// 2017/8/11/DAH: Now checked for complex types, which required some new
// specialisations (principally for checking if the determinant is 'zero').
//


#ifndef _UD_SQUARE_MATRIX_H
#define _UD_SQUARE_MATRIX_H


// Standard includes
#include <iostream>
#include <complex>
#include <utility>


using namespace std;


template< unsigned short dim, class T = double >
class smallSquareMatrix {

private:

	void __init();				// Allocates memory and other initialisation; all constructors should call.

protected:

	T *_M;
	T *_temp;			// Copy of M, used for internal calculations [i.e. Det()]

	// _indexing
	inline unsigned short _index( unsigned short i, unsigned short j ) const { return i*dim + j; };

	// Routines to aid computation of various quantities for 3x3 matrices; all inlined.
	inline T _3x3_cofac(unsigned short,unsigned short) const noexcept;
	inline T _3x3_adj( unsigned short i, unsigned short j ) const noexcept
		{ return ( (i&1)^(j&1) ? - _3x3_cofac(i,j) : _3x3_cofac(i,j) ); };
		// Note that the "(i&1)^(j&1)" [bitwise 'and', and 'xor'(=^)] is the same as "(i+j)%2", but hopefully a bit quicker.

public:

	// Constructors / destructor; note values are not initialised to zero.
	smallSquareMatrix() { __init(); }
	smallSquareMatrix( const smallSquareMatrix &M ) { __init(); allSet(M._M); }
	smallSquareMatrix( smallSquareMatrix &&M ) noexcept : _M(std::move(M._M)), _temp(std::move(M._temp)) {}
	smallSquareMatrix(T**);
	smallSquareMatrix(T*);
	virtual ~smallSquareMatrix();

	// Individual element manipulation
	inline void     addTo ( unsigned short i, unsigned short j, T val ) noexcept { _M[_index(i,j)] += val; }
	inline void     set   ( unsigned short i, unsigned short j, T val ) noexcept { _M[_index(i,j)]  = val; }
	inline T get   ( unsigned short i, unsigned short j ) const noexcept { return _M[_index(i,j)]; }
	inline void     allSet( T *x ) noexcept { for( unsigned short i=0; i<dim*dim; i++ ) _M[i] = x[i]; }

	// Block manipulation
	inline operator T*() noexcept { return _M; }
	inline T* data() noexcept { return _M; }

	// Matrix arithmetic.
	smallSquareMatrix& operator+= ( const smallSquareMatrix &rhs );
	smallSquareMatrix& operator-= ( const smallSquareMatrix &rhs );

	// Copy assignment.
	smallSquareMatrix& operator=( smallSquareMatrix rhs ) noexcept;

	// The determinant, inverse positive-definite flag and precon-matrix
	T det() noexcept;							// The determinant

	void adjugateInPlace() noexcept;			// Constructs the adjugate/adjunct (=det*inverse) in place; destroys '_temp' matrix in 3D.
	void adjugateToTemp () noexcept;			// Sim., but leaves '_M' alone and puts the adjugate into '_temp'

	bool invertInPlace() noexcept;				// Inverts in-place; returns 'false' if determinant is 'nan' or '; 'destroys '_temp' matrix in 3D.
	bool invertToTemp () noexcept;				// Sim, but stores M^{-1} in '_temp' and leaves '_M' alone

	bool positiveDefinite() noexcept;			// "True" if matrix is positive definite
	bool preconify(T scale=0.0);				// Converts to invert (if pos-def) or scale*identity (if not); 'true' if inverse

	// Multiplication. Will extend options as required.
	void vectorMultiply         ( const T *x, T *y ) const noexcept;		// Using the matrix in '_M'
	void vectorMultiplyUsingTemp( const T *x, T *y ) const noexcept;		// Using the matrix in '_temp'

	// Clear the matrix or the temporary storage space, i.e. set all elements to zero
	void clear    () noexcept;
	void clearTemp() noexcept;

	// Largest dimension currently supported
	static short largestDimensionSupported() { return 3; }

	// Output
	template< unsigned short d, class real >
	friend ostream& operator<< (ostream&,const smallSquareMatrix<d,real>&);

};


#pragma mark -
#pragma mark Constructors / destructor and clearing
#pragma mark -

// Constructors
template< unsigned short dim, class T >
smallSquareMatrix<dim,T>::smallSquareMatrix( T **x )
{
	// Add provided values term-by-term
	__init();
	for( short i=0; i<dim; i++ )
		for( short j=0; j<dim; j++ )
			_M[_index(i,j)] = x[i][j];
}

template< unsigned short dim, class T >
smallSquareMatrix<dim,T>::smallSquareMatrix( T *x )
{
	__init();
	for( short i=0; i<dim*dim; i++ ) _M[i] = x[i];
}

// Allocate memory for the matrix and a copy; all constructors should call this.
// May throw a bad_alloc exception.
template< unsigned short dim, class T >
void smallSquareMatrix<dim,T>::__init()
{
	_M    = new T[dim*dim];
	_temp = new T[dim*dim];
}

// Destructor
template< unsigned short dim, class T >
smallSquareMatrix<dim,T>::~smallSquareMatrix()
{
	delete [] _M;
	delete [] _temp;
}

// Set all elements to zero
template< unsigned short dim, class T >
void smallSquareMatrix<dim,T>::clear() noexcept
{
	for( short i=0; i<dim*dim; i++ ) _M[i] = 0.0;
}
template< unsigned short dim, class T >
void smallSquareMatrix<dim,T>::clearTemp() noexcept
{
	for( short i=0; i<dim*dim; i++ ) _temp[i] = 0.0;
}


#pragma mark -
#pragma mark Calculations
#pragma mark -

// Returns with the determinant, some template specialisation for common cases (in .cpp), in the hope that the
// compiler will be better able to apply optimisations.
template<>
double smallSquareMatrix<2,double>::det() noexcept;

template<>
float smallSquareMatrix<2,float>::det() noexcept;

template<>
double smallSquareMatrix<3,double>::det() noexcept;

template<>
float smallSquareMatrix<3,float>::det() noexcept;

template< unsigned short dim, class T >
T smallSquareMatrix<dim,T>::det() noexcept
{
	switch( dim )
	{
		case 1 : return _M[0];
		case 2 : return _M[0]*_M[3] - _M[1]*_M[2];
		case 3 : return _M[0]*(_M[4]*_M[8]-_M[5]*_M[7]) - _M[1]*(_M[3]*_M[8]-_M[5]*_M[6]) + _M[2]*(_M[3]*_M[7]-_M[4]*_M[6]);

		default: return 0.0;
	}
}

// Returns with the cofactor of element (i,j). Not sure there's any benefit to storing cofactors once
// calculated [i.e. to avoid recalculation, as in det()]; only a few f.p. operations anyway.
template< unsigned short dim, class T >
T smallSquareMatrix<dim,T>::_3x3_cofac( unsigned short i, unsigned short j ) const noexcept
{
	unsigned short
		x1 = ( i==0 ? 1 : 0 ),
		x2 = ( i==2 ? 1 : 2 ),
		y1 = ( j==0 ? 1 : 0 ),
		y2 = ( j==2 ? 1 : 2 );

	return _M[_index(x1,y1)]*_M[_index(x2,y2)] - _M[_index(x1,y2)]*_M[_index(x2,y1)];
}

// Inverts the matrix; returns in same place holder. Returns 'true' if inverted,
// or 'false' if not possible (because the determinant is either zero or nan).
// Basically calls adjugateInPlace() and then divides through by the determinant.
// Complex types require special handling; specialisations in .cpp.
template<>
bool smallSquareMatrix< 2, std::complex<double> >::invertInPlace() noexcept;

template<>
bool smallSquareMatrix< 3, std::complex<double> >::invertInPlace() noexcept;

template< unsigned short dim, class T >
bool smallSquareMatrix<dim,T>::invertInPlace() noexcept
{
	T _det = det();			// Calculate once and store locally

	// If determinant zero, or nan, return 0.0 (nb. "x!=x" returns 'true' if x is a nan, if IEEE compliant)
	if( !_det || (_det!=_det) ) return false;

	// Create the adjugate matrix in place
	adjugateInPlace();
	
	// Divide throughout by the determinant
	for( unsigned short i=0; i<dim*dim; i++ ) _M[i] /= _det;
	
	return true;			// Successfully inverted
}


// Sim. to above, but leaves the inverse in the '_temp' persistent storage space.
// Complex types require special handling; specialisations in .cpp file.
template<>
bool smallSquareMatrix< 2, std::complex<double> >::invertToTemp() noexcept;

template<>
bool smallSquareMatrix< 3, std::complex<double> >::invertToTemp() noexcept;

template< unsigned short dim, class T >
bool smallSquareMatrix<dim,T>::invertToTemp() noexcept
{
	T _det = det();			// Calculate once and store locally

	// If determinant zero, or nan, return 0.0 (nb. "x!=x" returns 'true' if x is a nan, if IEEE compliant)
	if( !_det || (_det!=_det) ) return false;

	// Create the adjugate matrix in place
	adjugateToTemp();
	
	// Divide throughout by the determinant
	for( short i=0; i<dim*dim; i++ ) _temp[i] /= _det;
	
	return true;			// Successfully inverted
}


#pragma mark -
#pragma mark Adjugates
#pragma mark -

// Creates the adjugate (or adjunct) matrix and stores it in '_M', i.e. replacing the current matrix.
// Note that this also destroys the temporary matrix. Common specialisations given (definitions in .cpp).
template<>
void smallSquareMatrix<2,double>::adjugateInPlace() noexcept;

template<>
void smallSquareMatrix<2,float>::adjugateInPlace() noexcept;

template<>
void smallSquareMatrix<3,double>::adjugateInPlace() noexcept;

template<>
void smallSquareMatrix<3,float>::adjugateInPlace() noexcept;

template< unsigned short dim, class T >
void smallSquareMatrix<dim,T>::adjugateInPlace() noexcept
{
	// First, make a temporary copy of the non-inverted matrix into the persistent storage space "_temp"
	for( short i=0; i<dim*dim; i++ ) _temp[i] = _M[i];

	// Adjugation procedure depends on dimension
	switch( dim )
	{
		case 1 : _M[0] = 1.0;									// 1-d is a bit easy
				 break;

		case 2: _M[0] =   _temp[3]; _M[3] =   _temp[0];
				_M[1] = - _temp[1]; _M[2] = - _temp[2];				// 2-d isn't much harder
				break;

		case 3:	for( unsigned short i=0; i<3; i++ )							// For 3d, use the protected method _x3x_adj()
					for( unsigned short j=0; j<3; j++ )
						_temp[_index(i,j)] = _3x3_adj(j,i);
				for( unsigned short i=0; i<dim*dim; i++ ) _M[i] = _temp[i];
				break;
	}
}

// Creates the adjugate (or adjunct) matrix and leaves it in the persistent storage space '_temp'
template<>
void smallSquareMatrix<2,double>::adjugateToTemp() noexcept;

template<>
void smallSquareMatrix<2,float>::adjugateToTemp() noexcept;

template<>
void smallSquareMatrix<3,double>::adjugateToTemp() noexcept;

template<>
void smallSquareMatrix<3,float>::adjugateToTemp() noexcept;

template< unsigned short dim, class T >
void smallSquareMatrix<dim,T>::adjugateToTemp() noexcept
{
	// Adjugation procedure depends on dimension
	switch( dim )
	{
		case 1 : _temp[0] = 1.0;											// 1-d is easy
				 break;

		case 2: _temp[0] =   _M[3]; _temp[3] =   _M[0];
				_temp[1] = - _M[1]; _temp[2] = - _M[2];						// 2-d isn't much harder
				break;

		case 3:	for( unsigned short i=0; i<3; i++ )							// For 3d, use the protected method _x3x_adj()
					for( unsigned short j=0; j<3; j++ )
						_temp[_index(i,j)] = _3x3_adj(j,i);
				break;
	}
}

#pragma mark -
#pragma mark Matrix arithmetic and assignment
#pragma mark -

template< unsigned short dim, class T >
smallSquareMatrix<dim,T>& smallSquareMatrix<dim,T>::operator+= ( const smallSquareMatrix &rhs )
{
	for( short i=0; i<dim*dim; i++ ) _M[i] += rhs._M[i];
	return *this;
}

template< unsigned short dim, class T >
smallSquareMatrix<dim,T>& smallSquareMatrix<dim,T>::operator-= ( const smallSquareMatrix &rhs )
{
	for( short i=0; i<dim*dim; i++ ) _M[i] -= rhs._M[i];
	return *this;
}

// Copy assignment.
template< unsigned short dim, class T >
smallSquareMatrix<dim,T>& smallSquareMatrix<dim,T>::operator=( smallSquareMatrix rhs ) noexcept
{
	std::swap( _M   , rhs._M    ); 
	std::swap( _temp, rhs._temp );

	return *this;
}


#pragma mark -
#pragma mark Vector-matrix multiplication
#pragma mark -

// Vector-matrix multiplication; definitions of the specialised version in .cpp.
template<>
void smallSquareMatrix<2,double>::vectorMultiply( const double *x, double *y ) const noexcept;

template<>
void smallSquareMatrix<2,float>::vectorMultiply( const float *x, float *y ) const noexcept;

template<>
void smallSquareMatrix<3,double>::vectorMultiply( const double *x, double *y ) const noexcept;

template<>
void smallSquareMatrix<3,float>::vectorMultiply( const float *x, float *y ) const noexcept;

template< unsigned short dim, class T >
void smallSquareMatrix<dim,T>::vectorMultiply( const T *x, T *y ) const noexcept
{
	switch( dim )
	{
		case 1 :	y[0] = _M[0]*x[0];
					break;
		
		case 2 :	y[0] = _M[0]*x[0] + _M[2]*x[1];
					y[1] = _M[1]*x[0] + _M[3]*x[1];
					break;

		case 3 :	y[0] = _M[0]*x[0] + _M[3]*x[1] + _M[6]*x[2];
					y[1] = _M[1]*x[0] + _M[4]*x[1] + _M[7]*x[2];
					y[2] = _M[2]*x[0] + _M[5]*x[1] + _M[8]*x[2];
					break;
	}
}

// Same, but using the "temp" matrix instead
template<>
void smallSquareMatrix<2,double>::vectorMultiplyUsingTemp( const double *x, double *y ) const noexcept;

template<>
void smallSquareMatrix<2,float>::vectorMultiplyUsingTemp( const float *x, float *y ) const noexcept;

template<>
void smallSquareMatrix<3,double>::vectorMultiplyUsingTemp( const double *x, double *y ) const noexcept;

template<>
void smallSquareMatrix<3,float>::vectorMultiplyUsingTemp( const float *x, float *y ) const noexcept;

template< unsigned short dim, class T >
void smallSquareMatrix<dim,T>::vectorMultiplyUsingTemp( const T *x, T *y ) const noexcept
{
	switch( dim )
	{
		case 1 :	y[0] = _temp[0]*x[0];
					break;

		case 2 :	y[0] = _temp[0]*x[0] + _temp[2]*x[1];
					y[1] = _temp[1]*x[0] + _temp[3]*x[1];
					break;
					
		case 3 :	y[0] = _temp[0]*x[0] + _temp[3]*x[1] + _temp[6]*x[2];
					y[1] = _temp[1]*x[0] + _temp[4]*x[1] + _temp[7]*x[2];
					y[2] = _temp[2]*x[0] + _temp[5]*x[1] + _temp[8]*x[2];
					break;
	}
}


#pragma mark -
#pragma mark For use as a preconditioner
#pragma mark -

// Is the matrix positive definite? -> Sylvester;s theorem
template<>
bool smallSquareMatrix<2,double>::positiveDefinite() noexcept;

template<>
bool smallSquareMatrix<2,float>::positiveDefinite() noexcept;

template<>
bool smallSquareMatrix<3,double>::positiveDefinite() noexcept;

template<>
bool smallSquareMatrix<3,float>::positiveDefinite() noexcept;

template< unsigned short dim, class T >
bool smallSquareMatrix<dim,T>::positiveDefinite() noexcept
{
	switch( dim )
	{
		case 1 : return ( _M[0]>0 );
		case 2 : return ( _M[0]>0 && det()>0 );
		case 3 : return ( _M[0]>0 && _3x3_cofac(2,2)>0 && det()>0 );
		
		default : return false;				// To avoid compiler warnings
	}
}



// If the matrix is positive definite, replaces it with its inverse; otherwise
// replaces it with the identity matrix. Useful for preconditioning a conjugate
// gradient problem.
// Returns "true" if inverted; "false" if identity
template< unsigned short dim, class T >
bool smallSquareMatrix<dim,T>::preconify( T scale )
{
	if( positiveDefinite() )
	{
		return invertInPlace();
	}
	else
	{
		// If no scale was given, find some characteristic value of the matrix
		if( !scale )
		{
			T sum = 0.0;
			for( unsigned short i=0; i<dim*dim; i++ ) sum += fabs(_M[i]);
			scale = ( sum ? dim*dim/sum : 1.0 );
		}
		
		for( unsigned short i=0; i<dim; i++ )
			for( unsigned short j=0; j<dim; j++ )
				set( i, j, scale*(i==j) );

		return false;
	}
}



#pragma mark -
#pragma mark Output
#pragma mark -

// The output operator
template< unsigned short dim, class T >
ostream& operator<< ( ostream &os, const smallSquareMatrix<dim,T> &M )
{
	for( short col=0; col<dim; col++ )
	{
		for( short row=0; row<dim; row++ ) os << M.get(row,col) << ( row==dim-1 ? "" : "\t" );
		os << endl;
	}
	return os;
}



#endif

