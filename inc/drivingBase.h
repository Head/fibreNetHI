/**
 * @file drivingBase.h
 * @author David Head
 * @brief Abstract base class for possible types of driving, including strain.
 */
 

#ifndef FIBRE_NETWORK_HI_DRIVING_BASE_H
#define FIBRE_NETWORK_HI_DRIVING_BASE_H


//
// Includes.
//
#include <array>
#include <iostream>
#include <complex>
#include <map>
#include <limits>

// Local includes.
#include "XMLParameters.h"

//
// Parsing attributes etc. from the parameters file.
//
namespace parameterParsing
{
	// Components of strain tensor (not used by specific strains such as simple shear etc.)
	const std::string strain_xx = "gamma_xx";		// \f$\gamma_{xx}\f$.
	const std::string strain_xy = "gamma_xy";		// \f$\gamma_{xy}\f$; see description of `strainTensor()`.
	const std::string strain_yx = "gamma_yx";		// \f$\gamma_{yx}\f$; see description of `strainTensor()`.
	const std::string strain_yy = "gamma_yy";		// \f$\gamma_{yy}\f$.

	// Pressure gradients.
	const std::string pressureGdt_X = "P_x";		// \f$\langle\partial_{x}P\rangle\f$.
	const std::string pressureGdt_Y = "P_y";		// \f$\langle\partial_{y}P\rangle\f$.
}


/**
 * @brief Abstract base class for possible types of driving, including strain.
  */
class drivingBase
{

private:

protected:

    // Shorthands.
    using moduliMap = std::map< std::string, std::complex<double> >;
    using tensor2D  = std::array< std::array< std::complex<double>, 2 >, 2 >;

    // Box geometry.
    double _X;
    double _Y;

    // Extent of network. Interpretation will depend on network geometry. Defaults to zero.
    double _netX;
    double _netY;

    // Pointer to the parameters object and the log file.
    const XMLParameters *_params;
          ostream       *_log;

public:

    //
    // Constructors / destructor.
    //
    drivingBase( double X, double Y, const XMLParameters *params, ostream *log )
        : _X(X), _Y(Y), _netX(0.0), _netY(0.0), _params(params), _log(log) {}
    virtual ~drivingBase() {}

	/// Initialise after returning from the construction. May just be log output.
	virtual void initialise() const;

    /// Setting the network extent after initialisation.
    virtual void setNetworkExtent( double netX, double netY ) noexcept { _netX = netX; _netY = netY; }

    /// Textual representation of the driving type used in e.g. save files.
    virtual std::string typeLabel() const = 0;

	/// Returns 'true' if this driving includes a pressure gradient.
	virtual bool hasPressureGradient() const noexcept
		{ return ( pressureGradient()[0]!=0.0 || pressureGradient()[1]!=0.0 ); }

	/// Returns 'true' if the strain tensor is traceless, i.e. preserves volume.
	virtual bool tracelessStrain() const noexcept
		{ return std::abs(strainTensor()[0][0]+strainTensor()[1][1]) < std::numeric_limits<double>::epsilon(); }

	/// Returns 'true' if the mean velocity relative to the network should be output in the table.
	virtual bool outputMeanFluidVelocity() const noexcept { return hasPressureGradient(); }

    //
    // Strain definition.
    //

    /**
     * @brief Strain tensor normalised such that the overall magnitude \f$\gamma=1\f$.
     * 
     * Returns the real strain tensor as a 2x2 matrix, where as per standard notation,
     * the first component is assumed to be the row index, and the second the column.
     * 
     * Since this is a linear problem, assumes the strain magnitude \f$\gamma=1\f$ throughout
     * (*i.e.* '1\f$\varepsilon\f$'), so entries will typically be \f$\pm 1\f$.
     * \f[
     *   \gamma = \left(
     *         \begin{array}{cc}
     *              \gamma_{00} & \gamma_{01} \\
     *              \gamma_{10} & \gamma_{11} 
     *          \end{array}
     *    \right)
     * \f]
     * 
     * Defaults to the zero strain tensor, *i.e.* that for which every element is zero.
     */
    virtual std::array< std::array<double,2>, 2 > strainTensor() const { return {{ {0.0,0.0}, {0.0,0.0} }}; }

    /**
     * @brief The point in the system taken to be the strain origin.
     * 
     * The (x,y) coordinates of the point in the system that is taken to be the origin for *e.g.* visualisation.
     * What this actually refers to depends on the strain. Does not affect evaluation of \f$G^{*}(w)\f$ or the non-
     * affinity and related parameters, but does affect *e.g.* visualisation.
     * 
     * Defaults to the centre of the box.
     */
    virtual std::array<double,2> origin() const { return { {0.5*_X,0.5*_Y} }; }

    //
    // Affine displacements and velocities given the strain.
    //

    /**
     * @brief The displacement at the given point as a complex 2-vector.
     * 
     * Returns the displacement at the given location as a complex vector, although it is expected
     * this will always be real. If not overloaded by a child class, will infer the displacement
     * from the (uniform) strain tensor and origin.
     * 
     * @param pos Pointer to real coordinates as a 2-vector.
     * @return Complex 2-vector.
     */
    virtual std::array< std::complex<double>, 2 > affineDisplacement( const double *pos ) const;

    /**
     * @brief The fluid velocity at the given point as a complex 2-vector
     * 
     * Returns the affine prediction for the fluid velocity at the given point, as a complex 2-vector for
     * convenience, although it will typically be purely imaginary. Defaults to the prediction given the
     * uniform strain tensor and the putative origin of the strain field.
     * 
     * @param pos Pointer to real coordinates as a 2-vector.
     * @param omega Frequency.
     * @return Complex 2-vector.
     */ 
    virtual std::array< std::complex<double>, 2 > affineVelocity( const double *pos, double omega ) const;

    /// Wrapper for the affine velocity given separate x and y arguments rather than a 2-vector.
    std::array< std::complex<double>, 2 > affineVelocity( double x, double y, double omega ) const
    {
        std::array<double,2> pos{ {x,y} };
        return affineVelocity( pos.data(), omega );
    }

    //
    // Pressure gradient.
    //

	/**
	 * @brief The pressure gradient as a 2-vector.
	 *
	 * Returns the mean pressure gradient in (x,y) directions. This is effectively the average
	 * over the whole volume (=area), and is applied as a shift across the boundary conditions
	 * for the velocity equations, as well as a balancing network force.
	 *
	 * @return Complex 2-vector of complex amplitudes.
	 */
	virtual std::array<std::complex<double>,2> pressureGradient() const { return {0,0}; }

    /**
     * @brief The pressure shift across the Lees-Edwards-like BCs, in the x-direction.
     * 
     * This applied whenever the pressure is accessed across a horizontal boundary, which currently
     * is only for the velocity equations with Stoke's flow, as the pressure equations only involve
     * the velocities. Typically purely imaginary.
	 *
	 * @return Complex amplitude for $X \times \langle\partial_{x}P\rangle$.
     */
    virtual std::complex<double> pressureShiftLeftRight() const { return _X * pressureGradient()[0]; }

    /**
     * @brief The pressure shift across the Lees-Edwards-like BCs, in the y-direction.
     * 
     * This applied whenever the pressure is accessed across a vertical boundary, which currently
     * is only for the velocity equations with Stoke's flow, as the pressure equations only involve
     * the velocities. Typically purely imaginary.
	 *
	 * @return Complex amplitude for $Y \times \langle\partial_{y}P\rangle$.
     */
    virtual std::complex<double> pressureShiftTopBottom() const { return _Y * pressureGradient()[1]; }

    //
    // Periodic boundary conditions.
    //

    /**
     * @brief Lees-Edwards shift in the horizontal direction.
     * 
     * The Lees-Edwards shift when crossing a boundary in the horisontal direction, for the given frequency.
     * Defaults to that given by the strain tensor.
     * 
     * @param omega Frequency.
     * @return Complex 2-vector.
     */
    virtual std::array< std::complex<double>, 2> fluidShiftLeftRight( double omega ) const
    {
        return {{ {0.0,strainTensor()[0][0]*omega*_X}, {0.0,strainTensor()[1][0]*omega*_X} }};
    }

    /**
     * @brief Lees-Edwards shift in the vertical direction.
     * 
     * The Lees-Edwards shift when crossing a boundary in the vertical direction, for the given frequency.
     * Defaults to that given by the strain tensor.
     * 
     * @param omega Frequency.
     * @return Complex 2-vector.
     */
    virtual std::array< std::complex<double>, 2> fluidShiftTopBottom( double omega ) const
    {
        return {{ {0.0,strainTensor()[0][1]*omega*_Y}, {0.0,strainTensor()[1][1]*omega*_Y} }};
    }

    /**
     * @brief Shifts to the diplacament field.
     * 
     * Returns a real 2-vector for the shift that should be applied for network displacements, for any
     * link that starts at (x1,y1) and ends at (x2,y2). Assumes that no link is more than one half of
     * any box dimension.
     * 
     * More precisely, if x2 is in a periodic copy to the right of x1 (as inferred from being near the
     * *left* boundary while x1 is near the *right* boundary), then the shift gets a contribution
     * \f[
     *   \Delta{\bf r}^{\rm horiz} = \left(
     *     \begin{array}{c}
     *       \gamma_{00}\\
     *       \gamma_{10}
     *     \end{array}
     *   \right)X\:,
     * \f]
     * with \f$X\f$ the horizontal system size. If x2 is in the *left* periodic copy, then minus this is
     * added to the shift.
     * 
     * For vertical copies the contribution is
     * \f[
     *    \Delta{\bf r}^{\rm vert} = \left(
     *     \begin{array}{c}
     *       \gamma_{01}\\
     *       \gamma_{11}
     *     \end{array}
     *   \right)Y\:,
     * \f]
     * when y2 is in the copy above y1, and minus this for the periodic copy below.
     * 
     * @param x1 x-coord of start point.
     * @param y1 y-coord of start point.
     * @param x2 x-coord of end point.
     * @param y2 y-coord of end point.
     * @return Shift that should added to the end-to-end vector for this link.
     */
    virtual std::array<double,2> getDispShift( double x1, double y1, double x2, double y2 ) const;

    //
    // Complex moduli.
    //

    /**
     * @brief Map of moduli extracted from the given strain.
     *
     * Returns an ordered std::map of labels (keys) and network moduli (values).
     * These are calculated by each child class, with possibly more than one for
     * each shear, depending on what is deemed 'relevant.' Defaults to an empty map.
     */
    virtual moduliMap networkModuli( tensor2D sigma_ij ) const { return {}; };

    /**
     * @brief Fluid contribution to the complex moduli.
     *
     * Starting from the deviatoric stress tensor of
     * \f[
     *   \nu\left(
     *     \partial_{i}v_{j} + \partial_{j}v_{i}
     *   \right)
     * \f]
     * and averaging over the volume, the contribution to the shear stress is found to
     * directly derive from the affine velocity (since during the volume average you integrate
     * over a derivative, giving the difference in velocities between boundaries, which is known).
     * 
     * The resulting expression is
     * \f[
     *  i\omega\nu\left(\gamma_{ij}+\gamma_{ji}\right)
     * \f]
     * 
     * Defaults to an empty map.
     */
    virtual moduliMap fluidModuli( double omega, double viscosity ) const { return {}; };
};


#endif


