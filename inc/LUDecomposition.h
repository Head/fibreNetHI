//
// A simple implementation of LU-decomposition with partial pivoting, essentially following
// the example in Numerical Recipes 2.3 except for a few simplifications and omissions.
//
// It is currently assumed that the matrix to be inverted is passed as a vector<T>,
// where the index corresponds to M*row+col, but there should be no issues generalising
// this by overloading the constructor.
//
// Defines custom exceptions.
//
// 23/11/2017/DAH: Templated to scalar type T to allow use for complex matrices. This also
// meant putting everything into this header file; if specialisations were desired for
// certain methods, then would use the .cpp file as per e.g. smallSquareMatrix<>.
//


#ifndef _UD_LUDECOMPOSITION_H
#define _UD_LUDECOMPOSITION_H


// Standard includes
#include <vector>
#include <float.h>
#include <stdexcept>
#include <sstream>
#include <cmath>
#include <iostream>

using namespace std;


template< typename T >
class LUDecomposition
{
private:
	
protected:

	// The solution matrix; initially the passed matrix but decomposed in-place
	int _M;								// The matrix size (MxM)
	vector<T> _LUDecomp;
	
	// List of index swaps; note only using 'int' as this method would surely be totally unsuitable for 'long'-sized matrices
	vector<int> _indices;
	
	// Performs the decomposition
	void _performDecomposition();
	
public:

	//
	// Constructors / destructor
	//
	LUDecomposition() {}
	virtual ~LUDecomposition() {}
	
	// Construction from an MxM matrix of given dimensions
	LUDecomposition(const vector<T>&,int);					// Same format as internal; just copy


	//
	// Solver
	//
			void solve(T*) const;											// Pass ptr to Ts (assumes at least size M)
			void solve( vector<T> *rhs ) const;								// Pass RHS vector; solves in place in the same vector
	inline	void solve( const vector<T> &RHS, vector<T> *x ) const			// Pass RHS and solution vector; keeps RHS vector intact
				{ *x = RHS; solve(x); }
	inline 	void solve( T *rhs, T *x ) const								// Other wrappers
				{ for( int i=0; i<_M; i++ ) x[i] = rhs[i]; solve(x); }
	inline	void solve( T *rhs, vector<T> *x ) const						// No need to check size here
				{ x->resize(_M); for( int i=0; i<_M; i++ ) (*x)[i] = rhs[i]; solve(&x->at(0)); }



	//
	// Custom exceptions
	//
	class ZeroRow;					// A row has all-zero elements (raised during decomposition)
	class ZeroDiagonal;				// A zero element along the diagonal was encountered during decomposition
	class RHSVectorWrongSize;		// Checked when applying the LU-decomp. to a RHS vector
};


#pragma mark -
#pragma mark Custom exceptions
#pragma mark -


// Errors that can arise during decomposition
template< typename T >
class LUDecomposition<T>::ZeroRow : public runtime_error {
private:
        string makeWhat( int i ) {     
                ostringstream out;
                out << "Cannot LU-decompose matrix: Row " << i << " has all zero elements" << endl;
                return out.str();
        }
public:
        ZeroRow( int i ) : runtime_error( makeWhat(i) ) {}
};

template< typename T >
class LUDecomposition<T>::ZeroDiagonal : public runtime_error {
private:
        string makeWhat( int i ) {     
                ostringstream out;
                out << "Cannot LU-decompose matrix: Row " << i << " has a zero element on the diagonal (possibly after row-interchange[s])" << endl;
                return out.str();
        }
public:
        ZeroDiagonal( int i ) : runtime_error( makeWhat(i) ) {}
};

// Errors that an arise when applying the decomposed matrix to a specific solution
template< typename T >
class LUDecomposition<T>::RHSVectorWrongSize : public runtime_error {
private:
        string makeWhat( int i ) {     
                ostringstream out;
                out << "Cannot apply LU-decomposed matrix to the RHS vector, as it is of the wrong size '" << i << "'" << endl;
                return out.str();
        }
public:
        RHSVectorWrongSize( int i ) : runtime_error( makeWhat(i) ) {}
};


#pragma mark -
#pragma mark Constructors / destructor
#pragma mark -


// Construction from an MxM matrix of given dimensions, in same row/column format as internally stored
template< typename T >
LUDecomposition<T>::LUDecomposition( const vector<T> &matrix, int M ) : _M(M), _LUDecomp(matrix)
{
	// This is where conversion from other matrix formats would be done if not simply copying over
	_performDecomposition();
}




#pragma mark -
#pragma mark Numerical analysis
#pragma mark -

// Actually performs the decomposition and stores the result in _matrix (with row interchanges).
template< typename T >
void LUDecomposition<T>::_performDecomposition()
{
	// Initialise the index list (for row interchanges; need by solve() so persistent) and (local) scaling factors
	_indices.resize(_M);
	vector<double> scalePerRow(_M);

	// Get the largest element in magnitude in each row, and store the reciprocal.
	double maxElt = 0.0, temp;
	for( int row=0; row<_M; row++ )
	{
		maxElt = 0.0;
		for( int col=0; col<_M; col++ )
			if( (temp=std::abs(_LUDecomp[row*_M+col]))>maxElt ) maxElt = temp;

		// If all elements (essentially) zero, raise an exception
		if( maxElt == 0.0 ) throw ZeroRow(row);
		
		scalePerRow[row] = 1.0 / maxElt;		// Now safe to take the reciprocal
	}	
	
	// Perform the decomposition with implicit pivoting
	int maxRow;					// Location of the largest element about which to pivot
	for( int k=0; k<_M; k++ )
	{
		// Initialise pivoting with Crout's algorithm
		maxElt = 0.0;
		maxRow = k;
		
		// Get the largest element (in magnitude) in row k starting from the diagonal; also store where it was found.
		// Note here the scaling is applied before comparison, i.e. this is implicit pivoting; could make an option?
		for( int row=k; row<_M; row++ )
			if( (temp=scalePerRow[row]*std::abs(_LUDecomp[row*_M+k])) > maxElt ) { maxElt = temp; maxRow = row; }

		// Row interchange (if needed)
		if( k != maxRow )
		{
			for( int col=0; col<_M; col++ )
				std::swap( _LUDecomp[maxRow*_M+col], _LUDecomp[k*_M+col] );

			scalePerRow[maxRow] = scalePerRow[k];	// Since only pivot the remainder of the column, don't need to swap scales
		}

		// Store the max indices; will need when constructing each solution
		_indices[k] = maxRow;

		// Check that the diagonal element is not zero; currently just raise an exception, but Numerical
		// Recipes in the LU decomp. section (p.52 in C++ Ed.) suggests a way around this
		if( _LUDecomp[k*(_M+1)] == 0.0 ) throw ZeroDiagonal(k);			// NB: k*(_M+1) = k*_M+k

		// Pivot
		for( int row=k+1; row<_M; row++ )
		{
			T piv = _LUDecomp[row*_M+k] /= _LUDecomp[k*(_M+1)];		// Divide by the pivot element (which is now on the diagonal)
			for( int col=k+1; col<_M; col++ )											// Crout's algorithm
				_LUDecomp[row*_M+col] -= piv*_LUDecomp[k*_M+col];
		}
	}
}


// Solver; pass RHS vector which is replaced with the solution (no range checking)
template< typename T >
void LUDecomposition<T>::solve( T *x ) const
{
	// Construct the solution, unravelling the row interchanges during pivoting; forward substitution first ...
	T sum;
	for( int row=0; row<_M; row++ )
	{
		sum = x[_indices[row]];
		x[_indices[row]] = x[row];
		
		for( int col=0; col<row; col++ ) sum -= _LUDecomp[row*_M+col] * x[col];

		x[row] = sum;
	}	
	
	// ... followed by the back substitution
	for( int row=_M-1; row>=0; row-- )
	{
		sum = x[row];

		for( int col=row+1; col<_M; col++ ) sum -= _LUDecomp[row*_M+col] * x[col];

		x[row] = sum / _LUDecomp[row*(_M+1)];			// NB: row*(_M+1) == row*_M + row
	}
}

// Wrapper for when passing a vector<T>; checks size and throws exception if not correct
template< typename T >
void LUDecomposition<T>::solve( vector<T> *rhs ) const
{
	if( int(rhs->size()) != _M ) throw RHSVectorWrongSize(rhs->size());
	solve( &rhs->at(0) );
}






#endif
