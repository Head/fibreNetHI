/**
 * @file networkTriSpringGenSierp.h
 * @author David Head
 * @brief Generalisation of the Sierpinski network to different generation rules. 
 */


 #ifndef FIBRE_NETWORK_HI_NETWORK_TRI_SPRING_GEN_SIERP_H
 #define FIBRE_NETWORK_HI_NETWORK_TRI_SPRING_GEN_SIERP_H


//
// Includes.
//

// Standard includes.
#include <set>

// Parent class.
#include "networkTriSpringSierpinski.h"


//
// Parameters
//
namespace parameterParsing
{
	/// The triangle to omit; one of M=middle (standard Sierpinksi triangle), L=left, T=top or R=right.
	const std::string netTSGenSierp_omitTriangle  = "omitTriangle";

	// Labels for each of the triangles that will be omitted (for all levels a triangle is omitted).
	const std::string netTSGenSierp_omitTriangle_M = "M";		///< Omit the middle triangle; standard Sierpinski.
	const std::string netTSGenSierp_omitTriangle_T = "T";		///< Omit the top triangle (normal orientation).
	const std::string netTSGenSierp_omitTriangle_L = "L";		///< Omit the left-most triangle (either orientation).
	const std::string netTSGenSierp_omitTriangle_R = "R";		///< Omit the right-most triangle (either orientation).
}





/**
 * @brief Triangualar spring network based on a generalised Sierpinksi triangle.
 * 
 * This differs from the standard Sierpinski in that the triangle that is omitted from one level to the
 * lower (smaller length) level is not always the middle one, and can be the top, bottom or left one.
 *
 * This follows the 2022 summer internship by Katerina Karakoulaki, that showed such triangles have the
 * same fractal dimension and (mean) coordination number as the standard Sierpinski triangle, but not every
 * node has the same degree - some are 6, some are 4.
 *
 * Parameters are the same as for the standard Sierpinksi (see parent class), plus:
 *
 *   <Parameter emptyTriangle="L" />		// L=left, R=right, M=middle (standard), T=top.
 *
 * Triangles are inverted vertically during the generation, but left/right remain the same.
 */  
class networkTriSpringGenSierp : public networkTriSpringSierpinski
{

private:

	/// Enumerated type for the omitted triangle.
	enum class __omittableTriangles { M, T, L, R };
	__omittableTriangles __omitTri;

protected:

	/// Modifies the parent's recursive calls to omit the requested triangle.
    virtual std::set<__triangle> _recurseTriangles( std::set<__triangle> triangles, int level ) const override;
 
	/// String label for class type.
	virtual std::string _typeLabel() const noexcept override { return "networkTriSpringGenSierp"; }

public:

    // Constructors / destructor.
	networkTriSpringGenSierp() {}
	networkTriSpringGenSierp( double X, double Y, const XMLParameters *params, ostream *log, const drivingBase *driving );
 	virtual ~networkTriSpringGenSierp() {}

};


#endif

 