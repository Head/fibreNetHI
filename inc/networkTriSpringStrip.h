/**
 * @file networkTriSpringStrip.h
 * @author David Head
 * @brief Specialisation of the triangular spring network to restrict it to a central region in the middle of the system.
 */
 

#ifndef FIBRE_NETWORK_HI_NETWORK_TRI_SPRING_STRIP_H
#define FIBRE_NETWORK_HI_NETWORK_TRI_SPRING_STRIP_H


//
// Includes.
//

// Standard includes.
#include <string>

// Parent class.
#include "networkTriSpring.h"


//
// Parameters
//
namespace parameterParsing
{
	const std::string netTSStrip_stripThickness  = "stripThickness";
}


/**
 * @brief Specialisation of the triangular spring network to restrict it to a central region in the middle of the system.
 * 
 * The region still spans the entire system horizontally - hence 'strip'.
 *
 * Volume is taken to include half-rows either side of the edge nodes.
 * 
 * Parameters defined as:
 * 
 * 	<Parameter networkType    ="triangular spring strip" />
 *	<Parameter stripThickness ="30.0" />
 */
class networkTriSpringStrip : public networkTriSpring
{

private:

protected:

    /// Geometry parameters.
    double _nominalStripThickness;

    // Geometry methods.
	virtual double _latticePos_y( int i, int j ) const noexcept override;
    virtual double _actualThickness() const { return _root3over2*(_numY-1); }
        ///< Actual thickness between both boundaries, with no empty space (ignoring dilution).
    
    // Components of lattice initialisation.
	virtual void _initLatticeDimensions() override;
	virtual void _trialConnection      ( int i1, int j1, int i2, int j2, double dilution=1.0 ) override;

    /// Periodicity limited to one axis.
    virtual unsigned int _globalIndex( int i, int j ) const noexcept override;

	/// Volume spanned by the network.
	virtual double _volume() const override { return _X * _actualThickness(); }

	/// String label for class type.
	virtual std::string _typeLabel() const noexcept override { return "networkTriSpringStrip"; }

 public:

    // Constructors / destructor.
    networkTriSpringStrip() {}
	networkTriSpringStrip( double X, double Y, const XMLParameters *params, ostream *log, const drivingBase *driving );
    virtual ~networkTriSpringStrip() {}

    //
    // Initialisation.
    //
    virtual void initialise() override;

	//
	// Extent (bulk geometry) of the network; required by some strain fields.
	//
	virtual double extentY() const noexcept override { return _actualThickness(); }

    //
    // Analysis.
    //
    virtual bool hasInterface() const noexcept override { return true; }
	virtual double interfacialFlux() const override;

};


#endif
