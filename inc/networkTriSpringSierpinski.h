/**
 * @file networkTriSpringSierpinksi,h
 * @author David Head
 * @brief Specialisation of triangualar spring network to some version of the Sierpinksi triangle / gasket.
 * 
 * Initialises springs as per two Sierpinski triangles, with opposing orientations, such that they fill the space,
 * but otherwise is a standard triangular spring network. There is also an option to only include fractality
 * below a given level of recursion, to give some control over the maximum range of scale invariance.
 * 
 * As currently implemented, the number of nodes in both the x and y directions must be the same, which needs
 * to be set up in parameters (these parameters are parsed by the parent class networkTriSpring). Also, the
 * number of nodes in each direction must be a power of 2 - the maximum level is defined as 2^{maxLevel} = nodesX/Y.
 */


#ifndef FIBRE_NETWORK_HI_NETWORK_SIERPINKSI_H
#define FIBRE_NETWORK_HI_NETWORK_SIERPINKSI_H


//
// Includes.
//

// Standard includes.
#include <utility>
#include <array>
#include <cmath>
#include <set>

// Parent class.
#include "networkTriSpring.h"


//
// Parameters
//
namespace parameterParsing
{
    /// Highest (longest) level for which fractality is present.
    const std::string netTSSierp_maxFractalLevel = "maxFractalLevel";

    /// Option to include the edges of the major triangles. Defaults to 'false'.
    const std::string netTSSierp_includeMajorEdges = "includeMajorEdges";

	/// Optional probability for random springs on top of the fractal geometry. Defaults to 0.0.
	const std::string netTSSierp_addRandomSpringProb = "addRandomSpringProb";
}


/**
 * @brief Triangualar spring network based on Sierpinksi triangles.
 * 
 * The number of lattice nodes in both direction must be equal and a power of 2; this
 * is not controlled by this class and must be consistent with the dimensions given in
 * the parameters fill, else an exception will be thrown during initialisation of the
 * lattice.
 * 
 * The maximum (largest) level `n` obeys \f$2^{n}=N \f$, where \f$ N \f$ is the
 * number of nodes in either the x or y direction.
 * 
 * A single optional parameter `maxFractalLevel` can be specified which controls how many
 * lower (shorter) levels are fractal; above this level, there is no fractality and the
 * network effectively consists of tiled smaller Sierpinski triangles (in fact, the same
 * recursive generation routine is used, but the middle triangle is included in higher
 * levels - if all 4 triangles are included at all levels, the resulting object is not fractal).
 * 
 * The lowest value of `maxFractalLevel` is 1, which means there is no fractality; this
 * will just generate the usual triangular spring network.
 * 
 * If not specified, fractality is present over all length scales possible, *i.e.* all levels.
 * 
 * By default edges of the 'major' triangles, i.e. the largest level of fractality, are not
 * included. This can be changed by including setting the parameter includeMajorEdges to 'true'.
 * If they are not included, every node has z=4 - in fact, a max. fractal level of m=1 gives
 * the kagome lattice (see papers w/Tom Lubensky), except when m matches the maximum size of the
 * system, when the two nodes at the tips of the triangles have z=2. This is because, for this case,
 * 'upwards' oriented triangles are not connected vertically to 'downwards' oriented triangles,
 * but to another (actually the same) upwards oriented triangle through the periodic boundaries;
 * and the same for downward-point triangles.
 *
 * 27/4/2022/DAH: Added option to also add random springs to the network, on top of the recursively
 * generated ones, with a probability that defaults to zero. Combined with the usual dilution parameter,
 * this allows springs to be randomly added or deleted from the fractal network.
 */  
class networkTriSpringSierpinski : public networkTriSpring
{

protected:

  	/// Types for nodes, edges, and triangles. May be used by child classes, so protected rather than private.
    using __node      = std::pair<int,int>;
    using __edge      = std::array<__node,2>;
    using __triangle  = std::array<__node,3>;
	
private:

    /// Gets (and checks) the number of levels for the Sierpinski triangle (during initialisation, prior to adding springs).
    void __getLevels();

    /// Returns with all of the small triangles that make up the full Sierpinksi triangle.
    std::set<__triangle> __getTriangles() const;

    /// Convert triangles to (unique) edges, i.e. node pairings.
    std::set<__edge> __getEdges( std::set<__triangle> triangles ) const;

    /// Returns all of the (normalised) edges of the major triangles.
    std::set<__edge> __getMajorEdges() const;

    /// Wraps node indices (i,j) such that i \in [0,_numX) and j \in [0,_numY).
    void __wrapNodeIndices( __node &e ) const;

    /// Returns a normalised edge, i.e. with each node indices' wrapped, and first node has lower indices.
    __edge __normedEdge( __node n1, __node n2 ) const;

protected:

	/// The main recursive function for generating the Sierpinski triangle.
	virtual std::set<__triangle> _recurseTriangles( std::set<__triangle> triangles, int level ) const;
  
	/// Returns a midnode between the two given nodes, with a tweak to ensure it remains correct with half-shifts.
    std::pair<int,int> _midNode( __node node1, __node node2, int offset_x ) const;

    /// Option to include the springs along the edges of the 'major' triangles (i.e. at the largest fractal level).
    bool _includeMajorEdges;

    /// Assume the number of lattice nodes in each direction is the same power of 2.
    int _highestLevel;

    /// Optional parameter for the maximum level at which fractality is present.
    int _maxFractalLevel;

	/// Optional parameter for the addition of random springs after the fractal network is generated.
	double _addRandomSpringProb;

    /// Check the lattice dimensions are suitable, then try to add the springs to the lattice.
  	virtual void _initSprings() override;

	/// Adds random springs with the given probability, checking first that they do not already exist.
	virtual void _refillSprings( double prob );

	/// String label for class type.
	virtual std::string _typeLabel() const noexcept override { return "networkTriSpringSierpinski"; }

public:

    // Constructors / destructor.
    networkTriSpringSierpinski() {}
	networkTriSpringSierpinski( double X, double Y, const XMLParameters *params, ostream *log, const drivingBase *driving );
    virtual ~networkTriSpringSierpinski() {}

    //
    // Debugging.
    //

    /// Check for repeated edges; also bad edges (i.e. with identical end nodes).
    void debugCheckEdges( std::set<__edge> edges ) const;
};



#endif


 