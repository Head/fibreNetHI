/**
 * @file fibreNetHI.h
 * @author David Head
 * @brief Class for the combined fibre network and fluid simulator.
 */
 

#ifndef FIBRE_NET_HI_H
#define FIBRE_NET_HI_H


//
// Includes
//

// Standard includes
#include <string>
#include <iostream>
#include <memory>
#include <fstream>
#include <stdexcept>
#include <sstream>
#include <math.h>
#include <chrono>

// Local includes
#include "XMLParameters.h"
#include "sequentialFilenames.h"

// Project includes
#include "networkBase.h"
#include "networkNone.h"
#include "networkTriSpring.h"
#include "networkTriSpringStrip.h"
#include "networkTriSpringSierpinski.h"
#include "networkTriSpringGenSierp.h"
#include "networkTriSpringCorrelated.h"

#include "fluidBase.h"
#include "fluidDragOnly.h"
#include "fluidStokes.h"

#include "matrixNetwork.h"
#include "matrixTriSpringStokes.h"
#include "matrixTriSpringDrag.h"

#include "drivingBase.h"
#include "drivingSimpleShear.h"
#include "drivingExtensionalShear.h"
#include "drivingPressureGdt.h"
#include "drivingArbStrain.h"


//
// Strings for attributes, types etc. in the parameters file.
//

/// String constants used for parsing the parameters file.
namespace parameterParsing
{
	// Box dimensions.
	const std::string boxDimX = "X";								///< X-dimension of the box.
	const std::string boxDimY = "Y";								///< Y-dimension of the box.
	const std::string boxDimY_commensurateTriangle = "triangle";	///< If used for the Y dimension, sets it to be \sqrt{3}/2 times X.

	// Networks.
	const std::string netType                    = "networkType";
	const std::string netTypeTriangularSprings   = "triangular spring";					///< Triangular spring network with bond dilution.
	const std::string netTypeNone                = "none";								///< No network at all.
	const std::string netTypeTriSpringStrip      = "triangular spring strip";			///< Triangular lattice restricted to a hoizontal 'strip'.
	const std::string netTypeTriSpringSierpinski = "triangular spring Sierpinski";		///< Sierpinksi triagle/gasket-based triangular lattice.
	const std::string netTypeTriSpringGenSierp   = "triangular spring gen Sierp";		///< Generalised Sierpinksi triangle-based networks.
	const std::string netTypeTriSpringCorrelated = "triangular spring correlated";		///< Triangular spring network with correlated bond dilution.

	// Fluids.
	const std::string fluidType               = "fluidType";
	const std::string fluidTypeDragOnly       = "drag only";		///< Just drag on each node; no hydrodynamic interactions.
	const std::string fluidTypeStokes         = "Stokes";			///< Network nodes coupled to a 2D Stokes fluid.

	// Solvers.
	const std::string solverType             = "solver";
	const std::string solverTypeDenseDirect  = "dense direct";		///< Very slow (cubic complexity); only for small systems / testing.
	const std::string solverTypeSparseDirect = "sparse direct";		///< Currently uses SuperLU which will need to be installed.

	// Strains.
	const std::string drivingType                 = "driving";
	const std::string drivingTypeSimpleShear      = "simple shear";			///< Simple shear as usually visualised.
	const std::string drivingTypeExtensionalShear = "extensional shear";	///< Bidirectional flow but still pure shear.
	const std::string drivingTypePressureGdt      = "pressure gradient";	///< Pressure gradient in the absence of strain.
	const std::string drivingTypeArbStrain        = "arbitrary strain";		///< Abritrary strain but no pressure gradient.

	// Optional table output.
	const std::string outputMaxForceMoment = "maxForceMoment";		///< To output the force moment distribution (up to the given moment).
	const std::string outputNonAffinity    = "output_NA_DC";		///< To output network and fluid non-affinity metrics, and the decoupling mesasure.
}


/**
 * @brief Class for the combined fibre network and fluid simulator.
 *
 * Class that initialises the network and fluid objects, and handles their interaction,
 * to generate results and output then in a table. There is also a method to save states
 * for offline analysis and visualisation using the associated Python scripts.
 * 
 * To keep the table sizes manageable, only the relevant moduli (as defined by the strain)
 * are output by default. To also output the nonaffinuty metrics, including the network/fluid
 * decoupling measure, then somewhere in the parameters file there should be the line
 * 
 *   `<Parameter output_NA_CD="True">`
 * 
 * (attribute name defined in the 'parameterParsing' namespace and may change in the future).
 * 
 * Similarly, to output moments of the network force distribution, there should be a line
 * 
 *   `<Parameter maxForceMoment="n" />`
 * 
 * where n is the highest moment output. The zero'th moment is not output as this requires
 * thresholding - better to use the built-in pebble game methods to determine the extent
 * of the rigid backbone.
 *
 * This class also handles the box geometry through the parameters `X` and `Y`.  For convenience, string
 * attributes are allowed for `Y`, which calculates a value that is somehow commensurate with `X`. Current options
 * are:
 * - `triangle` : \f$ Y = \frac{\sqrt{3}}{2}X\f$, commensurate with an equiliateral triangle geometry.
 */
class fibreNetHI
{

private:

	// Constants
	constexpr static int __nDigits = 4;					///< No. digits to use for the index filenames.
	static std::string __logFileName;					///< Filename for logging.
	static std::string __prefix;						///< Saved states will be indexed as per `<prefix>_nnnn<suffix>`.
	static std::string __suffix;

	constexpr static double _root3over2 = 0.8660254037844386;		///< For `Y` commensurate with an equilateral triangle.

protected:

    // Shorthands.
    using moduliMap = std::map< std::string, std::complex<double> >;

	/// Enumerated type for the solver.
	enum class _solverType { unknown, denseDirect, sparseDirect };
	_solverType _solver;

	/// Enumerated type for the implicit matrix.
	enum class _matrixType { unknown, triSpringStokes, triSpringDrag };
	_matrixType _matType;

	// Box dimensions.
	double _X;
	double _Y;

	// The two primary components; the fluid and the network objects, all derived from their bases.
	std::unique_ptr<networkBase> _network;
	std::unique_ptr<  fluidBase> _fluid;

	/// The strain driving.
	std::unique_ptr<drivingBase> _driving;
	
	/// The parameters file parsing object 
	std::unique_ptr<XMLParameters> _paramParser;

	/// The log file
	std::ofstream _logFile;
	
	/// The last frequency that a solution was found; negative for no solutions yet (or failed to converge).
	double _lastOmega;

	/// The maximum force moment to output. If not specified, no force moments are output.
	int _maxForceMoment;

	/// Flag to output the non-affinity and decoupling measures.
	bool _output_NA_DC;
	
	/// This object handles index filenames
	std::unique_ptr<sequentialFilenames> _fnameIndexer;
	
	/// Matrix for implicit solver, if can be defined for the network-fluid combination. Should match the type.
	std::unique_ptr< matrixBase< std::complex<double> > > _matrix;

	/// Also have a real, network-only matrix for the static solution \omega=0.
	std::unique_ptr< matrixNetwork<double> > _matrixStatic;

	// The solvers currently supported.
	virtual int _solveDenseDirect ( double freq );
	virtual int _solveSparseDirect( double freq );
	
	// Construct the matrix and the RHS vector, and map solution back to the network and fluid objects after solution. 
	virtual int  _constructMatrixAndRHS( double freq );				// Returns negative integer if failed.
	virtual void _recoverSolution      ( bool staticSolution );		// Arg='true' for static matrix, else dynamic.
	
	/**
	 * @brief Removes uniform drift induced by the periodic boundaries.
	 * 
	 * Resets the origins of both the displacement and velocity fields to counteract 'drift' produced by
	 * the periodic boundary conditions. More precisely, applies a complex offset vector \f$ {\bf u}_{0} \f$
	 * to all displacements - and a corresponding \f$ i\omega {\bf u}_{0} \f$ to all velocities (so that the
	 * coupling terms are preserved) - such that
	 * - the real part of \f$ {\bf u}_{0} \f$ minimises the non-affinity measure;
	 * - the imaginary part of \f$ {\bf u}_{0} \f$ has zero mean.
	 * 
	 * Note that in practice the values that minimise the non-affinity measure are very close to the mean anyway,
	 * within 1% on small test systems.
	 * 
	 * If there was no network, the same process is applied just to the fluid. 
	 */
	virtual void _resetOrigins();
	
public:

	fibreNetHI() {}
	fibreNetHI( std::string params_fname );
	virtual ~fibreNetHI() { _logFile << "\nFinished." << std::endl; }
	
	//
	// Solve.
	//
	/**
	 * @brief Solve for a single frequency
	 * 
	 * Solves the coupled fluid and network problem for the given frequency \f$\omega\f$.
	 * Each of the fluid and network objects will store their solutions (typically as a set
	 * of complex amplitudes) for subsequent analysis, saving to outout file *etc.*
	 * 
	 * @param freq The driving frequency \f$\omega\f$
	 * 
	 * @return 0 if successful (no or unknwon no. of iterations), >0 gives the number of iterations taken, <0 means failed.
	 */ 
	virtual int solve( double freq );
	
	//
	// Analysis
	//

	/**
	 * @brief Returns the coordination number of the immersed network.
	 * 
	 * Returns the mean coordination nunmber \f$ \langle z\rangle \f$ of the immersed network,
	 * defined as the number of network nodes that each node is directly connected to, on average.
	 * 
	 * @return Network coordination number \f$ \langle z\rangle \f$
	 */

	inline double networkCoordinationNumber() const { return _network->coordinationNumber(); }

	/**
	 * @brief Returns a scalar measure of the network non-affinity.
	 * 
	 * Returns a measure of the degree of network non-affinity for the most recent solve.
	 * How this is defined depends on the network, but should be zero for an affine solution,
	 * and increase monotonically as the degree of non-affinity increases.
	 * 
	 * @return Network non-affinity.
	 */
	inline double networkNonAffinity() const { return _network->nonAffinity(); }

	/**
	 * @brief Returns a scalar measure of the fluid non-affinity.
	 * 
	 * Returns a measure of the degree of fluid non-affinity for the most recent solve.
	 * How this is defined depends on the fluid, but should be zero for an affine solution,
	 * and increase monotonically as the degree of non-affinity increases.
	 * 
	 * @return Fluid non-affinity.
	 */
	inline double fluidNonAffinity() const { return _fluid->nonAffinity(); }

	/**
	 * @brief Returns a scalar measure of the degree of de-coupling between the network and fluid.
	 * 
	 * Returns zero for when the network and fluid trajectories for the most recent solve coincide,
	 * and increases as they diverge from each other. The trajectories are evaluated at network nodes
	 * as per
	 * 
	 * \f[
	 *   \frac{1}{N_{\rm nodes}\,\omega^{2}}
	 *   \sum_{\alpha}\left(
	 *     \left|i\omega u_{x}-v_{x}\right|^{2}
	 *     +
	 *     \left|i\omega u_{y}-v_{y}\right|^{2}
	 *   \right)
	 * \f]
	 * with the fluid velocities evaluated at the location of the network nodes.
	 * Note this returned value still needs to be divided by some length scale squared
	 * to make it dimensionless.
	 * 
	 * @return Fluid-network decoupling measure.
	 */
	double decouplingMeasure() const;		///< Still needs to be divided by some length scale squared.

	//
	// I/O
	//

	/**
	 * @brief Outputs the most recent solve to file.
	 * 
	 * The output filename is indexed, *i.e.* with an index that is incremented for each state output.
	 * The start of the file will contain information about the frequency, box dimensions, and any
	 * network and fluid parameters, including full specification of nodes positions / displacements *etc.*
	 * The exact format may vary in the future - please see the code in the `.cpp` file to check the
	 * current output.
	 * 
	 * This package contains a python script that can parse the files output by this method, for visualisation
	 * and offline analyses. This method and the corresponding Python script need to be kept synchronised
	 * when any changes are made.
	 */
	void saveSolution();

	/**
	 * @brief Outputs a row of metrics based on the last solve.
	 * 
	 * Outputs a row starting with the frequency, followed by the real and imaginary parts of any moduli
	 * relevant to the chosen strain. If optionally specified in the parameters file, non-affinity
	 * metrics, decoupling, and moments of the force distribution will also be output.
	 * 
	 * For the moduli, the absolute values for the out-of-phase contributions, both network and fluid, are
	 * taken when outputting to the table. This is because the model assumes a factor e^{i\omega t} for every
	 * degree of freedom, when  this should probably have been e^{-i\omega t}. In this way, the fluid phase
	 * is behind solid rather than ahead of it. Essentially this is a sign convention issue.
	 *
	 * This package includes a python script that reads in tables from one or more runs and displays
	 * the results graphically.
	 * 
	 * @param fname Table filename. If the file does not exist, will create one with suitable column headings.
	 */
	void tableRow( std::string fname ) const;
	
	//
	// Exceptions
	//
	class ParameterReadError;			// Defined in this header file.
	class NotYetImplementedError;		// Some feature or combination of features not yet implemented.
};


#pragma mark -
#pragma mark Exceptions
#pragma mark -

/**
 * @brief Custom exception class for a parameter read error.
 * 
 * Typical usage is
 * 
 * `throw ParameterReadError(<string message>,__FILE__,__LINE__)`
 * 
 * which will output the message
 * 
 * `Could not parse parameters or bad parameter; <why>`
 * 
 * and the point in the code that the exception was thrown.
 */
class fibreNetHI::ParameterReadError : public std::runtime_error
{
private:
        std::string makeWhat( std::string why, std::string file, int line )
        {
                std::ostringstream out;
                out << "Could not parse parameters or bad parameter; " << why
                        << " [in file " << file << ", line " << line << "]" << std::endl;
                return out.str();
        }

public:
        ParameterReadError( std::string why, std::string file, int line ) : std::runtime_error( makeWhat(why,file,line) ) {}
};

/**
 * @brief Custom exception class for a feature, or combination of features, that is not yet implemented.
 * 
 * Typical usage is
 * 
 * `throw NotYetImplementedError(<feature as a string>,__FILE__,__LINE__)`
 * 
 * which will output the message
 * 
 * `Feature (or combination of features) not yet implemented: <feature>`
 * 
 * and the point in the code that the exception was thrown.
 */
class fibreNetHI::NotYetImplementedError : public std::runtime_error
{
private:
        std::string makeWhat( std::string feature, std::string file, int line )
        {
                std::ostringstream out;
                out << "Feature (or combination of features) not yet implemented; " << feature
                        << " [in file " << file << ", line " << line << "]" << std::endl;
                return out.str();
        }

public:
        NotYetImplementedError( std::string feature, std::string file, int line ) : std::runtime_error( makeWhat(feature,file,line) ) {}
};


#endif
