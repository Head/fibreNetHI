/**
 * @file drivingArbStrain.h
 * @author David Head
 * @brief Arbitrary strain tensor, but no pressure gradient.
 */


#ifndef FIBRE_NETWORK_HI_DRIVING_ARB_STRAIN_H
#define FIBRE_NETWORK_HI_DRIVING_ARB_STRAIN_H


//
// Includes.
//

// Standard includes.
#include <complex>

// Parent class.
#include "drivingBase.h"


/**
 * @brief Arbitrary strain tensor, but no pressure gradiebt.
 */
class drivingArbStrain : public drivingBase
{

private:

protected:

	/// Mean strain as specified im the parameters file. Values set in constructor.
	std::array< std::array<double,2>, 2 > _strainTensor;

public:

    // Constructors / destructor.
    drivingArbStrain( double X, double Y, const XMLParameters *params, ostream *log );
    virtual ~drivingArbStrain() {}

    /// Textual representation of the driving type.
    virtual std::string typeLabel() const override { return "drivingArbStrain"; }

    /**
     * @brief General strain tensor, not necessariy symnmetric.
     * 
	 * The strain tensor with all 4 components allowed to be non-zero. Note symmetry is not assumed.
     * 
     * \f[
     *   \gamma = \left(
     *         \begin{array}{cc}
     *              \gamma_xx & \gamma_xy \\
     *              \gamma_yx & \gamma_yy 
     *          \end{array}
     *    \right)
     * \f]
     */
    virtual std::array< std::array<double,2>, 2 > strainTensor() const override { return _strainTensor; }

    /*
     * @brief Map of all normalised streses (assuming symmetry) extracted from the given strain.
     *
     * Returns an ordered `std::map` with three labels; `sigma_xx`, `sigma_xy` and `sigma_yy`, which are the corresponding
	 * components of the stress tensor divided by the oscillatory factor, *i.e.*
	 *
	 * \f[
	 *   sigma_{xx} = \frac{\sigma_{xx}}{e^{i\omega t}}
	 * \f]
	 *
	 * with similar expressions for `sigma_xy` snd `sigma_yy`.
	 *
	 * This means that, if \f$\gamma_{xx}=\gamma_{yy}=0\f$ and \f$\gamma_{xy}=1\f$, then the returned value
	 * of `sigma_xy` is just the standard shear modulus.
     */
    virtual moduliMap networkModuli( tensor2D sigma_ij ) const override;

    /**
     * @brief Returns an ordered `std::map` with the same labels as `networkModuli()`.
	 * 
	 * Returns an ordered `std::map` with the same labels as `networkModuli()` for parsing, but only
	 * the shear term `sigma_xy` has a non-zero value (equal to the viscosity); `sigma_xx` and `sigma_yy` are always
	 * returned as zero.
	 *
	 * If the strain applied is simple or extension shear, i.e. volume-preserving, it is better to use the
	 * corresponding specialised classes, for which the correct fluid terms are returned.
     */
    virtual moduliMap fluidModuli( double omega, double viscosity ) const override;
};



#endif


