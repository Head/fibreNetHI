/**
 * @file drivingPressureGdt.h
 * @author David Head
 * @brief Pure pressure gradient, in the absence of strain.
 */


#ifndef FIBRE_NETWORK_HI_DRIVING_PRESSURE_GDT_H
#define FIBRE_NETWORK_HI_DRIVING_PRESSURE_GDT_H


//
// Includes.
//

// Local includes.
#include "XMLParameters.h"

// Parent class.
#include "drivingBase.h"


/**
 * @brief Pressure gradient only, with no concurrent strain.
 */
class drivingPressureGdt : public drivingBase
{

private:

protected:

	/// Mean pressure gradients as a complex 2-array; typically purely imaginary.
	std::array<std::complex<double>,2> _pGdt;

public:

    // Constructors / destructor.
    drivingPressureGdt( double X, double Y, const XMLParameters *params, ostream *log );
    virtual ~drivingPressureGdt() {};

    /// Textual representation of the driving type.
    virtual std::string typeLabel() const override { return "drivingPressureGdt"; }

    /**
     * @brief The pressure gradient in x and y-directions as a complex 2-vector, normally purely imaginary.
     */
    virtual std::array<std::complex<double>,2> pressureGradient() const override { return _pGdt; }
};



#endif
