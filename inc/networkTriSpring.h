/**
 * @file networkTriSpring.h
 * @author David Head
 * @brief Triangular lattice, with equilateral triangles (so coarse-grains to an isotropic solid), possibly disordered.
 * 
 * Typical parameters for this class would look like:
 *	<Parameter networkType   ="triangular spring" />
 *	<Parameter latticeSpacing="1.0"  />                     <!-- Will fail if not commensurate with box dimensions -->
 *	<Parameter dilutionParam ="0.8"  />                     <!-- Bond dilution parameter -->
 *	<Parameter springConstant="1.0"  />                     <!-- Spring constant k -->
 *
 * To pre-stress the initial network, e.g.:
 *	<Parameter natLenWidth="0.02" />             			<!-- Standard deviation of the Gaussian -->
 *  <Parameter natLenMean="0.9" />							<!-- Mean natural length; defaults to lattice spacing -->
 *	<Parameter preStressSolver="Newton-Sparse Direct" />	<!-- Algorithm to be used to find the initial pre-stressed state  -->
 *	<Parameter preStressTolerance="1e-4" />
 *
 * To 'jiggle' nodes (to remove colinear bonds) by applying Gaussian displacemenst to each node's
 * x and y coordinates (i.i.d.r.v.), and then set the natural spring lengths to whatever they are after
 * this 'jiggling'. The amplitude (standard deviation) of the displacements are set as follows:
 * 
 *  <Parameter jiggleAmplitude="0.05" />
 * 
 * If both pre-stress and 'jiggling' are selected, then the width of prestress will be applied to the new natural
 * spring lengths after 'jiggling', and if a prestress natural length is specified, it will overwrite the natural
 * lengths calculated after the 'jiggling'. Note this means that if 'jiggling' is non-zero but the mean length is
 * specified as the lattice spacing, then the non-linear solver will restore the unfirom lattice (up to tolerance).
 * 
 * Also, really need to think of a more technical-sounding word than 'jiggling' ...
 */


#ifndef FIBRE_NETWORK_HI_NETWORK_TRI_SPRING_H
#define FIBRE_NETWORK_HI_NETWORK_TRI_SPRING_H


//
// Includes
//

// Standard includes
#include <string>
#include <cmath>
#include <complex>
#include <array>
#include <vector>
#include <atomic>
#include <sstream>
#include <forward_list>
#include <algorithm>
#include <chrono>
#include <list>

// Local includes
#include "XMLParameters.h"
#include "smallSquareMatrix.h"
#include "minFIRE.h"
#include "pebbleGame.h"

// Project includes
#include "networkBase.h"				// The parent (base) class
#include "pRNG.h"						// In pRNG:: namespace
#include "matrixTriSpringDrag.h"
#include "matrixTriSpringStokes.h"
#include "percPerBC.h"					// Algorithm of Livraghi et al. (2021).



//
// Parameters
//
namespace parameterParsing
{
	// Basic parameters. Only the dilution is optional (defaults to 1.0, i.e. no dilution).
	const std::string netTS_latticeSpacing  = "latticeSpacing";
	const std::string netTS_springConstant  = "springConstant";

	// Bond dilution. Must define (i) dilutionParam only, (ii) all 3 orientational dilutions, or (iii) none at all.
	const std::string netTS_dilutionParam  = "dilutionParam" ;		///< Isotropic dilution, usually \f$p\f$ in literature.
	const std::string netTS_dilution_E_W   = "dilution_E_W"  ;		///< Dilution for E-W (horizontal) springs.
	const std::string netTS_dilution_NW_SE = "dilution_NW_SE";		///< Dilution for NW-SE (up to left/down to right) springs.
	const std::string netTS_dilution_NE_SW = "dilution_NE_SW";		///< Dilution for NE-SW (up to right/down to left) springs.
	
	// These parameters will initiate a non-linear, network-only relaxation prior to applying the oscillatory strain
	// with the fluid (in some form) included.
	const std::string netTS_natLenWidth = "natLenWidth";	///< Width of natural length distriubtion. Defaults to 0.0.
	const std::string netTS_natLenMean  = "natLenMean" ;	///< Mean of natural length distribution. Defaults to the lattice spacing.

	// If any pre-stress is requested, also need to specify the solver and any parameters it requires.
	const std::string netTS_preStressSolver_typeAttr           = "preStressSolver"     ;	///< Specifier for the solver type.
	const std::string netTS_preStressSolver_tolerance          = "preStressTolerance"  ;	///< Tolerance for the non-linear solver.
	const std::string netTS_preStressSolver_NewtonSparseDirect = "Newton-Sparse Direct";	///< SuperLU in a Newton iteration.
	const std::string netTS_preStressSolver_FIRE               = "FIRE"                ;	///< Fast Inertial Relaxation Algorithm.
	const std::string netTS_preStressSolver_hybrid             = "hybrid"              ;	///< FIRE followed by Newton.
	const std::string netTS_preStressSolver_hybridTol          = "preStressHybridTol"  ;	///< When to switch solvers.

	// Can 'jiggle' initial node positions (re-setting natural lengths) to remove colinear bonds.
	const std::string netTS_jiggleAmplitude = "jiggleAmplitude";		///< Std. dev. of Gaussian applied in x and y directions.

	// Can use the pebble game algorithm to modify the network.
	const std::string netTS_pgRemoveRedundant    = "removeRedundantBonds";	///< True/False to remove redundant (=overconstrained) bonds.
	const std::string netTS_pgOnlyLargestCluster = "onlyLargestCluster"  ;	///< True/False to only retain the largest rigid cluster.

	// Can use the connectivity percolation detection of Livraghi et al., J. Chem. Theory Comput. 17(10) (2021).
	const std::string netTS_cpConnPercDimToLog = "connPercDimToLog";	///< True/False to output connectivity percolation dimension to the log file.
}


/**
 * @brief Triangular lattice, with equilateral triangles (so coarse-grains to an isotropic solid), possibly disordered.
 */
class networkTriSpring : public networkBase
{

private:

	//
	// Network construction.
	//

	/// Fills the tangent vector given the current node positions.
	virtual void __fillTangent( unsigned int index, int i1, int j1, int i2, int j2 );

	/// Fills the tangent vector given the current node positions.
	virtual void __fillTangent( unsigned int index, int n1, int n2 );

	/// Set tangent vectors and Lees-Edwards shifts using current node coordinates, assuming current connectivity.
	virtual void __setTgtsAndShifts();

	/// Removes the spring between the given nodes, and resets _connections and _l0 but *not* the tangents or shifts.
	virtual void __removeBond( int n1, int n2 );

	//
	// Energies, forces and the Hessian.
	//

	/// The net vector force on the given node.
	virtual std::array<double,2> __netForceOnNode( int n ) const;

	/// Energy "due to" a single node, here taken to be the sum of energies of all connected springs.
	virtual double __nodeEnergy( int n ) const;

	/// Gradient of the spring tension, i.e. the stiffness. Must match the full expression in __springTension().
	virtual double __springStiffness( int n1, short k ) const;

	/// The pre-tension in a single spring. Signed to be positive when the spring extension is positive.
	virtual double __springTension( int n, short k ) const;

	/// Returns the local Hessian for a single spring as a 2x2 smallSquareMatrix<>.
	smallSquareMatrix<2,double> __springHessian( unsigned int n, unsigned short k ) const;

	/// Sets the node position to the given 2-vector, wrapped to the primary cell.
	/// Also updates the tangent vectors but not the Lees-Edwards shifts.
	virtual void __setNodePosition( int n, std::array<double,2> new_x );

	/// Increments the node position. Also updates the tangent vectors.
	virtual void __incrementNodePosition( int n, std::array<double,2> dx );

	/// Fills the passed force vector with net forces on the reduced set of nodes.
	virtual void __fillForceVector_reduced( double *F ) const;

	//
	// Prestressed networks.
	//

	/// Solve for initial state with pre-stress using paramaters specified.
	virtual void __relaxPreStress();

	/// FIRE. Returns number of iterations taken, or negative for error. Optional tolerance defaults to the final tolerance.
	int __preStress_FIRE( double tol=0.0 );

	/// Newton's method, with sparse direct solver for the linear solver. Returns number of iterations taken, or negative for error.
	int __preStress_NewtonSparseDirect();

	/// Enumerated type for the algorithm to solve for a pre-stressed initial condition.
	enum class __preStressSolverType { noneRequired, FIRE, NewtonSparseDirect, hybrid };
	__preStressSolverType __preStress_solver;			///< Algorithm for prestressed state; enumerated class.

	double __preStress_natLenWidth;						///< Width of natural spring length distribution.
	double __preStress_natLenMean;						///< Mean of natural spring length distribution.
	double __preStress_tol;								///< Tolerance for the pre-stress non-linear solver.
	double __preStress_hybridTol;						///< For the hybrid solver, when to switch.

	//
	// Removal of colinear bonds by 'jiggling' network nodes.
	//
	double __jiggleAmplitude;
	virtual void __jiggleNodes();

	//
	// Parameters, objects and methods related to the pebble game method.
	//
	std::unique_ptr<pebbleGame> __pebbleGame;
	bool __pgRemoveRedundantBonds;
	bool __pgOnlyLargestCluster;

	/// Remove bonds after application of the pebble game algorithm, if selected as an option.
	void __applyPebbleGame();

	//
	// Parameters and methods related to the connectivity percolation algorithm of Livraghi et al. (2021).
	//
	bool __cpConnPercDimToLog;

	/// Calculates connectivity percolation dimension usign the algorithm of Livraghi et al. (2021).
	void __connectivityPercolation();

protected:

	// Commonly used constants.
	constexpr static double _root3over2      = 0.8660254037844386;
	constexpr static double _commensurateTol = 1e-3;						///< Absolute tolerance for checking lattice-box dimensions
	constexpr static double _divergence      = 1e10;						///< Used when checking for numerical divergence

	// Physical parameters
	double _a;										///< The default natural spring length = the lattice spacing.
	double _k;										///< The spring constant, assumed independent of frequency and uniform

	double _p_E_W;									///< Bond dilution for East-West (horizontal) springs.
	double _p_NW_SE;								///< Bond dilution for NW-SE (up to left/down to right) springs.
	double _p_NE_SW;								///< Bond dilution for NE-SW (up to right/down to left) springs.

	// The numbers of nodes in each direction.
	unsigned int _numX;
	unsigned int _numY;
	unsigned int _numNodes;							///< Total number of nodes, including completed disconnected ones.
	unsigned int _totalNumConnections;				///< Total number of springs; set at end of network initialisation.
	unsigned int _numReducedNodes;					///< Total number of reduced nodes, after removal of disconnected ones.

	/// Undeformed node positions (i.e. prior to adding _disps) in format (x0,y0,x1,y1,...).
	std::vector<double> _nodePositions;

	/// The displacement amplitudes for each node, i.e. current node positions are _nodePositions plus _disps.
	std::vector< std::complex<double> >  _disps;

	/// The natural lengths for every spring.
	std::vector<double> _l0;
	
	/// The tangent vectors between all potential pairs of nodes; precalculated.
	std::vector<double> _unitTangents;
	
	/// Connectivity. Maps global node indices to global node indices, -1 meaning no more connections.
	std::vector<int> _connections;

	/// Number of connections indexed by node, as even a short loop can add up to a significant run time.
	std::vector<int> _numConnections;
	
	/// The shifts for Lees-Edwards-like BCs for the given connection; same indexing as _connections. Real only.
	std::vector< std::array<double,2> > _perBC_dispShifts;
	
	// The x- and y-coordinates of the node position (in the primary cell) for the underlying regular lattice.
	virtual double _latticePos_x( int i, int j ) const noexcept;
	virtual double _latticePos_y( int i, int j ) const noexcept;
	virtual double _latticePos_x( unsigned int n ) const { int i,j; _splitGlobal(n,&i,&j); return _latticePos_x(i,j); }
	virtual double _latticePos_y( unsigned int n ) const { int i,j; _splitGlobal(n,&i,&j); return _latticePos_y(i,j); }

	/// The periodically-wrapped positions in the primary cell, changed in-place.
	virtual void _wrapToPrimaryCell( double &x, double &y ) const noexcept;

	/// The periodically-wrapped position vector from n1 to n2.
	virtual std::array<double,2> _nodeSeparation( unsigned int n1, unsigned int n2 ) const;

	// The global node index given x and y indices; includes periodic wrapping. Also the inverse.
	virtual unsigned int _globalIndex( int x, int y ) const noexcept;
	inline void _splitGlobal( unsigned int n, int *i, int *j ) const { *i = n % _numX; *j = n / _numX; }

	// Lattice initialisation.
	virtual void _initLatticeDimensions();									///< Sets _numX and _numY. Call before _initLattice().
	virtual void _initLattice          ();									///< Initialises arrays and calls _initSprings().
	virtual void _initSprings          ();									///< Adds all of the springs.

	/// Attempts to make a link between the given pairs of local indices; can also pass the dilution parameter.
	virtual void _trialConnection( int i1, int j1, int i2, int j2, double dilution=1.0 );

	/// The sum of r_{i} f_{j}; essentially the stress tensor prior to normalising to the volume.
	virtual std::array< std::array< std::complex<double>, 2 >, 2 > _sum_ri_fj() const override;

	// Routines related to connections once initialised.
	virtual bool                 _hasConnection  ( unsigned int n1, unsigned int n2 ) const;	///< Is there a spring between these two?
	virtual std::array<double,2> _tangentVector  ( unsigned int n1, unsigned int n2 ) const;	///< From first index to second. Zeros if none.
	virtual std::array<double,2> _meanNodePosn   () const;										///< Mean node position.
	virtual double               _maxForceMagSqrd() const;										///< Max. non-linear node imbalance.

	/// Fluid velocity for the given node, identified by its global index.
	inline std::array<std::complex<double>,2> _fluidVelocityAtNode( unsigned int n ) const
		{ return _fluidVelocityAt( _nodePositions[2*n], _nodePositions[2*n+1] ); }

	/// The base network spands the system box in box directions.
	virtual double _volume() const override { return _X * _Y; }

	/// String label for class type.
	virtual std::string _typeLabel() const noexcept override { return "networkTriSpring"; }

	/// Reduced set of nodes to solve for, based on some rationale (i.e. no completely disconnected nodes).
	void _generateReducedNodeMap() override;

	/**
	 * @brief Returns the local Hessian for all springs connected to a single node.
	 * 
	 * @param n Node for which the Hessian is to be calculated.
	 */
	smallSquareMatrix<2,double> _localHessian( unsigned int n ) const;


public:
	networkTriSpring() {}
	networkTriSpring( double X, double Y, const XMLParameters *params, ostream *log, const drivingBase *driving );
	virtual ~networkTriSpring() {}

	//
	// Initialisation
	//
	virtual void initialise() override;

	//
	// Solution routines
	//
	virtual std::array<double,2> realAffineOffset() override;
	virtual void offsetDisps( std::array< std::complex<double>, 2 > uOffset ) override;

	//
	// Assembling the matrix by terms. Templated when they can be real or complex.
	//
	template< typename T >
	void matrix_fillElasticTerms( matrixNetwork<T> *M, bool periodicShift=true ) const;

	template< typename T >
	void matrix_copyDisps( const T *sol );

	void matrix_fillPressGdtBodyForces( matrixNetwork< std::complex<double> > *M ) const;

	//
	// Constructs a reduced set of nodes for solvers.
	//
	virtual unsigned int numReducedNodes()  const noexcept override { return _numReducedNodes; };	///< Size of set of reduced nodes.
	virtual const std::vector<int>* reducedNodeMap() override { return &_reducedNodeMap; }			///< Reindexes nodes for solvers.

	//
	// Analysis
	//
	virtual unsigned int   numNodes()                  const noexcept override { return _numNodes; }
	virtual unsigned int   numConnections()            const noexcept override { return _totalNumConnections; }
	virtual unsigned int   maxConnections()            const noexcept override { return 3*_numNodes; }	// 3 per node w/o double counting.
	virtual unsigned short connectionsForNode( int n ) const          override { return _numConnections[n]; }

	/**
	 * @brief Returns the mean coordination number of the network.
	 * 
	 * Returns the mean number of springs connected to a node for the current network.
	 * Nodes with no connected springs are not included in the average.
	 * 
	 * @return Mean coordination number.
	 */
	virtual double coordinationNumber() const override;

	/**
	 * @brief Returns the mean displacement of all (non-reduced) nodes.
	 * 
	 * Returns the mean complex displacement from the most recent solve, skipping
	 * nodes that do not belong to the reduced node set.
	 * 
	 * @return Mean complex displacement.
	 */
	virtual std::array< std::complex<double>, 2 > meanDisplacement() const override;

	/**
	 * @brief Returns a scalar measure of network non-affinity that is zero for affine response, 
	 * and increases unboundedly for non-affine solutions.
	 * 
	 * Uses the most recent solve to evaluate
	 * \f[
	 *   \frac{1}{N a^{2}}\sum_{\alpha}\left(
	 *     \left|u_{x}^{\alpha}-u^{\rm aff}_{x}\right|^{2}
	 *     +
	 *     \left|u_{y}^{\alpha}-u^{\rm aff}_{y}\right|^{2}
	 *   \right)
	 * \f]
	 * with \f$a\f$ the lattice spacing and \f$N\f$ is the number of nodes.
	 * Nodes that are completely disconnected (*i.e.* have no springs) are excluded from the average.
	 * 
	 * Note that the origin should be offset to minimise this measure; see `realAffineOffset()`.
	 * 
	 * @return Network non-affinity metric.
	 * @see realAffineOffset()
	 */
	virtual double nonAffinity() const override;

	/**
	 * @brief Returns read-only access to the vector of ynode displacements from the most recent solve,
	 * ordered as (x1,y1,x2,y2,...).
	 **/
	virtual const std::vector<double> nodePositions() const override { return _nodePositions; }

	/**
	 * @brief Returns read-only access to the vector of ynode displacements from the most recent solve,
	 * ordered as (dx11,dy1,dx2,dy2,...)
	 **/
	inline const std::complex<double>* nodeDisplacements() const override { return _disps.data(); }

	/**
	 * @brief Returns moments of the force (i.e. spring tension) distribution.
	 * 
	 * Calculates the moments of the distribution of spring tensions starting from 1, and up to and
	 * including the maximum moment specified. Does not calculate the zero'th moment as this requires
	 * thresholding - to determine the extent of the backbone cluster, it is instead recommended to
	 * use the pebble game method.
	 * 
	 * Note it is the magnitude of tensions that are measured, and the real and imaginary components
	 * are output separately, i.e.
	 * \f[
	 * 	 {\rm fn\_real} = \frac{1}{N}\sum_{\alpha\beta}\left|
	 *     \Re{(f^{\alpha\beta})}
	 *   \right|^{n}
     * \f]
	 * \f[
	 * 	 {\rm fn\_imag} = \frac{1}{N}\sum_{\alpha\beta}\left|
	 *     \Im{(f^{\alpha\beta})}
	 *   \right|^{n}
     * \f]
	 * 
	 * @param maxMoments Outputs moments from 1 up to maxMoments inclusive.
	 * @return Vector of complex moments, of size maxMoments.
	 */
	virtual std::vector< std::complex<double> > forceMoments( int maxMoment ) const override;

	//
	// I/O
	//
	virtual void saveSolution( ostream &out ) const override;

	//
	// Debugging.
	//
	virtual void   checkInitialNetwork() const;					///< Checks network connectivity and consistency.
	virtual void   debugNodeReindexer () const;					///< Checks the mapping from network to matrix node indices.

	/**
	 * @brief The residual for the network node equations.
	 * 
	 * Returns the maximum magnitude of force imbalance of all nodes, given the
	 * drag force between the node and the fluid velocity at the node's location,
	 * as inferred from _fluidVelocityAt(...) and the passes drag coefficient $\zeta$.
	 * 
	 * @param zeta Drag coefficient $\zeta$.
	 * @return Max. residual over all nodes.
	 */
	virtual double debugNodeResidual( double zeta ) const override;
};


#pragma mark -
#pragma mark Templated methods implemented in header
#pragma mark -

// Copy the matrix solution vector back over to the internal vector, given the indexing map.
template< typename T >
void networkTriSpring::matrix_copyDisps( const T *sol )
{
	int m, M = numReducedNodes();

	// Matrix was in block format (ux,ux,ux,...,uy,uy,uy,...), with some nodes skipped.
	for( auto n=0u; n<_numNodes; n++ )
		if( (m=_reducedNodeMap[n])!=-1 )
		{
			_disps[2*n  ] = sol[m  ];
			_disps[2*n+1] = sol[m+M];
		}
}



#endif
