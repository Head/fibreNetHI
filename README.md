## Overview

Code to predict the linear viscoelastic response of a fibre network immersed into a solvent.
Based on the two-fluid model [MacKintosh & Levine,  Phys. Rev. Lett. 100, 018104 (2008)],
with the continuum "solid" replaced by a discrete fibre network.

Requires a makefile, but currently these are manually configured (they are actually quite basic).
A few examples are provided in the `makefiles` directory - hopefully it will be straightforward
to modify one of these to any system.

There is also a range of supporting Python scripts under `pyFibreNetHI`. These include codes both
to run batch jobs, and to visualise the output. For instance, from the root directory, 

`python3 pyFibreNetHI/runControl/runShearTriSpring.py test "range(5)" -b 1e-4 -e 1e0 -f 2 --launch=sh -X 32 -p 0.8`

will generate 5 separate runs called `test/run0`, `test/run1` *etc.*, and start them running from the shell.
The shear modulus can then be viewed using

`python3 pyFibreNetHI/plot.py test`

For options, call with `-h`, *e.g.*

`python3 pyFibreNetHI/run.py -h`

Some more complex run control classes - such as `runAllModuli` - also have specialised data analysis modules
in the `dataAnalysis` directory.

