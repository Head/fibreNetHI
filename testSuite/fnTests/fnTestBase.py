#
# Base class for fibreNetHI tests.
#


#
# Standard imports
#
import os
import glob


# Should be on $PYTHONPATH; if not, consider using sys.path.append() for a local fix.
from pyFibreNetHI.dataAnalysis import dataBase


# Failed test exceptions
class FailedTestError( AssertionError ):
	"One of the fibreNetHI tests failed"				# Just adds a doc string.

#
# Base class of the fibreNetHI test suite.
#
class fnTestBase( object ):

	#
	# Constructor; pass executable pathname (required), and optional flags for verbosity and whether or not to plot.
	#
	def __init__( self, executable, cmdLineArgs="", verbose=False, plot=False ):

		# Store parameters
		self._verbose = verbose
		self._plot    = plot

		# Where to find the executable.
		self._executable = executable

		#
		# Set up the test.
		#
		self.setUpForTest()

		#
		# Perform the test; executes "biofilm" with "-q" [quiet mode] if verbosity is off; all other arguments need to be supplied
		#
		cmdString = "{0} {1}".format(self._executable,cmdLineArgs)						# Executable name plus command line arguments.

		# These options provided by child class; 'None' by default. Note there is no validity check here.
		paramsFile = self._parameterFile()
		if self._parameterFile() != None:
			cmdString += " -p {}".format( self._parameterFile() )
		else:
			paramsFile = "parameters.xml"			# Also the default name for the C++ code.

		self._shellCmd( cmdString )

		#
		# Get data objects. Use the dataBase class on pyFibreNetHI.dataAnalysis.
		#
		self._table = dataBase.dataBase( ".",  verbose=self._verbose, paramsFileName=paramsFile )	# Table(s) output by the test run(s).

		#
		# Perform the tests
		#
		self.performTests()

		#
		# Clean up (class method, so it can be called purely to clean-up)
		#
		self.removeAuxiliaryFiles(self._verbose)

	#
	# Test the residuals in the log file.
	#
	def testResiduals( self ):

		if self._verbose:
			print( " - trying to open and read the log file to check residuals." )

		try:
			with open( self.logFileName() ) as file:
				for line in file:

					# Get all lines that mention "residual". May need updating in future.
					if line.casefold().find( "residual".casefold() ) != -1:

						# Get the residual from the line and convert to a float.
						residual = float( line.strip().split()[-1] )
						if self._verbose:
							print( " - checking residual {} [full line: '{}']".format(residual,line.strip().replace("\t"," ")) )

						# Check against the tolerance.
						tol = self.tolResidual()
						if residual > tol:
							raise FailedTestError( "at least one residual exceeded the tolerance" )
						else:
							if self._verbose:
								print( " - within tolerance of {}.".format(tol) )

		except:
			raise SystemError( "could not open and parse the log file to check residuals" )


	#
	# Execute shell command directly; will echo if in verbose mode.
	#
	def _shellCmd( self, cmd ):
		if self._verbose: print( "Executing shell command:\t'{}'".format(cmd) )
		return os.system( cmd )

	#
	# Overriden by derived classes, some optionally.
	#

	# Pre/post test options; some clean-up operations are provided given a list of files to delete.
	def setUpForTest( self ):
		self.removeAuxiliaryFiles()

	# Cleaning-up is performed by class methods that can be called by the script without instancing any test objects.
	@classmethod
	def removeAuxiliaryFiles( cls, verbose=False ):				# If overriden, should call parent method.

		if cls.filesToDelete() != None:
			if verbose: print( "Removing files: {}".format(cls.filesToDelete()) )
			for file in cls.filesToDelete():
				for matchedFile in glob.glob(file):
					os.remove( matchedFile )

	@classmethod
	def filesToDelete(cls): return None		# Will delete listed files after the test; can use wildcards.

	# Arguments for the executable code; None for no argument.
	def _parameterFile( self ): return None

	# Log file name.
	@classmethod
	def logFileName( cls ):
		return "log.out"
	
	# Tolerance for residuals.
	@classmethod
	def tolResidual( cls ):
		return 1e-10

	#
	# Class method to perform the test; must be overridden
	#
	@classmethod
	def performTest( cls, executable, verbosity, plot ):
		raise NotImplementedError
