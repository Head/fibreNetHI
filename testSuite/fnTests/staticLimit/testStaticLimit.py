#
# Tests for the affine limit at zero frequency; should invoke the optimised solver with only real parts of network d.o.f. 
#


# Standard imports
import math
import matplotlib.pyplot as plt
import numpy as np


# Base class
from fnTests import fnTestBase

class testStaticLimit( fnTestBase.fnTestBase ):

	def __init__( self, executable, cmdLineArgs, verbosity, plot, fname ):

		# Store private variables. Initialise before calling parent constructor (which needs these).
		self.__paramsFile = fname

		# Call parent method with required flags.
		super(testStaticLimit,self).__init__(executable,cmdLineArgs,verbosity,plot)


	# Test method.
	def performTests( self ):
	
		#
		# Sanity checks.
		#
		if self._table.numRows() != 1:
			raise fnTestBase.FailedTestError( "Expected single table row output but found {}.".format(self._table.numRows()) )

		if self._table.means("w")[0] != 0:
			raise NotImplementedError( "Driving frequency must be zero." )

		if self._table.p != 1.0:
			raise NotImplementedError( "Dilution parameter p must be 1." )

		# Parameters for this run.
		k = self._table.k

		#
		# Check 0: Residual in log files.
		#
		self.testResiduals()

		#
		# Check 1: G'(w) should matches the prediction.
		#

		# Label of the required quantity from the table.
		if self._table.driving == "simple shear"     : label = "G'_net"
		if self._table.driving == "extensional shear": label = "G'_net"
		if self._table.driving == "arbitrary strain" : label = "sigma_xy'_net"	# Also G, since \gamma_{xy}+\gamma_{yx}=1.

		# Extract G' from the table.
		Gprime = self._table.means(label)[0]

		# Prediction depends on the strain.
		pred = None

		if self._table.driving == "simple shear"     : pred = (math.sqrt(3)/4) * k
		if self._table.driving == "extensional shear": pred = (2/math.sqrt(3)) * k
		if self._table.driving == "arbitrary strain" : pred = (math.sqrt(3)/4) * k

		if pred==None:
			raise fnTestBase.FailedTestError("Shear type not recognised when testing for affinity.")

		# Tolerance depends on driving, as the expression for extensional shear is only valid for an infinite system.
		tol = ( self.tolGPrime_simpleShear() if self._table.driving=="simple shear" else self.tolGPrime_extensionalShear() )

		# Verbose output.
		if self._verbose:
			print( f"Measured network contribution {label}={Gprime} for k={k}." )
			print( " - the prediction is {}.".format(pred) )

		# Only single points so hardly worth a plot.
		if self._plot:
			print( " - (nothing to plot for this test)" )

		# Check and any verbose output.
		if abs(Gprime-pred) > tol:
			raise fnTestBase.FailedTestError( "G^{{\prime}}={0} significantly different from prediction {1}.".format(Gprime,pred) )
		else:
			if self._verbose:
				print( " - agrees with prediction to within tolerance." )

		#
		# Check 2: The non-affinity is low for simple shear; extensional shear not affine in this limit!
		#
		if self._table.driving in ("simple shear","arbitrary strain"):

			netNA = self._table.means("NA_net")[0]		# The prediction is zero in all cases.
			if self._verbose:
				print( " - measured network non-affinity for the same system is {0}.".format(netNA) )

			# No HI (even in fluid type was "Stokes"), so should be very close to zero.
			tol = self.tolNA()

			if abs(netNA) > tol:
				raise fnTestBase.FailedTestError( "Network non-affinity {0} significantly different from zero.".format(netNA) )
			else:
				if self._verbose:
					print( " - network non-affinity metric close enough to zero using tolerance of {0}.".format(tol) )
		else:
			if self._verbose:
				print( " - not checking NA as the low-frequency limit not affine for this shear." )

	# Arguments for the executable.
	def _parameterFile( self ):
		return self.__paramsFile


	#
	# Class methods
	#

	# Tolerances for checks.
	@classmethod
	def tolGPrime_simpleShear( cls ):
		return 1e-6
	
	@classmethod
	def tolGPrime_extensionalShear( cls ):
		return 1e-2

	@classmethod
	def tolNA( cls ):
		return 1e-10
	
	# Cleanup; can use wildcards.
	@classmethod
	def filesToDelete( cls ): return ["table.dat","log.out"]

	@classmethod
	def performTest( cls, executable, verbosity, plot ):

		for testNum in range(3):
			fname = "params_test{}.xml".format(testNum)
			print( "Performing static limit test with parameters '{}'".format(fname) )

			testStaticLimit(executable,"--static",verbosity,plot,fname)

			print( " - passed" )
