#
# Tests for driving purely by a pressure gradient.
#


# Standard imports
from math import *
import matplotlib.pyplot as plt


# Base class
from fnTests import fnTestBase

class testPressureGdt( fnTestBase.fnTestBase ):

	def __init__( self, executable, cmdLineArgs, verbosity, plot, fname ):

		# Store private variables. Initialise before calling parent constructor (which needs these).
		self.__paramsFile = fname

		# Call parent method with required flags.
		super(testPressureGdt,self).__init__(executable,cmdLineArgs,verbosity,plot)


	# Test method.
	def performTests( self ):
	
		#
		# Sanity checks.
		#
		if self._table.numRows() != 1:
			raise fnTestBase.FailedTestError( "Expected single table row output but found {}.".format(self._table.numRows()) )

		if abs(self._table.p - 1.0 ) > 1e-5:
			raise NotImplementedError( "Must be no dilution / dilution parameter = 1." )

		if self._table.driving != "pressure gradient":
			raise NotImplementedError( "Driving type must be 'pressure gradient'." )

		if self._table.fluidType != "Stokes":
			raise NotImplementedError( "Fluid type must be 'Stokes'." )

		# Parameters for this run.
		a    = self._table.a
		zeta = self._table.zeta

		# If no pressure gradient specified, take as zero.
		P_x, P_y = 0.0, 0.0

		try:
			P_x  = self._table.P_x
		except AttributeError:
			pass

		try:
			P_y  = self._table.P_y
		except AttributeError:
			pass

		# Final sanity check: one must be non-zero.
		if P_x==0.0 and P_y==0.0:
			raise NotImplementedError( "At least one pressure gradient, P_x or P_y, must be non-zero." )

		#
		# Check 0: Residual in log files.
		#
		self.testResiduals()

		#
		# Check measured q_{x}/q_{y} against prediction.
		#

		# Extract q_x and q_y from the table.
		q_x, q_y = [ self._table.means(label)[0] for label in ("q_x","q_y") ]

		# Predictions.
		pred_qx = P_x * sqrt(3)*a**2 / (2*zeta)
		pred_qy = P_y * sqrt(3)*a**2 / (2*zeta)

		# Verbose output.
		if self._verbose:
			print( "Measured relative velocity between fluid and network was q=({0},{1}).".format(q_x,q_y) )
			print( " - prediction is ({0},{1}).".format(pred_qx,pred_qy) )

		# Only single points so hardly worth a plot.
		if self._plot:
			print( " - (nothing to plot for this test)" )

		# Check and any verbose output.
		if abs(q_x-pred_qx) > testPressureGdt.tolRelVel():
			raise fnTestBase.FailedTestError( "q_x={0} significantly different from the prediction {1}.".format(q_x,pred_qx) )
		else:
			if self._verbose:
				print( " - q_x agrees with prediction to within tolerance." )

		if abs(q_y-pred_qy) > testPressureGdt.tolRelVel():
			raise fnTestBase.FailedTestError( "q_y={0} significantly different from the prediction {1}.".format(q_y,pred_qy) )
		else:
			if self._verbose:
				print( " - q_y agrees with prediction to within tolerance." )


	# Arguments for the executable.
	def _parameterFile( self ):
		return self.__paramsFile

	#
	# Class methods
	#

	# Tolerance for the relative velocities.
	@classmethod
	def tolRelVel( cls ):
		return 1e-2
	
	# Tolerance for residuals. Higher than usual because of the large value of \omega for these tests.
	@classmethod
	def tolResidual( cls ):
		return 1e-4
	
	# Cleanup; can use wildcards.
	@classmethod
	def filesToDelete( cls ): return ["table.dat","log.out"]

	@classmethod
	def performTest( cls, executable, verbosity, plot ):

		for testNum in range(2):
			fname = "params_test{}.xml".format(testNum)
			print( "Performing pressure gradient test with parameters '{}'".format(fname) )

			testPressureGdt(executable,"-w 1",verbosity,plot,fname)

			print( " - passed" )
