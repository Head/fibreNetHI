#
# Tests for correlated lattices.
#


# Standard imports
import math
import matplotlib.pyplot as plt
import numpy as np


# Base class
from fnTests import fnTestBase

class testCorrnLattice( fnTestBase.fnTestBase ):

	def __init__( self, executable, cmdLineArgs, verbosity, plot, fname ):

		# Store private variables. Initialise before calling parent constructor.
		self.__paramsFile = fname

		# Call parent method with required flags.
		super(testCorrnLattice,self).__init__(executable,cmdLineArgs,verbosity,plot)


	# Test method.
	def performTests( self ):
	
		#
		# Sanity checks.
		#
		if self._table.numRows() != 1:
			raise fnTestBase.FailedTestError( "Expected single table row output but found {}.".format(self._table.numRows()) )

		# Parameters for this run.
		w = self._table.means("w")[0]

		a    = self._table.a
		numX = round( self._table.X / a )
		numY = numX if self._table.Y=="triangle" else round( self._table.Y/(a*3**0.5/2) )

		c = self._table.correlationStrength		# Parameters specific to the correlated lattice class.
		f = self._table.siteOccupation

		#
		# Check 0: Residual in log files. Always do this as it is easy and quick.
		#
		self.testResiduals()

		#
		# Check for case c=0: Uncorrelated limit, so can estimate the number of springs which is in the log file.
		#
		if c==0.0:

			# Predicted number of springs based on the assumption of no correlations (c=0).
			predNumSprings = round( (numX*numY) * 3 * f**2 )		# 3 springs per bond, but need both sites to be occupied.
			numConnections = None									# Need to find from the log file.

			# Message for verbose output.
			if self._verbose:
				print( " - for the uncorrelated limit (c=0), testing that the number of springs/connections is close to the prediction {}.".format(predNumSprings) )
	
			# Scrape the number of springs/connections from the log file.
			with open( self.logFileName() ) as file:
				for line in file:

					# Get the line that has the specific text.
					if line.casefold().find( "connections, from a maximum".casefold() ) != -1:

						# Assume the number of connections is the second part of the line, after the '-'.
						numConnections = int( line.split()[1] )

			# Error message if could not find the number of connections.
			if not numConnections:
				raise fnTestBase.FailedTestError( "Could not extract number of connections/springs from the log file; has the log file format changed?" )

			# Output the parsed value.
			if self._verbose:
				print( " - extracted the actual number of connections to be {}.".format(numConnections) )

			# Only single points so hardly worth a plot.
			if self._plot:
				print( " - (nothing to plot for this test)" )

			# Get the relative error and compare to the allowed tolerance.
			relErr = abs( predNumSprings / numConnections - 1.0 )

			if relErr > self.tolNumSprings():
				msg  = "Number of connections/springs for the uncorrelated limit did not agree with the prediction to within tolerance"
				msg += " (actual was {} compared to the predicted {};".format(numConnections,predNumSprings)
				msg += " relative tolerance was {}).".format(self.tolNumSprings())
				raise fnTestBase.FailedTestError( msg )
			else:
				if self._verbose:
					print( " - number of connections/springs for the uncorrelated limit agrees with the prediction to within tolerance." )
		
			# Done for this c=0 case.
			return

		#
		# Check for case c=0.6: Use Fig. 2(a) of paper and compare to the measured G'(w).
		#
		if c==0.3:

			# Sanity check: Tests set-up with two values of f (not phi_1!), one non-rigid and one rigid as per the published data.
			if f not in (0.629,0.67):
				raise fnTestBase.FailedTestError("Value of f={} not expected for one of the correlated lattices (c>0).".format(f))

			# Also sanity check the frequency is low.
			if w > self.tolStaticFreq():
				raise fnTestBase.FailedTestError("Value of w={} not low enough for one of the correlated lattice checks (c>0).".format(w))

			# Get the shear modulus.
			Gprime = self._table.means("G'_net")[0]

			# Only single points so hardly worth a plot.
			if self._plot:
				print( " - (nothing to plot for this test)" )

			# Verbose output.
			if self._verbose:
				print( " - for the correlated lattice (c={}) and f={}, measured G'={} at very low frequency.".format(c,f,Gprime) )

			# If the low f, should be 'zero'.
			if f==0.629 and Gprime > self.tolGPrimeRigid():
				raise fnTestBase.FailedTestError("Network for low phi/f appeared to be rigid when it should not have been.")

			# If the high f, should be non-zero.
			if f==0.67 and Gprime < self.tolGPrimeRigid():
				raise fnTestBase.FailedTestError("Network for high phi/f appeared to be non-rigid when it should have been rigid.")

			# If still here, must have been successful.
			if self._verbose:
				print( " - network for c={} and f={} was (non-)rigid as per the published data (tolerance={}).".format(c,f,self.tolGPrimeRigid()) )
		

	# Arguments for the executable.
	def _parameterFile( self ):
		return self.__paramsFile


	#
	# Class methods
	#

	# Tolerance for the number of connections/springs.
	@classmethod
	def tolNumSprings( cls ): return 0.05

	# Tolerance for the low frequency to be effectively a static test.
	@classmethod
	def tolStaticFreq( cls ): return 1e-8

	# Tolerance for G' to be 'non-zero'.
	@classmethod
	def tolGPrimeRigid( cls ): return 1e-6

	# Cleanup; can use wildcards.
	@classmethod
	def filesToDelete( cls ): return ["table.dat","log.out"]

	@classmethod
	def performTest( cls, executable, verbosity, plot ):

		for testNum in range(3):
			fname = "params_test{}.xml".format(testNum)
			print( "Performing correlated lattice test with parameters '{}'".format(fname) )

			testCorrnLattice(executable,"-w 1e-10",verbosity,plot,fname)

			print( " - passed" )
