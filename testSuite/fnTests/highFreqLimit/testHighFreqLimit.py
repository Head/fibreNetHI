#
# Tests for the high-frequency limit.
#


# Standard imports
import math
import matplotlib.pyplot as plt
import numpy as np


# Base class
from fnTests import fnTestBase

class testHighFreqLimit( fnTestBase.fnTestBase ):

	def __init__( self, executable, cmdLineArgs, verbosity, plot, fname ):

		# Store private variables. Initialise before calling parent constructor (which needs these).
		self.__paramsFile = fname

		# Call parent method with required flags.
		super(testHighFreqLimit,self).__init__(executable,cmdLineArgs,verbosity,plot)


	# Test method.
	def performTests( self ):
	
		#
		# Sanity checks.
		#
		if self._table.numRows() != 1:
			raise fnTestBase.FailedTestError( "Expected single table row output but found {}.".format(self._table.numRows()) )

		if self._table.means("w")[0] < 1e5:
			raise NotImplementedError( "Driving frequency must be large." )

		# Parameters for this run.
		k = self._table.k
		p = self._table.p
		w = self._table.means("w")[0]

		#
		# Check 0: Residual in log files.
		#
		self.testResiduals()

		#
		# Check 1: G'(w) should match the affine prediction at high frequencies.
		#

		# Extract G' from the table.
		Gprime = self._table.means("G'_net")[0]

		# Prediction depends on the strain.
		pred = None

		if self._table.driving == "simple shear"     : pred = p * (math.sqrt(3)/4) * k
		if self._table.driving == "extensional shear": pred = p * (math.sqrt(3)/2) * k

		if pred==None:
			raise fnTestBase.NotImplementedError("Shear type not recognised when testing for affinity.")

		# Different tolerances for p<1 as for p=1 due to the small system sizes. Also the prediction for extensional shear is only
		# for an infinite system.
		tol = ( testHighFreqLimit.tolGPrime_noDiln() if p==1 else testHighFreqLimit.tolGPrime_wDiln() )
		if self._table.driving == "extensional shear": tol = testHighFreqLimit.tolGPrime_extShear()

		# Verbose output.
		if self._verbose:
			print( "Measured network contribution G'={0} at w={1} for p={2} and k={3}.".format(Gprime,w,p,k) )
			print( " - the affine prediction is {}.".format(pred) )

		# Only single points so hardly worth a plot.
		if self._plot:
			print( " - (nothing to plot for this test)" )

		# Check and any verbose output.
		if abs(Gprime-pred) > tol:
			raise fnTestBase.FailedTestError( "G^{{\\prime}}={0} significantly different from affine prediction {1}.".format(Gprime,pred) )
		else:
			if self._verbose:
				print( " - agrees with prediction to within tolerance." )

		#
		# Check 2: The non-affinity is low in all cases.
		#
		netNA = self._table.means("NA_net")[0]		# The prediction is zero in all cases.
		if self._verbose:
			print( " - measured network non-affinity for the same system is {0}.".format(netNA) )

		# If no HI, should be very close to zero, otherwise won't be zero with HI due to interpolation errors to the velocity mesh).
		tol = ( testHighFreqLimit.tolNetNA_dragOnly() if self._table.fluidType=="drag only" else testHighFreqLimit.tolNA_Stokes() )

		if abs(netNA) > tol:
			raise fnTestBase.FailedTestError( "Network non-affinity {0} significantly different from zero.".format(netNA) )
		else:
			if self._verbose:
				print( " - network non-affinity metric close enough to zero using tolerance of {0}.".format(tol) )

		# If Stokes flow, also check the fluid NA.
		if self._table.fluidType == "Stokes":
			flNA = self._table.means("NA_fl")[0]

			if self._verbose:
				print( " - also checking fluid non-affinity since this test is for Stokes flow." )
				print( " - measured fluid non-affinity was {0}.".format(flNA) )
			
			# Use same tolerance as network NA with HI.
			if abs(flNA) > testHighFreqLimit.tolNA_Stokes():
				raise fnTestBase.FailedTestError( "Fluid non-affinity {0} significantly different from zero.".format(flNA) )
			else:
				if self._verbose:
					print( " - fluid non-affinity metric close enough to zero using tolerance of {}.".format(testHighFreqLimit.tolNA_Stokes()) )


	# Arguments for the executable.
	def _parameterFile( self ):
		return self.__paramsFile


	#
	# Class methods
	#

	# Tolerance for checks.
	@classmethod
	def tolGPrime_noDiln( cls ):
		return 1e-6
	
	@classmethod
	def tolGPrime_wDiln( cls ):
		return 5e-2

	@classmethod
	def tolNetNA_dragOnly( cls ):
		return 1e-10

	@classmethod
	def tolNA_Stokes( cls ):
		return 1e-2
	
	@classmethod
	def tolGPrime_extShear( cls ):
		return 2e-2

	# Tolerance for residuals. Higher than usual because of the large value of \omega for these tests.
	@classmethod
	def tolResidual( cls ):
		return 1e-4
	
	# Cleanup; can use wildcards.
	@classmethod
	def filesToDelete( cls ): return ["table.dat","log.out"]

	@classmethod
	def performTest( cls, executable, verbosity, plot ):

		for testNum in range(3):
			fname = "params_test{}.xml".format(testNum)
			print( "Performing high frequency limit test with parameters '{}'".format(fname) )

			testHighFreqLimit(executable,"-w 1e6",verbosity,plot,fname)

			print( " - passed" )
