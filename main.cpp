/**
 * @file main.cpp
 * @author David Head
 * @brief Launch code for fibreNetHI; also parses command line arguments.
 */
 



//
// Includes
//

// Standard includes
#include <getopt.h>
#include <memory>
#include <iostream>
#include <thread>

// Project includes
#include "fibreNetHI.h"




/**
 * @mainpage fibreNetHI
 * 
 * Simple model to predict the linear viscoelastic response of a discrete,
 * typically disordered network in an immersed solvent. Based on the two-fluid model
 * as used by MacKintosh and Levine (PRL 2008).
 *
 * This file parses command line arguments, invokes the main object, and starts it
 * running. For options, call with `-h`.
 */
int main( int argc, char* argv[] )
{

	//
	// Parse command line arguments
	//
	
	// Variables for the arguments
	std::string parameters_fname = "parameters.xml";
	int staticSoln = 0, saveStates = 0;
	unsigned long pRNG_Seed = 0;
	double singleFreq = -1.0, beginFreq = 0.0, endFreq = 0.0, freqFactor = 0.5;

 	// Define the command line arguments; will expand this list as necessary
	// Flag options would be defined by: { "<name>", no_argument, &int_variable, [1,0] }
	static struct option long_options[] =
	{
		{ "parameters"  , required_argument, 0, 'p' },
		{ "seed"        , required_argument, 0, 's' },
		{ "omega"       , required_argument, 0, 'w' },
		{ "begin"       , required_argument, 0, 'b' },
		{ "end"         , required_argument, 0, 'e' },
		{ "factor"      , required_argument, 0, 'f' },
		{ "static"      , no_argument, &staticSoln, 1 },
		{ "save"        , no_argument, &saveStates, 1 },
		{0,0,0,0}
	};

	// Parse all arguments in turn
	int c, option_index = 0;
	while( true )
	{
		// The colon signifies an argument is required. Flag options not included here.
		c = getopt_long( argc, argv, "p:s:w:b:e:f:h", long_options, &option_index );

		if( c==-1 ) break;			// -1 signifies the end of the list

		switch( c )
		{
			case 0: break;											// Flag options are handled by getopt_long()

			case 'p' : parameters_fname =      optarg ; break;
			case 's' : pRNG_Seed        = atoi(optarg); break;
			case 'w' : singleFreq       = atof(optarg); break;
			case 'b' : beginFreq        = atof(optarg); break;
			case 'e' : endFreq          = atof(optarg); break;
			case 'f' : freqFactor       = atof(optarg); break;
			case 'h' :
			{
				std::cout << "fibreNetHI. Call with arguments:\n"
			              << " -p/--parameters <params_filename>    The pathname for the parameters file [parameters.xml]"   << std::endl
			              << " -s/--seed <seed>                     Seed to use for the pRNG [0, which uses system clock]"   << std::endl
			              << " -w/--omega <freq>                    Solve for a single angular frequency omega"              << std::endl
			              << " -b/--begin <freq>                    Start sweep frequency; end must also be defined"         << std::endl
			              << " -e/--end <freq>                      End sweep frequency; start must also be defined"         << std::endl
			              << " -f/--factor <val>                    Relative frequency steps from start to end [0.5]"        << std::endl
						  << "    --static                          First or only frequency is zero"                         << std::endl
						  << "    --save                            Saves states for each solve; sequential indexing"        << std::endl;
			              return 0;
			}
			case '?':
			default:
			{
				std::cerr << "Could not parse arguments; call with '-h' for options." << std::endl;
			}
		}
	}

	// Cannot select a static solution and a single frequency.
	if( staticSoln && singleFreq>0.0 )
	{
		std::cerr << "FATAL: Cannot specify a static solution and a single frequency; either call with a "
		          << "a frequency of zero, or just specify static." << std::endl;
		return EXIT_FAILURE;
	}

	// Selecting a single static solution is the same as a single frequency of zero.
	if( staticSoln && !(beginFreq||endFreq) ) singleFreq = 0.0;

	// Check no more than one frequency control was selected.
	if( singleFreq!=-1 && (beginFreq||endFreq) )
	{
		std::cerr << "FATAL: Cannot specify both single frequency and a sweep range." << std::endl;
		return EXIT_FAILURE;
	}
	
	// If a specific seed has been requested, initialise the (global) pRNG using it.
	if( pRNG_Seed > 0 ) pRNG::engine.seed( pRNG_Seed );
	
	// Create the (unique) main object pointer and initialise the solvers and network.
	std::unique_ptr<fibreNetHI> immersedNetwork( new fibreNetHI(parameters_fname) );
	
	//
	// Run control: Single or no frequency, with solution saved and parsable by the Python scripts.
	//
	if( !(beginFreq||endFreq) )
	{	
		// Do not attempt to solve for negative frequencies
		if( singleFreq<0.0 )
		{
			std::cerr << "FATAL: Frequency not specified or negative." << std::endl;
			return EXIT_FAILURE;
		}
	
		// If no frequecy specified, just output the initial state (which might be pre-stressed, i.e. require solving for).
		if( singleFreq==-1 )
		{
			std::cout << "No frequencies specified; will just return initial state." << std::endl;
			if( saveStates ) immersedNetwork->saveSolution();
			return EXIT_SUCCESS;
		}

		// If still here, solve for the required frequency.
		auto retVal = immersedNetwork->solve( singleFreq );
		if( saveStates ) immersedNetwork->saveSolution();

		// Returns 0 if for successful implicit solve, >0 iterations for explicit solve, <0 if failed.
		if( retVal>=0 )
		{
			if( retVal>0 ) std::cout << "Converged; took " << retVal << " iteration(s)" << std::endl;

			// Append to table file (will try to create if it doesn't exist); no error of could not open/create file.
			immersedNetwork->tableRow( "table.dat" );
		}
	}
	
	//
	// Run control: Frequency sweep, output to table only (no saved states).
	//
	if( beginFreq || endFreq )
	{
		// Sanity check
		if( beginFreq<=0.0 || endFreq<=0.0 )
		{
			std::cerr << "FATAL: Both ends of the sweep must be positive." << std::endl;
			return EXIT_FAILURE;			
		}
		if( freqFactor<=0.0 )
		{
			std::cerr << "FATAL: Frequency change factor must be positive." << std::endl;
			return EXIT_FAILURE;
		}
		if( (freqFactor<1.0&&beginFreq<endFreq) || (freqFactor>1.0&&beginFreq>endFreq) )
		{
			std::cerr << "FATAL: Given frequency change will not approach requested end frequency." << std::endl;
			return EXIT_FAILURE;
		}
		
		// If selected, start with the static (zero frequency) solution.
		if( staticSoln )
		{
			immersedNetwork->solve   ( 0.0 );				// Similar as in the frequency sweep loop.
			immersedNetwork->tableRow( "table.dat" );
			if( saveStates ) immersedNetwork->saveSolution();
		}

		// Start the frequency sweep.
		double omega = beginFreq;
		while( (freqFactor<1.0&&omega>endFreq) || (freqFactor>1.0&&omega<endFreq) )
		{
			// Get the solution for this frequency.
			immersedNetwork->solve( omega );				// Don't need return value.

			// Output to the table file.
			immersedNetwork->tableRow( "table.dat" );

			// Output an indexed save state if requested.
			if( saveStates ) immersedNetwork->saveSolution();
			
			// Update the frequency as per the required factor.
			omega *= freqFactor;
		}
	}
}



