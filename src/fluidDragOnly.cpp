#include "fluidDragOnly.h"

#pragma mark -
#pragma mark Constructor
#pragma mark -

fluidDragOnly::fluidDragOnly( double X, double Y, const XMLParameters *params, ostream *log, const drivingBase *driving  )
 : fluidBase(X,Y,params,log,driving)
{
	// Output to log file what has been constructed.
	*_log << std::endl << "Fluid type: fluidDragOnly" << std::endl;
	*_log << " - viscosity = " << _eta << std::endl;
}

#pragma mark -
#pragma mark Matrix
#pragma mark -

// Adds the drag forces, which acts as the coupling between the (affine) fluid and network nodes.
void fluidDragOnly::matrix_fillDragTerms( matrixTriSpringDrag *M, double omega, const std::set<int> netNodes, const std::vector<double> nodePosns ) const
{
	// Shorthand.
	std::complex<double> i_omega{0.0,omega};

	// Loop through all nodes in the network that exist in the natrix equation, e.g. are not entirely unconnected.
	for( auto n : netNodes )
	{
		// Add - \zeta * i\omega times the displacements to the equations.
		M->addDispsDiag_xy( n, - _zeta * i_omega );

		// Get the velocity at the nodal position, which will be the affine prediction.
		std::array< std::complex<double>, 2 > vel = velocityAt( nodePosns[2*n], nodePosns[2*n+1] );
		
		// Add \zeta times the velocity to the RHS of the displacement equatiuons.
		// Same sign as displacement term as (i) should be opposite sign, but (ii) moved to RHS.
		M->addDispsRHS_xy( n, vel, - _zeta );
	}	
}


#pragma mark -
#pragma mark I/O
#pragma mark -

// Just need to output the fluid type and the viscosity.
void fluidDragOnly::saveSolution( ostream &out ) const
{
	out << "fluidDragOnly" << std::endl
	    << _eta << " " << _zeta << std::endl;
}


