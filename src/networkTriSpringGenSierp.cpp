#include "networkTriSpringGenSierp.h"

#pragma mark -
#pragma mark Constructors / destructor
#pragma mark -

// Construct from a (read-only) XMLParameters object
networkTriSpringGenSierp::networkTriSpringGenSierp( double X, double Y, const XMLParameters *params, ostream *log, const drivingBase *driving )
	: networkTriSpringSierpinski(X,Y,params,log,driving)
{
	using namespace parameterParsing;

	// Get the optional triangle type; defaults to the standard Sierpinski triangle with the middle triangle omitted.
	__omitTri = __omittableTriangles::M;
	if( params->hasLabelParam(netTSGenSierp_omitTriangle) )
	{
		std::string label = params->labelParam(netTSGenSierp_omitTriangle);

		// Check valid triangle label; case sensitive.
		if(
			   label!=netTSGenSierp_omitTriangle_M
			&& label!=netTSGenSierp_omitTriangle_T
			&& label!=netTSGenSierp_omitTriangle_L
			&& label!=netTSGenSierp_omitTriangle_R
		)
			throw BadParameter(netTSGenSierp_omitTriangle,"omittable triangle type not recognised [nb. case-sensitive]",__FILE__,__LINE__);

		// Set the omit triangle enumerated type. No need to check for the default.
		if( label==netTSGenSierp_omitTriangle_T ) __omitTri = __omittableTriangles::T;
		if( label==netTSGenSierp_omitTriangle_L ) __omitTri = __omittableTriangles::L;
		if( label==netTSGenSierp_omitTriangle_R ) __omitTri = __omittableTriangles::R;
	}
}


#pragma mark -
#pragma mark Recursive network generation
#pragma mark -

// Modifies the parent's recursive calls to omit the requested triangle, rather than always the middle one.
std::set<networkTriSpringSierpinski::__triangle>
networkTriSpringGenSierp::_recurseTriangles( std::set<__triangle> triangles, int level ) const
{
    // If at final level of recursion, return with the current collection of triangles.
    if( level==0 ) return triangles;

    // Calculate (once per level) the offset required to ensure the indexing remains correct throughout.
    int offset_x = ( level==1 ? 1 : 0 );

    // Replace each triangle in the list with 3 (fractal) / 4 (non-fractal) triangles.
    std::set<__triangle> subTriangles;
    for( auto &tri : triangles )
    {
        // Get the index pairings for the mid-nodes. This includes a 'tweak' (offset) at the smallest level.
        __node
            mid01 = _midNode( tri[0], tri[1], offset_x ),
            mid02 = _midNode( tri[0], tri[2], offset_x ),
            mid12 = _midNode( tri[1], tri[2], offset_x );

		// For each triangle, only recurse if (a) level not above maxFractalLevel, or (b) not omitted.
		// Need to be careful about the order of the mapped vertices; see Python script or article
		// from Katerina's summer internship in 2022.
		if( level>_maxFractalLevel || __omitTri!=__omittableTriangles::T )
	        for( auto &tri1 : _recurseTriangles({{tri[0],mid01,mid02}},level-1) )
    	        subTriangles.insert( tri1 );

		if( level>_maxFractalLevel || __omitTri!=__omittableTriangles::L )
			for( auto &tri2 : _recurseTriangles({{mid01,tri[1],mid12}},level-1) )
				subTriangles.insert( tri2 );

		if( level>_maxFractalLevel || __omitTri!=__omittableTriangles::R )
			for( auto &tri3 : _recurseTriangles({{mid02,mid12,tri[2]}},level-1) )
				subTriangles.insert( tri3 );

		if( level>_maxFractalLevel || __omitTri!=__omittableTriangles::M )
			for( auto &tri4 : _recurseTriangles({{mid12,mid02,mid01}},level-1) )
                subTriangles.insert( tri4 );
    }

    return subTriangles;
}