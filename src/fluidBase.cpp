#include "fluidBase.h"

#pragma mark -
#pragma mark Constructors
#pragma mark -

fluidBase::fluidBase( double X, double Y, const XMLParameters *params, ostream *log, const drivingBase *driving )
		: _params(params), _log(log), _driving(driving), _X(X), _Y(Y), _omega(0.0)
{
	using namespace parameterParsing;

	// The params object will throw an exception if no (positive) viscosity has been provided.
	_eta = _params->scalarParam(fluid_viscosity);
	if( _eta <= 0.0 )
		throw BadParameter(fluid_viscosity,"must be positive",__FILE__,__LINE__);

	// Similarly for the coupling coefficient.
	_zeta = _params->scalarParam( fluid_dampCoefficient );
	if( _zeta <= 0.0 )
		throw BadParameter(fluid_dampCoefficient,"must be positive",__FILE__,__LINE__);	
}


