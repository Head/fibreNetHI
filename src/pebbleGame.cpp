#include "pebbleGame.h"


#pragma mark -
#pragma mark Constructors / destructor
#pragma mark -

pebbleGame::pebbleGame( int numNodes ) : _nNodes( numNodes )
{
    // Sanity check for the number of nodes.
    if( numNodes<=0 )
        throw std::range_error( "Bad number of nodes in pebbleGame constructor: Must be non-negative." );

    // Reset all of the persistent containers - classified edge lists, and free pebbles on nodes.
    _initContainers();
}

// Initialise all of the persistent containers given a known number of nodes _nNodes.
void pebbleGame::_initContainers()
{
    // Persistent lists of independent and redundant nodes. Initially empty.
    _eIndependent.clear();
    _eRedundant.clear();

    // Initialise the number of free pebbles on each node to be 2.
    _nFreePebbles.resize( _nNodes );
    _nFreePebbles.shrink_to_fit();          // Non-binding request to reduce memory (sets capacity to size).
    for( auto &n : _nFreePebbles ) n = 2;

    // The graph search sets the search status of all nodes involved. Resize the container now.
    _nodeSearchedFrom.resize( _nNodes );
    _nodeSearchedFrom.shrink_to_fit();

    // Initially there are no rigid clusters.
    _rigidClusters.clear();

    // Initialise the vector of viable search edges index by node.
    _viableSearchEdges.resize( _nNodes );
    for( auto &eVec : _viableSearchEdges  ) eVec.reserve( 6 );          // To reduce memory re-allocation, set capacity to something high.
    _viableSearchEdges.shrink_to_fit();
}


#pragma mark -
#pragma mark Pebble game algorithm
#pragma mark -

// Adds edges in random order and updates the persistent edge lists.
void pebbleGame::addEdges( const _edgeList &addEdges )
{
    // Copy all edges into a local, mutable std::deque.
    std::deque< _edge > edges( addEdges.begin(), addEdges.end() );

    // Shuffle the edges into a random order.
    std::random_device rd;
    std::mt19937 pRNG( rd() );
    std::shuffle( edges.begin(), edges.end(), pRNG );

    // Add one edge at a time, determining if each is redundant or independent, and updating the persistent edge lists accordingly.
    for( auto &e : edges )
        if( _trialEdgeRedundant(e) )
        {
            // Redundant bonds are undirected.
            _eRedundant.push_back( e );
        }
        else
        {
            // Choose to move the pebble from e.second to anchor the bond; hence direction is first->second.
            _eIndependent.push_back( e );
            _nFreePebbles[e.second]--;

            // Add to the container for valid edges indexed by node. Reference the actual edges in the _eIndependent container.
            _viableSearchEdges[e.second].push_back( &_eIndependent.back() );
        }
}

// The crux of the pebble game algorithm - determines if the given trial edge is redundant or independent.
// Does not add to the persistent edge sets, but may change the direction of existing edges and move pebbles.
bool pebbleGame::_trialEdgeRedundant( _edge edge )
{
    // Sanity check on the provided edge.
    if( edge.first<0 || edge.second<0 || edge.first>=_nNodes || edge.second>=_nNodes )
        throw std::range_error( "Bad edge in pebbleGame::_trialEdgeRedundant : At least one node index out-of-range." );

    if( edge.first==edge.second )
        throw std::range_error( "Bad edge in pebbleGame::_trialEdgeRedundant : Both nodes have the same index." );

    // Try to get 2 free pebbles at each end of the edge.
    for( auto &n : std::set<int>{edge.first,edge.second} )
        while( _nFreePebbles[n]<2 )
        {
            // Get the path to a node with a free pebble, traversing only along known independent edges, and in
            // the opposite direction of the edges (so that when the pebble is moved, it moves in the direction of the edges).
            std::vector<_edge*> pathToFreeNode = _pathToFreePebble( n, (n==edge.first?edge.second:edge.first) );

            // An empty returned path denotes the search failed.
            if( pathToFreeNode.empty() ) break;

            // Move the pebble from the end to the start of the path.
            _nFreePebbles[ pathToFreeNode.back()->first ]--;
            _nFreePebbles[n]++;
 
            // Also reverse all edges along this path, corresponding to the shuffling of anchored bonds during the pebble motion.
            for( auto e : pathToFreeNode )
            {
                // Flip the order of the nodes.
                std::swap( e->first, e->second );

                // Remove the reference from the list for the first end, and add to that for the second. On testing, this simple loop
                // was (possibly) slightly faster than using std::find, maybe because these vectors are typically very small.
                for( auto eIter=_viableSearchEdges[e->first].begin(); eIter!=_viableSearchEdges[e->first].end(); eIter++ )
                    if( *eIter==e ) { _viableSearchEdges[e->first].erase(eIter); break; }

                _viableSearchEdges[e->second].push_back( e );
            }
        }

    // See how many pebbles in total were moved to the ends of the trial edge.
    int totalFreePebbles = _nFreePebbles[edge.first] + _nFreePebbles[edge.second];

    // Quick check: Must be 3 or 4.
    if( totalFreePebbles<3 || totalFreePebbles>4 )
        throw std::range_error( "Error in pebbleGame::_trialEdgeRedundant : Somehow got a total of nodes at ends of trial edge that was not 3 or 4." );

    // If 3, the bond is redundant, otherwise it is independent.
    return ( totalFreePebbles == 3 );
}

// Graph search specialised to the pebble game method, and using (but not altering) persistent edge lists.
// Uses (and alters) the persistent container for the node search states.
std::vector<pebbleGame::_edge*> pebbleGame::_pathToFreePebble( int startNode, int invalidEndNode )
{
    // Every time a node is searched from (i.e. all viable edges from that node pushed to the stack), set its flag to 'true'.
    std::fill( _nodeSearchedFrom.begin(), _nodeSearchedFrom.end(), false );

    // Testing on bond diluted lattices near p_c showed breadth-first was faster than depth-first, so use a qeueue.
    std::deque< std::vector<_edge*> > queue;

    // Push all valid edges starting from startNode onto the queue.
    for( auto &e : _viableSearchEdges[startNode] ) queue.push_front( {e} );
    _nodeSearchedFrom[startNode] = true;

    // Loop until the queue is empty. 
    while( !queue.empty() )
    {
        // Pop the top path from the end of the queue.
        auto topPath = queue.back();
        queue.pop_back();

        // If the last node on this path is one of the target nodes - excluding the start and invalid nodes - then we are done.
        int lastNode = topPath.back()->first;
        if( _nFreePebbles[lastNode] )
            if( lastNode!=startNode && lastNode!=invalidEndNode )
                return topPath;

        // Find all new paths that continue from the end node of the just-popped path, and add them to front of the queue.
        if( !_nodeSearchedFrom[lastNode] )
        {
            _nodeSearchedFrom[lastNode] = true;

            // Add a new path for all valid edges extending from lastNode, to the start of the queue.
            for( auto e : _viableSearchEdges[lastNode] )
            {
                std::vector<_edge*> newPath{topPath};          // New path that extends the previous one by the new node.
                newPath.emplace_back( e );
                queue.push_front( newPath );
            }
        }
    }

    // If reached this point, the search failed and the stack is empty. Return an empty path to indicate this.
    return std::vector<_edge*>{};
}



#pragma mark -
#pragma mark Post-processing
#pragma mark -

// Identify all rigid clusters.
void pebbleGame::postProcess()
{
    //
    // Decompose all edges into clusters defined by mutual rigidity.
    //
    std::list< std::vector<_edge> > edgeClusters;
    edgeClusters.clear();

    // As an optimisation, keep track of all node pairings determined to be rigid.
    __mutuallyRigidNodes.clear();

    // As another optimisation, periodically sort the clusters in order of decreasing size, in the expectation
    // that new edges are most likely to be added to the largest cluster.
    int nSinceLastSort = 0;

    // Get all edges into a set (i.e. with ordered keys). On testing this appeared to improve performance,
    // possibly because consecutive path searches are more likely to start from similar nodes.
    std::set< _edge > allEdges;
    for( auto eIter = (_eIndependent.empty()?_eRedundant.begin():_eIndependent.begin()); eIter != _eRedundant.end(); )
    {
        // Add edge as undirected.
        int n1 = eIter->first, n2 = eIter->second;
        allEdges.emplace( std::make_pair( std::min(n1,n2), std::max(n1,n2) ) );

        // Switch between the two edge containers.
        if( ++eIter==_eIndependent.end() ) eIter = _eRedundant.begin();
    }   

    // Loop over all of these pre-assembled edges.
    for( auto &e : allEdges )
    {
        // Shorthands for the node indices for the new edge.
        auto n1 = e.first, n2 = e.second;

        // Try to add edge to an existing cluster for which both nodes are mutually rigid to any other edge.
        bool addedToExistingCluster = false;
        for( auto &clust : edgeClusters )
        {
            // Shorthands for the node indices for the edge to compare against.
            auto m1 = clust.front().first, m2 = clust.front().second;

            // Determine if these two edges are mutually rigid or not.
            bool mutuallyRigid = false;

            // If the edges share a node, just check the remaining pair.
            if( n1==m1 ) mutuallyRigid = _mutuallyRigid( n2, m2 );
            if( n1==m2 ) mutuallyRigid = _mutuallyRigid( n2, m1 );
            if( n2==m1 ) mutuallyRigid = _mutuallyRigid( n1, m2 );
            if( n2==m2 ) mutuallyRigid = _mutuallyRigid( n1, m1 );

            // If still not determined, compare any three pairings.
            if( n1!=m1 && n1!=m2 && n2!=m1 && n2!=m2 )
                mutuallyRigid = _mutuallyRigid(n1,m1) && _mutuallyRigid(n2,m2) && _mutuallyRigid(n1,m2);

            // If mutually rigid, add to this cluster.                
            if( mutuallyRigid )
            {
                clust.emplace_back( e );
                addedToExistingCluster = true;
                break;
            }
        }

        // If not added to any cluster, use to seed a new cluster.
        if( !addedToExistingCluster ) edgeClusters.push_back( {e} );

        // Periodically sort clusters in order of decreasing size, as subsequent edges most likely to be added to the larger clusters.
        // The factor here (which could be optimised) fixes the time between resets to the current largest cluster, with a sensible minumum.
        if( ++nSinceLastSort>10 && nSinceLastSort > 0.05*edgeClusters.begin()->size() )
        {
            edgeClusters.sort( [] ( const std::vector<_edge> &x, const std::vector<_edge> &y ) { return x.size() > y.size(); } );
            nSinceLastSort = 0;
        }
    }

    //
    // Map edge clusters to node clusters.
    //
    _rigidClusters.clear();

    for( auto &clust : edgeClusters )
    {
        std::set<int> nodes;

        for( auto &edge : clust )
        {
            nodes.insert( edge.first );
            nodes.insert( edge.second );
        }

        _rigidClusters.push_back( nodes );
    }

    //
    // Get all isolated nodes.
    //
    for( auto n=0; n<_nNodes; n++ )
    {
        bool inCluster = false;
        for( auto &clust : _rigidClusters )
            if( clust.count(n) )
            {
                inCluster = true;
                break;
            }
        
        if( !inCluster ) _isolatedNodes.insert( n );
    }
}

// Returns 'true' if the two nodes are mutually rigid during post-processing, but with an optimisation to avoid re-checking pairs.
bool pebbleGame::_mutuallyRigid( int n1, int n2 )
{
    auto orderedPair = std::make_pair( std::min(n1,n2), std::max(n1,n2) );

    // Check if this pairing has already been checked.
    auto previous = __mutuallyRigidNodes.find(orderedPair);

    // If found, return the previous result.
    if( previous != __mutuallyRigidNodes.end() ) return previous->second;

    // If still here, node pairing has not yet been checked. Calculate using the pebble game and store for future reference.
    bool mRigid = _trialEdgeRedundant( orderedPair );
    __mutuallyRigidNodes[orderedPair] = mRigid;

    return mRigid;
}

// Returns all nodes in the largest cluster.
std::set<int> pebbleGame::largestRigidCluster() const
{
    std::set<int> largestCluster{};

    for( auto &clust : _rigidClusters )
        if( clust.size() > largestCluster.size() )
            largestCluster = clust;
    
    return largestCluster;
}

// Returns a set of nodes that belong to 2 or more clusters.
std::set<int> pebbleGame::allPivotNodes() const
{
   std::set<int> nodes;

    for( auto n=0; n<_nNodes; n++ )
    {
        int numOccurrences = 0;
        for( auto &clust : _rigidClusters )
            numOccurrences += clust.count(n);           // Consistency check will fail if this is >1.
        
        // If appeared 2 times or more, must be a pivot node - no need to continue.
        if( numOccurrences>1 )
        {
            nodes.insert( n );
            continue;
        }
    }
    
    return nodes;    
}



#pragma mark -
#pragma mark Debugging
#pragma mark -

// Consistency checks for the numbers of free pebbles, and the persistent containers for the two types of edge.
bool pebbleGame::checkEdgeConsistency() const
{
    // The number of free pebbles should be 2 per node, minus the number of independent edges.
    if( numFreePebbles() != 2*_nNodes - numIndependentEdges() )
    {
        std::cerr << "Edge consistency fail: Incorrect residual number of free pebbles/d.o.f. given the number of independent edges/bonds." << std::endl;
        return false;
    }

    // Each node should have 0, 1 or 2 pebbles.
    for( auto &n : _nFreePebbles )
        if( n<0 || n>2 )
        {
            std::cerr << "Edge onsistency fail: At least one node had an invalid number of free pebbles/d.o.f.." << std::endl;
            return false;
        }

    // Lambda functions for counting edges in an edge list.
    auto countIndepEdge = [this] ( _edge edge ) { return std::count( _eIndependent.begin(), _eIndependent.end(), edge ); };
    auto countRedunEdge = [this] ( _edge edge ) { return std::count( _eRedundant  .begin(), _eRedundant  .end(), edge ); };

    // Check that there are no repeat edges, of either orientation.
    for( auto &edge : _eRedundant )
    {
        // Quick check the node indices themselves are valid.
        if( edge.first<0 || edge.second<0 || edge.first>=_nNodes || edge.second>=_nNodes || edge.first==edge.second )
        {
            std::cerr << "Edge consistency fail: Invalid node indices for a redundant edge (out of range, or both equal)." << std::endl;
            return false;
        }

        // Get the edge with the reversed direction.
        std::pair<int,int> revEdge = std::make_pair( edge.second, edge.first );

        // Only one redundant edge, with the same order.
        if( countRedunEdge(edge)>1 || countRedunEdge(revEdge) )
        {
            std::cerr << "Edge consistency fail: Redundant bond appears in redundant list more than once, possibly reversed." << std::endl;
            return false;
        }

        // Check it does not appear in the list of independent edges; again, in any order.
        if( countIndepEdge(edge) || countIndepEdge(revEdge) )
        {
            std::cerr << "Edge consistency fail: Redundant bond appears in independent edge list, possibly reversed." << std::endl;
            return false;
        }
    }

    // Same again, this time checking the independent edges only appear once, including reversed.
    for( auto &edge : _eIndependent )
    {
        // Check the node indices themselves are valid.
        if( edge.first<0 || edge.second<0 || edge.first>=_nNodes || edge.second>=_nNodes || edge.first==edge.second )
        {
            std::cerr << "Edge consistency fail: Invalid node indices for an independent edge (out of range, or both equal)." << std::endl;
            return false;
        }

        // The edge with the node indices reversed.
        std::pair<int,int> revEdge = std::make_pair( edge.second, edge.first );

        // Only one redundant edge, with the same order.
        if( countIndepEdge(edge)>1 || countIndepEdge(revEdge) )
        {
            std::cerr << "Edge consistency fail: Independent bond appears in independent list more than once, possibly reversed." << std::endl;
            return false;
        }

        // Check it does not appear in the list of independent edges; again, in any order.
        if( countRedunEdge(edge) || countRedunEdge(revEdge) )
        {
            std::cerr << "Edge consistency fail: Independent bond appears in redundant edge list, possibly reversed." << std::endl;
            return false;
        }
    }

    // If still here, all checks passed.
    return true;
}

// Consistency checks for the rigid clusters identified after post-processing.
bool pebbleGame::checkClusterConsistency() const
{
    // All isolated nodes should not appear in any rigid clusters.
    for( auto &n : _isolatedNodes )
        for( auto &clust : _rigidClusters )
            if( clust.count(n) )
            {
                std::cerr << "Cluster consistency fail: At least one isolated node also appeared in at least one rigid cluster." << std::endl;
                return false;
            }
    
    // Conversly, none of the nodes in the rigid clusters should appear on the list of isolated nodes.
    for( auto &clust : _rigidClusters )
        for( auto &n : clust )
            if( _isolatedNodes.count(n) )
            {
                std::cerr << "Cluster consistency fail: At least one node in a rigid cluster also appeared in the set of isolated clusters." << std::endl;
                return false; 
            }

    // Also check all nodes appear somewhere.
    for( auto n=0; n<_nNodes; n++ )
        if( !_isolatedNodes.count(n) )
        {
            bool inCluster = false;
            for( auto &clust : _rigidClusters )
                if( clust.count(n) )
                {
                    inCluster = true;
                    break;
                }
            
            if( !inCluster )
            {
                std::cerr << "Cluster consistency fail: At least one node in a rigid cluster also appeared in the set of isolated clusters." << std::endl;
                return false; 
            }
        }

    // Check all isolated nodes have 2 remaining degrees of freedom.
    for( auto &n : allIsolatedNodes() )
        if( _nFreePebbles[n] != 2 )
        {
            std::cerr << "Cluster consistency fail: At least one isolated node did not have exactly 2 free pebbles / d.o.f." << std::endl;
            return false;
        }

    // If reached here, all tests passed.
    return true;
}

