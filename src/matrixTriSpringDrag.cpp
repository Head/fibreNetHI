#include "matrixTriSpringDrag.h"


#pragma mark -
#pragma mark Constructor
#pragma mark -

matrixTriSpringDrag::matrixTriSpringDrag( unsigned int netNodes, std::ostream *log=nullptr, const std::vector<int> *indexer=nullptr )
	: matrixTriSpringBase(netNodes,2*netNodes,log,indexer)
{}


#pragma mark -
#pragma mark Debugging
#pragma mark -

// Checks the equation for the given solution. Uses dense matrix format, so will be slow for large systems.
double matrixTriSpringDrag::debugResidual( const std::complex<double> *u ) const
{
	// The matrix in dense format.
	std::vector< std::complex<double> > matrix = getDenseMatrix();
	
	// Maximum residual
	double maxMagRes = 0.0;

	// Loop over all rows of the matrix
	for( auto i=0u; i<_numScalars; i++ )
	{
		std::complex<double> res{0.0,0.0};
		
		// Add the displacement contributions; j is the node index (xxxx...yyyy... format here, xyxyxy... format for u)
		for( auto j=0u; j<_numNetNodes; j++ )
		{
			res += matrix[ _index(i,_startDisps_x+j) ] * u[2*j  ];
			res += matrix[ _index(i,_startDisps_y+j) ] * u[2*j+1];
		}

		// DEBUG: Print current residual (which at this point is the LHS) against the RHS, row by row.
//		std::cout << i << " " << res << " " << _RHS[i] << std::endl;
		
		// Subtract off the RHS
		res -= _RHS[i];

		// Compare to the current maximum and update if necessary
		maxMagRes = std::max( maxMagRes, std::abs(res) );
	}
	
	return maxMagRes;
}
