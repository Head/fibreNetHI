#include "pRNG.h"

// Initialise the engine to a different 'seed' each time [can specify with seed() method]
std::random_device pRNG::randomDevice;
std::mt19937_64    pRNG::engine( pRNG::randomDevice() );

// Distributions
std::uniform_real_distribution<double> pRNG::dist_unitUniform( 0.0, 1.0 );
std::normal_distribution<double>       pRNG::dist_unitNormal ( 0.0, 1.0 );

// Shorthand accessors
double pRNG::uniform() { return pRNG::dist_unitUniform( pRNG::engine ); }
double pRNG::normal () { return pRNG::dist_unitNormal ( pRNG::engine ); }