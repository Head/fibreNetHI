// Template specialisations
#include "smallSquareMatrix.h"


#pragma mark -
#pragma mark Determinant
#pragma mark -

template<>
double smallSquareMatrix<2,double>::det() noexcept
	{ return _M[0]*_M[3] - _M[1]*_M[2]; }

template<>
float smallSquareMatrix<2,float>::det() noexcept
	{ return _M[0]*_M[3] - _M[1]*_M[2]; }

template<>
double smallSquareMatrix<3,double>::det() noexcept
	{ return _M[0]*(_M[4]*_M[8]-_M[5]*_M[7]) - _M[1]*(_M[3]*_M[8]-_M[5]*_M[6]) + _M[2]*(_M[3]*_M[7]-_M[4]*_M[6]); }

template<>
float smallSquareMatrix<3,float>::det() noexcept
	{ return _M[0]*(_M[4]*_M[8]-_M[5]*_M[7]) - _M[1]*(_M[3]*_M[8]-_M[5]*_M[6]) + _M[2]*(_M[3]*_M[7]-_M[4]*_M[6]); }


#pragma mark -
#pragma mark Adjugates
#pragma mark -

// Creates the adjugate (or adjunct) matrix and stores it in '_M', i.e. replacing the current matrix.
// Note that this also destroys the temporary matrix. Common specialisations given.

template<>
void smallSquareMatrix<2,double>::adjugateInPlace() noexcept
{
	for( unsigned short i=0; i<4; i++ ) _temp[i] = _M[i];
	_M[0] =   _temp[3];
	_M[1] = - _temp[1];
	_M[2] = - _temp[2];	
	_M[3] =   _temp[0];
}

template<>
void smallSquareMatrix<2,float>::adjugateInPlace() noexcept
{
	for( unsigned short i=0; i<4; i++ ) _temp[i] = _M[i];
	_M[0] =   _temp[3];
	_M[1] = - _temp[1];
	_M[2] = - _temp[2];	
	_M[3] =   _temp[0];
}

template<>
void smallSquareMatrix<3,double>::adjugateInPlace() noexcept
{
	for( unsigned short i=0; i<3; i++ )							// For 3d, use the protected method _x3x_adj()
		for( unsigned short j=0; j<3; j++ )
			_temp[_index(i,j)] = _3x3_adj(j,i);

	for( unsigned short i=0; i<9; i++ ) _M[i] = _temp[i];
}

template<>
void smallSquareMatrix<3,float>::adjugateInPlace() noexcept
{
	for( unsigned short i=0; i<3; i++ )							// For 3d, use the protected method _x3x_adj()
		for( unsigned short j=0; j<3; j++ )
			_temp[_index(i,j)] = _3x3_adj(j,i);

	for( unsigned short i=0; i<9; i++ ) _M[i] = _temp[i];
}

// Creates the adjugate (or adjunct) matrix and leaves it in the persistent storage space '_temp'
template<>
void smallSquareMatrix<2,double>::adjugateToTemp() noexcept
{
	_temp[0] =   _M[3];
	_temp[1] = - _M[1];
	_temp[2] = - _M[2];	
	_temp[3] =   _M[0];
}

template<>
void smallSquareMatrix<2,float>::adjugateToTemp() noexcept
{
	_temp[0] =   _M[3];
	_temp[1] = - _M[1];
	_temp[2] = - _M[2];	
	_temp[3] =   _M[0];
}

template<>
void smallSquareMatrix<3,double>::adjugateToTemp() noexcept
{
	for( unsigned short i=0; i<3; i++ )							// For 3d, use the protected method _x3x_adj()
		for( unsigned short j=0; j<3; j++ )
			_temp[_index(i,j)] = _3x3_adj(j,i);
}

template<>
void smallSquareMatrix<3,float>::adjugateToTemp() noexcept
{
	for( unsigned short i=0; i<3; i++ )							// For 3d, use the protected method _x3x_adj()
		for( unsigned short j=0; j<3; j++ )
			_temp[_index(i,j)] = _3x3_adj(j,i);
}



#pragma mark -
#pragma mark Vector-matrix multiplication
#pragma mark -

// Vector-matrix multiplication
template<>
void smallSquareMatrix<2,double>::vectorMultiply( const double *x, double *y ) const noexcept
{
	y[0] = _M[0]*x[0] + _M[2]*x[1];
	y[1] = _M[1]*x[0] + _M[3]*x[1];
}

template<>
void smallSquareMatrix<2,float>::vectorMultiply( const float *x, float *y ) const noexcept
{
	y[0] = _M[0]*x[0] + _M[2]*x[1];
	y[1] = _M[1]*x[0] + _M[3]*x[1];
}

template<>
void smallSquareMatrix<3,double>::vectorMultiply( const double *x, double *y ) const noexcept
{
	y[0] = _M[0]*x[0] + _M[3]*x[1] + _M[6]*x[2];
	y[1] = _M[1]*x[0] + _M[4]*x[1] + _M[7]*x[2];
	y[2] = _M[2]*x[0] + _M[5]*x[1] + _M[8]*x[2];
}

template<>
void smallSquareMatrix<3,float>::vectorMultiply( const float *x, float *y ) const noexcept
{
	y[0] = _M[0]*x[0] + _M[3]*x[1] + _M[6]*x[2];
	y[1] = _M[1]*x[0] + _M[4]*x[1] + _M[7]*x[2];
	y[2] = _M[2]*x[0] + _M[5]*x[1] + _M[8]*x[2];
}

// Same, but using the "temp" matrix instead
template<>
void smallSquareMatrix<2,double>::vectorMultiplyUsingTemp( const double *x, double *y ) const noexcept
{
	y[0] = _temp[0]*x[0] + _temp[2]*x[1];
	y[1] = _temp[1]*x[0] + _temp[3]*x[1];
}

template<>
void smallSquareMatrix<2,float>::vectorMultiplyUsingTemp( const float *x, float *y ) const noexcept
{
	y[0] = _temp[0]*x[0] + _temp[2]*x[1];
	y[1] = _temp[1]*x[0] + _temp[3]*x[1];
}

template<>
void smallSquareMatrix<3,double>::vectorMultiplyUsingTemp( const double *x, double *y ) const noexcept
{
	y[0] = _temp[0]*x[0] + _temp[3]*x[1] + _temp[6]*x[2];
	y[1] = _temp[1]*x[0] + _temp[4]*x[1] + _temp[7]*x[2];
	y[2] = _temp[2]*x[0] + _temp[5]*x[1] + _temp[8]*x[2];
}

template<>
void smallSquareMatrix<3,float>::vectorMultiplyUsingTemp( const float *x, float *y ) const noexcept
{
	y[0] = _temp[0]*x[0] + _temp[3]*x[1] + _temp[6]*x[2];
	y[1] = _temp[1]*x[0] + _temp[4]*x[1] + _temp[7]*x[2];
	y[2] = _temp[2]*x[0] + _temp[5]*x[1] + _temp[8]*x[2];
}


#pragma mark -
#pragma mark Positive-definiteness
#pragma mark -

// Returns "true" if the matrix is positive-definite, "false" if not (i.e. zero det or negative)
// Uses Sylvester's criterion, namely that positive definite <=> all "upper-left" matrices
// have positive determinants ("upper-left" being the upper-left 1x1, 2x2 etc. matrices).
template<>
bool smallSquareMatrix<2,double>::positiveDefinite() noexcept
	{ return ( _M[0]>0 && det()>0 ); }

template<>
bool smallSquareMatrix<2,float>::positiveDefinite() noexcept
	{ return ( _M[0]>0 && det()>0 ); }

template<>
bool smallSquareMatrix<3,double>::positiveDefinite() noexcept
	{ return( _M[0]>0 && _3x3_cofac(2,2)>0 && det()>0 ); }

template<>
bool smallSquareMatrix<3,float>::positiveDefinite() noexcept
	{ return( _M[0]>0 && _3x3_cofac(2,2)>0 && det()>0 ); }


#pragma mark -
#pragma mark Inverting
#pragma mark -

template<>
bool smallSquareMatrix< 2, std::complex<double> >::invertToTemp() noexcept
{
	std::complex<double> _det = det();

	if( abs(_det)==0 ) return false;
	if( _det.imag() != _det.imag() || _det.real() != _det.real() ) return false;

	adjugateToTemp();
	
	for( short i=0; i<2*2; i++ ) _temp[i] /= _det;
	
	return true;
}

template<>
bool smallSquareMatrix< 3, std::complex<double> >::invertToTemp() noexcept
{
	std::complex<double> _det = det();

	if( abs(_det)==0 ) return false;
	if( _det.imag() != _det.imag() || _det.real() != _det.real() ) return false;

	adjugateToTemp();
	
	for( short i=0; i<3*3; i++ ) _temp[i] /= _det;
	
	return true;
}

template<>
bool smallSquareMatrix< 2, std::complex<double> >::invertInPlace() noexcept
{
	std::complex<double> _det = det();			// Calculate once and store locally

	if( std::abs(_det)==0 ) return false;
	if( _det.imag() != _det.imag() || _det.real() != _det.real() ) return false;

	adjugateInPlace();
	
	for( unsigned short i=0; i<2*2; i++ ) _M[i] /= _det;
	
	return true;
}

template<>
bool smallSquareMatrix< 3, std::complex<double> >::invertInPlace() noexcept
{
	std::complex<double> _det = det();			// Calculate once and store locally

	if( std::abs(_det)==0 ) return false;
	if( _det.imag() != _det.imag() || _det.real() != _det.real() ) return false;

	adjugateInPlace();
	
	for( unsigned short i=0; i<3*3; i++ ) _M[i] /= _det;
	
	return true;
}




