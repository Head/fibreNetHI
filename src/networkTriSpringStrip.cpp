#include "networkTriSpringStrip.h"

#pragma mark -
#pragma mark Constructors / destructor
#pragma mark -

// Construct from a (read-only) XMLParameters object
networkTriSpringStrip::networkTriSpringStrip( double X, double Y, const XMLParameters *params, ostream *log, const drivingBase *driving )
	: networkTriSpring(X,Y,params,log,driving)
{
	using namespace parameterParsing;

    // Thickness of the horizontal strip. Must be >0 and less than the box dimensions.
	_nominalStripThickness = _params->scalarParam( netTSStrip_stripThickness );

	if( _nominalStripThickness <= 0.0 )
		throw BadParameter(netTSStrip_stripThickness,"must be positive",__FILE__,__LINE__);

    if( _nominalStripThickness >= _Y )
		throw BadParameter(netTSStrip_stripThickness,"cannot exceed box's Y-dimension",__FILE__,__LINE__);
}


#pragma mark -
#pragma mark Lattice initialisation
#pragma mark -

// Post-construction initialisation.
void networkTriSpringStrip::initialise()
{
    networkTriSpring::initialise();

    // Additional log lines.
    *_log << " - actual strip thickness " << _actualThickness()
          << ", corresponding to " << _numY << " rows of horizontal springs and "
          << _numY-1 << " rows of 'diagonal' springs." << std::endl;
}

void networkTriSpringStrip::_initLatticeDimensions()
{
	using namespace parameterParsing;

    // Spans x-direction, so same as usual triangular spring lattice.
	if( remainder(_X,2*_a) > _commensurateTol )
		throw BadParameter(netTS_latticeSpacing,"not commensurate with X-dimension (or odd number of nodes)",__FILE__,__LINE__);

	_numX = static_cast<unsigned int>( round(_X/_a)  );

    // Since not periodic in the y-direction, do not require vertical extent to be commensurate with the lattice.
    // Do however want it to be an odd number so can perfectly centre around y=Y/2.
    _numY = static_cast<unsigned int>( round(_nominalStripThickness/(_root3over2*_a)) );
    if( !_numY ) throw BadParameter(netTSStrip_stripThickness,"this thickness corresponds to zero lattice rows",__FILE__,__LINE__);
  
    // If even. decrement.
    if( !(_numY%2) ) _numY--;
    if( !_numY ) throw BadParameter(netTSStrip_stripThickness,"cannot achieve an odd number of lattice rows within this thickness (required)",__FILE__,__LINE__);
  }

void networkTriSpringStrip::_trialConnection( int i1, int j1, int i2, int j2, double dilution )
{
    // Do not attempt to periodically wrap in the vertical direction.
    if( j1==int(_numY) || j2==int(_numY) ) return;

    // Otherwise the same as the parent class, 
    networkTriSpring::_trialConnection(i1,j1,i2,j2,dilution);
}


#pragma mark -
#pragma mark Geometry
#pragma mark -

// Shift to centre strip vertically.
double networkTriSpringStrip::_latticePos_y( int i, int j ) const noexcept
{
    return _Y/2 + (j-static_cast<double>(_numY)/2.0+0.5)*_a*_root3over2;
}

// Periodic wrapping in x-direction only.
unsigned int networkTriSpringStrip::_globalIndex( int i, int j ) const noexcept
{
	if( i < 0                        ) i += _numX;
	if( i >= static_cast<int>(_numX) ) i -= _numX;

	return i + j*_numX;
}


#pragma mark -
#pragma mark Analysis
#pragma mark -

// Outputs the mean flux outwards across both interfaces, where the flux is defined as i\omega u - v
// normalised to the number of nodes across bother interfaces, the lattice spacing a, and omega.
// Only the modulus is returned (not the real and imaginary components separately).
double networkTriSpringStrip::interfacialFlux() const
{
    // Constants.
    std::complex<double> i_omega{0.0,_omega};

    // The running total.
    std::complex<double> totalFlux{0.0,0.0};

    // Loop across both interfaces at once.
    for( auto i=0u; i<_numX; i++ )
    {
        auto node_upper = _globalIndex(i,_numY-1), node_lower = _globalIndex(i,0);

        std::complex<double>
            uy_upper = _disps[2*node_upper+1],
            uy_lower = _disps[2*node_lower+1],
            vy_upper = _fluidVelocityAtNode(node_upper)[1],
            vy_lower = _fluidVelocityAtNode(node_lower)[1];

        totalFlux += i_omega * uy_upper - vy_upper;
        totalFlux -= i_omega * uy_lower - vy_lower;
    }

    return std::abs(totalFlux) / (2*_a*_numX*_omega);
}

