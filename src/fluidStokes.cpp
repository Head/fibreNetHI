#include "fluidStokes.h"


#pragma mark -
#pragma mark Initialisation and clear-up
#pragma mark -

// Constructor
fluidStokes::fluidStokes( double X, double Y, const XMLParameters *params, ostream *log, const drivingBase *driving  )
	: fluidBase(X,Y,params,log,driving)
{
	using namespace parameterParsing;

	//
	// Parameters
	//
	
	// Required parameters; recall XMLParameters objects throw exceptions if requested parameters are entirely absent
	// Requested cell size; actual cell size may differ to fit box dimensions.
	double requestedCellSize = _params->scalarParam( fluidS_cellSizeAttr );
	if( requestedCellSize <= 0.0 )
		throw BadParameter(fluidS_cellSizeAttr,"must be positive",__FILE__,__LINE__);

	//
	// Post-processing of parameters
	//
	
	// Actual cell sizes must perfectly fit box dimensions, therefore will typically not match the request done.
	// Easiest to calculate the number in each direction first [use round(); no issue being smaller than expected].
	_numX = round( _X / requestedCellSize );
	_numY = round( _Y / requestedCellSize );
	
	// For finite difference calculations, helps to assume at least 3 cells in each direction.
	if( _numX<3 || _numY<3 )
		throw BadParameter(fluidS_cellSizeAttr,"need at least 3 cells in each direction",__FILE__,__LINE__);

	// The size of a single cell, and the total number of cells
	_dx       = _X / _numX;
	_dy       = _Y / _numY;
	_numCells = _numX * _numY;
	
	// Can now re-size the lattices and their copies
	_P.resize(   _numCells, {0.0,0.0} );
	_v.resize( 2*_numCells, {0.0,0.0} );
	
	// Save memory
	_P.shrink_to_fit();
	_v.shrink_to_fit();
	
	//
	// Output to log file what the final parameters actually were
	//
	*_log << std::endl << "Fluid type: fluidStokes" << std::endl
	      << " - viscosity = " << _eta << std::endl
	      << " - " << _numX << " x " << _numY << " mesh cells, total " << _numCells << std::endl
	      << " - size of a single mesh cell = (" << _dx << "," << _dy << "); nb. may not exactly match requested size." << std::endl;
}


#pragma mark -
#pragma mark Utility
#pragma mark -

// Periodically wraps grid indices. Can happen after converting from node coordinates which lie outside the
// box after some non-trivial generation of the initial condition (i.e. with prestresses).
void fluidStokes::_wrap( unsigned int &i, unsigned int &j ) const
{
	while( i <  0     ) i += _numX;
	while( i >= _numX ) i -= _numX;

	while( j <  0     ) j += _numY;
	while( j >= _numY ) j -= _numY;
}


#pragma mark -
#pragma mark Interpolation
#pragma mark -

// Velocity mesh indices and coefficients for the given position using interpolation.
void fluidStokes::_velocityMeshInterp(
	double x,
	double y,
	std::vector<unsigned int> *indices,
	std::vector<double> *coeffs,
	std::vector<int> *shifts_x,
	std::vector<int> *shifts_y
	) const
{
	// Clear the passed containers ready for filling.
	indices->clear();
	coeffs->clear();
	shifts_x->clear();
	shifts_y->clear();

	// Wrap x-coordinate to [0,_X) and y-coordinate to [0,_Y).
	while( x < 0.0 ) x += _X;
	while( x >= _X ) x -= _X;
	while( y < 0.0 ) y += _Y;
	while( y >= _Y ) y -= _Y;

	//
	// Call an interpolation method. May make this selectable in future, but for now, comment one out.
	//

	//__interpBilinear(x,y,indices,coeffs,shifts_x,shifts_y);
	__interpGaussian(x,y,indices,coeffs,shifts_x,shifts_y);

	//
	// Sanity checks for the containers returned by the interpolation scheme.
	//

	// Make sure all containers are of the same size.
	if( indices->size()!=coeffs->size() || indices->size()!=shifts_x->size() || indices->size()!=shifts_y->size() )
		throw SystemError("interpolation of velocity mesh indices","containers of different sizes",__FILE__,__LINE__);

	// Total weight (sum of all coefficients) must be 1 to within f.p. errors. No std::reduce() without C++17!
	double tol = 1e-7;
	if( std::abs( std::accumulate(coeffs->begin(),coeffs->end(),0.0) - 1.0 ) > tol )
		throw SystemError("interpolation of velocity mesh indices","weights did not add to 1",__FILE__,__LINE__);
}

// Interpolation method: Bilinear.
void fluidStokes::__interpBilinear(
	double x,
	double y,
	std::vector<unsigned int> *indices,
	std::vector<double> *coeffs,
	std::vector<int> *shifts_x,
	std::vector<int> *shifts_y ) const
{
	//
	// Map coordinates to mesh points. (x,y) has been wrapped prior to calling this method.
	//

	// Get the velocity mesh indices of the lower-left velocity mesh grid point.
	int i = static_cast<unsigned int>( x/_dx );
	int j = static_cast<unsigned int>( y/_dy );

	// Sanity check for the indices.
	if( i<0 || j<0 || i>=int(_numX) || j>=int(_numY) )
		throw SystemError("bilinear interpolation of velocity mesh indices","index or indices out-of-range",__FILE__,__LINE__);
	
	// Get the velocity mesh indices to the right, and to the above, with periodic wrapping.
	int ip1 = ( i==int(_numX)-1 ? 0 : i+1 );
	int jp1 = ( j==int(_numY)-1 ? 0 : j+1 );

	// Reduced variables.
	double red_x = ( x - i*_dx ) / _dx;
	double red_y = ( y - j*_dy ) / _dy;

	// Sanity check for the reduced variables.
	if( red_x<0.0 || red_y<0.0 || red_x>=1.0 || red_y>=1.0 )
		throw SystemError("bilinear interpolation of velocity mesh coefficients","reduced variable(s) out-of-range",__FILE__,__LINE__);

	//
	// Fill in the indices and the coefficients.
	//

	// Indices.
	indices->resize(4);
	indices->at(0) = _index(i  ,j  );
	indices->at(1) = _index(ip1,j  );
	indices->at(2) = _index(i  ,jp1);
	indices->at(3) = _index(ip1,jp1);

	// Coefficients / weights for each node.
	coeffs->resize(4);
	coeffs->at(0) = 1.0 - red_x - red_y + red_x*red_y;
	coeffs->at(1) =     + red_x         - red_x*red_y;
	coeffs->at(2) =             + red_y - red_x*red_y;
	coeffs->at(3) =                     + red_x*red_y;

	// Periodic wrapping in the x-direction.
	shifts_x->resize( 4, 0 );
	shifts_x->at(1) = ( ip1==0 ? 1 : 0 );
	shifts_x->at(3) = ( ip1==0 ? 1 : 0 );

	// Periodic wrapping in the y-direction.
	shifts_y->resize( 4, 0 );
	shifts_y->at(2) = ( jp1==0 ? 1 : 0 );
	shifts_y->at(3) = ( jp1==0 ? 1 : 0 );
}

// Interpolation method: Gaussian.
void fluidStokes::__interpGaussian(
	double x,
	double y,
	std::vector<unsigned int> *indices,
	std::vector<double> *coeffs,
	std::vector<int> *shifts_x,
	std::vector<int> *shifts_y ) const
{
	// Indexing is as follows, with (x,y) somewhere within the square (3,4,7,8):
	//       (0)    (1)
	// (2)   (3)    (4)    (5)
	// (6)   (7)    (8)    (9)
	//       (10)   (11)
	
	// Resize the containers.
	indices ->resize(12);
	coeffs  ->resize(12);
	shifts_x->resize(12,0);
	shifts_y->resize(12,0);

	//
	// Map coordinates to mesh points. (x,y) has been wrapped prior to calling this method.
	//

	// Get the velocity mesh indices of the lower-left velocity mesh grid point.
	int i = static_cast<unsigned int>( x/_dx );
	int j = static_cast<unsigned int>( y/_dy );

	// Sanity check for this index.
	if( i<0 || j<0 || i>=int(_numX) || j>=int(_numY) )
		throw SystemError("Gaussian interpolation of velocity mesh indices","index or indices out-of-range",__FILE__,__LINE__);

	// This interpolation has NN and NNN indices, offset to the right/above.
	int ip1=i+1, jp1=j+1, im1=i-1, jm1=j-1, ip2=i+2, jp2=j+2; 

	// Apply periodic BCs and calculate shifts at the same time.
	if( ip1 >= int(_numX) )
	{
		ip1 -= _numX;
		shifts_x->at(1 ) = 1;				// All interpolation indices to the right of (x,y).
		shifts_x->at(4 ) = 1;
		shifts_x->at(8 ) = 1;
		shifts_x->at(11) = 1;
		shifts_x->at(5 ) = 1;
		shifts_x->at(9 ) = 1;
	}
	if( ip2 >= int(_numX) )
	{
		ip2 -= _numX;
		shifts_x->at(5) = 1;				// Just the rightmost 2 indices; redundant if ip1 also wrapped.
		shifts_x->at(9) = 1;				// Note still only 1 shift needs to be applied (i.e. only 1 "-_numX").
	}
	if( im1 < 0 )
	{
		im1 += _numX;
		shifts_x->at(2) = -1;				// Negative shifts when wrapping around the left edge.
		shifts_x->at(6) = -1;
	}

	if( jp1 >= int(_numY) )
	{
		jp1 -= _numY;
		shifts_y->at(0) = 1;				// All interpolation indices above (x,y).
		shifts_y->at(1) = 1;
		shifts_y->at(2) = 1;
		shifts_y->at(3) = 1;
		shifts_y->at(4) = 1;
		shifts_y->at(5) = 1;
	}
	if( jp2 >= int(_numY) )
	{
		jp2 -= _numY;
		shifts_y->at(0) = 1;				// Just the uppermost 2 indices; redundant if jp1 also wrapped.
		shifts_y->at(1) = 1;
	}
	if( jm1 < 0 )
	{
		jm1 += _numY;
		shifts_y->at(10) = -1;				// Negative shifts when wrapping around the bottom edge.
		shifts_y->at(11) = -1;
	}

	// Can now fill in the indices. See diagram in above comment.
	indices->at(0 ) = _index(i  ,jp2);
	indices->at(1 ) = _index(ip1,jp2);
	indices->at(2 ) = _index(im1,jp1);
	indices->at(3 ) = _index(i  ,jp1);
	indices->at(4 ) = _index(ip1,jp1);
	indices->at(5 ) = _index(ip2,jp1);
	indices->at(6 ) = _index(im1,j  );
	indices->at(7 ) = _index(i  ,j  );
	indices->at(8 ) = _index(ip1,j  );
	indices->at(9 ) = _index(ip2,j  );
	indices->at(10) = _index(i  ,jm1);
	indices->at(11) = _index(ip1,jm1);

	// Range-squared over which the weighting decays. Multiple by value <1 for shorter range / quicker decay.
	// 0.4 chosen after trial-and-error on a simple system to "minimise" the non-affinity.
	double R2 = 0.4*( _dx*_dx + _dy*_dy);

	// Coefficients. Note must use unwrapped indices for calculating the distances.
	auto gauss = [R2]( double dx, double dy ) { return exp( -(dx*dx+dy*dy)/R2 ); };

	coeffs->at(0 ) = gauss( x - _dx* i   , y - _dy*(j+2) );		// Compare to the indices-at(...) above.
	coeffs->at(1 ) = gauss( x - _dx*(i+1), y - _dy*(j+2) );
	coeffs->at(2 ) = gauss( x - _dx*(i-1), y - _dy*(j+1) );
	coeffs->at(3 ) = gauss( x - _dx* i   , y - _dy*(j+1) );
	coeffs->at(4 ) = gauss( x - _dx*(i+1), y - _dy*(j+1) );
	coeffs->at(5 ) = gauss( x - _dx*(i+2), y - _dy*(j+1) );
	coeffs->at(6 ) = gauss( x - _dx*(i-1), y - _dy* j    );
	coeffs->at(7 ) = gauss( x - _dx* i   , y - _dy* j    );
	coeffs->at(8 ) = gauss( x - _dx*(i+1), y - _dy* j    );
	coeffs->at(9 ) = gauss( x - _dx*(i+2), y - _dy* j    );
	coeffs->at(10) = gauss( x - _dx* i   , y - _dy*(j-1) );
	coeffs->at(11) = gauss( x - _dx*(i+1), y - _dy*(j-1) );

	// Ensure coefficients add to 1, to within f.p. errors.
	double sum = std::accumulate( coeffs->begin(), coeffs->end(), 0.0 );
	for( auto &c : *coeffs ) c /= sum;

	// For debugging, can output the relative weight of the inner 4 points; should
	// probably be no less than 50%, so that the total weight of the outer points is < 50%.
	// std::cout << "Relative weight of NN points = "
	//           << coeffs->at(3) + coeffs->at(4) + coeffs->at(7) + coeffs->at(8)
	//           << std::endl;
}
			

#pragma mark -
#pragma mark Solver
#pragma mark -

// Calculates the imaginary offset required to minimise the deviation between the actual and affine predictions.
// Returned as a real 2-vector that is (1/i) times the actual offset (i.e. needs to be multiplied by i).
std::array<double,2> fluidStokes::imagAffineOffset( double omega ) const
{	
	// Get the strain tensor for this field, assuming as elsewhere an overall strain magnitude \gamma=1.
	std::array< std::array<double,2>, 2> gamma = _driving->strainTensor();

	// Evaluate the offset as per the equations derived in the notes.
	std::array<double,2> vOffset{0.0,0.0};
	for( auto i=0u; i<_numX; i++ )
		for( auto j=0u; j<_numY; j++ )
		{
			auto n = _index(i,j);

			// Add terms node-by-node as per the expression.
			for( auto k=0u; k<2; k++ )
			{
				vOffset[k] += _v[2*n+k].imag();		// v.imag() is (v-v^{star})/2 divided by i.

				vOffset[k] -= omega * gamma[k][0] * ( i*_dx - _driving->origin()[0] );		// Positional term.
				vOffset[k] -= omega * gamma[k][1] * ( j*_dy - _driving->origin()[1] );
			}
		}

	// Divide through by the number of nodes in the summation just performed.
	for( auto k=0u; k<2; k++ ) vOffset[k] /= _numCells;

	return vOffset;
}

// Returns with the mean velocity evaluated at all mesh nodes.
std::array< std::complex<double>,2 > fluidStokes::meanVelocity() const
{
	// Get the total velocity measured at all nodes.
	std::array< std::complex<double>, 2 > sumVels{0.0,0.0};
	for( auto i=0u; i<_numX; i++ )
		for( auto j=0u; j<_numY; j++ )
		{
			auto n = _index(i,j);
			for( auto k=0u; k<2; k++ ) sumVels[k] += _v[2*n+k];
		}

	// Divide by the total number of nodes (=total number of cells).
	for( auto k=0u; k<2; k++ ) sumVels[k] /= _numCells;

	return sumVels;
}

// Subtracts all of the velocities by the given complex vector.
void fluidStokes::offsetVelocities( std::array< std::complex<double>,2 > vOffset )
{
	for( auto n=0u; n<_numCells; n++ )
		for( auto k=0u; k<2; k++ )
			_v[2*n+k] -= vOffset[k];
}

// Fills the drag terms that couple the fluid to the network.
void fluidStokes::matrix_fillDragTerms( matrixTriSpringStokes *M, double omega, const std::set<int> netNodes, const std::vector<double> nodePosns ) const
{
	// Local variables.
	std::vector<unsigned int> indices;
	std::vector<double> coeffs;
	std::vector<int> shifts_x;
	std::vector<int> shifts_y;

	// Shorthands.
	std::complex<double> i_omega{0.0,omega};

	// Shifts for Lees-Edwards-like BCs (with \gamma_{0}=1).
	std::array< std::complex<double>,2 > perBC_leftRight_dv = _driving->fluidShiftLeftRight(omega);
	std::array< std::complex<double>,2 > perBC_topBottom_dv = _driving->fluidShiftTopBottom(omega);

	// Loop through all nodes in the network mesh that exist in the matrix equation.
	for( auto n : netNodes )
	{
		//
		// Constants.
		//

		// Interpolation to the velocity mesh nodes surrounding this network node.
		_velocityMeshInterp( nodePosns[2*n], nodePosns[2*n+1], &indices, &coeffs, &shifts_x, &shifts_y );

		// Area of a single mesh rectangle. This needs to be multiplied to all contributions to the fluid mesh
		// for the delta function that is multiplied to the drag froce - so that integrating the drag term
		// over the whole mesh gives equal-and-opposite the sum of the same terms in the network equations.
		double recip_dA = 1 / ( _dx * _dy );

		//
		// Drag term proportional to the node velocity.
		//

		// Add - \zeta * i\omega times the displacements to the equations.
		M->addDispsDiag_xy( n, - _zeta * i_omega );

		// Subtract the same from the corresponding velocity d.o.f. Total contribution adds to \zeta i\omega,
		// with the factor for the delta function (inverse of area of a single mesh rectangle).
		for( auto v=0u; v<indices.size(); v++ )
			M->addVelsDisps_xy( indices[v], n, recip_dA * _zeta * i_omega * coeffs[v] );

		//
		// Drag term proportional to the fluid velocity.
		//

		// Again, all contributions to the fluid mesh have an additional factor deriving from the delta function.
		for( auto v=0u; v<indices.size(); v++ )
		{
			M->addDispsVels_xy( n, indices[v],              _zeta * coeffs[v] );
			M->addVelsDiag_xy (    indices[v], - recip_dA * _zeta * coeffs[v] );
		}

		// If some of the interpolated velocity mesh nodes were actually through the periodic BCs,
		// move shifts to the RHS vector - hence sign flip relative to the matrix terms.
		for( auto v=0u; v<indices.size(); v++ )
		{
			// Periodicity in the x-axis.
			M->addDispsRHS_xy( n         , perBC_leftRight_dv, - shifts_x[v] * _zeta * coeffs[v]            );
			M->addVelsRHS_xy ( indices[v], perBC_leftRight_dv,   shifts_x[v] * _zeta * coeffs[v] * recip_dA );

			// Periodicity in the y-axis.
			M->addDispsRHS_xy( n         , perBC_topBottom_dv, - shifts_y[v] * _zeta * coeffs[v]            );
			M->addVelsRHS_xy ( indices[v], perBC_topBottom_dv,   shifts_y[v] * _zeta * coeffs[v] * recip_dA );
		}
	}
}

// Fills the v and P terms for the velocity and pressure equations.
void fluidStokes::matrix_fillStokesTerms( matrixTriSpringStokes *M, double omega ) const
{
	//
	// Shorthands and local variables.
	//

	// Scratch variables
	double dx2 = _dx * _dx;
	double dy2 = _dy * _dy;

	// Velocity shifts for Lees-Edwards-like BCs (with \gamma_{0}=1).
	std::array< std::complex<double>,2 > perBC_leftRight_dv = _driving->fluidShiftLeftRight(omega);
	std::array< std::complex<double>,2 > perBC_topBottom_dv = _driving->fluidShiftTopBottom(omega);

	// Pressure shifts.
	std::complex<double> perBC_leftRight_dP = _driving->pressureShiftLeftRight();
	std::complex<double> perBC_topBottom_dP = _driving->pressureShiftTopBottom();

	// Loop over all fluid mesh nodes.
	for( auto i=0u; i<_numX; i++ )
		for( auto j=0u; j<_numY; j++ )
		{
			// Local indices for adjacent sites, with periodic wrapping.
			auto
				ip1 = ( i==_numX-1 ? 0       : i+1 ),
				im1 = ( i==0       ? _numX-1 : i-1 ),
				jp1 = ( j==_numY-1 ? 0       : j+1 ),
				jm1 = ( j==0       ? _numY-1 : j-1 );
			
			// Global indices for the same.
			auto
				i_j     = _index(i  ,j  ),
				ip1_j   = _index(ip1,j  ),
				im1_j   = _index(im1,j  ),
				i_jp1   = _index(i  ,jp1),
				i_jm1   = _index(i  ,jm1),
				im1_jm1 = _index(im1,jm1),
				ip1_jp1 = _index(ip1,jp1);

			//
			// Velocity equations.
			//

			// Velocity-velocity terms: \eta \del^{2} {\bf v}.
			M->addVelsVels_xy( i_j, i_j  , -2 * _eta/dx2 );		// x and y cpts of the vector both obey the same Laplacian.
			M->addVelsVels_xy( i_j, ip1_j,      _eta/dx2 );
			M->addVelsVels_xy( i_j, im1_j,      _eta/dx2 );

			M->addVelsVels_xy( i_j, i_j  , -2 * _eta/dy2 );
			M->addVelsVels_xy( i_j, i_jp1,      _eta/dy2 );
			M->addVelsVels_xy( i_j, i_jm1,      _eta/dy2 );
			
			// Add general Lees-Edwards-like periodic terms to the RHS.
			if( i==_numX-1 )
			{
				M->addVelsRHS_x( i_j, - _eta * perBC_leftRight_dv[0] / dx2 );
				M->addVelsRHS_y( i_j, - _eta * perBC_leftRight_dv[1] / dx2 );
			}
			if( i==0 )
			{
				M->addVelsRHS_x( i_j, _eta * perBC_leftRight_dv[0] / dx2 );
				M->addVelsRHS_y( i_j, _eta * perBC_leftRight_dv[1] / dx2 );
			}
			if( j==_numY-1 )
			{
				M->addVelsRHS_x( i_j, - _eta * perBC_topBottom_dv[0] / dy2 );
				M->addVelsRHS_y( i_j, - _eta * perBC_topBottom_dv[1] / dy2 );
			}
			if( j==0 )
			{
				M->addVelsRHS_x( i_j, _eta * perBC_topBottom_dv[0] / dy2 );
				M->addVelsRHS_y( i_j, _eta * perBC_topBottom_dv[1] / dy2 );
			}

			//
			// Velocity-pressure terms: - \del P.
			//
			M->addVelsPress_x( i_j, i_j    , -1/(2*_dx) );			// Approximate \partial_{x} as (1/2dx)( P_{ij} + P_{ij-1} - P_{i-1j} - P_{i-1j-1}),
			M->addVelsPress_x( i_j, i_jm1  , -1/(2*_dx) );			// so there is also some averaging over j and j-1. Note also we use i-1, j-1 etc.
			M->addVelsPress_x( i_j, im1_j  ,  1/(2*_dx) );			// rather than i+1, j+1 - this is because of how the fluid and pressure meshes
			M->addVelsPress_x( i_j, im1_jm1,  1/(2*_dx) );			// are staggered (and it's only a first-order derivative).

			M->addVelsPress_y( i_j, i_j    , -1/(2*_dy) );			// Similar to the \partial_{x} case, but now for gradients in the y-direction,
			M->addVelsPress_y( i_j, i_jm1  ,  1/(2*_dy) );			// i.e. (1/2dy) ( P_{ij} + P_{i-1j} - P_{ij-1} - P_{i-1j-1} ).
			M->addVelsPress_y( i_j, im1_j  , -1/(2*_dy) );
			M->addVelsPress_y( i_j, im1_jm1,  1/(2*_dy) );

			// Lees-Edwards-like shift for any pressure gradients. No right/top shifts due to the way the meshes are staggered.
			if( i==0 ) M->addVelsRHS_x( i_j, perBC_leftRight_dP / _dx );
				// Minus signs for (a) left-hand boundary (i-1), (iii) factor of -1/2dx, and (iii) moved to RHS.
				// Equal contribution from two terms (im1_j; im1_jm1), so factor of 2 dropped.
				// No y-term as one contribution is -1/(2dx) and the other is +1/(2dx), so they cancel.

			if( j==0 ) M->addVelsRHS_y( i_j, perBC_topBottom_dP / _dy );	// Sim. to above, now through the bottom boundary.

			//
			// Pressure equations.
			//

			// Add divergence terms to the matrix.
			M->addPressVel_x( i_j, ip1_jp1,   1/(2*_dx) );
			M->addPressVel_x( i_j, ip1_j  ,   1/(2*_dx) );
			M->addPressVel_x( i_j, i_jp1  , - 1/(2*_dx) );
			M->addPressVel_x( i_j, i_j    , - 1/(2*_dx) );

			M->addPressVel_y( i_j, ip1_jp1,   1/(2*_dy) );
			M->addPressVel_y( i_j, ip1_j  , - 1/(2*_dy) );
			M->addPressVel_y( i_j, i_jp1  ,   1/(2*_dy) );
			M->addPressVel_y( i_j, i_j    , - 1/(2*_dy) );

			// Periodic shifts to the RHS with a sign flip. Cancels for certain BCs, e.g. simple shear.
			if( ip1==0 ) M->addPressRHS( i_j, - perBC_leftRight_dv[0]/_dx );	// Same shift applied twice, hence no '2*_dx'.
			if( jp1==0 ) M->addPressRHS( i_j, - perBC_topBottom_dv[1]/_dy );	// Sim.

			// No pressure gradient (perBC) terms, as only velocities appear in the pressure equations.
		}
}

// Get the velocities back from the solution vector.
void fluidStokes::matrix_copyVels( const std::complex<double> *sol )
{
	// 'sol' already starts at the velocities, but still need to convert from (vx,vx,vx,...,vy,vy,vy,...) format.
	for( auto n=0u; n<_numCells; n++ )
	{
		_v[2*n  ] = sol[n          ];
		_v[2*n+1] = sol[n+_numCells];
	}
}

// Get the pressures back from the solution vector. No need to change format this time.
void fluidStokes::matrix_copyPress( const std::complex<double> *sol )
{
	// 'sol' already starts at the pressures.
	for( auto n=0u; n<_numCells; n++ ) _P[n] = sol[n];
}


#pragma mark -
#pragma mark Analysis
#pragma mark -

// The velocity at the specified point, interpolated from the solution mesh
// Similar to biofilm meshRectangularBase::concentrationAtPoint_2D(), except fully periodic here.
std::array<std::complex<double>,2> fluidStokes::velocityAt( double x, double y ) const
{
	// Shifts for Lees-Edwards-like BCs (with \gamma_{0}=1).
	std::array< std::complex<double>,2 > perBC_topBottom_dv = _driving->fluidShiftTopBottom(_omega);
	std::array< std::complex<double>,2 > perBC_leftRight_dv = _driving->fluidShiftLeftRight(_omega);

	// Fill the interpolation containers.
	std::vector<unsigned int> indices;
	std::vector<double> coeffs;
	std::vector<int> shifts_x;
	std::vector<int> shifts_y;

	_velocityMeshInterp( x, y, &indices, &coeffs, &shifts_x, &shifts_y );

	// Get the interpolated velocity, with any shifts due to periodic BCs.
	std::array< std::complex<double>,2 > vel;

	for( auto v=0u; v<indices.size(); v++ )
	{
		for( auto k=0; k<2; k++ )
		{
			// Add weighted contribution for the mesh point used in the interpolation.
			vel[k] += coeffs[v] * _v[2*indices[v]+k];

			// Periodic BCs in the x-direction. 'shifts_x[k]' is zero if not at an x-boundary.
			vel[k] += shifts_x[v] * coeffs[v] * perBC_leftRight_dv[k];

			// Periodic BCs in the y-direction. 'shifts_y[k]' is zero if not at a y-boundary.
			vel[k] += shifts_y[v] * coeffs[v] * perBC_topBottom_dv[k];
		}
	}

	return vel;
}

// Returns a scalar measure for the degree of non-affinity in the fluid flow. Will still need to be divided by some length scale squared.
double fluidStokes::nonAffinity() const
{
	// Initialise the non-affinity measure.
	double NA = 0.0;
	
	// Cannot define for \omega=0.
	if( !_omega ) return NA;

	// Loop over all mesh nodes.
	for( auto i=0u; i<_numX; i++ )
		for( auto j=0u; j<_numY; j++ )
		{
			// Affine velocity.
			std::array< std::complex<double>,2 > vAff = _driving->affineVelocity( i*_dx, j*_dy, _omega );

			// Deviation from actual to affine velocity, in x and y directions.
			std::complex<double>
				dev_vx = _v[2*_index(i,j)  ] - vAff[0],
				dev_vy = _v[2*_index(i,j)+1] - vAff[1];
			
			// Update the measure.
			NA += std::abs( dev_vx*conj(dev_vx) + dev_vy*conj(dev_vy) );
		}
	
	// Normalise and return. Will still need to be divided by some length scale squared.
	return NA / ( _numCells * _omega * _omega );
}


#pragma mark -
#pragma mark I/O
#pragma mark -

// Output state to be read-in by e.g. the associated Python script
void fluidStokes::saveSolution( ostream &out ) const
{
	// First line is the fluid type.
	out << "fluidStokes" << std::endl;
	
	// Second line is the viscosity and the drag coefficient.
	out << _eta << " " << _zeta << std::endl;
	
	// Third line is the number of mesh cells in each direction.
	out << _numX << " " << _numY << std::endl;
	
	// Output the pressure mesh as a grid (so human inspection of the file is more meaningful).
	for( auto j=0u; j<_numY; j++ )
		for( auto i=0u; i<_numX; i++ )
			out << _P[_index(i,j)] << ( i==_numX-1 ? "\n" : "\t" );

	// Similar for the velocity mesh, x as one block, followed by y in one block (again, for 'readability')
	for( auto j=0u; j<_numY; j++ )
		for( auto i=0u; i<_numX; i++ )
			out << _v[2*_index(i,j)  ] << ( i==_numX-1 ? "\n" : "\t" );

	for( auto j=0u; j<_numY; j++ )
		for( auto i=0u; i<_numX; i++ )
			out << _v[2*_index(i,j)+1] << ( i==_numX-1 ? "\n" : "\t" );
}


#pragma mark -
#pragma mark Debugging
#pragma mark -

// Returns the infinite norm for the pressure mesh. No attempt at optimisations or parallelisation.
double fluidStokes::debugPressureResidual() const
{
	// Shifts for Lees-Edwards-like BCs (with \gamma_{0}=1).
	std::array< std::complex<double>,2 > perBC_topBottom_dv = _driving->fluidShiftTopBottom(_omega);
	std::array< std::complex<double>,2 > perBC_leftRight_dv = _driving->fluidShiftLeftRight(_omega);

	// Loop over the entire pressure mesh.
	double maxResidual = 0.0;
	for( auto i=0u; i<_numX; i++ )
		for( auto j=0u; j<_numY; j++ )
		{
			// Clear the 'right hand side' scalar.
			std::complex<double> rhs{0.0,0.0};

			// Local indices of adjacent nodes, with periodic wrapping.
			unsigned int
				ip1 = ( i + 1 ) % _numX,
				jp1 = ( j + 1 ) % _numY;
			
			// Global indices of the same.
			unsigned int
				i_j     = _index( i  , j   ),
				ip1_j   = _index( ip1, j   ),
				i_jp1   = _index( i  , jp1 ),
				ip1_jp1 = _index( ip1, jp1 );

			// Divergence of the 4 velocity nodes centred on pressure node (i,j) should be zero.
			rhs += ( _v[2*ip1_jp1  ] + _v[2*ip1_j  ] - _v[2*i_jp1  ] - _v[2*i_j  ] ) / ( 2*_dx );
			rhs += ( _v[2*ip1_jp1+1] - _v[2*ip1_j+1] + _v[2*i_jp1+1] - _v[2*i_j+1] ) / ( 2*_dy );
			
			// Shifts arising from periodic BCs.
			if( ip1==0 ) rhs += perBC_leftRight_dv[0]/_dx;
			if( jp1==0 ) rhs += perBC_topBottom_dv[1]/_dy;

			// Update the infinite-norm residual
			maxResidual = std::max( maxResidual, abs(rhs) );
		}

	return maxResidual;
}

// Returns the infinite norm for the velocity mesh.
double fluidStokes::debugVelocityResidual(
	std::set<int> nIndices, 
	std::vector<double> nPosns,
	const std::complex<double>* nDisps ) const
{
	//
	// Local variables and shorthands.
	//
	std::complex<double> i_omega{0.0,_omega};
	double
		dx2 = _dx * _dx,
		dy2 = _dy * _dy,
		recip_dA = 1 / ( _dx * _dy ),
		residual,
		maxResidual = 0.0;

	// Velocity shifts for Lees-Edwards-like BCs (with \gamma_{0}=1).
	std::array< std::complex<double>,2 > perBC_leftRight_dv = _driving->fluidShiftLeftRight(_omega);
	std::array< std::complex<double>,2 > perBC_topBottom_dv = _driving->fluidShiftTopBottom(_omega);

	// Pressure shifts.
	std::complex<double> perBC_leftRight_dP = _driving->pressureShiftLeftRight();
	std::complex<double> perBC_topBottom_dP = _driving->pressureShiftTopBottom();

	// Containers for the interpolation from network nodes to the velocity mesh.
	std::vector<unsigned int> indices;
	std::vector<double> coeffs;
	std::vector<int> shifts_x;
	std::vector<int> shifts_y;

	//
	// Construct a local map of all drag forces deriving from the network nodes.
	//

	// Initialise.
	std::vector< std::complex<double> > dragForceMesh;
	dragForceMesh.resize( 2*_numCells, {0.0,0.0} );

	// Loop over all of the provided network node indices.
	for( auto n : nIndices )
	{
		_velocityMeshInterp( nPosns[2*n], nPosns[2*n+1], &indices, &coeffs, &shifts_x, &shifts_y );

		// Add the drag force to the corresponding velocity mesh points.
		for( auto v=0u; v<indices.size(); v++ )
			for( auto k=0u; k<2; k++ )
			{
				// Relative velocity in x (k==0) or y (k==1) direction.
				std::complex<double> dVel = _v[2*indices[v]+k] - i_omega * nDisps[2*n+k];

				// Periodic BCs for this velocity mesh point.
				dVel += static_cast<double>(shifts_x[v]) * perBC_leftRight_dv[k];
				dVel += static_cast<double>(shifts_y[v]) * perBC_topBottom_dv[k];

				// Substract _zeta times this from the force mesh, with weight and 1/(dA) for the delta function.
				dragForceMesh[2*indices[v]+k] -= recip_dA * coeffs[v] * _zeta * dVel;
			}
		}

	//
	// Now calculate the residual, i.e. the maximum deviation over all velocity mesh nodes.
	//
	for( auto i=0u; i<_numX; i++ )
		for( auto j=0u; j<_numY; j++ )
		{
			std::array<std::complex<double>,2> rhs{{0.0,0.0}};
			
			// Local indices, taking into account periodic wrapping
			auto
				ip1 = ( i==_numX-1 ? 0       : i+1 ),
				im1 = ( i==0       ? _numX-1 : i-1 ),
				jp1 = ( j==_numY-1 ? 0       : j+1 ),
				jm1 = ( j==0       ? _numY-1 : j-1 );
			
			// Global indices of the same.
			unsigned int
				i_j     = _index( i  , j   ),
				ip1_j   = _index( ip1, j   ),
				im1_j   = _index( im1, j   ),
				i_jp1   = _index( i  , jp1 ),
				i_jm1   = _index( i  , jm1 ),
				im1_jm1 = _index( im1, jm1 );

			// The viscous term: \eta \del^{2} {\bf v}. 
			for( auto k=0u; k<2u; k++ )
				rhs[k] += _eta * (
					  ( _v[2*ip1_j+k] + _v[2*im1_j+k] - 2.0*_v[2*i_j+k] ) / dx2
					+ ( _v[2*i_jp1+k] + _v[2*i_jm1+k] - 2.0*_v[2*i_j+k] ) / dy2					
				);

			// Since the velocity mesh is affected by Lees-Edwards-type BCs, apply them now.
			if( i==_numX-1 ) for( auto k=0u; k<2u; k++ ) rhs[k] += _eta * perBC_leftRight_dv[k] / dx2;
			if( i==0       ) for( auto k=0u; k<2u; k++ ) rhs[k] -= _eta * perBC_leftRight_dv[k] / dx2;
			if( j==_numY-1 ) for( auto k=0u; k<2u; k++ ) rhs[k] += _eta * perBC_topBottom_dv[k] / dy2;
			if( j==0       ) for( auto k=0u; k<2u; k++ ) rhs[k] -= _eta * perBC_topBottom_dv[k] / dy2;

			// Subtract the gradient of the pressure, taking into account the staggered mesh and averaging.
			rhs[0] -= 0.5*( _P[i_j] + _P[i_jm1] - _P[im1_j] - _P[im1_jm1] )/_dx;
			rhs[1] -= 0.5*( _P[i_j] + _P[im1_j] - _P[i_jm1] - _P[im1_jm1] )/_dy;

			// Pressure gradient.
			if( i==0 ) rhs[0] -= perBC_leftRight_dP / _dx;
			if( j==0 ) rhs[1] -= perBC_topBottom_dP / _dy;

			// Add the drag force from the pre-assembled mesh.
			for( auto k=0u; k<2; k++ ) rhs[k] += dragForceMesh[2*i_j+k];

			// Get a residual for this mesh node.
			residual = sqrt( abs(rhs[0])*abs(rhs[0]) + abs(rhs[1])*abs(rhs[1]) );

			// Update the max. residual.
			maxResidual = std::max( maxResidual, residual );	
		}

	return maxResidual;
}


