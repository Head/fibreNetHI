#include "drivingArbStrain.h"

#pragma mark -
#pragma mark Constructor
#pragma mark -

drivingArbStrain::drivingArbStrain( double X, double Y, const XMLParameters *params, ostream *log )
    : drivingBase(X,Y,params,log)
{
	using namespace parameterParsing;

	// Set four components of the stress tensor.
	double
		gamma_xx = _params->scalarParamOrDefault(strain_xx,0.0),
		gamma_xy = _params->scalarParamOrDefault(strain_xy,0.0),
		gamma_yx = _params->scalarParamOrDefault(strain_yx,0.0),
		gamma_yy = _params->scalarParamOrDefault(strain_yy,0.0);
	
	// Store in the persistent std::array< std::array<double,2>, 2 >.
	_strainTensor[0][0] = gamma_xx;
	_strainTensor[0][1] = gamma_xy;
	_strainTensor[1][0] = gamma_yx;
	_strainTensor[1][1] = gamma_yy;
}

#pragma mark -
#pragma mark Moduli
#pragma mark -

// Returns network stresses, assuming symmetry - only sigma_xy is output, not sigma_yx.
drivingBase::moduliMap drivingArbStrain::networkModuli( tensor2D sigma_ij ) const
{
	drivingBase::moduliMap symSigma;
  
	symSigma["sigma_xx"] = sigma_ij[0][0];
	symSigma["sigma_yy"] = sigma_ij[1][1];
	symSigma["sigma_xy"] = 0.5*( sigma_ij[0][1] + sigma_ij[1][0] );
	
	return symSigma;
}

// Returns the shear fluid modulus, and empty terms for the non-shear components.
drivingBase::moduliMap drivingArbStrain::fluidModuli( double omega, double viscosity ) const
{
	drivingBase::moduliMap sigma_ij;
  
	sigma_ij["sigma_xx"] = 0.0;
	sigma_ij["sigma_xy"] = {0.0,omega*viscosity} ;		// Imaginary component only.
	sigma_ij["sigma_yy"] = 0.0;

	return sigma_ij;
}
