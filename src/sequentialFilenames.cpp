#include "sequentialFilenames.h"


#pragma mark -
#pragma mark Constructors
#pragma mark -

sequentialFilenames::sequentialFilenames( int numDigits, std::string prefix, std::string suffix )
	: _currentIndex(0), _numDigits(numDigits), _prefix(prefix), _suffix(suffix)
{
	// Sanity check for the provided no. of digits.
	if( _numDigits<1 )
		throw std::range_error( "Cannot initialise sequentialFilenames object with less than 1 digit" );

	// Check the current index
	if( _currentIndex<0 )
		throw std::range_error( "Cannot initialise sequentialFilenames object since the passed index less than 1 digit" );
}


#pragma mark -
#pragma mark Modifiers
#pragma mark -

// Set the current index to a new value.
void sequentialFilenames::setIndex( int newIndex )
{
	// Sanity check
	if( newIndex < 0 )
		throw std::range_error( "Cannot have negative index [in sequentialFilenames::setIndex()]");
	if( newIndex > _maxIndex(_numDigits) )
		throw std::range_error( "Index exceeds allowed range [in sequentialFilenames::setIndex()]");
		
	// Set the persistent index count to the new value.
	_currentIndex = newIndex;
}


#pragma mark -
#pragma mark Utility routines
#pragma mark -

// Returns the value with leading zeros up to the exact number of digits required.
std::string sequentialFilenames::_fixedZeros( int i, int numDigits )
{
	std::ostringstream out;

	out.fill( '0' );				// Set the fill character
	out.width( numDigits );			// Set the width
	out << i;						// Convert i to string

	return out.str();
}

// The maximum index given the number of digits
int sequentialFilenames::_maxIndex( int nDigits )
{
	int base = 10;
	for( auto digit=1; digit<nDigits; digit++ ) base *= 10;
	return base - 1;
}


#pragma mark -
#pragma mark Conversion from index to filename and vice versa
#pragma mark -

// Returns the index filename with the given prefix; if not provided, will use instance variables.
std::string sequentialFilenames::indexedFilename( int index, int nDigits, std::string prefix, std::string suffix )
{
	// Construct the filename from the prefix, index, and suffix.
	std::ostringstream out;
	out << prefix << "_" << _fixedZeros(index,nDigits) << suffix;
	return out.str();
}

// Returns the index no. of the given filename; -1 if unsuccessful. Works by finding the first underscore
// after skipping the prefix (if provided) and removing the suffix (if provided).
int sequentialFilenames::indexOfFilename( std::string fname, std::string prefix, std::string suffix )
{
	// First remove any subdirectories; assumes UNIX filenames.
	std::string::size_type dir = fname.find_last_of( '/' );
	if( dir != std::string::npos ) fname = fname.substr( dir+1, fname.size()-dir-1 );
	
	// If prefix provided, check it matches the start of the filename. This still works if prefix=="".
	if( fname.compare(0,prefix.size(),prefix)!=0 )
		throw std::runtime_error("Cannot extract index from filename; prefix does not match [in sequentialFilenames::indexOfFilename()]");

	// Same for the suffix
	if( fname.compare(fname.size()-suffix.size(),suffix.size(),suffix)!=0 )
		throw std::runtime_error("Cannot extract index from filename; suffix does not match [in sequentialFilenames::indexOfFilename()]");
	
	// Take a substring without the prefix or suffix(does nothing if prefix=="")
	std::string tmp   = fname.substr( prefix.size(), fname.size()-prefix.size() );
	std::string name  = tmp  .substr( 0            , tmp  .size()-suffix.size() );

	// Get position of first '_'; assume this comes just before the index.
	std::string::size_type digStart = name.find_first_of( "_" );
	if( digStart==std::string::npos ) return -1;

	// Find the position of the first non-digit character. Increment numStart first (currently still pointing to the underscore).
	auto digEnd = ++digStart;
	while( std::isdigit(name[digEnd]) )
		{ if( ++digEnd==name.size() ) break; }			// Also break if all remaining characters are digits.

	// Convert the remainder to a integer and return
	return atoi( name.substr(digStart,digEnd-digStart).c_str() );
}

// Try to find the highest index of all files in a directory; -1 if not found.
// Basically uses ::indexOfFilename() repeatedly on putative filenames.
// Could easily generalise (i.e. start from index other than 0; start from maxIndex and count down).
int sequentialFilenames::largestFileIndex( int nDigits, std::string dir, std::string prefix, std::string suffix )
{
	auto index = -1;
	while( true )
	{
 		// Create a putative filename using index+1 (assuming UNIX for directories, if one provided)
 		std::string putative_fname = dir + (dir.size()?"/":"") + indexedFilename(index+1,nDigits,prefix,suffix);
 		
 		// See if a file with this name exists for input.
 		std::ifstream file( putative_fname.c_str(), std::ios::in );
 		
 		// If not, break; else increase index by 1 and continue.
 		if( !file ) break;
 		index++;
 	}
 	
 	// Return with the index; if state 0 not found, will return -1.
 	return index;
}


