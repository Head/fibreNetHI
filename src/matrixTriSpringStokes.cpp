#include "matrixTriSpringStokes.h"


#pragma mark -
#pragma mark Constructor
#pragma mark -

matrixTriSpringStokes::matrixTriSpringStokes(
	unsigned int netNodes,
	unsigned int meshNodes,
	std::ostream *log=nullptr, 
	const std::vector<int> *indexer=nullptr )
	: matrixTriSpringBase(netNodes,2*netNodes+3*meshNodes,log,indexer), _numMeshNodes(meshNodes)
{
	// Calculate the starting points for each block of fluid quantities,
	// starting from wherever the network left off.
	_startVels_x  = numNetworkRows();
	_startVels_y  = _startVels_x + _numMeshNodes;
	_startPress   = _startVels_y + _numMeshNodes;

	//
	// Log output
	//
	if( _log ) *_log << "\nCreated matrix and RHS vector for implicit solution: networkTriSpring and fluidStokes" << std::endl
	                 << " - " << _numScalars << " scalars in total." << std::endl;
}


#pragma mark -
#pragma mark Matrix terms
#pragma mark -

// Displacement rows involving the velocities.
void matrixTriSpringStokes::addDispsVels_xy( unsigned int n, unsigned int v, std::complex<double> val )
{
	int m = _matrixNodeIndex(n);

	_addToMatrix(_startDisps_x+m,_startVels_x+v,val);
	_addToMatrix(_startDisps_y+m,_startVels_y+v,val);
}

// Adds the same constant to the x and y-components of the given velocity equations involving a network node.
void matrixTriSpringStokes::addVelsDisps_xy( unsigned int v, unsigned int n, std::complex<double> val )
{
	int m = _matrixNodeIndex(n);

	_addToMatrix(_startVels_x+v,_startDisps_x+m,val);
	_addToMatrix(_startVels_y+v,_startDisps_y+m,val);
}

// Adds the same constant to the x and y-components of the diaginal terms for the given velocity mesh.
void matrixTriSpringStokes::addVelsDiag_xy( unsigned int v, std::complex<double> val )
{
	_addToMatrix(_startVels_x+v,_startVels_x+v,val);
	_addToMatrix(_startVels_y+v,_startVels_y+v,val);
}

// Velocity rows involving the velocity.
void matrixTriSpringStokes::addVelsVels_xy( unsigned int v1, unsigned int v2, double val )
{
	_addToMatrix(_startVels_x+v1,_startVels_x+v2,val);
	_addToMatrix(_startVels_y+v1,_startVels_y+v2,val);
}


#pragma mark -
#pragma mark RHS Vector
#pragma mark -

// Add 2-vector to the RHS of rows for given velocity node.
void matrixTriSpringStokes::addVelsRHS_xy( unsigned int v, std::array<std::complex<double>,2> twoVec, double weight )
{
	addVelsRHS_x( v, weight*twoVec[0] );
	addVelsRHS_y( v, weight*twoVec[1] );
}


#pragma mark -
#pragma mark Debugging
#pragma mark -

// Checks the equation for the given solution. Uses dense matrix format, so will be slow for large systems.
double matrixTriSpringStokes::debugResidual( const std::complex<double> *u, const std::complex<double> *v, const std::complex<double> *P ) const
{
	// The matrix in dense format.
	std::vector< std::complex<double> > matrix = getDenseMatrix();
	
	// Maximum residual
	double maxMagRes = 0.0;

	// Loop over all rows of the matrix
	for( auto i=0u; i<_numScalars; i++ )
	{
		std::complex<double> res{0.0,0.0};
		
		// Add the displacement contributions; j is the node index (xxxx...yyyy... format here, xyxyxy... format for u)
		for( auto j=0u; j<_numNetNodes; j++ )
		{
			res += matrix[ _index(i,_startDisps_x+j) ] * u[2*j  ];
			res += matrix[ _index(i,_startDisps_y+j) ] * u[2*j+1];
		}
				
		// Add the velocity contributions
		for( auto j=0u; j<_numMeshNodes; j++ )
		{
			res += matrix[ _index(i,_startVels_x+j) ] * v[2*j  ];
			res += matrix[ _index(i,_startVels_y+j) ] * v[2*j+1];
		}
		
		// Add the pressure contribution
		for( auto j=0u; j<_numMeshNodes; j++ )
			res += matrix[ _index(i,_startPress+j) ] * P[j];
		
		// DEBUG: Print current residual row by row
//		std::cout << i << " " << res << " " << _RHS[i] << std::endl;
		
		// Subtract off the RHS
		res -= _RHS[i];

		// Compare to the current maximum and update if necessary
		maxMagRes = std::max( maxMagRes, std::abs(res) );
	}
	
	return maxMagRes;
}
