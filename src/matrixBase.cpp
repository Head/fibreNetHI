// Template specialisations.
#include "matrixBase.h"


//
// Sparse direct solver. Specialised and the SuperLU library requires different function calls depending
// on the type of scalar.
//

// std::complex<double> version.
template<>
int matrixBase< std::complex<double> >::solveSparseDirect()
{
	// Timing.
	using namespace std::chrono;
	high_resolution_clock::time_point startTime;
	duration<double> elaspedTime;

	// Need the SuperLU library to be installed (signalled by compiler macro 'SUPERLU').
#ifndef SUPERLU
	std::cerr << "Cannot solve using sparse direct method: SuperLU not available (install and clean make)." << std::endl;
	return -1;
#endif	
	
	// Shorthands.
	int n = numDOF(), nnz = numNonZero();

	//
	// Set up the matrix and RHS, with timing.
	//
	startTime = high_resolution_clock::now();

	// Get the matrix in CSR ('Compressed Sparse Row') format.
	const std::vector< std::complex<double> > *vals = nullptr;
	const std::vector<int> *cols = nullptr, *offsets = nullptr;
	_getCSRMatrix( &vals, &cols, &offsets );

	// SuperLU seems to require/prefer its own arrays, so create and copy over the data.
	doublecomplex *slu_vals    = doublecomplexMalloc(nnz);
	int           *slu_cols    = intMalloc(nnz);
	int           *slu_offsets = intMalloc(n+1);
	if( slu_vals==nullptr || slu_cols==nullptr || slu_offsets==nullptr )
	{
		std::cerr << "Could not allocate memory for the SuperLU matrix arrays." << std::endl;
		return -1;
	}
	for( auto i=0; i<nnz; i++ ) { slu_vals[i].r = vals->at(i).real(); slu_vals[i].i = vals->at(i).imag(); slu_cols[i] = cols->at(i); }
	for( auto i=0; i<n+1; i++ ) slu_offsets[i] = offsets->at(i);
	
	// Get the matrix into the format required by SuperLU.
	SuperMatrix A;
	zCreate_CompCol_Matrix( &A, n, n, nnz, slu_vals, slu_cols, slu_offsets, SLU_NC, SLU_Z, SLU_GE );
	
	//
	// RHS vector B: Dense matrix with one column.
	//
	SuperMatrix B;
	
	doublecomplex *slu_rhs = doublecomplexMalloc(n);
	if( slu_rhs==nullptr )
	{
		std::cerr << "Could not allocate memory for the SuperLU RHS vector array." << std::endl;
		return -1;
	}
	for( auto i=0; i<n; i++ ) { slu_rhs[i].r = _RHS[i].real(); slu_rhs[i].i = _RHS[i].imag(); }
	
	zCreate_Dense_Matrix( &B, n, 1, slu_rhs, n, SLU_DN, SLU_Z, SLU_GE );
	
	elaspedTime = duration_cast<duration<double>>( high_resolution_clock::now() - startTime );
	if( _log ) *_log << " - matrix and RHS set-up in " << elaspedTime.count() << " seconds." << std::endl;

	//
	// Options and stats.
	//
	superlu_options_t options;
	set_default_options( &options );
	options.ColPerm = MMD_ATA;
	options.Equil = NO;
	options.PivotGrowth = YES;
	
	SuperLUStat_t stats;
	StatInit( &stats );
		
	//
	// Permutation vectors. Preserve the column permutations if possible. SuperLU documentation suggests it should be possible
	// to re-use 'perm_c' when A maintains its sparsity pattern, but in trials this always gave an error and inspection of 
	// zgssc.c suggests any options.Fact other than DOFACT will give an error (as of serial SuperLU 5.2.1).
	//
	int *perm_r = intMalloc(n), *perm_c = intMalloc(n);
	if( perm_r==nullptr || perm_c==nullptr )
	{
		std::cerr << "Could not allocate memory for the SuperLU permutation vectors." << std::endl;
		return -1;
	}

	//
	// Solve with timing.
	//
	int info;
	SuperMatrix L, U;
	startTime = high_resolution_clock::now();

	zgssv( &options, &A, perm_c, perm_r, &L, &U, &B, &stats, &info );

	elaspedTime = duration_cast<duration<double>>( high_resolution_clock::now() - startTime );
	if( _log ) *_log << " - zgssv() returned in time " << elaspedTime.count() << " seconds." << std::endl;

	if( info!=0 )
	{
		std::cerr << "SuperLU solve failed with code " << info << "." << std::endl;
		return -1;
	}	
	
	//
	// Map solution (now in B) to this object's solution vector.
	//
	DNformat *store = (DNformat*) B.Store;
	doublecomplex *slu_sol = (doublecomplex*) store->nzval;
	for( auto i=0; i<n; i++ ) _sol[i] = { slu_sol[i].r, slu_sol[i].i };
		
	//	
	// Clear up.
	//
	StatFree( &stats );
	
	SUPERLU_FREE( perm_r );				// Column permutations maintained as long as possible.

	Destroy_SuperNode_Matrix(&L);
	Destroy_CompCol_Matrix(&U);

	Destroy_SuperMatrix_Store(&B);
	SUPERLU_FREE( slu_rhs );

	Destroy_CompCol_Matrix(&A);			// Also deallocates the arrays.

	return 0;
}

// double version.
template<>
int matrixBase<double>::solveSparseDirect()
{
	// Timing.
	using namespace std::chrono;
	high_resolution_clock::time_point startTime;
	duration<double> elaspedTime;

	// Need the SuperLU library to be installed.
#ifndef SUPERLU
	std::cerr << "Cannot solve using sparse direct method: SuperLU not available; check makefile." << std::endl;
	return -1;
#endif	
	
	// Shorthands.
	int n = numDOF(), nnz = numNonZero();

	//
	// Set up the matrix and RHS, with timing.
	//
	startTime = high_resolution_clock::now();

	// Get the matrix in CSR ('Compressed Sparse Row') format.
	const std::vector<double> *vals = nullptr;
	const std::vector<int> *cols = nullptr, *offsets = nullptr;
	_getCSRMatrix( &vals, &cols, &offsets );

	// SuperLU seems to require/prefer its own arrays, so create and copy over the data.
	double *slu_vals    = doubleMalloc(nnz);
	int    *slu_cols    = intMalloc(nnz);
	int    *slu_offsets = intMalloc(n+1);
	if( slu_vals==nullptr || slu_cols==nullptr || slu_offsets==nullptr )
	{
		std::cerr << "Could not allocate memory for the SuperLU matrix arrays." << std::endl;
		return -1;
	}
	for( auto i=0; i<nnz; i++ ) { slu_vals[i] = vals->at(i); slu_cols[i] = cols->at(i); }
	for( auto i=0; i<n+1; i++ ) slu_offsets[i] = offsets->at(i);
	
	// Get the matrix into the format required by SuperLU.
	SuperMatrix A;
	dCreate_CompCol_Matrix( &A, n, n, nnz, slu_vals, slu_cols, slu_offsets, SLU_NC, SLU_D, SLU_GE );
	
	//
	// RHS vector B: Dense matrix with one column.
	//
	SuperMatrix B;
	
	double *slu_rhs = doubleMalloc(n);
	if( slu_rhs==nullptr )
	{
		std::cerr << "Could not allocate memory for the SuperLU RHS vector array." << std::endl;
		return -1;
	}
	for( auto i=0; i<n; i++ ) slu_rhs[i] = _RHS[i];
	
	dCreate_Dense_Matrix( &B, n, 1, slu_rhs, n, SLU_DN, SLU_D, SLU_GE );
	
	elaspedTime = duration_cast<duration<double>>( high_resolution_clock::now() - startTime );
	if( _log ) *_log << " - matrix and RHS set-up in " << elaspedTime.count() << " seconds." << std::endl;

	//
	// Options and stats.
	//
	superlu_options_t options;
	set_default_options( &options );
	options.ColPerm = MMD_ATA;
	options.Equil = NO;
	options.PivotGrowth = YES;
	
	SuperLUStat_t stats;
	StatInit( &stats );
		
	//
	// Permutation vectors. Preserve the column permutations if possible. SuperLU documentation suggests it should be possible
	// to re-use 'perm_c' when A maintains its sparsity pattern, but in trials this always gave an error and inspection of 
	// zgssc.c suggests any options.Fact other than DOFACT will give an error (as of serial SuperLU 5.2.1).
	//
	int *perm_r = intMalloc(n), *perm_c = intMalloc(n);
	if( perm_r==nullptr || perm_c==nullptr )
	{
		std::cerr << "Could not allocate memory for the SuperLU permutation vectors." << std::endl;
		return -1;
	}

	//
	// Solve with timing.
	//
	int info;
	SuperMatrix L, U;
	startTime = high_resolution_clock::now();

	dgssv( &options, &A, perm_c, perm_r, &L, &U, &B, &stats, &info );

	elaspedTime = duration_cast<duration<double>>( high_resolution_clock::now() - startTime );
	if( _log ) *_log << " - zgssv() returned in time " << elaspedTime.count() << " seconds." << std::endl;

	if( info!=0 )
	{
		std::cerr << "SuperLU solve failed with code " << info << "." << std::endl;
		return -1;
	}	
	
	//
	// Map solution (now in B) to this object's solution vector.
	//
	DNformat *store = (DNformat*) B.Store;
	double *slu_sol = (double*) store->nzval;
	for( auto i=0; i<n; i++ ) _sol[i] = slu_sol[i];
		
	//	
	// Clear up.
	//
	StatFree( &stats );
	
	SUPERLU_FREE( perm_r );				// Column permutations maintained as long as possible.

	Destroy_SuperNode_Matrix(&L);
	Destroy_CompCol_Matrix(&U);

	Destroy_SuperMatrix_Store(&B);
	SUPERLU_FREE( slu_rhs );

	Destroy_CompCol_Matrix(&A);			// Also deallocates the arrays.

	return 0;
}
