#include "networkTriSpringCorrelated.h"


#pragma mark -
#pragma mark Constructors / destructor
#pragma mark -

// Construct from a (read-only) XMLParameters object
networkTriSpringCorrelated::networkTriSpringCorrelated( double X, double Y, const XMLParameters *params, ostream *log, const drivingBase *driving )
	: networkTriSpring(X,Y,params,log,driving)
{
	using namespace parameterParsing;

    // Sanity check: Do not want normal bond dilution on top of the dilution algorithm (currently).
    if( _p_E_W!=1.0 || _p_NW_SE!=1.0 || _p_NE_SW!=1.0 )
		throw BadParameter("[any/all random dilution parameters]","must be 1.0 or not defined with correlated dilution",__FILE__,__LINE__);
    
    // Parameter 'c' from the article, referred to as the 'correlation strength.'
    _correlationStrength = _params->scalarParam( netTSCorrn_correlationStrength );      // Will raise exception if not defined.

	if( _correlationStrength < 0.0 )
		throw BadParameter(netTSCorrn_correlationStrength,"must be non-negative",__FILE__,__LINE__);

    if( _correlationStrength >= 1.0 )
		throw BadParameter(netTSCorrn_correlationStrength,"must be less than 1.0",__FILE__,__LINE__);

    // Parameter 'f' from the article, referred to as the target fraction of occupied sites.
    _siteOccupation = _params->scalarParam( netTSCorrn_siteOccupation );      // Will raise exception if not defined.

	if( _siteOccupation <= 0.0 )
		throw BadParameter(netTSCorrn_siteOccupation,"must be positive",__FILE__,__LINE__);

    if( _siteOccupation >= 1.0 )
		throw BadParameter(netTSCorrn_siteOccupation,"must be less than 1.0",__FILE__,__LINE__);
}


#pragma mark -
#pragma mark Correlated lattice generation algorithm
#pragma mark -

// Adds springs between sites that become occupied following Model 1 of Zhang et al. (PRL 2019).
void networkTriSpringCorrelated::_initSprings()
{
	// Time this algorithm in case it starts to become a bottle-neck.
	using namespace std::chrono;
	high_resolution_clock::time_point startTime = high_resolution_clock::now();

	// Local storage for site occupation.
	std::vector<int> sites(_numNodes,0);

	// The target number of sites to occupy.
	unsigned int tgtOccupied = _numNodes * _siteOccupation;

	//
	// Occupy sites according to the algorithm. May require optimisation for high
	// requested site occupation fractions - could start from fully filled and remove sites?
	//
	auto numOccupied = 0u;
	while( numOccupied < tgtOccupied )
	{
		// Select random site.
		unsigned int
			i = pRNG::uniform() * _numX,
			j = pRNG::uniform() * _numY;

		// Sanity check for floating point errors giving values outside of range.
		if( i<0 || j<0 || i>=_numX || j>=_numY ) continue;

		// Also skip if this site is already occupied.
		if( sites[_globalIndex(i,j)] ) continue;

		// Count number of occupied sites in all 6 lattice directions.
		// Note _globalIndex(i,j) already handles periodic BCs.
		auto Nnn = sites[ _globalIndex(i-1,j) ]			// Left-right on lattice, so no half-shifts.
		         + sites[ _globalIndex(i+1,j) ];

		Nnn += sites[ _globalIndex(( j%2 ? i-1 : i   ),j+1) ]		// Up and to the left.
		     + sites[ _globalIndex(( j%2 ? i   : i+1 ),j+1) ];		// Up and to the right.

		Nnn += sites[ _globalIndex(( j%2 ? i-1 : i   ),j-1) ]		// Down and to the left;
			 + sites[ _globalIndex(( j%2 ? i   : i+1 ),j-1) ];		// matches 2 grid rows above.
	 
		// Occupation probability based on the rules of the model.
		auto prob = pow( 1-_correlationStrength, 6-Nnn );
		if( pRNG::uniform() < prob )
		{
			numOccupied++;
			sites[_globalIndex(i,j)] = 1;
		}
	}

	//
	// Add springs to adjacent occupied sites. Mirrors _initSprings() in parent class.
	//
	for( auto i=0u; i<_numX; i++ )
		for( auto j=0u; j<_numY; j++ )
			if( sites[_globalIndex(i,j)] )		// Must have this 'central' site occupied.
			{
				// Horizontal springs. Reminder that _globalIndex(i,j) incorporates periodic BCs.
				if( sites[_globalIndex(i+1,j)] )
					_trialConnection(i,j,i+1,j,1.0);							// '1.0' here is _p_E_W.

				// Diagonal springs 'up and to the left'.
				if( sites[_globalIndex(( j%2 ? i-1 : i ),j+1)] )
					_trialConnection( i, j, ( j%2 ? i-1 : i ), j+1, 1.0 );		// '1.0' is _p_NW_SE.

				// Diagonal springs 'up and to the right'.
				if( sites[_globalIndex(( j%2 ? i : i+1 ),j+1)] )
					_trialConnection( i, j, ( j%2 ? i : i+1 ), j+1, 1.0 );		// '1.0' is _p_NE_SW.
		}

	// DEBUG: Display site occupation lattice without half-shifts between rows. Will be upside-down!
	/*
	for( auto j=0u; j<_numY; j++ )
	{
		for( auto i=0u; i<_numX; i++ ) std::cout << sites[_globalIndex(i,j)];
		std::cout << endl;
	}
	*/

	//
	// Output to log, with timing information.
	//
	duration<double> elaspedTime = duration_cast<duration<double>>( high_resolution_clock::now() - startTime );
	if( _log )
		*_log << " - generated a correlated lattice with springs between occupied sites ("
		      << numOccupied << " from a maximum " << _numNodes << "), in "
			  << elaspedTime.count() << " seconds" << std::endl;
}

