#include "networkTriSpring.h"


#pragma mark -
#pragma mark Template specialisations for matrix_fillDisps()
#pragma mark -

// Fills the terms for the displacement equations that involve network node displacements.
template<>
void networkTriSpring::matrix_fillElasticTerms( matrixNetwork< std::complex<double> > *M, bool periodicShift ) const
{
	// Local variables.
	int n2;

	// Loop over all nodes.
	for( auto n1=0u; n1<_numNodes; n1++ )
	{
		// Loop over all springs connected to this node. Note that for isolated nodes this will be an empty loop.
		short k = -1;
		while( ++k<6 && (n2=_connections[6*n1+k])!=-1 )
		{
			smallSquareMatrix<2,double> local_Hij = __springHessian(n1,k);

			// Add the local matrix to the global.
			M->addDispsLocalHessian( local_Hij, n1, n2 );

			// Contribution to right-hand side vector. Note this is applied to the "other" node n2, which has a minus
			// sign, but then moved to the other side of the equation, which flips the sign back again.
			if( periodicShift )
			{
				double shift[2] = { _perBC_dispShifts[6*n1+k][0], _perBC_dispShifts[6*n1+k][1] };
				M->addDispsRHS_xy( local_Hij, n1, shift );
			}
		}
	}
}

template<>
void networkTriSpring::matrix_fillElasticTerms( matrixNetwork<double> *M, bool periodicShift ) const
{
	int n2;

	for( auto n1=0u; n1<_numNodes; n1++ )
	{
		// Skip isolated nodes before adding their contribution to the diagonal.
		if( !_numConnections[n1] ) continue;

		short k = -1;
		while( ++k<6 && (n2=_connections[6*n1+k])!=-1 )
		{
			smallSquareMatrix<2,double> local_Hij = __springHessian(n1,k);

			M->addDispsLocalHessian( local_Hij, n1, n2 );

			// Since the static solution has no drag term.
			if( periodicShift )
			{
				double shift[2] = { _perBC_dispShifts[6*n1+k][0], _perBC_dispShifts[6*n1+k][1] };
				M->addDispsRHS_xy( local_Hij, n1, shift );
			}
		}
	}
}


#pragma mark -
#pragma mark Constructors / destructor and initialisation
#pragma mark -

// Construct from a (read-only) XMLParameters object
networkTriSpring::networkTriSpring( double X, double Y, const XMLParameters *params, ostream *log, const drivingBase *driving )
	: networkBase(X,Y,params,log,driving)
{
	using namespace parameterParsing;

	//
	// Network parameters
	//
	_a = _params->scalarParam( netTS_latticeSpacing );
	if( _a <= 0.0 )
		throw BadParameter(netTS_latticeSpacing,"must be positive",__FILE__,__LINE__);

	_k = _params->scalarParam( netTS_springConstant );
	if( _k <= 0.0 )
		throw BadParameter(netTS_springConstant,"must be positive",__FILE__,__LINE__);

	//
	// Dilution. Must be one of: (i) isotropic dilution param, (ii) all 3 orientational params, or (iii) no dilution at all.
	//
	
	// Initialise to no dilution.
	_p_E_W = _p_NW_SE = _p_NE_SW = 1.0;

	// Isotropic dilution.
	if( _params->hasScalarParam(netTS_dilutionParam) )
	{
		// Get the value and check validity.
		double p = _params->scalarParam( netTS_dilutionParam );
		if( p<0.0 || p>1.0 )
			throw BadParameter(netTS_dilutionParam,"Must be in the range [0,1]",__FILE__,__LINE__);
		
		// Set to all 3 directional dilution parameters.
		_p_E_W = _p_NW_SE = _p_NE_SW = p;
	}
	
	// Anisotropic dilution.
	if(
		   _params->hasScalarParam(netTS_dilution_E_W  )
		|| _params->hasScalarParam(netTS_dilution_NW_SE)
		|| _params->hasScalarParam(netTS_dilution_NE_SW)
	)
	{
		// Make sure the isotropic dilution parameter has *not* been defined.
		if( _params->hasScalarParam(netTS_dilutionParam) )
			throw BadParameter(netTS_dilutionParam,"Cannot be defined simultaneously with orientational dilutions",__FILE__,__LINE__);
		
		// Make sure all 3 orientaional dilutions are defined. Check each separately to give a specific error message.
		if( !_params->hasScalarParam(netTS_dilution_E_W) )
			throw BadParameter(netTS_dilution_E_W,"All 3 orientational dilution paraneters (or none) must be defined)",__FILE__,__LINE__);

		if( !_params->hasScalarParam(netTS_dilution_NW_SE) )
			throw BadParameter(netTS_dilution_NW_SE,"All 3 orientational dilution paraneters (or none) must be defined)",__FILE__,__LINE__);

		if( !_params->hasScalarParam(netTS_dilution_NE_SW) )
			throw BadParameter(netTS_dilution_NE_SW,"All 3 orientational dilution paraneters (or none) must be defined)",__FILE__,__LINE__);

		// Extract the values.
		_p_E_W   = _params->scalarParam(netTS_dilution_E_W  );
		_p_NW_SE = _params->scalarParam(netTS_dilution_NW_SE);
		_p_NE_SW = _params->scalarParam(netTS_dilution_NE_SW);

		// Check each value for validity.
		if( _p_E_W<0.0 || _p_E_W>1.0 )
			throw BadParameter(netTS_dilution_E_W,"Must be in the range [0,1]",__FILE__,__LINE__);

		if( _p_NW_SE<0.0 || _p_NW_SE>1.0 )
			throw BadParameter(netTS_dilution_NW_SE,"Must be in the range [0,1]",__FILE__,__LINE__);

		if( _p_NE_SW<0.0 || _p_NE_SW>1.0 )
			throw BadParameter(netTS_dilution_NE_SW,"Must be in the range [0,1]",__FILE__,__LINE__);
	}


	//
	// 'Jiggling' of network nodes (prior to pre-stress). An amplitude of 0.0 means no jiggling.
	//
	__jiggleAmplitude = _params->scalarParamOrDefault( netTS_jiggleAmplitude, 0.0 );
	if( __jiggleAmplitude < 0.0 )
		throw BadParameter(netTS_jiggleAmplitude,"must be non-negative",__FILE__,__LINE__);


	//
	// Pre-stress.
	//

	// By default, no prestress and hence no relaxation required prior to applying the oscillatory strain.
	__preStress_solver = __preStressSolverType::noneRequired;

	// Check for pre-stress in the form of (some) natural spring lengths not equal to the lattice spacing.
	// Zero mean length means not specified - will use initial lattice spacing, possibly with 'jiggling.'
	__preStress_natLenWidth = _params->scalarParamOrDefault( netTS_natLenWidth, 0.0 );
	__preStress_natLenMean  = _params->scalarParamOrDefault( netTS_natLenMean , 0.0 );
	
	// Check the values are not obviously invalid.
	if( __preStress_natLenWidth<0.0 )
		throw BadParameter(netTS_natLenWidth,"Must be non-negative",__FILE__,__LINE__);

	if( __preStress_natLenMean<0.0 )
		throw BadParameter(netTS_natLenMean,"Must be non-negative",__FILE__,__LINE__);

	// If solver is needed, extract additional parameters; at least need a tolerance.
	if(
		__preStress_natLenWidth									// Randomness added to all l0.
		|| ( !__jiggleAmplitude && (__preStress_natLenMean!=_a && __preStress_natLenMean!=0.0) )	// Uniform lattice with mean l0 != lattice spacing a.
		|| (  __jiggleAmplitude && __preStress_natLenMean )		// Jiggled nodes with any specified mean l0.
	)
	{
		//
		// Choice of relaxation allgorithm.
		//
		if( _params->labelParam(netTS_preStressSolver_typeAttr)==netTS_preStressSolver_NewtonSparseDirect )
			__preStress_solver = __preStressSolverType::NewtonSparseDirect;

		if( _params->labelParam(netTS_preStressSolver_typeAttr)==netTS_preStressSolver_FIRE )
			__preStress_solver = __preStressSolverType::FIRE;
		
		if( _params->labelParam(netTS_preStressSolver_typeAttr)==netTS_preStressSolver_hybrid )
			__preStress_solver = __preStressSolverType::hybrid;

		// If still here and solver not yet specified, have a problem ...
		if( __preStress_solver==__preStressSolverType::noneRequired )
			throw BadParameter(netTS_preStressSolver_typeAttr,"Solver type not recognised",__FILE__,__LINE__);

		//
		// Parameters for the non-linear solver.
		//

		// All solvers require a final tolerance.
		__preStress_tol = _params->scalarParam( netTS_preStressSolver_tolerance );
		if( __preStress_tol <= 0.0 )
			throw BadParameter(netTS_preStressSolver_tolerance,"Must be positive",__FILE__,__LINE__);
		
		// The hybrid solver also requires a larger tolerance, for when to switch solvers.
		if( __preStress_solver==__preStressSolverType::hybrid )
		{
			__preStress_hybridTol = _params->scalarParam( netTS_preStressSolver_hybridTol );
			if( __preStress_hybridTol <= 0.0 )
				throw BadParameter(netTS_preStressSolver_hybridTol,"Must be positive",__FILE__,__LINE__);
			
			if( __preStress_hybridTol < __preStress_tol )
				throw BadParameter(netTS_preStressSolver_hybridTol,"Must be >= final tolerance",__FILE__,__LINE__);
		}
	}

	//
	// Pebble game-related options.
	//
	__pgRemoveRedundantBonds = _params->flagParamOrDefault( netTS_pgRemoveRedundant   , false );
	__pgOnlyLargestCluster   = _params->flagParamOrDefault( netTS_pgOnlyLargestCluster, false );

	//
	// Connectivity percolation with periodic BCs, as per the algorithm of Livraghi et al.,
	// J. Chem. Theory Comput. 17(10) (2021).
	//
	__cpConnPercDimToLog = _params->flagParamOrDefault( netTS_cpConnPercDimToLog, false );
}

// Post-construction initialisation. Called before linking to the fluid object.
void networkTriSpring::initialise()
{
	networkBase::initialise();

	// Get the number of nodes in each direction.
	_initLatticeDimensions();

	// Initialise the lattice arrays and connections. Will also perform pebble game and connectivity dimension
	// calculation if specified by the parameters.
	_initLattice();

	// Relax prestresses to get mechanical equilibrium in the absence of a fluid.
	__relaxPreStress();

	// Check the initial network for connectivity and force balance. Throws an exception if it finds a problem.
	checkInitialNetwork();

	//
	// Output to log file what the parameters were actually parsed as.
	//
	*_log << " - lattice dimensions (" << _numX << "," << _numY << "); total " << _numNodes << " nodes" << std::endl;
	*_log << " - lattice spacing a=" << _a << std::endl;
	*_log << " - spring constant k=" << _k << std::endl;

	if( _p_E_W==_p_NW_SE && _p_E_W==_p_NE_SW && _p_E_W<1.0 )
		*_log << " - isotropic dilution p=" << _p_E_W << std::endl;
	else if( _p_E_W<1.0 )
		*_log << " - anisotropic dilution parameters E-W=" << _p_E_W << ", NW-SE=" << _p_NW_SE << ", NE-SW=" << _p_NE_SW << std::endl;

	*_log << " - coordination number z=" << coordinationNumber() << std::endl;
	*_log << " - " << numConnections() << " connections, from a maximum of " << maxConnections() << std::endl;

	if( __preStress_natLenWidth || __preStress_natLenMean!=_a)
		*_log << " - natural lengths normally distributed with mean " << __preStress_natLenMean
		     << " (zero means initial lengths), and standard deviation " << __preStress_natLenWidth << std::endl;
	
	if( __jiggleAmplitude )
		*_log << " - nodes 'jiggled' with amplitude " << __jiggleAmplitude << std::endl;
	
	if( __pgRemoveRedundantBonds )
		*_log << " - all bonds identified as redundant (=overconstrained) by the pebble game algorithm were removed" << std::endl;

	if( __pgOnlyLargestCluster )
		*_log << " - all bonds not in the largest rigid cluster were removed" << std::endl;

	if( _numReducedNodes < _numNodes )
		*_log << " - reduced set of " << _numReducedNodes << " non-isolated nodes used by solvers" << std::endl;
}

void networkTriSpring::_initLatticeDimensions()
{
	using namespace parameterParsing;

	// Want an isotropic lattice, which restricts the (ratio of the) box dimensions once 'a' is known.
	// For simplicity, also assume that the number of nodes in each direction is even.
	if( remainder(_X,2*_a) > _commensurateTol )
		throw BadParameter(netTS_latticeSpacing,"not commensurate with X-dimension (or odd number of nodes)",__FILE__,__LINE__);

	if( remainder(_Y,2*_a*_root3over2) > _commensurateTol )
		throw BadParameter(netTS_latticeSpacing,"not commensurate with Y-dimension (or odd number of nodes)",__FILE__,__LINE__);

	// Fix the number of nodes in the x and y-directions.
	_numX = static_cast<unsigned int>( round(_X/              _a) );
	_numY = static_cast<unsigned int>( round(_Y/(_root3over2*_a)) );
}

// Initialise the lattice once _numX, _numY and _a have been set to sensible values.
void networkTriSpring::_initLattice()
{
	// Sometimes useful to have the total number of nodes to hand; calculate once now.
	_numNodes = _numX * _numY;
	
	// Undeformed node positions.
	_nodePositions.resize( 2*_numNodes, 0.0 );			// With ordering ( x0, y0, x1, y1, ... ); each x/y real.

	// Solution vector for the nodes: Complex displacement amplitudes for x and y directions
	_disps.resize( 2*_numNodes, {0.0,0.0} );			// Same ordering as _nodePositions, but each x/y now complex.
	
	// The natural lengths for every spring. Defaults to the lattice spacing.
	_l0.resize( 6*_numNodes );

	// Maximum no. contacts per node is 6. '-1' means no connection.
	_connections.resize( 6*_numNodes, -1 );

	// Number of connections index by node.
	_numConnections.resize( _numNodes, 0 );
	
	// Shifts in the (real) displacement vector for those connections that cross a Lees-Edwards(-like) boundary.
	_perBC_dispShifts.resize( 6*_numNodes, {0.0,0.0} );
	
	// Pre-calculate tangent vectors; convenient to do this in _trialConnection().
	_unitTangents.resize( 12*_numNodes, 0.0 );
	
	//
	// Save memory by removing extra capacity from the std::vector's (since network topology never changes).
	//
	_nodePositions   .shrink_to_fit();
	_disps           .shrink_to_fit();
	_l0              .shrink_to_fit();
	_connections     .shrink_to_fit();
	_perBC_dispShifts.shrink_to_fit();
	_unitTangents    .shrink_to_fit();

	//
	// Initial node positions to the regular lattice with spacing _a.
	//
	for( auto i=0u; i<_numX; i++ )
		for( auto j=0u; j<_numY; j++ )
		{
			auto n = _globalIndex(i,j);

			_nodePositions[2*n  ] = _latticePos_x(n);
			_nodePositions[2*n+1] = _latticePos_y(n);
		}
	
	// 'Jiggle' nodes now, prior to insertion of springs.
	__jiggleNodes();

	// Add the springs. Can be overwritten by child classes.
	_initSprings();

	// Make any modifications resulting from the application of the pebble game algorithm.
	__applyPebbleGame();

	// Check connectivity percolation now.
	__connectivityPercolation();

	// Set the total number of connections now the topology is fixed, without double-counting.
	_totalNumConnections = std::accumulate(_numConnections.begin(),_numConnections.end(),0)/2;

	// Set the tangent vectors and Lees-Edwards shifts given the connections just made.
	__setTgtsAndShifts();

	// Generate mapping to the reduced set of nodes that needs to be solved for (by some rationale).
	_generateReducedNodeMap();
}

// Move all nodes by a random amount to remove colinear bonds.
void networkTriSpring::__jiggleNodes()
{
	if( !__jiggleAmplitude ) return;		// Zero means no jiggling; have already checked not negative.

	// Change all node positions.
	for( auto i=0u; i<2*_numNodes; i++ )
		_nodePositions[i] += __jiggleAmplitude * pRNG::normal();
}

// Attempts to add springs to every possible combination, with some not added as defined by the dilution parameter.
void networkTriSpring::_initSprings()
{
	// Loop over all possible connections and call _potentialConection() for each (where any dilution is applied).
	// No periodicity applied at this stage.
	for( auto i=0u; i<_numX; i++ )
		for( auto j=0u; j<_numY; j++ )
		{
			_trialConnection( i, j, i+1,               j  , _p_E_W   );			// Horizontal spings
			_trialConnection( i, j, ( j%2 ? i-1 : i ), j+1, _p_NW_SE );			// Up and to the left
			_trialConnection( i, j, ( j%2 ? i : i+1 ), j+1, _p_NE_SW );			// Up and to the right
		}
}

// Attempts to make a single connection between the two given nodes, taking into account (possiblu
// orientation-dependent) dilution unless the 'noDilution' flag is set to 'true'. Also sets the natural length, but not the tangent
// vector or the Lees-Edwards shift which will need to be set afterwards.
void networkTriSpring::_trialConnection( int i1, int j1, int i2, int j2, double dilution )
{
	// Check to see if this bond is to be 'diluted;' if so, do nothing. Don't check if 1.0 (pRNG may occasionaly return 1.0!)
	if( dilution<1.0 )
		if( pRNG::uniform() > dilution ) return;

	// If still here, add a bond between the two nodes.
	unsigned int index1 = _globalIndex(i1,j1), index2 = _globalIndex(i2,j2);

	// If the bond already exists, do nothing.
	if( _hasConnection(index1,index2) ) return;
	
	// Natural length for this spring. Use the current separation (which may not be the lattice spacing
	// if 'jiggling' was selected), or the mean l0 specified for the pre-stress. Either way, then add any
	// further random amounts for pre-stress.
	std::array<double,2> dx = _nodeSeparation(index1,index2);
	double l0 = ( __preStress_natLenMean ? __preStress_natLenMean : std::sqrt(dx[0]*dx[0] + dx[1]*dx[1]) );

	l0 += __preStress_natLenWidth*pRNG::normal();		// Add any requested randomness to l0.

	if( l0<=0.0 ) throw SystemError("initialising natural spring lengths (l0)","found an l0<=0.0",__FILE__,__LINE__);

	// Add index2 to the connections list of index1.
	unsigned short k=0u;
	while( _connections[6*index1+k] != -1 )
	{
		if( ++k ==6 )
			throw SystemError("network initialisation","exceeded 6 connections",__FILE__,__LINE__);
	}
	_connections   [6*index1+k] = index2;
	_l0            [6*index1+k] = l0    ;
	_numConnections[  index1  ]++;

	// Sim. connect index1 to index2
	k=0;
	while( _connections[6*index2+k] != -1 )
	{
		if( ++k ==6 )
			throw SystemError("network initialisation","exceeded 6 connections",__FILE__,__LINE__);
	}
	_connections   [6*index2+k] = index1;
	_l0            [6*index2+k] = l0    ;
	_numConnections[  index2  ]++;
}

// Applies the pebble game method and removes bonds according to the selected options.
void networkTriSpring::__applyPebbleGame()
{
	// Return immediately if no option selected that utilises the pebble game.
	if( !__pgOnlyLargestCluster && !__pgRemoveRedundantBonds ) return;

	// Initialise the timer and related quantities.
	using namespace std::chrono;
	high_resolution_clock::time_point startTime;

	//
	// Determine sets of redundant (overconstrained) and independent bonds.
	//

	// Time the application of the pebble game algorithm.
	startTime = high_resolution_clock::now();

	// Initialise the pebble game object.
	__pebbleGame = std::unique_ptr<pebbleGame>( new pebbleGame(_numNodes) );

	// Assemble a list of unique bonds (=edges in the graph).
	std::list< std::pair<int,int> > bonds;

	// Add bonds as pairs of edges that have a connection, avoiding double counting.
	int n2;
	for( auto n1=0u; n1<_numNodes; n1++ )
	{
		short k = -1;
		while( ++k<6 && (n2=_connections[6*n1+k])!=-1 )
			if( int(n1)>n2 )
				bonds.push_back( std::make_pair(n1,n2) );
	}

	// Perform the pebble game to determine which of these bonds are redundant.
	__pebbleGame->addEdges( bonds );

	// Output timing and result of the pebble game to the log file.
	duration<double> elaspedTime = duration_cast<duration<double>>( high_resolution_clock::now() - startTime );
	if( _log )
		*_log << " - pebble game method found "
			  << __pebbleGame->numRedundantEdges() << " redundant and "
			  << __pebbleGame->numIndependentEdges() << " independent bonds in "
			  << elaspedTime.count() << " seconds" << std::endl;
	
	// Remove those bonds identified by the pebble game as redundant.
	if( __pgRemoveRedundantBonds )
	{
		for( auto &edge : *__pebbleGame->redundantEdges() )
			__removeBond( edge.first, edge.second );
		
		if( _log )
			*_log << " - removed all " << __pebbleGame->numRedundantEdges()
			      << " redundant bonds as returned by the pebble game method" << std::endl;
	}

	//
	// Post-processing to determine all rigid clusters and keep the largest.
	//
	if( __pgOnlyLargestCluster )
	{
		// Reset the timer.
		startTime = high_resolution_clock::now();

		// Perform the post-processing.
		__pebbleGame->postProcess();

		// Get the largest cluster; will be the rigid backbone if it spans the system
		auto bbClust = __pebbleGame->largestRigidCluster();

		// Output timing and other information.
		duration<double> elaspedTime = duration_cast<duration<double>>( high_resolution_clock::now() - startTime );
		if( _log )
			*_log << " - pebble game post-processing found "
			      << __pebbleGame->numRigidClusters() << " rigid cluster(s), the largest comprising "
				  << bbClust.size() << " nodes, in "
				  << elaspedTime.count() << " seconds" << std::endl;

		// Loop over all bonds, and remove any with an end node that is not in the largest cluster.
		// Note this only considers bonds that exist now, so any redundant bonds remove above will
		// not be checked, and there will be no error for repeatedly removing the same bond.
		int n2;
		for( auto n1=0u; n1<_numNodes; n1++ )
		{
			unsigned short k = 0u;
			while( k<6 && (n2=_connections[6*n1+k])!=-1 )
				if( !bbClust.count(n1) || !bbClust.count(n2) )
					__removeBond( n1, n2 );
				else
					k++;
		}

		// Tell the world what we've done ...
		if( _log )
			*_log << " - removed all bonds not between pairs of nodes belonging to the largest rigid cluster" << std::endl;
	}

	//
	// Uncomment for debugging (consistency checks). Can be slow.
	//
	// __pebbleGame->checkEdgeConsistency();
	// __pebbleGame->checkClusterConsistency();

	// No longer need the pebble game object, so free up memory.
	__pebbleGame.reset();
}

// Uses the algorithm of Livraghi et al. (2021), implemented in a separate class, to determine and
// output to the log file, the connectivity dimension of the network. Note that the reduced node
// map does not exist at this stage, but don't need it anyway.
void networkTriSpring::__connectivityPercolation()
{
	// Only if selected by the requisite parameter.
	if( !__cpConnPercDimToLog ) return;

	// Time this in case it start to get slow.
	using namespace std::chrono;
	high_resolution_clock::time_point startTime = high_resolution_clock::now();

	// Need a map from edges to periodic shifts, here both of type std::array<int,2> as d=2 always.
	// Also need a set of node indices, which ned not be contiguous.
	std::set<int> nodeIndices;
	std::map< std::array<int,2>, std::array<int,2> > edgesWithShifts;

	// Loop through all bonds, avoiding double-counting.
	int n2;
	for( auto n1=0u; n1<_numNodes; n1++ )
	{
		short k = -1;
		while( ++k<6 && (n2=_connections[6*n1+k])!=-1 )
			if( int(n1)>n2 )
			{
				// Add nodes indices to the set.
				nodeIndices.insert( n1 );
				nodeIndices.insert( n2 );

				// Get the (i,j) indices for each node using non-reduced node indices.
				int i1, j1, i2, j2;
				_splitGlobal( n1, &i1, &j1 );
				_splitGlobal( n2, &i2, &j2 );

				// Edge from n1 to n2; must have same "direction" as the periodic offset to come.
				std::array<int,2> edge{ int(n1), int(n2) };

				// For these triangular spring networks, an index difference of 2 or more must mean periodic wrapping.
				std::array<int,2> shift{0,0};

				if( i2 < i1-1 ) shift[0] =  1;		// Second node actually in periodic copy to the right, etc.
				if( i2 > i1+1 ) shift[0] = -1;		// - x-indices increase from left to right.

				if( j2 < j1-1 ) shift[1] =  1;		// Second node actually in periodic copy 'below'. etc.
				if( j2 > j1+1 ) shift[1] = -1;		// - y-indices increase from bottom to top.
					// Because n1>n2, this latter case can never happen - leave in for possible future changes.

				// Add the egde and the shift to the std::map.
				edgesWithShifts.emplace( std::make_pair(edge,shift) );
			}
	}

	// The Livraghi et al. algorithm is implemented in a separate class.
	percPerBC<2> connPercDim;

	// In fact, the percPerBC<> class also determines clusters and the percolation dimension for each of those,
	// but for now, just output the information requested. Also output timing information.
	duration<double> elaspedTime = duration_cast<duration<double>>( high_resolution_clock::now() - startTime );
	if( _log )
		*_log << " - connectivity percolation dimension "
		      << connPercDim.percolationDimension(nodeIndices,edgesWithShifts)
		      << ", with "
			  << connPercDim.numClusters() << " disjoint cluster(s)"
			  << " (calculation took " << elaspedTime.count() << "s)"
			  << std::endl;
}

// Sets the tanget vectors and Lees-Edwards shifts assuming current node positions and fixed connectivity.
void networkTriSpring::__setTgtsAndShifts()
{
	// Make sure all node positions are wrapped to the primary cell.
	for( auto n=0u; n<_numNodes; n++ )
		_wrapToPrimaryCell( _nodePositions[2*n], _nodePositions[2*n+1] );

	// Loop through all nodes and connections.
	for( auto n1=0u; n1<_numNodes; n1++ )
	{
		unsigned short k1=-1;
		int n2;

		while( ++k1<6 && (n2=_connections[6*n1+k1])!=-1 )
		{
			// Shorthands for the positions of both nodes at the ends of the connection.
			double
				x1 = _nodePositions[2*n1  ],
				y1 = _nodePositions[2*n1+1],
				x2 = _nodePositions[2*n2  ],
				y2 = _nodePositions[2*n2+1];

			// Fill the tangent vector and shift for n1->n2.
			__fillTangent( 6*n1+k1, n1, n2 );
			_perBC_dispShifts[6*n1+k1] = _driving->getDispShift(x1,y1,x2,y2);

			// Local index k for n2->n1 generally different to n1->n2; loop to find.
			unsigned short k2 = -1;
			while( int(n1) != _connections[6*n2+(++k2)] );

			__fillTangent( 6*n2+k2, n2, n1 );
			_perBC_dispShifts[6*n2+k2] = _driving->getDispShift(x2,y2,x1,y1);
		}
	}
}

// Removes a single bond from the system, but only updates the _l0 and _connections containers,
// _not_ the tangents or shifts, which will need to be (re-)calculated after the removal.
void networkTriSpring::__removeBond( int n1, int n2 )
{
	// Find the index of connections for n1 corresponding to n2.
	short k=-1;
	while( _connections[6*n1+(++k)]!=n2 && k<6 );

	if( k==6 ) throw SystemError("bond removal","bond between given nodes does not exist",__FILE__,__LINE__);

	// Shift all connections and l0's 'one to the left'.
	for( ; k<6; k++ )
	{
		_connections[6*n1+k] = ( k==5 ? -1 : _connections[6*n1+k+1] );
		_l0[6*n1+k] = ( k==5 ? 0.0 : _l0[6*n1+k+1] );
	}
	_numConnections[n1]--;

	// Same again for the other node.
	k=-1;
	while( _connections[6*n2+(++k)]!=n1 && k<6 );

	if( k==6 ) throw SystemError("bond removal","bond between given nodes does not exist",__FILE__,__LINE__);

	// Shift all connections and l0's 'one to the left'.
	for( ; k<6; k++ )
	{
		_connections[6*n2+k] = ( k==5 ? -1 : _connections[6*n2+k+1] );
		_l0[6*n2+k] = ( k==5 ? 0.0 : _l0[6*n2+k+1] );
	}
	_numConnections[n2]--;
}


#pragma mark -
#pragma mark Relax prestress
#pragma mark -

// Relaxes pre-stress as per the specified algorithm.
void networkTriSpring::__relaxPreStress()
{
	// Return immediately if no relaxation required.
	if( __preStress_solver==__preStressSolverType::noneRequired ) return;

	// Message to stdout as this can be slow.
	std::cout << "Relaxing prestress using the specified non-linear algorithm." << std::endl;

	// Initialise the timer and related quantities.
	using namespace std::chrono;
	high_resolution_clock::time_point startTime;
	startTime = high_resolution_clock::now();

	// Call the required non-linear solver.
	int retVal;
	switch( __preStress_solver )
	{
		// Pure FIRE. Robust but slow (scales badly with num. DOF).
		case __preStressSolverType::FIRE:
			retVal = __preStress_FIRE();
			break;

		// Pure Newton-Sparse Direct. Fast but can converge to unphysical solutions.
		case __preStressSolverType::NewtonSparseDirect:
			retVal = __preStress_NewtonSparseDirect();
			break;
		
		// Hybrid: FIRE followed by Newton-Sparse Direct, switching at a user-defined tolerance.
		case __preStressSolverType::hybrid:
			retVal = __preStress_FIRE( __preStress_hybridTol );
			if( retVal>=0 ) retVal = __preStress_NewtonSparseDirect();
			break;

		// Should never reach here.
		default:
			std::cerr << "Unrecognised pre-stress solver ( in " << __FILE__ << ", line " << __LINE__ << ")." << std::endl;
			return;
	}

	// Negative return value means some sort of failure.
	if( retVal<0 ) throw runtime_error( "Pre-stress relaxation failed." );

	// Send the total time taken and final maximum force imbalance to the log file.
	duration<double> elaspedTime = duration_cast<duration<double>>( high_resolution_clock::now() - startTime );
	if( _log )
		*_log << " - pre-stress relaxed in time " << elaspedTime.count() << " seconds;"
			  << " final max. force imbalance=" << _maxForceMagSqrd() << "." << std::endl;
}

// FIRE for relaxation of the pre-stress. Optional tolerance; defaults to __preStress_tol.
int networkTriSpring::__preStress_FIRE( double tol )
{
	int nIters;

	// Copy nodes to be solved for into a separate, local vector.
	std::vector<double> posn_reduced( 2*_numReducedNodes );
	posn_reduced.shrink_to_fit();
	_copyToReduced( _nodePositions, posn_reduced );

	// Initialise the minimiser. Only use 1 thread as (a) installed SuperLU is serial anyway, and (b) for problem sizes
	// that can be solved in a reasonable time, vectors are so small that advantages of parallelisation are minimal.
	minFIRE min( 2*numReducedNodes(), 1 );      // Num. DOF, num threads. Can also specify MD integrator.

	try
	{
		// Start the minimisation. Returns the number of iterations taken.
		nIters = min.minimise(
			posn_reduced.data(),
			[this,&posn_reduced]( const double *U, double *F )
			{
				_copyFromReduced(posn_reduced,_nodePositions);		// Updates _nodePositions with current solution U.
				__setTgtsAndShifts();								// Re-calculates tgts and BC shifts from _nodePositions.
				__fillForceVector_reduced(F);						// Fills the force vector for reduced nodes only.
			},
			( tol ? tol : __preStress_tol ),		// Tolerance; defaults to the overall tolerance.
			0.01,									// Initial dt.
			0.1,									// Initial alpha
    	    10000									// Max. iters per DOF.
		);

		// FIRE does not shift node positions as all updates are equal-and-opposite (some may drift through the
		//periodic boundaries and be re-assigned to the primary copy, so the mean node position may change).

		// Return with the number of iterations.
		return nIters;
	}
	catch( const std::exception &err )
	{
		// Failure message to stderr as well as the log file.
		std::cerr << "Pre-stress relaxation by FIRE failed: " << err.what() << std::endl;
		*_log << " - pre-stress relaxation by FIRE failed: " << err.what();

		return -1;
	}
}

// Newton's method, with sparse direct solver for the linear solver.
int networkTriSpring::__preStress_NewtonSparseDirect()
{
	// We may already have converged, if this was part of a hybrid solver and the tolerances were equal.
	if( _maxForceMagSqrd()<__preStress_tol ) return 0;

	// Local variables and shorthands.
	int m;
	auto maxIters=100;
	std::array<double,2> meanPosBefore, meanPosAfter;

	// Will need a force vector for the RHS of the linearised equations.
	std::vector<double> F( 2*_numReducedNodes );

	// Use a local matrix for the linearised problem. No log output for this matrix, hence 'nullptr'.
	matrixNetwork<double> M( _numReducedNodes, 2*_numReducedNodes, nullptr, &_reducedNodeMap );

	// Main iteration loop.
	auto nIters = 0u;
	do
	{
		// Set linear matrix problem given current node positions.
		M.clearSameSparsity();
		matrix_fillElasticTerms( &M, false );		// Final 'false' means ignore periodic shifts.

		// Fill the force vector with the current net forces on each node, and use to set the RHS vector.
		__fillForceVector_reduced( F.data() );
		for( auto n=0u; n<numNodes(); n++ )
			if( (m=_reducedNodeMap[n])!=-1 )
				M.addDispsRHS_xy( n, &F[2*m] );


		// Linear solve using a sparse direct solver.
		auto retVal = M.solveSparseDirect();
		if( retVal<0 ) return -1;			// Solver will have already given an error message.

		// Get the mean node position prior to updating them all.
		meanPosBefore = _meanNodePosn();

		// Increment node positions with the solution to the linearised problem.
		for( auto n=0u; n<numNodes(); n++ )
			if( (m=_reducedNodeMap[n]) != -1 )
			{
				_nodePositions[2*n  ] += M.disps()[                 m];
				_nodePositions[2*n+1] += M.disps()[_numReducedNodes+m];
			}

		// Ensure the mean node position has not drifted. Does not affect evaluation of G^{*} if omitted,
		// but helps to ensure overall network position is not too far from the original version.
		meanPosAfter = _meanNodePosn();
		for( auto n=0u; n<numNodes(); n++ )
			for( auto k=0u; k<2; k++ )
				_nodePositions[2*n+k] += meanPosBefore[k] - meanPosAfter[k];

		// Re-linearise tangents for these new node positions.
		__setTgtsAndShifts();

		// Debugging.
//		std::cout << "After " << nIters+1 << " iterations, residual is " << _maxForceMagSqrd() << "." << std::endl;

	} while( ++nIters<(unsigned int)maxIters && _maxForceMagSqrd()>__preStress_tol );

	return nIters;
}


#pragma mark -
#pragma mark Network energies and forces
#pragma mark -

// Sets the node position to the given 2-vector. Will also update the tangent vectors.
void networkTriSpring::__setNodePosition( int n, std::array<double,2> new_x )
{
	_nodePositions[2*n  ] = new_x[0];
	_nodePositions[2*n+1] = new_x[1];

	_wrapToPrimaryCell( _nodePositions[2*n], _nodePositions[2*n+1] );

	// Will need to re-calculate the tangent vectors for all connected nodes, in both directions.
	short k = -1;
	int   n2;
	while( ++k<6 && (n2=_connections[6*n+k])!=-1 )
	{
		__fillTangent( 6*n+k, n, n2 );

		// Local index k for n2->n generally different to n->n2; loop to find.
		short k2 = -1;
		while( n != _connections[6*n2+(++k2)] );
		__fillTangent( 6*n2+k2, n2, n );
	}
}

// Increments the node position. Also updates the tangent vectors.
void networkTriSpring::__incrementNodePosition( int n, std::array<double,2> dx )
{
	std::array<double,2> new_x{ _nodePositions[2*n]+dx[0], _nodePositions[2*n+1]+dx[1] };
	__setNodePosition(n,new_x);
}

// The net force on a single node using positions alone (i.e. ignoring displacemnets).
std::array<double,2> networkTriSpring::__netForceOnNode( int n ) const
{
	std::array<double,2> fNet{0.0,0.0};

	short k = -1;
	int n2;
	while( ++k<6 && (n2=_connections[6*n+k])!=-1 )
	{
		double fScalar = __springTension(n,k);

		fNet[0] += fScalar * _unitTangents[2*(6*n+k)  ];
		fNet[1] += fScalar * _unitTangents[2*(6*n+k)+1];

		if( _unitTangents[2*(6*n+k)]==_unitTangents[2*(6*n+k)+1] && _unitTangents[2*(6*n+k)]==0.0 )
			throw SystemError("calculation of nodal force","tangent vector null",__FILE__,__LINE__);
	}

	return fNet;
}

// The energy associated with a single node; here the sum of spring energies for all connected springs.
double networkTriSpring::__nodeEnergy( int n ) const
{
	double sum = 0.0;
	short k = -1;
	int n2;
	while( ++k<6 && (n2=_connections[6*n+k])!=-1 )
	{
		std::array<double,2> dx = _nodeSeparation(n,n2);						// From n1 to n2.

		double dl = std::sqrt(dx[0]*dx[0]+dx[1]*dx[1]) - _l0[6*n+k];			// Extension.
		double kSpring = __springStiffness(n,k);								// Stiffness.

		sum += 0.5 * kSpring * dl * dl;
	}

	return sum;
}

// The tension in a single spring; positive for positive extension.
double networkTriSpring::__springTension( int n, short k ) const
{
	std::array<double,2> dx = _nodeSeparation(n,_connections[6*n+k]);		// From n to "n2".

	double
		dl = std::sqrt(dx[0]*dx[0]+dx[1]*dx[1]) - _l0[6*n+k],				// Extension.
		ks = __springStiffness(n,k);										// Spring constant.

	return ks * dl;
}

// Gradient of the spring tension.
double networkTriSpring::__springStiffness( int n, short k ) const
{
	return _k;
}

// Returns the local Hessian for forces on node n due to node at other end of bond index k.
smallSquareMatrix<2,double> networkTriSpring::__springHessian( unsigned int n, unsigned short k ) const
{
	// Separation vector between the two node's undeformed positions.
	std::array<double,2> dx = _nodeSeparation(n,_connections[6*n+k]);

	// Pre-tension, current length and stiffness.
	double length     = std::sqrt( dx[0]*dx[0] + dx[1]*dx[1] );
	double preTension = __springTension  (n,k);
	double stiffness  = __springStiffness(n,k);

	if( !length ) throw SystemError("generating local Hessian","zero spring length",__FILE__,__LINE__);

	// Get the local Hessian for this spring.
	smallSquareMatrix<2,double> Hij;
	double P_ij;

	for( unsigned short i=0; i<2; i++ )
		for( unsigned short j=0; j<2; j++ )
		{
			P_ij = dx[i] * dx[j] / (length*length);				// Longitudinal projection.

			// Change to longitudinal tension.
			Hij.set( i, j, stiffness * P_ij );	

			// Rotation of prestress.
			Hij.addTo( i, j, (preTension/length)*( (i==j) - P_ij ));
		}

	if( Hij[0]==Hij[1] && Hij[0]==Hij[2] && Hij[0]==Hij[3] && Hij[0]==0.0 )
		throw SystemError("calculation of local Hessian","all components zero",__FILE__,__LINE__);

	return Hij;
}

// Returns the local Hessian for forces on node n due to all connected springs.
smallSquareMatrix<2,double> networkTriSpring::_localHessian( unsigned int n ) const
{
	smallSquareMatrix<2,double> Hij;

	Hij.clear();

	short k = -1;
	while( ++k<6 && _connections[6*n+k]!=-1 )
		Hij += __springHessian(n,k);

	return Hij;
}

// Fills the reduced (i.e. only for nodes to be solved for) force vector without double counting.
void networkTriSpring::__fillForceVector_reduced( double *F ) const
{
	// Clear the reduced force vector.
	for( auto i=0u; i<2*_numReducedNodes; i++ ) F[i] = 0.0;

	//
	// Sum all spring forces and apply equal-and-opposite to the nodes.
	//
	int n2, m1, m2;
	for( auto n1=0u; n1<_numNodes; n1++ )
	{
		// Use the mapped index for this node.
		m1 = _reducedNodeMap[n1];

		// Get the net force due to all springs connected to this node. Use the network indices n1, n2
		// when calculating the force, but the re-indexed m1, m2 when adding results to the force vector.
		short k = -1;
		while( ++k<6 && (n2=_connections[6*n1+k])!=-1 )
		{
			if( n2>int(n1) ) continue;				// Avoid double counting.

			m2 = _reducedNodeMap[n2];		// No need to check if n2 is connected or not.

			double
				fScalar = __springTension(n1,k),
				f_x     = fScalar * _unitTangents[2*(6*n1+k)  ],
				f_y     = fScalar * _unitTangents[2*(6*n1+k)+1];

			F[2*m1  ] += f_x;					// Update the main force vector.
			F[2*m1+1] += f_y;

			F[2*m2  ] -= f_x;					// Newton III.	
			F[2*m2+1] -= f_y;
		}
	}
}


#pragma mark -
#pragma mark Utility routines
#pragma mark -

// The global node index given x and y indices; includes periodic wrapping
unsigned int networkTriSpring::_globalIndex( int i, int j ) const noexcept
{
	if( i < 0                        ) i += _numX;
	if( i >= static_cast<int>(_numX) ) i -= _numX;				// To avoid compiler warnings on Desktop

	if( j < 0                        ) j += _numY;
	if( j >= static_cast<int>(_numY) ) j -= _numY;

	return i + j*_numX;
}

// Fills in the tangent vector for the given index and connection (defined by local indices)
void networkTriSpring::__fillTangent( unsigned int index, int i1, int j1, int i2, int j2 )
{
	__fillTangent( index, _globalIndex(i1,j1), _globalIndex(i2,j2) );
}

// Fills in the tangent vector for the given index and connection (defined by local indices)
void networkTriSpring::__fillTangent( unsigned int index, int n1, int n2 )
{
	// The separation vector between (unperturbed) node positions.
	std::array<double,2> dx = _nodeSeparation(n1,n2);

	// Normalise
	double mag = sqrt( dx[0]*dx[0] + dx[1]*dx[1] );
	if( !mag )
		throw SystemError("tangent vectors initialisation","zero magnitude prior to normalisation",__FILE__,__LINE__);
	
	// Fill in the vector
	_unitTangents[2*index  ] = dx[0] / mag;
	_unitTangents[2*index+1] = dx[1] / mag;
}

// x- and y-coordinates of the un-displaced node. Note need i and j for x because of the alternating half-shift;
// also specify both for y for consistency. Note the partial-cell increments to avoid nodes 'on' the boundary
double networkTriSpring::_latticePos_x( int i, int j ) const noexcept
	{ return _a*( 0.25 + (j%2?i:i+0.5) ); }			// Quarter-cell shift (half would put nodes on the boundary)
double networkTriSpring::_latticePos_y( int i, int j ) const noexcept
	{ return _a*(j+0.5)*_root3over2; }				// Half-cell shift vertically

// Wraps the given x and y-coords to the primary cell in-place.
void networkTriSpring::_wrapToPrimaryCell( double &x, double &y ) const noexcept
{
	while( x < 0.0 ) x += _X;
	while( x > _X  ) x -= _X;

	while( y < 0.0 ) y += _Y;
	while( y > _Y  ) y -= _Y;
}

// The vector separation between positions of node n1 to node n2.
std::array<double,2> networkTriSpring::_nodeSeparation( unsigned int n1, unsigned int n2 ) const
{
	std::array<double,2> dx = { _nodePositions[2*n2]-_nodePositions[2*n1], _nodePositions[2*n2+1]-_nodePositions[2*n1+1] };

	// No Lees-Edwards-like BCs as these refer to (unperturbed) node positions.
	if( dx[0] < - _X/2 ) dx[0] += _X;
	if( dx[0] >   _X/2 ) dx[0] -= _X;
	if( dx[1] < - _Y/2 ) dx[1] += _Y;
	if( dx[1] >   _Y/2 ) dx[1] -= _Y;

	return dx;
}

// Determine if two nodes (globally-indexed) have a connection between them.
bool networkTriSpring::_hasConnection( unsigned int n1, unsigned int n2 ) const
{
	int otherNode;

	short k = -1;
	while( ++k<6 && (otherNode=_connections[6*n1+k])!=-1 ) if( otherNode==int(n2) ) return true;
	
	return false;
}

// The unit tangent vector from the first node to the second one (undeformed positions). (0,0) if no connection.
std::array<double,2> networkTriSpring::_tangentVector( unsigned int n1, unsigned int n2 ) const
{
	int otherNode;

	short k = -1;
	while( ++k<6 && (otherNode=_connections[6*n1+k])!=-1 )
		if( otherNode==int(n2) )
			return { _unitTangents[2*(6*n1+k)], _unitTangents[2*(6*n1+k)+1] };

	return {0.0,0.0};
}

// The mean node position.
std::array<double,2> networkTriSpring::_meanNodePosn() const
{
	std::array<double,2> meanPos{0.0,0.0};

	for( auto n=0u; n<_numNodes; n++ )
		for( auto k=0; k<2; k++ )
			meanPos[k] += _nodePositions[2*n+k];

	for( auto k=0; k<2; k++ ) meanPos[k] /= _numNodes;

	return meanPos;
}

// The maximum force imbalance in the non-linear problem, i.e. using node displacements and ignore the
// linearised displacements stored in _disps.
double networkTriSpring::_maxForceMagSqrd() const
{
	double maxForceMagSqrd = 0.0;
	for( auto n=0u; n<_numNodes; n++ )
	{
		std::array<double,2> f = __netForceOnNode(n);
		maxForceMagSqrd = std::max( maxForceMagSqrd, f[0]*f[0]+f[1]*f[1] );
	}

	return std::sqrt( maxForceMagSqrd );
}


#pragma mark -
#pragma mark Reduced nodes
#pragma mark -

// Creates the reduced node re-indexer.
void networkTriSpring::_generateReducedNodeMap()
{
	// Resize the map from network (global) indices to reduced nodes.
	_reducedNodeMap.resize( _numNodes );
	_reducedNodeMap.shrink_to_fit();

	// Map from global indices as per this network class, to reduced nodes.
	int reducedIndex = 0;
	for( auto n=0u; n<_numNodes; n++ )
	{
		short k = _numConnections[n];

		// If at least one connection, add to the reduced node set; else -1 to denote not in the reduced set.
		_reducedNodeMap[n] = ( k ? reducedIndex++ : -1 );
	}

	// Store the total number of reduced nodes.
	_numReducedNodes = std::count_if( _reducedNodeMap.begin(), _reducedNodeMap.end(), [](int m){ return m!=-1; } );

	// Check the index map for consistency.
	debugNodeReindexer();
}

#pragma mark -
#pragma mark Linear coupled problem
#pragma mark -

// Find the offset that would minimise the deviation from the affine prediction.
std::array<double,2> networkTriSpring::realAffineOffset()
{
	// Get the strain tensor for this field, assuming as elsewhere an overall strain magnitude \gamma=1.
	std::array< std::array<double,2>, 2> gamma = _driving->strainTensor();

	// Evaluate the offset.
	std::array<double,2> uOffset{0.0,0.0};
	for( auto n=0u; n<_numNodes; n++ )
	{
		if( !_numConnections[n] ) continue;			// Skip disconnected nodes.

		// Add terms node-by-node.
		for( auto i=0u; i<2; i++ )
		{
			uOffset[i] += _disps[2*n+i].real();		// (u+u^{star})/2 is just the real part.

			for( auto j=0u; j<2; j++ )
				uOffset[i] -= gamma[i][j] * ( _nodePositions[2*n+j] - _driving->origin()[j] );
		}
	}

	// Divide through by the number of nodes in the summation just performed.
	for( auto i=0u; i<2; i++ ) uOffset[i] /= _numReducedNodes;

	return uOffset;
}

// Returns the mean complex displacement from the most recent solve, skipping
// nodes that do not belong to the reduced node set.
std::array< std::complex<double>, 2 > networkTriSpring::meanDisplacement() const
{
	std::array< std::complex<double>, 2 > uSum{0.0,0.0};

	// Get the total displacement.
	for( auto n=0u; n<_numNodes; n++ )
	{
		if( !_numConnections[n] ) continue;			// Skip disconnected nodes.

		// Add x and y-components to the running total.
		for( auto k=0u; k<2; k++ ) uSum[k] += _disps[2*n+k];
	}

	// Divide by the number of nodes in the sum just performed.
	for( auto k=0u; k<2; k++ ) uSum[k] /= _numReducedNodes;

	return uSum;
}

// Subtract a complex offset from all nodes displacements.
void networkTriSpring::offsetDisps( std::array< std::complex<double>, 2 > uOffset )
{
	// Subtract the given displacement offset equally to all nodes.
	for( auto n=0u; n<_numNodes; n++ )
		if( _numConnections[n] )
		{
			_disps[2*n  ] -= uOffset[0];
			_disps[2*n+1] -= uOffset[1];
		}
}

#pragma mark -
#pragma mark Non-templated matrix terms
#pragma mark -

// Pressure gradients only make sense with an explicit fluid, so no real-only option.
void networkTriSpring::matrix_fillPressGdtBodyForces( matrixNetwork< std::complex<double> > *M ) const
{
	// Need at least 1 node for this to be possible.
	unsigned int nNodes = numReducedNodes();
	if( !nNodes )
		throw SystemError("applying body forces to balance pressure gradient","no network nodes",__FILE__,__LINE__);

	// Get total force due to the pressure gradient. Sign: (i) -1 as P appears in velocity equations as - \del P,
	// (ii) -1 as need to balance this force, and (iii) -1 as moved to RHS of matrix equation.
	std::array<std::complex<double>,2> totalF;
	for( auto k=0u; k<2; k++ ) totalF[k] = - _X * _Y * _driving->pressureGradient()[k];

	// Add as body force averaged over all nodes.
	for( auto n=0u; n<_numNodes; n++ )
		M->addDispsRHS_xy( n, totalF, 1.0/nNodes );
}


#pragma mark -
#pragma mark Analysis
#pragma mark -

// Change in the full stress tensor, not normalised to the volume, i.e. sum of r_{i} f_{j}.
// This is the usual virial expansion without the inertial term "-m v_{i} v_[j}".
std::array< std::array< std::complex<double>, 2 >, 2 > networkTriSpring::_sum_ri_fj() const
{
	std::array< std::array< std::complex<double>, 2 >, 2 > sum{{ {0.0,0.0}, {0.0,0.0} }};

	// Loop over all springs and increment the shear stress accordingly.
	int n2;
	for( auto n1=0u; n1<_numNodes; n1++ )
	{
		// Loop over all springs connected to this node.
		short k = -1;
		while( ++k<6 && (n2=_connections[6*n1+k])!=-1 )
		{
			// Don't double count springs.
			if( static_cast<unsigned int>(n2) > n1 ) continue;
			
			// Vector separation between undeformed lattice points, including periodic BCs.
			std::array<double,2> r0 = _nodeSeparation(n1,n2);

			// Initial force between these two nodes; non-zero if pre-stressed.
			double tau     = __springTension(n1,k);
			double tHat[2] = { _unitTangents[2*(6*n1+k)], _unitTangents[2*(6*n1+k)+1] };
			std::array<double,2> f0{ tau*tHat[0], tau*tHat[1] };

			// Change in end-to-end position vector for the two nodes connected by this spring.
			std::array< std::complex<double>, 2 > dr;
			for( auto i=0u; i<2; i++ ) dr[i] = _disps[2*n2+i] - _disps[2*n1+i] + _perBC_dispShifts[6*n1+k][i];

			// Linearised change in force vector for this spring.
			smallSquareMatrix<2,double> Hij = __springHessian(n1,k);
			std::array< std::complex<double>,2 > df{0.0,0.0};
			for( auto i=0u; i<2; i++ )
				for( auto j=0u; j<2; j++ )
					df[i] += Hij[2*i+j] * dr[j];

			// Increment the sum of r_{i} f_{j}. Should be symmetrical if both terms included.
			for( auto i=0u; i<2; i++ )
				for( auto j=0u; j<2; j++ )
				{
					sum[i][j] += r0[i] * df[j];				// Change in force.
					sum[i][j] += dr[i] * f0[j]; 			// Change in position; vanishes w/o prestress.
				}
		}
	}

	// Check that the matrix really is symmetrical to within some tolerance - symmetry has been assumed
	// when extracting the shear stress.
	double asym = std::abs( sum[0][1] - sum[1][0] );
	if( asym > 1e-7 * _volume() )
		throw SystemError("calculating stress tensor","matrix for \\sum r_{i} f_{j} not symmetric",__FILE__,__LINE__);

	return sum;
}

// The coordination number, skipping totally unconnected nodes.
double networkTriSpring::coordinationNumber() const
{
	// The total number of connections, and the number of nodes that were included in the count.
	unsigned int totalNodes = 0, totalConns = 0;
	
	// Loop over all nodes.
	short k;
	for( auto n=0u; n<_numNodes; n++ )
	{
		if( (k=_numConnections[n])>0 )
		{
			totalNodes++;
			totalConns += static_cast<unsigned int>(k);
		}
	}

	// Return with the mean
	return static_cast<double>(totalConns) / totalNodes;
}

// Returns moments of the force distribution (here interpreted as spring tensions) up to and including the
// specified maximum. Does not output the zero'th moment, so the first entry is the first moment.
std::vector< std::complex<double> > networkTriSpring::forceMoments( int maxMoment ) const
{
	// Initialise the vector of moments.
	std::vector< std::complex<double> > fMoms( maxMoment, {0.0,0.0} );

	// Loop over all springs.
	int n2;
	for( auto n1=0u; n1<_numNodes; n1++ )
	{
		short k = -1;
		while( ++k<6 && (n2=_connections[6*n1+k])!=-1 )
		{
			// Avoid double counting.
			if( static_cast<unsigned int>(n2) > n1 ) continue;
			
			// Change in end-to-end position vector for the two nodes connected by this spring.
			std::array< std::complex<double>, 2 > dr;
			for( auto i=0u; i<2; i++ ) dr[i] = _disps[2*n2+i] - _disps[2*n1+i] + _perBC_dispShifts[6*n1+k][i];

			// Linearised change in force vector for this spring.
			smallSquareMatrix<2,double> Hij = __springHessian(n1,k);
			std::array< std::complex<double>,2 > df{0.0,0.0};
			for( auto i=0u; i<2; i++ )
				for( auto j=0u; j<2; j++ )
					df[i] += Hij[2*i+j] * dr[j];
			
			// Convert to scalar (complex) spring tension.
			std::complex<double> f{ df[0]*_unitTangents[2*(6*n1+k)] + df[1]*_unitTangents[2*(6*n1+k)+1] };

			// Calculate magnitudes of the momemts from 1 up to and including the maximum moment.
			for( auto i=0; i<maxMoment; i++ )
				fMoms[i] += std::complex<double>{ pow(std::abs(f.real()),i+1), pow(std::abs(f.imag()),i+1) };
		}
	}

	// Normalise the moments to the number of springs, and return.
	for( auto i=0; i<maxMoment; i++ ) fMoms[i] /= _totalNumConnections;

	return fMoms;
}

// Returns the non-affinity parameter as used in Broedersz et al., Nature Physics 7, 983 (2011).
double networkTriSpring::nonAffinity() const
{
	// Return zero if there are no nodes (e.g. dilution parameter set to zero).
	if( !_numReducedNodes ) return 0.0;

	// Calculate the non-affinity as per the expression.
	double sumSqrd = 0.0;
	for( auto n=0u; n<_numNodes; n++ )
	{
		// Skip disconnected nodes.
		if( !_numConnections[n] ) continue;

		// The affine prediction for the displacement of the node at this point in space.
		std::array< std::complex<double>,2 > uAff = _driving->affineDisplacement( &_nodePositions[2*n] );

		// Update the running total with the deviation from affinity.
		std::complex<double>
			diff_x{ _disps[2*n  ] - uAff[0] },
			diff_y{ _disps[2*n+1] - uAff[1] };

		sumSqrd += std::abs( diff_x*conj(diff_x) + diff_y*conj(diff_y) );
	}

	// Normalise and return (again, \gamma_{0}=1.0 assumed).
	return sumSqrd / ( _numReducedNodes * _a * _a );	
}


#pragma mark -
#pragma mark I/O
#pragma mark -

// Save network and solution as text.
void networkTriSpring::saveSolution( ostream &out ) const
{
	// First line is the type of network.
	out << _typeLabel() << std::endl;
	
	// Next line gives the lattice dimensions, and the number of non-trivial nodes, which may be less than _numX * _numY.
	out << _numX << " " << _numY << " " << _numReducedNodes << std::endl;
	
	// Next line gives the lattice parameters.
	out << _a << " " << _k << " " << _p_E_W << " " << _p_NW_SE << " " << _p_NE_SW << std::endl;

	// Output the nodes one by one as:
	// <global node index> <undeformed x> <undeformed y> <conn1> <conn2> ... <conn6>
	// \ <tension1> <tension2> <tension3> ... <tension6> <Re(u_x)> <Im(u_x)> <Re(u_y)> <Im(u_y)>
	// where the tensions refer to the initial state, and the complex amplitude
	// is only output if an oscillatory solution was found.
	int i, j, m, n2;
	for( auto n=0u; n<_numNodes; n++ )
	{
		// Do not output nodes for which there was no solution found, i.e. those not in the reduced map.
		if( (m=_reducedNodeMap[n])==-1) continue;

		// Use the reduced node index in the output file.
		_splitGlobal( n, &i, &j );
		out << m
		    << " " << _nodePositions[2*n] << " " << _nodePositions[2*n+1];
		
		// For the connected nodes, ensure they also use the reduced indexing.
		// -1 is used for both "no connection" and "not in the reduced map".
		for( auto k=0; k<6; k++ )
		{
			n2 = _connections[6*n+k];
			out << " " << ( n2==-1 ? -1 : _reducedNodeMap[n2] );
		} 

		// The tensions in the connecting springs.
		for( auto k=0u; k<6; k++ )
			out << " " << ( _connections[6*n+k]==-1 ? 0.0 : __springTension(n,k) );

		// Only output the real parts of the displacements for the static solution.
		if( _omega==0.0 )
			out << " " << _disps[2*n].real() << " " << _disps[2*n+1].real();

		// Complex solution for finite driving frequency. Note an undefined omega (==-1) will output nothhing.
		if( _omega>0.0 )
			out << " " << _disps[2*n  ].real() << " " << _disps[2*n  ].imag()
				<< " " << _disps[2*n+1].real() << " " << _disps[2*n+1].imag();

		out << std::endl;
	}

	// DEBUG; check the offsets by listing all non-zero ones.
// 	for( auto n1=0u; n1<_numNodes; n1++ ) {
// 		short k = -1; int n2;
// 		while( ++k<6 && (n2=_connections[6*n1+k])!=-1 ) {
// 			if( _xOffsets[6*n1+k] )	std::cout << "n1=" << n1 << "; k=" << k << "; offset=" << _xOffsets[6*n1+k] << std::endl;
// 		}
// 	}
}


#pragma mark -
#pragma mark Debugging
#pragma mark -

// Check the network connectivity and force balance prior to applying the oscillatory shear. Throws an exceptions if fail.
void networkTriSpring::checkInitialNetwork() const
{
	//
	// Connectivity
	//
	double eps = 1e-7;					// Tolerance.
	int n1, n2;
	unsigned int totalConns = 0;
	for( n1=0; n1<static_cast<int>(_numNodes); n1++ )
	{
		// Get the global index for the connected node		
		short k1 = -1;
		while( ++k1<6 && (n2=_connections[6*n1+k1])!=-1 )
		{
			// 1. Make sure the connection is symmetric
			short k2 = -1;
			while( ++k2<6 && _connections[6*n2+k2]!=n1 ) {}
			if( k2==6 ) throw SystemError("checking network connectivity","un-symmeterised connection",__FILE__,__LINE__);
			
			// 2. Make sure the tangent vectors are equal-and-opposite, to within floating point tolerance.
			if(
				fabs( _unitTangents[2*(6*n1+k1)  ] + _unitTangents[2*(6*n2+k2)  ] ) > eps
				||
				fabs( _unitTangents[2*(6*n1+k1)+1] + _unitTangents[2*(6*n2+k2)+1] ) > eps
			)
				throw SystemError("checking network connectivity","un-symmeterised unit tangent",__FILE__,__LINE__);

			// 3. Make sure the natural spring length is the same, regardless of which node it is read off from.
			if( fabs(_l0[6*n1+k1]-_l0[6*n2+k2]) > eps )
				throw SystemError("checking network connectivity","un-symmeterised natural spring length",__FILE__,__LINE__);
		}

		totalConns += k1;

		// 4. Make sure the stored number of connections matches the actual number.
		if( k1 != _numConnections[n1] )
			throw SystemError("checking network connectivity","inconsistent number of connections",__FILE__,__LINE__);
	}

	// 5. Make sure the total number of connections matches the stored value, taking into account double counting.
	if( totalConns != 2*_totalNumConnections )
		throw SystemError("checking network connectivity","inconsistent total connection count",__FILE__,__LINE__);


	//
	// Force balance at nodes.
	//

	// Calculate the maximum force imbalance.
	double maxForceMagSqrd = 0.0;
	for( auto n=0u; n<_numNodes; n++ )
	{
		std::array<double,2> f = __netForceOnNode(n);
		maxForceMagSqrd = std::max( maxForceMagSqrd, f[0]*f[0] + f[1]*f[1] );
	}

	// The tolerance to compare against. If their is no prestress, choose a value somewhat larger than the machine epsilon.
	double fTol = ( __preStress_solver==__preStressSolverType::noneRequired ? 1e-10 : __preStress_tol );
	
	// Raise an exception if force imabalance looks too large.
	if( std::sqrt(maxForceMagSqrd)>fTol )
		throw SystemError("checking force balance after prestress","exceeds tolerance",__FILE__,__LINE__);
}

// The largest deviation of the RHS of the node balance equation for the oscillatory shear.
double networkTriSpring::debugNodeResidual( double zeta ) const
{
	// Similar to the update() routines; no optimisation/parallelisation attempted.
	int n2;
	double residual, maxResidual = 0.0;
	std::complex<double> i_omega{0.0,_omega};
	std::array<std::complex<double>,2> f;

	// Get the body force applied to all nodes to balance any pressure gradient.
	std::array<std::complex<double>,2> bodyF;
	for( auto k=0u; k<2; k++ ) bodyF[k] = - _X * _Y * _driving->pressureGradient()[k] / static_cast<double>(numReducedNodes());

	// Loop over all nodes.
	for( auto n1=0u; n1<_numNodes; n1++ )
	{
		// Skip disconnected nodes as they were never included in the matrix equation.
		if( !_numConnections[n1] ) continue;

		// Start from the body force for this node (currently, zero unless there is a pressure gradient).
		for( auto k=0u; k<2; k++ ) f[k] = bodyF[k];

		// Sum the elastic network forces from all nodes connected to this one.
		short k = -1;
		while( ++k<6 && (n2=_connections[6*n1+k])!=-1 )
		{
			// Calculate the force by combining the (real) Hessian for this spring with the (complex) displacements.
			smallSquareMatrix<2,double> Hij = __springHessian(n1,k);

			for( auto i=0u; i<2; i++ )
				for( auto j=0u; j<2; j++ )
					f[i] += Hij[2*i+j] * ( _disps[2*n2+j] - _disps[2*n1+j] + _perBC_dispShifts[6*n1+k][j] );
		}

		// Drag force. Skip for static solutions.
		if( _omega>0.0 )
		{
			// The velocity at the node, provided by the fluid object via the base class,
			std::array<std::complex<double>,2> v = _fluidVelocityAtNode(n1);

			// Subtract off from the elastic force, so the total force is (should be) zero.
			for( auto k=0u; k<2u; k++ )
				f[k] -= zeta * ( v[k] - i_omega * _disps[2*n1+k] );
		}

		// Get the magnitude of this node's residual
		residual = sqrt( std::abs(f[0])*abs(f[0]) + std::abs(f[1])*abs(f[1]) );

		// Update the maximum if necessary
		maxResidual = std::max( maxResidual, residual );
	}	

	return maxResidual;
}

 // Checks the node reindexer for consistency and throws an exception if it fails.
void networkTriSpring::debugNodeReindexer() const
{
	// Total size should match the number of network nodes.
	if( _reducedNodeMap.size() != _numNodes )
		throw SystemError("checking network to matrix node map","map size not equal to number of network nodes",__FILE__,__LINE__);

	// Each matrix node should appear exactly once.
	for( auto m=0u; m<_numReducedNodes; m++ )
		if( std::count(_reducedNodeMap.begin(),_reducedNodeMap.end(),m)!=1 )
			throw SystemError("checking network to matrix node map","at least one matrix node did not appear exactly once",__FILE__,__LINE__);

	// The only other value that should appear is -1, and it should appear a known number of times.
	if( std::count(_reducedNodeMap.begin(),_reducedNodeMap.end(),-1) != _numNodes - _numReducedNodes )
		throw SystemError("checking network to matrix node map","wrong number of network nodes with no matrix node",__FILE__,__LINE__);
}
