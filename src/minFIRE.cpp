#include "minFIRE.h"



#pragma mark -
#pragma mark Custom exceptions (templated in .h)
#pragma mark -

class minFIRE::ExceededMaxIterations : public std::runtime_error
{
private:
    std::string makeWhat( std::string algorithm, std::string file, int line )
    {
        std::ostringstream out;
        out << "Exceeded maximum no. of iterations for " << algorithm
            << " [in file " << file << ", line " << line << "]" << std::endl;
        return out.str();
    }

public:
    ExceededMaxIterations( std::string algorithm, std::string file, int line )
        : std::runtime_error( makeWhat(algorithm,file,line) ) {}
};


#pragma mark -
#pragma mark Constructors / destructor
#pragma mark -

minFIRE::minFIRE( int numDOF, int numThreads, fireMDIntegrator MDType )
    : _nDOF(numDOF), _MDType(MDType), _nThreads(numThreads)
{
    //
    // Sanity check for the provided parameters.
    //
    if( _nDOF    <1 ) throw BadParameterError<int>("no. of DOF"    ,_nDOF     ,"must be positive",__FILE__,__LINE__);
    if( _nThreads<1 ) throw BadParameterError<int>("no. of threads", _nThreads,"must be positive",__FILE__,__LINE__);

    //
    // Initialise the velocity and acceleration vectors.
    //
    _v.resize(_nDOF,0.0);
    _v.shrink_to_fit();

    _A.resize(_nDOF,0.0);
    _A.shrink_to_fit();

    //
    // Default FIRE-specific parameters.
    //
    _nMin   = 5;
    _fInc   = 1.1;
    _fAlpha = 0.99;
    _fDec   = 0.5;
    _dtMax  = 0.0;          // If never set, will use a default factor times the initial time step.
    _dtLast = 0.0;          // Final time step used in FIRE.

    //
    // Can pre-calculate thread partitions as always applied to vectors of size _nDOF.
    //
    _threadStarts.resize( _nThreads );
    _threadSizes .resize( _nThreads );

    for( auto t=0; t<_nThreads; t++ )
    {
        _threadStarts[t] = t*(_nDOF/_nThreads) + ( t<_nDOF-(_nDOF/_nThreads)*_nThreads ? t : _nDOF-(_nDOF/_nThreads)*_nThreads );
        _threadSizes [t] =   (_nDOF/_nThreads) + ( t<_nDOF-(_nDOF/_nThreads)*_nThreads ? 1 : 0 );
    }
}


#pragma mark -
#pragma mark Vector operations
#pragma mark -

double minFIRE::_dot( const double *x, const double *y ) const
{
    // Don't invoke threads if there is only one.
    if( _nThreads==1 )
    {
        double sum = 0.0;
        for( auto i=0; i<_nDOF; i++ ) sum += x[i] * y[i];
        return sum;
    }

    // std::reduce is C++17, so to remain compatible with older compilers, use a simple
    // task-based partial sum calculation that only requires C++11.
    std::vector< std::future<double> > pSumFutures;

    for( auto t=0; t<_nThreads; t++ )
        pSumFutures.push_back(
            std::async(
                [x,y] ( int i0, int n )
                    { double pSum = 0.0; for( auto i=i0; i<i0+n; i++ ) pSum += x[i] * y[i]; return pSum; },
                _threadStarts[t],
                _threadSizes [t]
            )
        );

    double sum = 0.0;
    for( auto t=0; t<_nThreads; t++ ) sum += pSumFutures[t].get();

    return sum;
}

void minFIRE::_daxpy( double *x, const double *y, double a ) const
{
    // Do not invoke threads if only one.
    if( _nThreads==1 )
    {
        for( auto i=0; i<_nDOF; i++ ) x[i] += a * y[i];
        return;
    }

   std::vector<std::thread> threads;

    for( auto t=0; t<_nThreads; t++ )
        threads.push_back(
            std::thread(
                [a,x,y] ( int i0, int n ) { for( auto i=i0; i<i0+n; i++ ) x[i] += a * y[i]; },
                _threadStarts[t],
                _threadSizes [t]
            )
        );

    for( auto &t : threads ) t.join();
}

void minFIRE::_daxpy_mod( double *x, const double *y, double a, double b ) const
{
    // Do not invoke threads if only one.
    if( _nThreads==1 )
    {
        for( auto i=0; i<_nDOF; i++ ) x[i] = a*x[i] + b*y[i];
        return;
    }

    std::vector<std::thread> threads;

    for( auto t=0; t<_nThreads; t++ )
        threads.push_back(
            std::thread(
                [a,b,x,y] ( int i0, int n ) { for( auto i=i0; i<i0+n; i++ ) x[i] = a*x[i] + b*y[i]; },
                _threadStarts[t],
                _threadSizes [t]
            )
        );

    for( auto &t : threads ) t.join();
}

void minFIRE::_dabxyz( double *x, const double *y, const double *z, double a, double b ) const
{
    // Do not invoke threads if only one.
    if( _nThreads==1 )
    {
        for( auto i=0; i<_nDOF; i++ ) x[i] += a*y[i] + b*z[i];
        return;
    }
    
    std::vector<std::thread> threads;

    for( auto t=0; t<_nThreads; t++ )
        threads.push_back(
            std::thread(
                [a,b,x,y,z] ( int i0, int n ) { for( auto i=i0; i<i0+n; i++ ) x[i] += a*y[i] + b*z[i]; },
                _threadStarts[t],
                _threadSizes [t]
             )
        );

    for( auto &t : threads ) t.join();
}

void minFIRE::_clear( double *x ) const
{
    // Do not invoke threads if only one.
    if( _nThreads==1 )
    {
        for( auto i=0; i<_nDOF; i++ ) x[i] = 0.0;
        return;
    }
 
    std::vector<std::thread> threads;

    for( auto t=0; t<_nThreads; t++ )
        threads.push_back(
            std::thread(
                [x] ( int i0, int n ) { for( auto i=i0; i<i0+n; i++ ) x[i] = 0; },
                _threadStarts[t],
                _threadSizes [t]
            )
        );

    for( auto &t : threads ) t.join();
}


#pragma mark -
#pragma mark MD
#pragma mark -

// Performs a single MD step using the selected integrator.
void minFIRE::_singleMDStep( double *x, const double *accn, double dt )
{
    switch( _MDType )
    {
        // Forward Euler: x(t+dt) = x(t) + dt v(t) + (dt^{2}/2) a(t); v(t+dt) = v(t) + dt a(t).
        case fireMDIntegrator::fEuler :
        {
            _dabxyz( x, _v, accn, dt, 0.5*dt*dt );
            _daxpy ( _v, accn, dt );
            break;
        }

        // Velocity Verlet: x(t+dt) = x + dt v(t) + (dt^{2}/2) a(t); v(t+dt) = v(t) + (dt/2)( a(t) + a(t+dt) )
        case fireMDIntegrator::vVerlet :
        {
            _dabxyz( x, _v, accn, dt, 0.5*dt*dt );
            _daxpy ( _v, accn, 0.5*dt );                // Remainder calculated in _singleMDStep_post().
            break;
              
        }

        default:
            throw std::runtime_error( "Did not recognise MD integrator in FIRE algorithm." );
    }
};

// Additional step called after the acceleration evaluation at time t+dt has been calculated.
void minFIRE::_singleMDStep_post( double *x, const double *accn, double dt )
{
    switch( _MDType )
    {
        case fireMDIntegrator::vVerlet :
        {
            _daxpy( _v, accn, 0.5*dt );
            break;
        }

        case fireMDIntegrator::fEuler :
        default:
            break;
            // Would have already warned about an unknown integrator in _singleMDStep().
    }
}
  

#pragma mark -
#pragma mark Minimisation
#pragma mark -

// Uses std::function, which is heavyweight - should be okay here as there will be many many f.p. calculations
// for each call of minimise().
int minFIRE::minimise(
    double *x,
    std::function<void(const double*,double*)> fillAccn,
    double accnTol,
    double init_dt,
    double init_alpha,
    int maxItersPerDOF )
{
    //
    // Sanity checks.
    //
    if( accnTol        <= 0.0 ) throw BadParameterError<double>("faccnTol"          ,accnTol       ,"must be positive"    ,__FILE__,__LINE__);
    if( maxItersPerDOF <= 0   ) throw BadParameterError<int>   ("max. iters per DOF",maxItersPerDOF,"must be positive"    ,__FILE__,__LINE__);
    if( init_dt        <= 0.0 ) throw BadParameterError<double>("initial time step" ,init_dt       ,"must be positive"    ,__FILE__,__LINE__);
    if( init_alpha     <= 0.0 ) throw BadParameterError<double>("initial \\alpha"   ,init_alpha    ,"must be positive"    ,__FILE__,__LINE__);
    if( _dtMax         <  0.0 ) throw BadParameterError<double>("max. time step"    ,_dtMax        ,"must be non-negative",__FILE__,__LINE__);

    if( _fInc <= 1.0 ) throw BadParameterError<double>("timestep increment fInc"        ,_fDec,"must be >1" ,__FILE__,__LINE__);
    if( _nMin <  1   ) throw BadParameterError<int>   ("min. iters since positive power",_nMin,"must be >=1",__FILE__,__LINE__);

    if( _fDec   >= 1.0 || _fDec   <= 0.0 ) throw BadParameterError<double>("timestep decrement fDec",_fDec  ,"must be in (0,1)",__FILE__,__LINE__);
    if( _fAlpha >= 1.0 || _fAlpha <= 0.0 ) throw BadParameterError<double>("alpha decrement fAlpha" ,_fAlpha,"must be in (0,1)",__FILE__,__LINE__);
 

    //
    // Main iteration loop.
    //
    double alpha = init_alpha, dtMax = ( _dtMax ? _dtMax : __defaultDeltaTMaxFactor*init_dt );
    double mag_a, mag_v, P;   

    _dtLast = init_dt;                  // Time step is stored persistently and the final value accessible after minimise() returns.

    int tSincePNeg = -1;                // Iterations since the power P was last negative. -1 means it has not yet been negative.

    fillAccn( x, _A.data() );           // Get a(t=0) given x(t=0).
    _clear(_v);                         // Clear the velocity vector, i.e. v(t=0)=0.
  
    int nIters = 0;
    while( true )
    {
        // MD step to get x(t+dt) and v(t+dt) (the latter calculation incomplete for velocity Verlet).
        _singleMDStep( x, _A, _dtLast );

        // Get F(t+dt) based on x(t+dt).
        fillAccn( x, _A.data() );

        // Some algorithms (i.e. velocity Verlet) also update v(t+dt) based on a(t+dt).
        _singleMDStep_post( x, _A, _dtLast );

        // Now have a(t+dt) and v(t+dt). Get magnitudes of both and check for convergence.
        mag_a = _mag(_A);
        if( mag_a < accnTol ) return nIters;
        mag_v = _mag(_v);

        // F1. Get the power.
        P = _dot( _A, _v );

        // F2. Update v.
        _daxpy_mod( _v, _A, 1-alpha, alpha*mag_v/mag_a );

        // F3 [really P3(i) as P3 and P4 in the paper are alternatives]. Adaptive time step and damping for P>0 ("going in the right direction").
        if( P>0 )
        {
            if( tSincePNeg==-1 or ++tSincePNeg>_nMin )          // Always true until first time P<0 (paper wasn't clear here).
            {
                _dtLast = std::min( _dtLast*_fInc, dtMax );
                alpha  *= _fAlpha;
            }
        }

        // F4 [or P3(ii)]. Adaptation for P <= 0 ("going in the wrong direction").
        if( P<=0.0 )
        {
            tSincePNeg = 0;

            _dtLast *= _fDec;
            alpha    = init_alpha;

            _clear(_v);
        }

        // Check for exceeding the maximum number of iterations.
        if( ++nIters == maxItersPerDOF*_nDOF ) throw ExceededMaxIterations("FIRE relaxation",__FILE__,__LINE__);
    }

}


#pragma mark -
#pragma mark Debugging
#pragma mark -

// Similar to the main minimisation routine, but only uses very basic overdamped MD.
int minFIRE::debugOverdampedMD(
        double *x,
        std::function<void(const double*,double*)> fillAccn,
        double accnTol,
        double gamma,
        int maxItersPerDOF
    )
{
    // Sanity check for parameters.
    if( accnTol        <= 0.0 ) throw BadParameterError<double>("accnTol"           ,accnTol       ,"must be positive",__FILE__,__LINE__);
    if( maxItersPerDOF <= 0   ) throw BadParameterError<int>   ("max. iters per DOF",maxItersPerDOF,"must be positive",__FILE__,__LINE__);

    // Main iteration loop.
    fillAccn( x, _A.data() );
 
    int nIters = 0;
    while( true )
    {
        // Overdamped MD.
        _daxpy( x, _A, gamma );

        // Check the magnitude of the acceleration vector against the tolerance.
        double mag_a = _mag(_A);
        if( mag_a < accnTol ) return nIters;

        // Check for exceeding the maximum number of iterations.
        if( ++nIters == maxItersPerDOF*_nDOF ) throw ExceededMaxIterations("overdamped MD relaxation",__FILE__,__LINE__);

        // Get the acceleration at this new position.
        fillAccn( x, _A.data() );
    }
}




