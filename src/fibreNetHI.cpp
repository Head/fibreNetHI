#include "fibreNetHI.h"


#pragma mark -
#pragma mark Constants
#pragma mark -

std::string fibreNetHI::__logFileName = "log.out";					// Filename for the log file
std::string fibreNetHI::__prefix      = "save";						// Saved states will be indexed as per "<prefix>_nnnn<suffix>"
std::string fibreNetHI::__suffix      = ".out";


#pragma mark -
#pragma mark Constructors / destructor
#pragma mark -

fibreNetHI::fibreNetHI( std::string params_fname )
{
	using namespace parameterParsing;

	//
	// Parameter extraction and log file
	//
	
	// Try to open the log file. Append so multiple runs on same directory record all logged messages, not just the last one.
	_logFile.open( __logFileName, std::ios::app );

	// Output some introductory text
	_logFile << "-- fibreNetHI --" << std::endl
	         << "Simulates fibre networks immersed in a solution, specialised to the linear viscoelastic response." << std::endl
	         << "David Head, University of Leeds, UK (2017-2022)." << std::endl;

	// Try to open the parameters file; the XMLParameters object will throw an exception if not found
	_paramParser.reset( new XMLParameters() );
	_paramParser->parseFile( params_fname );

	// Echo parameters to the log file.
	_logFile << "\n" << *_paramParser << "\n" << std::endl;

	// Box geometry. X must be specified. Y can be declared a string to give a value that is somehow commensurate with X.
	_X = _paramParser->scalarParam( boxDimX );
	try
	{
		 if( _paramParser->labelParam(boxDimY)==boxDimY_commensurateTriangle ) _Y = _root3over2 * _X;
	}
	catch( const XMLParameters::CouldNotFindParameterError &err )	// Could not parse 'boxDimY' as a string.
	{
		_Y = _paramParser->scalarParam( boxDimY );
	}

	// Check dimensions are positive
	if( _X <= 0.0 ) throw ParameterReadError("X-dimension not positive",__FILE__,__LINE__ );
	if( _Y <= 0.0 ) throw ParameterReadError("Y-dimension not positive",__FILE__,__LINE__ );
	
	//
	// Set up the driving object first as need by network and fluid.
	//
	_driving = nullptr;

	if( _paramParser->labelParam(drivingType) == drivingTypeSimpleShear )
		_driving = std::unique_ptr<drivingSimpleShear>( new drivingSimpleShear(_X,_Y,_paramParser.get(),&_logFile) );
	
	if( _paramParser->labelParam(drivingType) == drivingTypeExtensionalShear )
		_driving = std::unique_ptr<drivingExtensionalShear>( new drivingExtensionalShear(_X,_Y,_paramParser.get(),&_logFile) );

	if( _paramParser->labelParam(drivingType) == drivingTypePressureGdt )
		_driving = std::unique_ptr<drivingPressureGdt>( new drivingPressureGdt(_X,_Y,_paramParser.get(),&_logFile) );
	
	if( _paramParser->labelParam(drivingType) == drivingTypeArbStrain )
		_driving = std::unique_ptr<drivingArbStrain>( new drivingArbStrain(_X,_Y,_paramParser.get(),&_logFile) );

	if( _driving == nullptr ) throw ParameterReadError("driving type not recognised",__FILE__,__LINE__);


	//
	// Set up the network object.
	//
	_network = nullptr;

	if( _paramParser->labelParam(netType) == netTypeNone )
		_network = std::unique_ptr<networkNone>( new networkNone(_X,_Y,_paramParser.get(),&_logFile,_driving.get()) ); 

	if( _paramParser->labelParam(netType) == netTypeTriangularSprings )				// Throws an exception if the label not defined.
		_network = std::unique_ptr<networkTriSpring>( new networkTriSpring(_X,_Y,_paramParser.get(),&_logFile,_driving.get()) ); 

	if( _paramParser->labelParam(netType) == netTypeTriSpringStrip )
		_network = std::unique_ptr<networkTriSpringStrip>( new networkTriSpringStrip(_X,_Y,_paramParser.get(),&_logFile,_driving.get()) );

	if( _paramParser->labelParam(netType) == netTypeTriSpringSierpinski )
		_network = std::unique_ptr<networkTriSpringSierpinski>( new networkTriSpringSierpinski(_X,_Y,_paramParser.get(),&_logFile,_driving.get()));

	if( _paramParser->labelParam(netType) == netTypeTriSpringGenSierp )
		_network = std::unique_ptr<networkTriSpringGenSierp>( new networkTriSpringGenSierp(_X,_Y,_paramParser.get(),&_logFile,_driving.get()));

	if( _paramParser->labelParam(netType) == netTypeTriSpringCorrelated )
		_network = std::unique_ptr<networkTriSpringCorrelated>( new networkTriSpringCorrelated(_X,_Y,_paramParser.get(),&_logFile,_driving.get()));

	if( _network == nullptr ) throw ParameterReadError("network type not recognised",__FILE__,__LINE__);
	
	// Post-construction initialisation.
	_network->initialise();

	//
	// Set up the fluid object.
	//
	_fluid = nullptr;

	if( _paramParser->labelParam(fluidType) == fluidTypeDragOnly )
		_fluid = std::unique_ptr<fluidDragOnly>( new fluidDragOnly(_X,_Y,_paramParser.get(),&_logFile,_driving.get()) );
	
	if( _paramParser->labelParam(fluidType) == fluidTypeStokes )
		_fluid = std::unique_ptr<fluidStokes>( new fluidStokes(_X,_Y,_paramParser.get(),&_logFile,_driving.get()) );
	
	if( _fluid == nullptr ) throw ParameterReadError("fluid type not recognised",__FILE__,__LINE__);
	
	// Check compatibility with driving.
	if( _fluid->incompressible() && !_driving->tracelessStrain() )
		throw ParameterReadError("cannot have incompressible fluid with a strain that is not traceless",__FILE__,__LINE__);

	//
	// Link fluid to network objects.
	//
	_network->setFluidVelocityAt( [&](double x,double y)
		{ return _fluid->velocityAt(x,y);} );	

	//
	// Fill in the network extent in the driving object; some strain types require this.
	//
	_driving->setNetworkExtent( _network->extentX(), _network->extentY() );
	_driving->initialise();

	//
	// Get the solver type.
	//
	_solver = _solverType::unknown;
	if( _paramParser->labelParam(solverType)==solverTypeDenseDirect  ) _solver = _solverType::denseDirect ;
	if( _paramParser->labelParam(solverType)==solverTypeSparseDirect ) _solver = _solverType::sparseDirect;

	if( _solver==_solverType::unknown ) throw ParameterReadError("no solver type specified",__FILE__,__LINE__);
	
	//
	// Matrix for network plus fluid.
	//
	_matrix  = nullptr;
	_matType = _matrixType::unknown;

	// Triangular spring lattice with Stokes flow.
	if( 
		(
			   _paramParser->labelParam(netType)==netTypeTriangularSprings
			|| _paramParser->labelParam(netType)==netTypeTriSpringStrip
			|| _paramParser->labelParam(netType)==netTypeTriSpringSierpinski
			|| _paramParser->labelParam(netType)==netTypeTriSpringGenSierp
			|| _paramParser->labelParam(netType)==netTypeTriSpringCorrelated
		)
		&&
		_paramParser->labelParam(fluidType)==fluidTypeStokes
	)
	{
		// Get the network node number/mapping, and the number of fluid cells.
		auto numNetNodes = _network->numReducedNodes(), numMeshNodes = _fluid->numCells();	// Known by this stage.
		const std::vector<int>* indexer = _network->reducedNodeMap();						// Skips 'redundant' nodes.

		_matrix  = std::unique_ptr<matrixTriSpringStokes>( new matrixTriSpringStokes(numNetNodes,numMeshNodes,&_logFile,indexer) );
		_matType = _matrixType::triSpringStokes;
	}
	
	// Triangular spring lattice with drag only.
	if( 
		(
			   _paramParser->labelParam(netType)==netTypeTriangularSprings
			|| _paramParser->labelParam(netType)==netTypeTriSpringStrip
			|| _paramParser->labelParam(netType)==netTypeTriSpringSierpinski
			|| _paramParser->labelParam(netType)==netTypeTriSpringGenSierp
			|| _paramParser->labelParam(netType)==netTypeTriSpringCorrelated
		)
		&&
		_paramParser->labelParam(fluidType)==fluidTypeDragOnly
	)
	{
		// Get the network node number and mapping.
		auto numNetNodes = _network->numReducedNodes();
		const std::vector<int>* indexer = _network->reducedNodeMap();

		// Initialise the matrix.
		_matrix  = std::unique_ptr<matrixTriSpringDrag>( new matrixTriSpringDrag(numNetNodes,&_logFile,indexer) );
		_matType = _matrixType::triSpringDrag;
	}

	//
	// Matrix for the static solution, which is real and can only depend on the network.
	//
	_matrixStatic = std::unique_ptr< matrixNetwork<double> >(
			new matrixNetwork<double>(_network->numReducedNodes(),2*_network->numReducedNodes(),&_logFile,_network->reducedNodeMap())
		);

	//
	// Filename indexer. Set to one greater than any existing file, or zero.
	//
	_fnameIndexer = std::unique_ptr<sequentialFilenames>( new sequentialFilenames(__nDigits,__prefix,__suffix) );
	if( _fnameIndexer->largestFileIndex() != -1 )									// Checks local directory.
		_fnameIndexer->setIndex( _fnameIndexer->largestFileIndex() + 1 );
	
	//
	// Initialise instance variables
	//
	_lastOmega = -1.0;

	// Optional table output.
	_maxForceMoment = _paramParser->integerParamOrDefault( outputMaxForceMoment, 0 );
	if( _maxForceMoment < 0 ) throw ParameterReadError("max. force moment must be positive",__FILE__,__LINE__);

	_output_NA_DC = _paramParser->flagParamOrDefault( outputNonAffinity, false );
}


// Resets the origins (i.e. mean displacements and velocities) so as to have zero network drift - more precisely,
// offsets the origin of the displacements so as to minimise the deviation from the affine prediction. i\omega times
// this is then subtracted from the fluid velocities to preserve coupling. If there was no network, the same process
// is applied just to the fluid.
void fibreNetHI::_resetOrigins()
{
	// When there is a network that may drift.
	if( _network->canDrift() && _network->numReducedNodes() )
	{
		_logFile << " - offsetting network origins, preserving coupling to any fluid." << std::endl;

		// Offset: Real part chosen to minimise non-affinity, imaginary part chosen to give zero mean.
		std::array<double,2> minNA = _network->realAffineOffset();
		std::array< std::complex<double>,2 > meanDisp = _network->meanDisplacement();

		// Combine into a single complex 2-vector.
		std::array< std::complex<double>,2 > uOffset;
		for( auto k=0u; k<2; k++ )
		{
			uOffset[k].real( minNA   [k]        );
			uOffset[k].imag( meanDisp[k].imag() );
		}

		// Apply to the network nodes.
		_network->offsetDisps( uOffset );

		// If there is a fluid, must ensure the coupling is preserved by this shift.
		if( _lastOmega && _fluid->canDrift() )
		{
			std::complex<double> i_omega{0.0,_lastOmega};
			_fluid->offsetVelocities( {i_omega*uOffset[0],i_omega*uOffset[1]} );
		}
	}
	else if( _lastOmega && _fluid->canDrift() )
	{
		_logFile << " - offsetting fluid origins (no network)." << std::endl;

		// If only fluid, make sure it's imaginary part minimises the non-affinity, and
		// the real part has zero mean.
		std::array<double,2> minNA = _fluid->imagAffineOffset( _lastOmega );
		std::array< std::complex<double>,2 > meanVel = _fluid->meanVelocity();

		// Combine into a single complex 2-vector.
		std::array< std::complex<double>,2 > vOffset;
		for( auto k=0u; k<2; k++ )
		{
			vOffset[k].real( meanVel[k].real() );
			vOffset[k].imag( minNA  [k]        );
		}		

		// Offset all fluid velocities.
		_fluid->offsetVelocities( vOffset );
	}
}


#pragma mark -
#pragma mark Solvers
#pragma mark -

// Selection of solver and anything that goes before and after.
int fibreNetHI::solve( double omega )
{
	// Set the last solution frequency to a negative value; only store frequency once a solution is found.
	_lastOmega = -1.0;
	
	// Initialise any network and fluid quantities used by the solver.
	_network->initialiseForSolution( omega );
	_fluid  ->initialiseForSolution( omega );

	// Call the required solver.
	int retVal = -1;
	switch( _solver )
	{
		case _solverType::denseDirect : retVal = _solveDenseDirect (omega); break;
		case _solverType::sparseDirect: retVal = _solveSparseDirect(omega); break;
		default: std::cerr << "Somehow reached solve() with no recognised solver!" << std::endl;
	}
	
	// If failed to solve, bail now (assume an error message has already been sent).
	if( retVal==-1 ) return retVal;
	
	// Store the frequency that this solution corresponds to.
	_lastOmega = omega;

	// Reset the origins (as solver may introduce drift) in such a way that the equations are still obeyed.
	_resetOrigins();

	// Tear-down solvers (i.e. deallocate any memory required during iteration)
	_network->tearDownAfterSolution();
	_fluid  ->tearDownAfterSolution();

	// Output residuals to log file (should be small).
	_logFile << " - network node residual:\t"   << _network->debugNodeResidual( _fluid->getZeta() ) << std::endl;
	if( omega )
	{
		double
			vRes = _fluid->debugVelocityResidual(_network->reducedNodes(),_network->nodePositions(),_network->nodeDisplacements()),
			pRes = _fluid->debugPressureResidual();

		// Output both only if one is non-zero, otheriwse assume they are trivial (e.g. no fluid d.o.f.)
		if( vRes || pRes )
			_logFile << " - fluid velocity residual:\t" << vRes << std::endl
		             << " - fluid pressure residual:\t" << pRes << std::endl;
	}

	return retVal;
}

// Construct the matrix for use by the implicit solvers. Returns a negative integer if it fails.
int fibreNetHI::_constructMatrixAndRHS( double omega )
{
	// Sanity check: Do we have a matrix (and RHS) that can be filled and solved.
	if( _matrix==nullptr )
	{
		std::cerr << "Could not solve implicitly; no matrix object for this combination of network and fluid." << std::endl;
		return -1;
	}
	

	//
	// For the static solution, use the real, network-only matrix.
	//
	if( !omega )
	{
		// Reset matrix just in case called twice for with \omega=0, although this is very unlikely in practice.
		_matrixStatic.get()->fullReset();

		// Only needs the elastic terms as the drag coupling will be identically zero.
		// Will need to extend this list for networks not derived from networkTriSpring.
		dynamic_cast<networkTriSpring*>(_network.get())->matrix_fillElasticTerms( _matrixStatic.get() );

		return 0;
	}


	//
	// Construct the matrix for dynamical (complex) solutions. Depends on matrix type.
	//
	
	// Clear the matrix while preserving the sparsity (if already constructed for a different frequency).
	_matrix.get()->clearSameSparsity();

	// Triangular spring network coupled to Stokes flow.
	if( _matType==_matrixType::triSpringStokes )
	{
		// For convenience, get casted pointers.
		networkTriSpring      *net    = dynamic_cast<networkTriSpring*     >(_network.get());
		fluidStokes           *fluid  = dynamic_cast<fluidStokes*          >(_fluid  .get());
		matrixTriSpringStokes *matrix = dynamic_cast<matrixTriSpringStokes*>(_matrix .get());
	
		// Fill in the various terms in the matrix.
		net  ->matrix_fillElasticTerms( matrix );
		fluid->matrix_fillDragTerms   ( matrix, omega, net->reducedNodes(), net->nodePositions() );
		fluid->matrix_fillStokesTerms ( matrix, omega );

		// If there is a pressure gradient, need to apply a restoring force to the network.
		if( _driving->hasPressureGradient() ) net->matrix_fillPressGdtBodyForces( matrix );

		return 0;
	}
	
	// Triangular spring network with drag only (i.e. no HI).
	if( _matType==_matrixType::triSpringDrag )
	{
		// Sanity check: Drag-only cannot support pressure gradients.
		if( _driving->hasPressureGradient() )
			throw NotYetImplementedError("pressure gradient with drag-only fluid",__FILE__,__LINE__);

		// Cast the required pointers for shorthands.
		networkTriSpring    *net    = dynamic_cast<networkTriSpring*   >(_network.get());
		fluidDragOnly       *fluid  = dynamic_cast<fluidDragOnly*      >(_fluid  .get());
		matrixTriSpringDrag *matrix = dynamic_cast<matrixTriSpringDrag*>(_matrix .get());

		// Fill in the various terms in the matrrix.
		net  ->matrix_fillElasticTerms( matrix );
		fluid->matrix_fillDragTerms   ( matrix, omega, net->reducedNodes(), net->nodePositions() );
		
		return 0;
	}
	
	// Shouldn't reach here.
	std::cerr << "Somehow reached '_constructMatrixAndRHS()' without a suitable matrix being defined." << std::endl;
	return -1;
}

// Dense direct solver such as (normal) LU. Can be useful for testing but will scale poorly with system size.
int fibreNetHI::_solveDenseDirect( double omega )
{
	// Start-up message.
	_logFile << "\nStarting the implicit solver (dense direct) for omega=" << omega << std::endl;	
	
	// Try to construct the matrix and the RHS vector. May fail.
	int constructRet = _constructMatrixAndRHS(omega);
	if( constructRet<0 ) return constructRet;

	//
	// Solve using a dense direct method. If negative, solution wasn't found. Use static matrix if \omega=0.
	//
	auto retVal = ( omega ? _matrix->solveDenseDirect() : _matrixStatic->solveDenseDirect() );
	if( retVal<0 ) return retVal;
	
	//
	// Map solution vector back to the network and fluid objects.
	//
	_recoverSolution( (omega==0.0) );

	// Return with 0 (success) or a negative (failed to solve).
	return retVal;
}

// Sparse direct solver if available, such as SuperLU.
int fibreNetHI::_solveSparseDirect( double omega )
{
	// Start-up message.
	_logFile << "\nStarting the implicit solver (sparse direct) for omega=" << omega << std::endl;	
	
	// Try to construct the matrix and the RHS vector. May fail.
	int constructRet = _constructMatrixAndRHS(omega);
	if( constructRet<0 ) return constructRet;

	// Solve using a sparse direct method. If negative, solution wasn't found. Use static matrix if \omega=0.
	auto retVal = ( omega ? _matrix->solveSparseDirect() : _matrixStatic->solveSparseDirect() );
	if( retVal<0 ) return retVal;

	// Get the solution vector into the fluid and network objects.
	_recoverSolution( (omega==0.0) );
		
	// Return with 0 (success) or a negative (failed to solve).
	return retVal;
}

// Recover the solution vector and place into the fluid and network objects after solution.
void fibreNetHI::_recoverSolution( bool staticSolution )
{
	// For the static solution, only need to copy over the network node displacements.
	if( staticSolution )
	{
		// This should mirror the similar step in _constructMatrixAndRHS().
		if(
			   _matType==_matrixType::triSpringStokes
			|| _matType==_matrixType::triSpringDrag
		)
			dynamic_cast<networkTriSpring*>(_network.get())->matrix_copyDisps( _matrixStatic->disps() );

		return;
	}

	// Triangular spring network coupled to Stokes flow.
	if( _matType==_matrixType::triSpringStokes )
	{	
		// For convenience (again), get casted pointers.
		networkTriSpring      *net    = dynamic_cast<networkTriSpring*     >(_network.get());
		fluidStokes           *fluid  = dynamic_cast<fluidStokes*          >(_fluid  .get());
		matrixTriSpringStokes *matrix = dynamic_cast<matrixTriSpringStokes*>(_matrix .get());

		// Map solution vector back to the respective objects.
		net  ->matrix_copyDisps( matrix->disps() );
		fluid->matrix_copyVels ( matrix->vels () );
		fluid->matrix_copyPress( matrix->press() );
	}

	// Triangular spring network with drag only (i.e. no HI).
	if( _matType==_matrixType::triSpringDrag )
	{
		// Cast the required pointers for shorthands.
		networkTriSpring    *net    = dynamic_cast<networkTriSpring*   >(_network.get());
		matrixTriSpringDrag *matrix = dynamic_cast<matrixTriSpringDrag*>(_matrix .get());

		// Map solution back to the network object.
		net->matrix_copyDisps( matrix->disps() );
	}
}


#pragma mark -
#pragma mark Analysis
#pragma mark -

// Measure of decoupling between the fluid and tne network. Still needs to be divided by some length scale squared.
double fibreNetHI::decouplingMeasure() const
{
	// Initialise the decoupling measure.
	double DC = 0.0;
	
	// Can't define for \omega=0, or if there are no network nodes.
	if( !_lastOmega || !_network->numReducedNodes() ) return DC;

	// Shorthands.
	std::complex<double> i_omega{0.0,_lastOmega};
	std::vector <double> posns = _network->nodePositions();
	
	// Loop over network nodes. May be zero (e.g. networkNone).
	for( auto n=0u; n<_network->numNodes(); n++ )
	{
		// Skip disconnected nodes.
		if( !_network->connectionsForNode(n) ) continue;

		// Network node displacements, x and y components.
		std::complex<double>
			iwu_x = i_omega * _network->nodeDisplacements()[2*n  ],
			iwu_y = i_omega * _network->nodeDisplacements()[2*n+1];

		// Fluid velocity at the same location as the node.
		std::array< std::complex<double>, 2 > v = _fluid->velocityAt( posns[2*n], posns[2*n+1] );

		// Update the decoupling measure.
		DC += std::abs( (iwu_x-v[0])*conj(iwu_x-v[0]) + (iwu_y-v[1])*conj(iwu_y-v[1]) );
	}
	
	// Normalise and return. Note \gamma_{0}=1. Will still need to be divided by some length scale squared.
	return DC / ( _network->numReducedNodes() * _lastOmega * _lastOmega );
}


#pragma mark -
#pragma mark I/O
#pragma mark -

// Saves the solution. Will need to be matched (and kept in sync) with some other code/script
// that reads these files. Note that the format (ordering etc.) is strict.
void fibreNetHI::saveSolution()
{
	// Try to open the file with the indexed filename. Also increases the index if the save was successful.
	ofstream out( _fnameIndexer->filename() );
	if( !out.is_open() )
	{
		std::cerr << "Could not open state file '" << _fnameIndexer->filename() << "' for writing." << std::endl;
		return;
	}
	_logFile << "Saving state file to '" << _fnameIndexer->filename() << "'." << std::endl;
	
	// High precision for output, as may be used to restart and for data analysis.
	out.precision(16);
	
	// Overall header; one line only. Must start with 'fibreNetHI'; other text can appear afterwards but will be ignored.
	out << "fibreNetHI" << std::endl;
	
	// Box dimensions
	out << _X << " " << _Y << std::endl;
	
	// Frequency of last solution. If negative, convergence was not achieved.
	out << _lastOmega << std::endl;
	
	// Save the driving label, all 4 components of the strain tensor, and both pressure gradients.
	out << _driving->typeLabel() << " "
	    << _driving->strainTensor()[0][0] << " "			// Strain is real (magnitudes of strain).
	    << _driving->strainTensor()[0][1] << " "
	    << _driving->strainTensor()[1][0] << " "
	    << _driving->strainTensor()[1][1] << " "
	    << _driving->pressureGradient()[0].real() << " "		// Pressure gradients are complex/
	    << _driving->pressureGradient()[0].imag() << " "
	    << _driving->pressureGradient()[1].real() << " "
	    << _driving->pressureGradient()[1].imag()
		<< std::endl;

	// Save the network.
	_network->saveSolution( out );
	
	// Save the fluid, except for the static solution.
	if( _lastOmega ) _fluid->saveSolution(out);
	
	// Update the filename index.
	(*_fnameIndexer)++;
}

// Out a single table row to the given stream. Uses the last frequency stored.
void fibreNetHI::tableRow( std::string fname ) const
{
	// All moduli to be output, as an ordered std::map with std::string keys and std::complex<double> values.
	moduliMap networkModuli = _network->complexModuli();
	moduliMap fluidModuli   = _fluid  ->complexModuli();

	// Does the file already exist?
	std::ifstream inFile( fname );
	bool firstRow = !inFile.is_open();
	inFile.close();

	// Open the file in append mode.
	std::ofstream tableFile( fname, std::fstream::app );

	// If failed to open, give an error message.
	if( !tableFile.is_open() )
	{
		std::cerr << "Could not open table file '" << fname << "' for output." << std::endl;
		return;
	}

	// If first time, output the headings. Must match the values (see below) and the Python script that reads these in.
	if( firstRow )
	{
		tableFile << "w";

		// Headings for the moduli, which depends on the strain. May be none, one, or more than one.
		for( auto &mod : networkModuli ) tableFile << "\t" << mod.first + "'_net\t" << mod.first << "''_net";
		for( auto &mod : fluidModuli   ) tableFile << "\t" << mod.first + "'_fl\t"  << mod.first << "''_fl" ;

		// Headings for the non-affinity and decoupling measures, if required.
		if( _output_NA_DC ) tableFile << "\tNA_net\tNA_fl\tDC";

		// Interfacial flux, if there is an interface.
		if( _network->hasInterface() ) tableFile << "\tflux";

		// Network force moments, if requested.
		if( _maxForceMoment!=-1 )
			for( auto m=1; m<=_maxForceMoment; m++ )
				tableFile << "\tf" << m << "_real\tf" << m << "_imag";

		// Mean fluid velocity relative to the network, if sensible.
		if( _driving->outputMeanFluidVelocity() ) tableFile << "\tq_x\tq_y";

		tableFile << std::endl;
	}

	// Output a row of values. Must match the headings (see above).
	tableFile << _lastOmega;

	// The various moduli for the given strain. Ensure positive output for the out-of-phase terms
	// - model assumes e^{i\omega t}; should probably have been e^{-i\omega t} so that fluid phase
	//   is behind solid (rather than ahead of it, so the sign convention needs correcting.
	for( auto &mod : networkModuli ) tableFile << "\t" << mod.second.real() << "\t" << std::abs(mod.second.imag());
	for( auto &mod : fluidModuli   ) tableFile << "\t" << mod.second.real() << "\t" << std::abs(mod.second.imag());

	// The non-affinity and decoupling measures, if requested.
	if( _output_NA_DC )
		tableFile << "\t" << networkNonAffinity() << "\t" << fluidNonAffinity() << "\t" << decouplingMeasure();

	// Interfacial flux, if meaningful.
	if( _network->hasInterface() )
		tableFile << "\t" << ( _lastOmega ? _network->interfacialFlux() : 0.0 );

	// Moments of the network force distribution.
	if( _maxForceMoment > 0 )
		for( auto &f : _network->forceMoments(_maxForceMoment) )
			tableFile << "\t" << f.real() << "\t" << f.imag();

	// Mean fluid velocity relative to the network, if sensible.
	if( _driving->outputMeanFluidVelocity() )
	{
		std::array< std::complex<double>,2 > meanVel = _fluid->meanVelocity();
		tableFile << "\t" << - meanVel[0].imag() << "\t" << - meanVel[1].imag();
	}

	tableFile << std::endl;
}