#include "drivingBase.h"

#pragma mark -
#pragma mark Initialisation
#pragma mark -

// Outputs summary of driving to the log file.
void drivingBase::initialise() const
{
	if( _log )
		*_log << std::endl
		      << "Driving type: " << typeLabel() << std::endl
		      << " - strain tensor\t(\t" << strainTensor()[0][0] << "\t" << strainTensor()[0][1] << "\t)" << std::endl
			  << "                \t(\t" << strainTensor()[1][0] << "\t" << strainTensor()[1][1] << "\t)" << std::endl
			  << " - pressure gradient ( " << pressureGradient()[0] << ", " << pressureGradient()[1] << " )"
			  << std::endl;
}


#pragma mark -
#pragma mark Affine predictions
#pragma mark -

// Affine displacement. Returned as complex but will normally be purely real.
std::array< std::complex<double>, 2 > drivingBase::affineDisplacement( const double *pos ) const
{
    std::array< std::complex<double>, 2 > uAff{{ {0.0,0.0}, {0.0,0.0} }};

    for( auto i=0u; i<2; i++ )
        for( auto j=0u; j<2; j++ )
            uAff[i] += strainTensor()[i][j] * ( pos[j] - origin()[j] );

    return uAff;
}

// Affine velocity. Returned as complex but will normally be purely imaginary.
std::array< std::complex<double>, 2 > drivingBase::affineVelocity( const double *pos, double omega ) const
{
    std::complex<double> i_omega{0.0,omega};

    std::array< std::complex<double>, 2 > vAff{{ {0.0,0.0}, {0.0,0.0} }};

    for( auto i=0u; i<2; i++ )
        for( auto j=0u; j<2; j++ )
            vAff[i] += i_omega * strainTensor()[i][j] * ( pos[j] - origin()[j] );

    return vAff;        
}


#pragma mark -
#pragma mark Periodic boundaries
#pragma mark -

// Shift for network displacements for links starting at (x1,y1) and ending at (x2,y2).
std::array<double,2> drivingBase::getDispShift( double x1, double y1, double x2, double y2 ) const
{
    std::array<double,2> shift{0.0,0.0};

    if( x1>3*_X/4 && x2<_X/4 ) { shift[0] += strainTensor()[0][0]*_X; shift[1] += strainTensor()[1][0]*_X; }
    if( x2>3*_X/4 && x1<_X/4 ) { shift[0] -= strainTensor()[0][0]*_X; shift[1] -= strainTensor()[1][0]*_X; }

    if( y1>3*_Y/4 && y2<_Y/4 ) { shift[0] += strainTensor()[0][1]*_Y; shift[1] += strainTensor()[1][1]*_Y; }
    if( y2>3*_Y/4 && y1<_Y/4 ) { shift[0] -= strainTensor()[0][1]*_Y; shift[1] -= strainTensor()[1][1]*_Y; }

    return shift;
}
