#include "networkBase.h"

#pragma mark -
#pragma mark Analysis
#pragma mark -

// Returns with a complex modulus suitable for the type of strain.
std::map< std::string, std::complex<double> > networkBase::complexModuli() const
{
	// Network 'volume' (area in 2D).
	double V = _volume();
	if( !V ) return {{ }};

	// The stress tensor, i.e. the sum of r_{i} f_{j} divided by the network volume.
	std::array< std::array< std::complex<double>, 2 >, 2 > sigma_ij = _sum_ri_fj();
	for( auto i=0u; i<2u; i++ )
		for( auto j=0u; j<2u; j++ )
			sigma_ij[i][j] /= V;

	// The driving object will return a map of moduli calculated from the stress tensor.
	return _driving->networkModuli( sigma_ij );
}

#pragma mark -
#pragma mark Reduced node sets
#pragma mark -

// The default mapping from normally-indexed nodes to reduced nodes is just 1-1.
void networkBase::_generateReducedNodeMap()
{
	_reducedNodeMap.resize( numNodes() ) ;
	for( auto n=0u; n<numNodes(); n++ ) _reducedNodeMap[n] = n;			// Identity map.
}

// List of nodes that exist in the reduced set and therefore should be solved for.
const std::set<int> networkBase::reducedNodes() const
{
	// Need the re-indexing map to be defined.
	if( _reducedNodeMap.size() != numNodes() )
		throw SystemError("generating list of nodes requiring solving","reindexing map not yet defined",__FILE__,__LINE__);

	std::set<int> nodes;
	for( auto n=0u; n<numNodes(); n++ )
		if( _reducedNodeMap[n] != -1 )
			nodes.insert( n );
		
	return nodes;
}

// Copies 2-dimensional vectors from the normal node list [size numNodes()] to a reduced one [size numReducedNodes()].
void networkBase::_copyToReduced( const std::vector<double> &from, std::vector<double> &to ) const
{
	for( auto n=0u; n<numNodes(); n++ )
	{
		int m = _reducedNodeMap[n];
		if( m != -1 )
		{
			to[2*m  ] = from[2*n  ];
			to[2*m+1] = from[2*n+1];
		}
	}
}

// Copies 2-dimensional vectors from the reduced node list [size numReducedNodes()] to a normal sized one [size numNodes()].
void networkBase::_copyFromReduced( const std::vector<double> &from, std::vector<double> &to ) const
{
	for( auto n=0u; n<numNodes(); n++ )
	{
		int m = _reducedNodeMap[n];
		if( m != -1 )
		{
			to[2*n  ] = from[2*m  ];
			to[2*n+1] = from[2*m+1];
		}
	}
}



