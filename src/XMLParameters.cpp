// Class definition
#include "XMLParameters.h"


#pragma mark -
#pragma mark Parsing
#pragma mark -

// Initialise from filename
void XMLParameters::parseFile( const std::string &filename )
{
	// Try to open up the text reader object; throws exception if ptr is returned NULL.
	__readerPtr = xmlNewTextReaderFilename(filename.c_str());
	if( __readerPtr==NULL )
	{
		ostringstream msg;
		msg << "FATAL: Could not load parameters from file '" << filename << "' [in XMLParameters::parseFile()]" << endl;
		throw ios_base::failure(msg.str());
	}

	// Perform the parsing
	__parse();
}
	
// Initialise from a memory block
void XMLParameters::parseMemory( const char* buffer, int size )
{
	// Try to open up the text reader object; throws exception if ptr is returned NULL.
	__readerPtr = xmlReaderForMemory( buffer, size, NULL, NULL, 0 );
	if( __readerPtr==NULL )
	{
		ostringstream msg;
		msg << "FATAL: Could not parse parameters from memory block [in XMLParameters::parseMemory()]" << endl;
		throw ios_base::failure(msg.str());
	}

	// Continue with the parsing
	__parse();
}

// Parse from the xmlTextReader ptr __readerPtr (assumed to have been successfully opened)
void XMLParameters::__parse()
{
	// Walk through the tree node by node
	int retValue = xmlTextReaderRead( __readerPtr );				// Returns '1' if a node was read
	while( retValue==1 )
	{
		if( !_processNode(__readerPtr) ) { retValue=0; break; }		// If parsed all parameters, break with the 'okay' return value
		retValue = xmlTextReaderRead( __readerPtr );
	}
	
	// Clear up
	xmlFreeTextReader( __readerPtr );
	
	// The final return from xmlTextReaderRead() should have been 0, if all went well.
	if( retValue != 0 )
	{
		ostringstream msg;
		msg << "FATAL: Failed during reading of parameters [in XMLParameters::__parse()]" << endl;
		throw ios_base::failure(msg.str());
	}
}


#pragma mark -
#pragma mark Utility
#pragma mark -

// Routines that indicate if a supplied string is likely to be integer or scalar (called AFTER checking for bools/flags)
bool XMLParameters::_looksLikeInteger( string value ) const
{
	bool integer = true;
	for( string::const_iterator iter=value.begin(); iter!=value.end(); iter++ )
		integer = integer and __integer_char(*iter);
	return integer;
}
bool XMLParameters::_looksLikeFloat( string value ) const
{
	bool decimal = true;
	for( string::const_iterator iter=value.begin(); iter!=value.end(); iter++ )
		decimal = decimal and __decimal_char(*iter);
	return decimal;
}

// Returns a C++ string for the current node; defaults to "" 
string XMLParameters::_nodeName( xmlTextReaderPtr _readerPtr ) const
{
	// Get name as xmlChar object, which is freed (if not NULL) after converting to a string.
	xmlChar *name = xmlTextReaderName( _readerPtr );
	string nameStr = ( name==NULL ? "" : (char*) name );
	if( name ) xmlFree( name );
	
	return nameStr;
}


#pragma mark -
#pragma mark Parsing
#pragma mark -

// Parse a single node, as called by the text reader loop in the constructor.
// Returns 'true' if Parameters may exist that have yet to be parsed; false otherwise.
bool XMLParameters::_processNode( xmlTextReaderPtr _readerPtr )
{
	// Get the name (tag) of the node
	string nameStr = _nodeName( _readerPtr );

	// Skip certain types of node
	if( xmlTextReaderNodeType(_readerPtr)==14 && nameStr.compare("#text")==0 ) return true;		// DTD nodes with the name "#text"; 
	if( xmlTextReaderNodeType(_readerPtr)==8 ) return true;										// Comment nodes

	//
	// See if we are entering or leaving a Parameters block.
	//
	if( nameStr == "Parameters" )
	{
		_inParametersBlock = !_inParametersBlock;
		return _inParametersBlock;
	}
	
	// If not in the Parameters block (yet), return 'true' so we continue parsing more nodes
	if( !_inParametersBlock ) return true;
	
	// If still here, try to parse any element nodes
	if( xmlTextReaderNodeType(_readerPtr)==1 ) _parseElementNode(_readerPtr);
	
	// Keep on parsing nodes until the Parameters block is closed
	return true;
}

// Parses a single element node; may be overriden by child classes. Base class parses '<Parameter.../>' single parameter
// lines, and <ParameterGroup>...</ParameterGroup> parameter blocks.
void XMLParameters::_parseElementNode( xmlTextReaderPtr _readerPtr )
{
	// If an element node, call the _parseSingleParameter() method, which may be overriden by a child class.
	if( _nodeName(_readerPtr)=="Parameter" )
	{
		int id = _parseSingleParameter( _readerPtr );
		if( id != -1 ) _ungroupedIDs.push_back(id);				// -1 means failed to parse
	}
	
	// Parameter block; assume only one level deeper for all contained parameters.
	if( _nodeName(_readerPtr)=="ParameterGroup" && xmlTextReaderNodeType(_readerPtr)==1 )
	{
		// Expect a 'tag' attribute, which will be used to identify this group.
		map<string,string> attribs = _getAllAttributes(_readerPtr);
		
		// If not a lone 'tag' attribute, ignore and continue 
		if( attribs.size()!=1 || attribs.count("tag")!= 1 ) return;
		
		// Create an empty vector<vector<long>> with this tag name as key
		_groups[ attribs["tag"] ];
		
		// Add a new (empty) vector<long> to this group
		vector<long> thisGroup;
		_groups[ attribs["tag"] ].push_back( thisGroup );
		
		// Loop through all of the child nodes. Could also generalise this method and call recursively.
		int retValue, id;
		while( true )
		{
			// Read next node; break if none
			if( (retValue=xmlTextReaderRead(_readerPtr)) != 1 ) break;

			// Get (as a C++ string) the name for this node; defaults to ""
			string inGroupName = _nodeName( _readerPtr );

			// Skip text and comment nodes as above
			if( xmlTextReaderNodeType(_readerPtr)==14 && inGroupName.compare("#text")==0 ) continue;
			if( xmlTextReaderNodeType(_readerPtr)==8 ) continue;
			
			// If the end of a group, break from this while loop.
			if( inGroupName == "ParameterGroup" ) break;
			
			// Get the parameter and add its i.d. to the group vector<long> just created.
			id = _parseSingleParameter( _readerPtr );
			if( id != -1 ) _groups[ attribs["tag"] ].back().push_back( id );
		}
	}
}

// Returns all attributes for the current node in the form of a map
map<string,string> XMLParameters::_getAllAttributes( xmlTextReaderPtr _readerPtr ) const
{
	map<string,string> attribs;
	while( xmlTextReaderMoveToNextAttribute(_readerPtr) )
	{
		xmlChar
			*attrName  = xmlTextReaderName (_readerPtr),
			*attrValue = xmlTextReaderValue(_readerPtr);
		
		// If either not specified, continue to the next without adding to the map
		if( attrName==NULL || attrValue==NULL ) continue;

		// If neither NULL, convert to std::strings and store in the map
		attribs[ (char*)attrName ] = (char*)attrValue;

		// Release memory for the XML strings
		xmlFree( attrName  );
		xmlFree( attrValue );
	}
	
	return attribs;
}


// Parses a single parameters node; returns the unique ID used, or -1 if not successfully parsed
int XMLParameters::_parseSingleParameter( xmlTextReaderPtr _readerPtr )
{
	// This class only deals with Parameter single-parameter objects.
	// Get attributes as an associative array
	map<string,string> attribs = _getAllAttributes( _readerPtr );

	//
	// Option 1: Parameter stored verbosely with "type", "name" and "value" attributes
	//
	if( attribs.size()==3 && attribs.count("type" )==1 && attribs.count("name" )==1 && attribs.count("value" )==1 )
	{
		// Try to parse the value according to its type; case sensitive (lower case assumed for attribute names).
		
		// Scalars
		if( attribs["type"]=="scalar" || attribs["type"]=="float" )
		{
			_scalars[_uniqueID] = _to_float(attribs["value"]);			// Returns 0.0 if could not be converted
			_names  [_uniqueID] = attribs["name"];
			return _uniqueID++;
		}
		
		// Integers
		if( attribs["type"]=="int" || attribs["type"]=="integer" )
		{
			_integers[_uniqueID] = _to_long(attribs["value"]);		// Returns 0 if could not be converted
			_names   [_uniqueID] = attribs["name"];
			return _uniqueID++;
		}

		// Switches
		if( attribs["type"]=="flag" || attribs["type"]=="bool" )
		{
			_flags[_uniqueID] = ( attribs["value"]=="yes" || attribs["value"]=="true" );
			_names[_uniqueID] = attribs["name"];
			return _uniqueID++;
		}

		// Labels
		if( attribs["type"]=="label" || attribs["type"]=="string" )
		{
			_labels[_uniqueID] = attribs["value"];
			_names [_uniqueID] = attribs["name"];
			return _uniqueID++;
		}
	}
	
	//
	// Option 2: The more concise form '<Parameter name="value" />'
	//
	if( attribs.size()==1 )
	{
		// The name and value of the parameter correspond to the only entry in the map.
		string name  = attribs.begin()->first ;
		string value = attribs.begin()->second;

		// Need to determine type of the value. First try boolean.
		if( value=="true" || value=="True" || value=="TRUE" || value=="yes" || value=="Yes" || value=="YES"  ) 
		{
			_names[_uniqueID] = name;
			_flags[_uniqueID] = true;
			return _uniqueID++;
		}
		if( value=="false" || value=="False" || value=="FALSE" || value=="no" || value=="No" || value=="NO" )
		{
			_names[_uniqueID] = name;
			_flags[_uniqueID] = false;
			return _uniqueID++;
		}

		// If not, see if all numbers or minus signs; if nothing else, must be an integer (note that if the string had units
		// at the end, this would automatically violate this criterion and assume it was a scalar; this is fine, as dimensional
		// units are usually non-integer anyway)
		if( _looksLikeInteger(value) )
		{
			_names   [_uniqueID] = name;
			_integers[_uniqueID] = _to_long(value);
			return _uniqueID++;
		}

		// Try again for floats
		if( _looksLikeFloat(value) )
		{
			_names  [_uniqueID] = name;
			_scalars[_uniqueID] = _to_float(value);
			return _uniqueID++;
		}

		// If still here, assume it is a text variable
		_names [_uniqueID] = name;
		_labels[_uniqueID] = value;
		return _uniqueID++;
	}

	// If still here, was not parsed. Return -1.
	return -1;
}



#pragma mark -
#pragma mark Output
#pragma mark -

// Definition of overloaded '<<' operator acting on an ostream; outputs in XML format as a single block
ostream& operator<< ( ostream &os, const XMLParameters &p )
{
	// Open tag for whole block (nb: not the whole file, so no ?xml-element)
	os << "<Parameters>" << endl;
	
	// First list all ungrouped parameters
	for( vector<long>::const_iterator c_iter=p._ungroupedIDs.begin(); c_iter!=p._ungroupedIDs.end(); c_iter++ )
		if( *c_iter != -1 )
		{
			os << "\t";
			p._outputSingleParameter( *c_iter, os );
			os << "\n";
		}

	// Now list the groups
	for( map<string, vector< vector<long> > >::const_iterator tagIter=p._groups.begin(); tagIter!=p._groups.end(); tagIter++ )
	{
		os << "<!-- Grouped parameters for tag '" << tagIter->first << "' -->" << endl;
		for( vector< vector<long> >::const_iterator groupIter=tagIter->second.begin(); groupIter!=tagIter->second.end(); groupIter++ )
		{
			os << "\t<ParameterGroup tag='" << tagIter->first << "'>\n";
			
			for( vector<long>::const_iterator paramIter=groupIter->begin(); paramIter!=groupIter->end(); paramIter++ )
			{
				os << "\t\t";
				p._outputSingleParameter( *paramIter, os );
				os << "\n";
			}
			
			os << "\t</ParameterGroup>\n";
		}
	}
	
	// Close tag for the whole output
	os << "</Parameters>";

	return os;
}

// Outputs a single parameter as a single, self-closing XML node (with no tabs prefixed or eol) to the given output stream
void XMLParameters::_outputSingleParameter( long id, ostream &os ) const
{
	os << "<Parameter name='" << _names.find(id)->second << "'";

	if( _scalars.find(id) != _scalars.end() )
		os << " type='scalar' value='" << _scalars.find(id)->second << "'";

	if( _integers.find(id) != _integers.end() )
		os << " type='int' value='" << _integers.find(id)->second << "'";

	if( _flags.find(id) != _flags.end() )
		os << " type='flag' value='" << ( _flags.find(id)->second ? "true" : "false" ) << "'";

	if( _labels.find(id) != _labels.end() )
		os << " type='label' value='" << _labels.find(id)->second << "'";

	os << "/>";				// End of element node; also anew line
}


#pragma mark -
#pragma mark Direct control of ungrouped parameters
#pragma mark -

// Add ungrouped parameters; raises and exception if one with the same name and type already exists
void XMLParameters::addScalarParam( const string &name, const double &val )
{
	if( hasScalarParam(name) || hasIntegerParam(name) ) throw ParameterAlreadyExists(name);
		
	_scalars[_uniqueID] = val;
	_names[_uniqueID]   = name;
	_ungroupedIDs.push_back(_uniqueID++);
}

void XMLParameters::addIntegerParam( const string &name, const long &val )
{
	if( hasIntegerParam(name) ) throw ParameterAlreadyExists(name);

	_integers[_uniqueID] = val;
	_names[_uniqueID] = name;
	_ungroupedIDs.push_back(_uniqueID++);
}

void XMLParameters::addLabelParam( const string &name, const string &val )
{
	if( hasLabelParam(name) ) throw ParameterAlreadyExists(name);

	_labels[_uniqueID] = val;
	_names[_uniqueID] = name;
	_ungroupedIDs.push_back(_uniqueID++);
}

void XMLParameters::addFlagParam( const string &name, const bool &val )
{
	if( hasFlagParam(name) ) throw ParameterAlreadyExists(name);

	_flags[_uniqueID] = val;
	_names[_uniqueID] = name;
	_ungroupedIDs.push_back(_uniqueID++);
}

// Change existing, ungrouped parameters
void XMLParameters::changeScalarParam( const string &name, const double &val )
{
	long ID = _IDForName(name);
	if( ID==-1 || ( _scalars.find(ID)==_scalars.end() && _integers.find(ID)==_integers.end() ) ) throw CouldNotFindParameterError(name);
	if( _scalars .find(ID)!=_scalars .end() ) _scalars[ID]  = val;
	if( _integers.find(ID)!=_integers.end() ) _integers[ID] = (long)val;
}

void XMLParameters::changeIntegerParam( const string &name, const long &val )
{
	long ID = _IDForName(name);
	if( ID==-1 || _integers.find(ID) == _integers.end() ) throw CouldNotFindParameterError(name);
	_integers[ID] = val;
}

void XMLParameters::changeLabelParam( const string &name, const string &val )
{
	long ID = _IDForName(name);
	if( ID==-1 || _labels.find(ID) == _labels.end() ) throw CouldNotFindParameterError(name);
	_labels[ID] = val;
}

void XMLParameters::changeFlagParam( const string &name, const bool &val )
{
	long ID = _IDForName(name);
	if( ID==-1 || _flags.find(ID) == _flags.end() ) throw CouldNotFindParameterError(name);
	_flags[ID] = val;
}

// Remove parameters; throw exception if they do not exist
void XMLParameters::removeScalarParam( const string &name )
{
	long ID = _IDForName(name);
	if(ID==-1 || ( _scalars.find(ID)==_scalars.end() && _integers.find(ID)==_integers.end() ) ) throw CouldNotFindParameterError(name);
	_removeUngroupedID(ID);
}
void XMLParameters::removeIntegerParam( const string &name )
{
	long ID = _IDForName(name);
	if( ID==-1 || _integers.find(ID) == _integers.end() ) throw CouldNotFindParameterError(name);
	_removeUngroupedID(ID);
}
void XMLParameters::removeLabelParam( const string &name )
{
	long ID = _IDForName(name);
	if( ID==-1 || _labels.find(ID) == _labels.end() ) throw CouldNotFindParameterError(name);
	_removeUngroupedID(ID);
}
void XMLParameters::removeFlagParam( const string &name )
{
	long ID = _IDForName(name);
	if( ID==-1 || _flags.find(ID) == _flags.end() ) throw CouldNotFindParameterError(name);
	_removeUngroupedID(ID);
}

// Remove a grouped ID from the name map, ID vector and value map
void XMLParameters::_removeUngroupedID( long ID )
{		
	// Remove the name (assume exists)
	_names.erase(ID);

	// Value maps
	if( _scalars .find(ID)!=_scalars .end() ) _scalars .erase(ID);
	if( _integers.find(ID)!=_integers.end() ) _integers.erase(ID);
	if( _labels  .find(ID)!=_labels  .end() ) _labels  .erase(ID);
	if( _flags   .find(ID)!=_flags   .end() ) _flags   .erase(ID);
	
	// Remove from the list of ungroued IDs
	for( vector<long>::iterator iter=_ungroupedIDs.begin(); iter!=_ungroupedIDs.end(); iter++ )
		if( *iter == ID ) { _ungroupedIDs.erase(iter); break; }
}

// Adding or changing the value of parameter without specifying the type.
void XMLParameters::overwriteOrAddParam( std::string name, std::string value )
{
	//
	// Remove any parameter with the same label.
	//
	try 
	{
		removeScalarParam( name );
	}
	catch( CouldNotFindParameterError &err ) {}

	try 
	{
		removeIntegerParam( name );
	}
	catch( CouldNotFindParameterError &err ) {}

	try 
	{
		removeFlagParam( name );
	}
	catch( CouldNotFindParameterError &err ) {}

	try 
	{
		removeLabelParam( name );
	}
	catch( CouldNotFindParameterError &err ) {}

	//
	// Create new parameter of the required type.
	//

	// Check for a boolean type first of all.
	if( value=="true" || value=="True" || value=="TRUE" || value=="yes" || value=="Yes" || value=="YES"  ) 
	{
		addFlagParam(name,true);
		return;
	}

	if( value=="false" || value=="False" || value=="FALSE" || value=="no" || value=="No" || value=="NO" )
	{
		addFlagParam(name,false);
		return;
	}
	
	// Integer parameters.
	if( _looksLikeInteger(value) && !hasScalarParam(name) )
	{
		addIntegerParam( name, _to_long(value) );
		return;
	}
	
	// Float parameters.
	if( _looksLikeFloat(value) )
	{
		addScalarParam( name, _to_float(value) );
		return;
	}

	// If still here, assume it was a label.
	addLabelParam( name, value );
}

#pragma mark -
#pragma mark Checking existence of parameters
#pragma mark -

// Ungrouped parameters
bool XMLParameters::hasScalarParam( const string &name ) const
{
	int ID = _IDForName(name);
	return ( (ID!=-1) && ( _scalars.find(ID)!=_scalars.end()||_integers.find(ID)!=_integers.end() ) );		// Also check integers
}
bool XMLParameters::hasIntegerParam( const string &name ) const
{
	int ID = _IDForName(name);
	return ( (ID!=-1) && (_integers.find(ID) != _integers.end()) );
}
bool XMLParameters::hasLabelParam( const string &name ) const
{
	int ID = _IDForName(name);
	return ( (ID!=-1) && (_labels.find(ID) != _labels.end()) );
}
bool XMLParameters::hasFlagParam( const string &name ) const
{
	int ID = _IDForName(name);
	return ( (ID!=-1) && (_flags.find(ID) != _flags.end()) );
}

// Is there a group-type with the given tag?
bool XMLParameters::hasGroupTag( const string &tag ) const
{
	return ( _groups.find(tag) != _groups.end() );
}

// Returns the unique ID for the given parameter name, or -1 if it was not found
long XMLParameters::_IDForName( const string &name ) const
{
	for( map<long,string>::const_iterator c_iter=_names.begin(); c_iter!=_names.end(); c_iter++ )
		if( name.compare(c_iter->second)==0 )
			return c_iter->first;
	return -1;
}

// Parameter of any type with given name exists within given group and tag
bool XMLParameters::hasParamInGroupWithTag( const string &name, const long &grp, const string &tag ) const
{
	if( !hasGroupTag(tag) ) return false;

	if( grp<0 || grp>=numGroupsForTag(tag) ) return false;

	for( vector<long>::const_iterator c_iter=_groups.find(tag)->second[grp].begin(); c_iter!=_groups.find(tag)->second[grp].end(); c_iter++ )
		if( name.compare(_names.find(*c_iter)->second)==0 ) return true;

	return false;
}


#pragma mark -
#pragma mark Accessing parameters
#pragma mark -

// No. groups for a given tag; -1 if could not find the tag
int XMLParameters::numGroupsForTag( const string &tag ) const
{
	if( !hasGroupTag(tag) ) return -1;
	return _groups.find(tag)->second.size();		// Since this is a const method, cannot use _groups[tag]
}

// No. of parameters for given group number and tag name
int XMLParameters::numParamsForGroupWithTag( const int &groupNum, const string &tag ) const
{
	if( !hasGroupTag(tag) ) return -1;
	if( groupNum<0 or groupNum>=numGroupsForTag(tag) ) return -1;
	return _groups.find(tag)->second[groupNum].size();
}

// Return ungrouped parameters or raise an exception if not found (after trying for an integer)
double XMLParameters::scalarParam( const string &name ) const
{
	long ID = _IDForName(name);
	if( _scalars .find(ID)!=_scalars .end() ) return _scalars .find(ID)->second;		// On scalars list; if not ...
	if( _integers.find(ID)!=_integers.end() ) return _integers.find(ID)->second;		// ... try the integers; if not ...
	throw CouldNotFindParameterError(name);												// not defined.
}
long XMLParameters::integerParam( const string &name ) const
{
	if( !hasIntegerParam(name) ) throw CouldNotFindParameterError(name);
	return _integers.find(_IDForName(name))->second;
}
bool XMLParameters::flagParam( const string &name ) const
{
	if( !hasFlagParam(name) ) throw CouldNotFindParameterError(name);
	return _flags.find(_IDForName(name))->second;
}
string XMLParameters::labelParam( const string &name ) const
{
	if( !hasLabelParam(name) ) throw CouldNotFindParameterError(name);
	return _labels.find(_IDForName(name))->second;
}

// Return ungrouped parameters or the specified default if not found
double XMLParameters::scalarParamOrDefault( const string &name, const double &def ) const
{
	if( hasIntegerParam(name) ) return integerParam(name);
	if( hasScalarParam (name) ) return _scalars.find(_IDForName(name))->second;
	return def;
}
long XMLParameters::integerParamOrDefault( const string &name, const long &def ) const
{
	if( !hasIntegerParam(name) ) return def;
	return _integers.find(_IDForName(name))->second;
}
bool XMLParameters::flagParamOrDefault( const string &name, const bool &def ) const
{
	if( !hasFlagParam(name) ) return def;
	return _flags.find(_IDForName(name))->second;
}
string XMLParameters::labelParamOrDefault( const string &name, const string &def ) const
{
	if( !hasLabelParam(name) ) return def;
	return _labels.find(_IDForName(name))->second;
}

// Parameters within groups; exception-throwing; no default version
double XMLParameters::scalarParamForGroupWithTag( const string &name, const int &grpNum, const string &tag ) const
{
	if( !hasGroupTag(tag) ) throw CouldNotFindTagError(tag);
	if( grpNum<0 || grpNum>=numGroupsForTag(tag) ) throw InvalidGroupNumberError(tag,grpNum);
	
	for( vector<long>::const_iterator c_iter=_groups.find(tag)->second[grpNum].begin(); c_iter!=_groups.find(tag)->second[grpNum].end(); c_iter++ )
		if( name.compare(_names.find(*c_iter)->second)==0 )
			if( _scalars.find(*c_iter) != _scalars.end() )
				return _scalars.find(*c_iter)->second;

	// If the scalars list failed, try the integer one
	for( vector<long>::const_iterator c_iter=_groups.find(tag)->second[grpNum].begin(); c_iter!=_groups.find(tag)->second[grpNum].end(); c_iter++ )
		if( name.compare(_names.find(*c_iter)->second)==0 )
			if( _integers.find(*c_iter) != _integers.end() )
				return _integers.find(*c_iter)->second;

	throw CouldNotFindParameterError(name);
}

long XMLParameters::integerParamForGroupWithTag( const string &name, const int &grpNum, const string &tag ) const
{
	if( !hasGroupTag(tag) ) throw CouldNotFindTagError(tag);
	if( grpNum<0 || grpNum>=numGroupsForTag(tag) ) throw InvalidGroupNumberError(tag,grpNum);
	
	for( vector<long>::const_iterator c_iter=_groups.find(tag)->second[grpNum].begin(); c_iter!=_groups.find(tag)->second[grpNum].end(); c_iter++ )
		if( name.compare(_names.find(*c_iter)->second)==0 )
			if( _integers.find(*c_iter) != _integers.end() )
				return _integers.find(*c_iter)->second;

	throw CouldNotFindParameterError(name);
}
bool XMLParameters::flagParamForGroupWithTag( const string &name, const int &grpNum, const string &tag ) const
{
	if( !hasGroupTag(tag) ) throw CouldNotFindTagError(tag);
	if( grpNum<0 || grpNum>=numGroupsForTag(tag) ) throw InvalidGroupNumberError(tag,grpNum);
	
	for( vector<long>::const_iterator c_iter=_groups.find(tag)->second[grpNum].begin(); c_iter!=_groups.find(tag)->second[grpNum].end(); c_iter++ )
		if( name.compare(_names.find(*c_iter)->second)==0 )
			if( _flags.find(*c_iter) != _flags.end() )
				return _flags.find(*c_iter)->second;

	throw CouldNotFindParameterError(name);
}
string XMLParameters::labelParamForGroupWithTag( const string &name, const int &grpNum, const string &tag ) const
{
	if( !hasGroupTag(tag) ) throw CouldNotFindTagError(tag);
	if( grpNum<0 || grpNum>=numGroupsForTag(tag) ) throw InvalidGroupNumberError(tag,grpNum);
	
	for( vector<long>::const_iterator c_iter=_groups.find(tag)->second[grpNum].begin(); c_iter!=_groups.find(tag)->second[grpNum].end(); c_iter++ )
		if( name.compare(_names.find(*c_iter)->second)==0 )
			if( _labels.find(*c_iter) != _labels.end() )
				return _labels.find(*c_iter)->second;

	throw CouldNotFindParameterError(name);
}



// Parameters within groups; versions with defaults
double XMLParameters::scalarParamForGroupWithTagOrDefault( const string &name, const int &grpNum, const string &tag, const double &def ) const
{
	if( !hasGroupTag(tag) ) return def;
	if( grpNum<0 || grpNum>=numGroupsForTag(tag) ) return def;
	
	for( vector<long>::const_iterator c_iter=_groups.find(tag)->second[grpNum].begin(); c_iter!=_groups.find(tag)->second[grpNum].end(); c_iter++ )
		if( name.compare(_names.find(*c_iter)->second)==0 )
			if( _scalars.find(*c_iter) != _scalars.end() )
				return _scalars.find(*c_iter)->second;

	// If no scalar found, try integers instead
	for( vector<long>::const_iterator c_iter=_groups.find(tag)->second[grpNum].begin(); c_iter!=_groups.find(tag)->second[grpNum].end(); c_iter++ )
		if( name.compare(_names.find(*c_iter)->second)==0 )
			if( _integers.find(*c_iter) != _integers.end() )
				return _integers.find(*c_iter)->second;

	return def;
}

long XMLParameters::integerParamForGroupWithTagOrDefault( const string &name, const int &grpNum, const string &tag, const long &def ) const
{
	if( !hasGroupTag(tag) ) return def;
	if( grpNum<0 || grpNum>=numGroupsForTag(tag) ) return def;
	
	for( vector<long>::const_iterator c_iter=_groups.find(tag)->second[grpNum].begin(); c_iter!=_groups.find(tag)->second[grpNum].end(); c_iter++ )
		if( name.compare(_names.find(*c_iter)->second)==0 )
			if( _integers.find(*c_iter) != _integers.end() )
				return _integers.find(*c_iter)->second;

	return def;
}
bool XMLParameters::flagParamForGroupWithTagOrDefault( const string &name, const int &grpNum, const string &tag, const bool &def ) const
{
	if( !hasGroupTag(tag) ) return def;
	if( grpNum<0 || grpNum>=numGroupsForTag(tag) ) return def;
	
	for( vector<long>::const_iterator c_iter=_groups.find(tag)->second[grpNum].begin(); c_iter!=_groups.find(tag)->second[grpNum].end(); c_iter++ )
		if( name.compare(_names.find(*c_iter)->second)==0 )
			if( _flags.find(*c_iter) != _flags.end() )
				return _flags.find(*c_iter)->second;

	return def;
}
string XMLParameters::labelParamForGroupWithTagOrDefault( const string &name, const int &grpNum, const string &tag, const string &def ) const
{
	if( !hasGroupTag(tag) ) return def;
	if( grpNum<0 || grpNum>=numGroupsForTag(tag) ) return def;
	
	for( vector<long>::const_iterator c_iter=_groups.find(tag)->second[grpNum].begin(); c_iter!=_groups.find(tag)->second[grpNum].end(); c_iter++ )
		if( name.compare(_names.find(*c_iter)->second)==0 )
			if( _labels.find(*c_iter) != _labels.end() )
				return _labels.find(*c_iter)->second;

	return def;
}



