#include "networkTriSpringSierpinski.h"


#pragma mark -
#pragma mark Constructors / destructor
#pragma mark -

networkTriSpringSierpinski::networkTriSpringSierpinski(
    double X,
    double Y,
    const XMLParameters *params,
    ostream *log,
    const drivingBase *driving )
	: networkTriSpring(X,Y,params,log,driving)
{
	using namespace parameterParsing;

    // Optional parameter for the maximum level over which fractality is present.
    // Check for validity in ::_initSprings(), once the maximum level is known.
    // Defaults to -1, meaning set to the total number of levels once known.
    _maxFractalLevel = _params->integerParamOrDefault(netTSSierp_maxFractalLevel,-1);

    // Optional parameter to include the edges of the 'major' triangles (i.e. the largest
    // fractal level).
    _includeMajorEdges = _params->flagParamOrDefault(netTSSierp_includeMajorEdges,false);

	// Optional parameter to add random springs after the fractal lattice has been generated.
	_addRandomSpringProb = _params->scalarParamOrDefault(netTSSierp_addRandomSpringProb,0.0);
	if( _addRandomSpringProb<0.0 || _addRandomSpringProb>1.0 )
		throw BadParameter(netTSSierp_addRandomSpringProb,"must be in the range [0,1]",__FILE__,__LINE__);
}

#pragma mark -
#pragma mark Spring generation
#pragma mark -

//
// Initialise springs. Called during initialisation; after the arrays have been sized and lattice dimensions determined.
//
void networkTriSpringSierpinski::_initSprings()
{
    // Get the number of levels in the Sierpinski triangle; also check lattice dimensions are suitable.
    __getLevels();

    // Construct the small triangles that make up the full Sierpinksi triangle.
    std::set<__triangle> allTriangles = __getTriangles();

    // Remove edges along the edges of the 'major' triangles, i.e. at the highest fractal level.
    std::set<__edge> unwantedEdges;
    if( !_includeMajorEdges ) unwantedEdges = __getMajorEdges();

    // Convert triangles to (unique) edges and attempt a connection for each one.
    for( auto &e : __getEdges(allTriangles) )
    {
        // Skip unwanted edges. Easiest to include them in the recursive routine, then optionally remove them here.
        if( !_includeMajorEdges && unwantedEdges.find(e)!=unwantedEdges.end() ) continue;

        // Attempt to add a spring between these two nodes (may yet be 'diluted').
        _trialConnection( e[0].first, e[0].second, e[1].first, e[1].second );
    }

	// Call method to fill random springs. Could move _refillSprings() to parent class in future if another
	// child needed it, but prefer to avoid adding more code to parent class if avoidable.
	_refillSprings( _addRandomSpringProb );
}

// Adds springs at random with the given probability, checking first they do not exist. Bond dilution is overriden.
void networkTriSpringSierpinski::_refillSprings( double prob )
{
	// Skip all of the below if the probability is zero (assumed already checked to be in range [0,1]).
	if( !prob ) return;

	// Loop through all possible connections.
	for( auto i=0u; i<_numX; i++ )
		for( auto j=0u; j<_numY; j++ )
		{
			// For each orientation, add with given probability - _trialConnection() checks it does not already exist.
			// Override normal bond dilution when calling _trialConnection (last arg is 'true').
			if( pRNG::uniform() <= prob ) _trialConnection( i, j, i+1,               j  , true );
			if( pRNG::uniform() <= prob ) _trialConnection( i, j, ( j%2 ? i-1 : i ), j+1, true );	
			if( pRNG::uniform() <= prob ) _trialConnection( i, j, ( j%2 ? i : i+1 ), j+1, true );
		}
}



#pragma mark -
#pragma mark Recursive network generation
#pragma mark -

// Tries to get the number of levels, and checks suitability of lattice dimensions.
void networkTriSpringSierpinski::__getLevels()
{
	using namespace parameterParsing;

     // Requires numbers of nodes in both directions to be the same.
    if( _numX != _numY )
        throw BadParameter("(lattice dimensions)","box dimensions must give same number of nodes in x and y directions",__FILE__,__LINE__);

    // Now get the number of levels in total. 
    _highestLevel = 1;
    while( 1<<_highestLevel < int(_numX) ) _highestLevel++;

    // Number of nodes must also be a power of 2.
    if( 1<<_highestLevel != int(_numX) )
       throw BadParameter("(lattice dimensions)","lattice nodes must be a power of 2 in each direction",__FILE__,__LINE__);
    
    // If a number of fractal levels was specified, check it is suitable; else assume all levels are fractal.
    if( _maxFractalLevel == -1 )
    {
        _maxFractalLevel = _highestLevel;
    }
    else
    {
        // 1 fractal level means no fractality at all, i.e. attempt to fill all springs; less makes no sense.
        if( _maxFractalLevel < 1 )
            throw BadParameter(netTSSierp_maxFractalLevel,"number of fractal levels must be >=1 (1 means no fractality)",__FILE__,__LINE__);
        
        // Cannot have more fractal levels than there are levels.
        if( _maxFractalLevel > _highestLevel )
            throw BadParameter(netTSSierp_maxFractalLevel,"number of fractal levels cannot exceed the total number of levels",__FILE__,__LINE__);
    }

    //
    // Output extra information to the log file.
    //
    *_log << " - number of levels in Sierpinski triangle=" << _highestLevel << std::endl;
    *_log << " - number of fractal levels=" << _maxFractalLevel << std::endl;    
}

// Get all of the (small) triangles that make up the Sierpinski triangles, fractal over the required number of levels.
std::set<networkTriSpringSierpinski::__triangle>
networkTriSpringSierpinski::__getTriangles() const
{
    // Shorthands.
    int L = 1<<_highestLevel;                                     // Should be _numX==_numY; already checked.

    // Full list of triangles, including repeated edges.
    std::set<__triangle> allTriangles;

    // Start with the 'upper' triangle, i.e. the one with the horizontal edge at the 'bottom'.
	// Vertices indexed anti-clockwise starting from the top (can matter for child classes).
    for( auto &tri : _recurseTriangles({ {{ {L/2,L}, {0,0}, {L,0} }} },_highestLevel) )
        allTriangles.insert( tri );

    // Repeat for the 'lower' triangle, i.e. the one with the horizontal edge at 'top'.
	// Vertices indexed clockwise starting from the bottom (i.e. 'top' prior to inversion).
    for( auto &tri : _recurseTriangles({ {{ {0,0}, {-L/2,L}, {L/2,L} }} },_highestLevel) )
        allTriangles.insert( tri );

    return allTriangles;
}

// The main recursion function for generating the Sierpinski triangle, with possibility of some non-fractal levels.
std::set<networkTriSpringSierpinski::__triangle>
networkTriSpringSierpinski::_recurseTriangles( std::set<__triangle> triangles, int level ) const
{
    // If at final level of recursion, return with the current collection of triangles.
    if( level==0 ) return triangles;

    // Calculate (once per level) the offset required to ensure the indexing remains correct throughout.
    int offset_x = ( level==1 ? 1 : 0 );

    // Replace each triangle in the list with 3 (fractal) / 4 (non-fractal) triangles.
    std::set<__triangle> subTriangles;
    for( auto &tri : triangles )
    {
        // Get the index pairings for the mid-nodes. This includes a 'tweak' (offset) at the smallest level.
        __node
            mid01 = _midNode( tri[0], tri[1], offset_x ),
            mid02 = _midNode( tri[0], tri[2], offset_x ),
            mid12 = _midNode( tri[1], tri[2], offset_x );

        // Create new sequences of triangles in each of the 3 corners of the original triangle.
        for( auto &tri1 : _recurseTriangles({{tri[0],mid01,mid02}},level-1) )
            subTriangles.insert( tri1 );

       for( auto &tri2 : _recurseTriangles({{tri[1],mid01,mid12}},level-1) )
            subTriangles.insert( tri2 );

       for( auto &tri3 : _recurseTriangles({{tri[2],mid02,mid12}},level-1) )
            subTriangles.insert( tri3 );

        // If not yet at a fractal level, also include the central triangle in the recursion.
        if( level > _maxFractalLevel )
            for( auto &tri4 : _recurseTriangles({{mid01,mid02,mid12}},level-1) )
                subTriangles.insert( tri4 );
    }

    return subTriangles;
}

// Returns a set of the (normalised) edges of the major triangles, i.e. those at the largest fractal level.
std::set< networkTriSpringSierpinski::__edge >
networkTriSpringSierpinski::__getMajorEdges() const
{
    std::set<__edge> majorEdges;

    int nMinor = 1<<_maxFractalLevel;       // Number of springs along the edges of each major triangle.

    // Horizontal springs that run along major triangle edges.
    for( auto j=0u; j<_numY; j+=nMinor )
        for( auto i=0u; i<_numX; i++ )
            majorEdges.insert( __normedEdge({i,j},{i+1,j}));

    // Springs that run along the angled edges of all major triangles.
    int iShift;
    for( auto i=0u; i<_numX; i+=nMinor )
        for( auto j=0u; j<_numY; j++ )
        {
            iShift = i + j/2 + ( j%2 ? 1 : 0 );
            majorEdges.insert( __normedEdge( {iShift,j}, {iShift+(j%2?0:1),j+1} ) );                 // SW to NE.

            iShift = i + j/2;
            majorEdges.insert( __normedEdge( {_numX-iShift,j}, {_numX-iShift+(j%2?0:1)-1,j+1} ) );   // SE to NW.
        }
    
    return majorEdges;
}


// Returns a midnode between the two given nodes, with a tweak to ensure it remains correct with half-shifts.
std::pair<int,int> networkTriSpringSierpinski::_midNode( __node node1, __node node2, int xOffset ) const
{
    __node mid;

    // Must always round downwards, so away from zero if negative. 
    int x = node1.first + node2.first + xOffset;
    mid.first = ( x<0 ? (x-1)/2 : x/2 );

    int y = node1.second + node2.second;
    mid.second = ( y<0 ? (y-1)/2 : y/2 );

    return mid;
}

// Converts triangles to unique edges.
std::set< networkTriSpringSierpinski::__edge >
networkTriSpringSierpinski::__getEdges( std::set<__triangle> triangles ) const
{
    std::set<__edge> edges;

    for( auto &tri : triangles )
    {
        edges.emplace( __normedEdge(tri[0],tri[1]) );
        edges.emplace( __normedEdge(tri[0],tri[2]) );
        edges.emplace( __normedEdge(tri[1],tri[2]) );
    }

//    debugCheckEdges( edges );       // Check for repeated edges, or bad edges (with both nodes the same).
 
    return edges;
}


#pragma mark -
#pragma mark Utility methods
#pragma mark -

// Wraps node indices (i,j) such that i \in [0,_numX) and j \in [0,_numY).
void networkTriSpringSierpinski::__wrapNodeIndices( __node &e ) const
{
    while( e.first <  0          ) e.first += _numX;
    while( e.first >= int(_numX) ) e.first -= _numX;

    while( e.second <  0          ) e.second += _numY;
    while( e.second >= int(_numX) ) e.second -= _numY;
}

// Returns a normalised edge, i.e. with each node indices' wrapped, and first node has lower indices.
networkTriSpringSierpinski::__edge
networkTriSpringSierpinski::__normedEdge(
    networkTriSpringSierpinski::__node n1,
    networkTriSpringSierpinski::__node n2 ) const
{
    __edge e{{n1,n2}};

    // Wrap node indices.
    __wrapNodeIndices(e[0]);
    __wrapNodeIndices(e[1]);

    // Order so that x index of node 1 is less than that of node 2.
    if( e[0].first > e[1].first )
        std::swap( e[0], e[1] );

    // If the x-indices are the same, order by y instead.
    if( e[0].first==e[1].first )
        if( e[0].second > e[1].second )
            std::swap( e[0], e[1] );

    return e;
}


#pragma mark -
#pragma mark Debugging.
#pragma mark -
 
/// Check for repeated edges; also bad edges (i.e. with identical end nodes).
void networkTriSpringSierpinski::debugCheckEdges( std::set<__edge> edges ) const
{
   // Check for bad edges, i.e. both nodes the same.
    for( auto &e : edges )
        if( e[0]==e[1] )
            throw SystemError("constructing edges from triangles","same nodes for both ends",__FILE__,__LINE__);

    // Check for copies.
    for( auto &e1 : edges )
        for( auto &e2 : edges )
            if( e1 != e2 )
            {
                __node n1_1{e1[0]}, n1_2{e1[1]}, n2_1{e2[0]}, n2_2{e2[1]};

                __wrapNodeIndices(n1_1);
                __wrapNodeIndices(n1_2);
                __wrapNodeIndices(n2_1);
                __wrapNodeIndices(n2_2);

                bool repeatedEdge = false;

                // Don't assume the edges have been normalise correctly - check every option.
                // (although since we check e1 against e2 and e2 against e1, half should be enough ...
                // ... this is overkill but safe).
                if( n1_1==n2_1 && n1_2==n2_2 ) repeatedEdge = true;
                if( n1_2==n2_1 && n1_1==n2_2 ) repeatedEdge = true;
                if( n1_1==n2_2 && n1_2==n2_1 ) repeatedEdge = true;
                if( n1_2==n2_2 && n1_1==n2_1 ) repeatedEdge = true;

                if( repeatedEdge )
                    throw SystemError("constructing edges from triangles","repeated edhe",__FILE__,__LINE__);
            }

}