#include "drivingPressureGdt.h"

#pragma mark -
#pragma mark Constructor
#pragma mark -

drivingPressureGdt::drivingPressureGdt( double X, double Y, const XMLParameters *params, ostream *log )
	: drivingBase(X,Y,params,log)
{
	using namespace parameterParsing;

	// Pressure gradients in x and y-directions.
	double
		Px = _params->scalarParamOrDefault(pressureGdt_X,0.0),
		Py = _params->scalarParamOrDefault(pressureGdt_Y,0.0);
	
	// Convert to a purely imaginary 2-vector.
	_pGdt[0] = { 0.0, Px };
	_pGdt[1] = { 0.0, Py };
}
