#
# Reads in frequency sweep data and plots. Call from the command line.
#
# Initialises according to the general 'runBase' class, and therefore assumes
# nothing beyond what is included in the standard output files. Specialist
# plotting classes may be found in the 'plotControl' sub-module.
#


#
# Imports.
#

# Standard imports.
import sys
import argparse
import matplotlib.pyplot as plt

# Local imports.
from dataAnalysis import dataBase


#
# Intended to be called from the command line.
#
if __name__ == "__main__":

	#
	# Command line arguments. Dirs to plot, all assumed to have a number of 'run?' subdirs.
	#
	parser = argparse.ArgumentParser( description="Plot table data from fibreNetHI" )

	parser.add_argument( "dirs", help="directories not including 'run?'", nargs="*", default=['.'] )
	parser.add_argument( "--verbose", help="Verbose output", action="store_true" )

	parser.add_argument( "modulus", help="Modulus label to plot", nargs="?", type=str, default="G" )
	parser.add_argument( "--net", help="Only plot network contributions", action="store_true" )
	parser.add_argument( "--fl", help="Only plot fluid contributions", action="store_true" )

	parser.add_argument( "--netNA", help="Overlay plot of network non-affinity (if any)", action="store_true" ) 
	parser.add_argument( "--flNA", help="Overlay plot of fluid non-affinity (if any)", action="store_true" )
	parser.add_argument( "--DC", help="Overlay plot of decoupling measure (if any)", action="store_true" ) 
	parser.add_argument( "--flux", help="Overlap plot of interfacial flux (if any)", action="store_true" )

	parser.add_argument( "--fmoms", help="Plot the force moments instead of moduli", action="store_true" )

	args = parser.parse_args()



	# Parse the data files into objects.
	objects = [ dataBase.dataBase(dir,verbose=args.verbose) for dir in args.dirs ]

	#
	# Force momemnts are plotted separately to other quantities.
	#
	if args.fmoms:

		# Only single runs allowed for the force moment plots (too much data otherwise).
		if len(objects)>1:
			print( "This script currently only supports single data sets for displaying force moments." )
			sys.exit(-1)
		
		# Shorthand for the only set of runs.
		obj = objects[0]
		
		# Start from the first moment and increment as long as their is data in the table.
		# Note that the zero'th moment is not currently output by the C++ code, so start labels at 1.
		mom = 0
		while True:
			mom += 1

			clr = ["r","b","c","m","y","k"][mom%6]			# Colours for different moments. Avoid red and green together.
			label = "f^{}".format(mom)						# Label by moment only. 

			try:
				plt.errorbar( obj.means("w"), obj.means("f{}_real".format(mom)), obj.errors("f{}_real".format(mom)), color=clr, ls="-", label=label )
				plt.errorbar( obj.means("w"), obj.means("f{}_imag".format(mom)), obj.errors("f{}_imag".format(mom)), color=clr, ls="--" )

			except KeyError:
				break

		# Finalise the plot.
		plt.xlabel( r"$\omega$" )
		plt.ylabel( "Moment" )
		plt.semilogx()				# Moments can be zero, and odd moments can be negative.
		plt.legend()
		plt.show()

		# Do not contiue to the remainder of the script, which is the plotting moduli.
		sys.exit(-1)

	#
	# Plot the requested modulus, and overlay any other quantities requested.
	#

	# Sanity check.
	if args.net and args.fl:
		print( "Cannot select both network and fluid; select neither to plot the sum." )
		sys.exit(-1)

	# Set up the axes, with the potential for shared x-axes with another quantity.
	fig, axes1 = plt.subplots()

	# Guidelines first, using the first data set for the frequencies.
	w = objects[0].means("w")
	axes1.plot( w, objects[0].eta*w, "k--" )

	# What moduli to plot, storage and loss moduli.
	storageStr = args.modulus + "'"  + ( "_net" if args.net else "" ) + ( "_fl" if args.fl else "" )
	lossStr    = args.modulus + "''" + ( "_net" if args.net else "" ) + ( "_fl" if args.fl else "" )

	for i, obj in enumerate(objects):
		clr = ["r","b","c","m","y","k"][i%6]

		# Plot shear moduli.
		axes1.errorbar( obj.means("w"), obj.means(storageStr), obj.errors(storageStr), color=clr, fmt="-" , label="{0}".format(obj.dir()) )
		axes1.errorbar( obj.means("w"), obj.means(lossStr   ), obj.errors(lossStr   ), color=clr, fmt="--" )
		

	#
	# Finalise axes1, for the moduli.
	#
	axes1.loglog()
	axes1.set_xlabel( r"$\omega$" )
	axes1.legend()

	#
	# Optionally overlay another measure using the right-hand axes.
	#
	if args.netNA + args.flNA + args.DC + args.flux >= 1:

		# Can only plot one at a time.
		if args.netNA + args.flNA + args.DC + args.flux > 1:
			print( "Can only plot one of the optional metrics (NA/flux etc.)." )
			sys.exit()

		# Quantity to plot.
		quant = None
		if args.netNA   : quant = "NA_net"
		if args.flNA    : quant = "NA_fl"
		if args.DC      : quant = "DC"
		if args.flux	: quant = "flux"
		
		# Plot it using the right-hand axes.
		axes2 = axes1.twinx()

		# Plot the quantity - if it exists.
		try:
			for i, obj in enumerate(objects):
				clr = ["r","b","c","m","y","k"][i%6]
				axes2.errorbar( w, obj.means(quant), obj.errors(quant), color=clr, fmt=":" )

			print( "Plotting {} with dotted line; right-hand axes.".format(quant) )
			axes2.loglog()

		except KeyError:
			print( "Could not find quantity '{}' in the table data.".format(quant) )

	
	#
	# Display.
	#
	plt.show()



