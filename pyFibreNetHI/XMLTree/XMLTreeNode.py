#
# Single node in the XMLTree; currently simple and restrictive,
# as befits its original purpose, but could easily be extended
# and generalised if need be.
#


# Standard imports
import xml.sax			# XML Parsing



#
# Single node in the tree
#
class XMLTreeNode( object ):

	def __init__( self, tag, attributes=None ):
		"""Initialise wtih tag and attributes; these are subsequently read-only.
		"attributes" is assumed to be an instance of xml.sax.xmlreader.AttributesImpl,
		which is converted into a key:value dictionary upon initialisation.

		To permit full recovery from saved data, can also initialise with a output
		of __repr__(). The key for this is that the second argument is None.
		"""

		# Initialise variables
		self._children   = []			# Can be appended, cleared and read
		self._parentNode = None			# Read only
		self._dataString = ""			# Read-write
		self._tagName    = None			# Read-only
		self._attributes = {}			# Read-only

		if attributes==None:

			# Initialising from __repr__() of tag, which is typically a string; convert to list first
			try:
				tag = eval(tag)
			except:
				pass

			self._tagName    = tag[0]	# Must match __repr__()
			self._attributes = tag[1]
			self._dataString = tag[2]
		else:
			self._tagName    = tag
			self._attributes = ( {} if attributes==None else XMLTreeNode.attrs(attributes) )


	#
	# Accessing node attributes and modification after initialisation
	#
	
	def addChild( self, newChild ):
		"""Adds a new child to the list of children."""
		self._children.append( newChild )
	
	def numChildren( self ):
		"""No. of children; also accessible as len()."""
		return len(self._children)
	def __len__(self):
		"""No. of children; also accessible as numChildren()."""
		return self.numChildren()
	
	def clearChildren( self ):
		"""Clear all children."""
		self._children = []

	def child( self, n ):
		"""Returns the n'th node, or None if out of range."""
		if n<0 or n>=len(self): return None
		return self._children[n]

	# These properties are read-only
	@property
	def tagName(self):
		return self._tagName
	
	@property
	def attributes(self):
		return self._attributes
	
	# Parent is read-write
	@property
	def parentNode( self ):
		return self._parentNode
	@parentNode.setter
	def parentNode( self, value ):
		self._parentNode = value

	# The children property can be read as normal, but can only be extended by using "addChild()"
	@property
	def children(self):
		return self._children

	# The data string is read-write
	@property
	def dataString(self):
		return self._dataString
	@dataString.setter
	def dataString(self,value):
		self._dataString = value

	def numChildren( self ):
		"""Returns the number of children."""
		return len(self._children)
	
	# Simplified output; non-recoverable. Tag name followed by attributes dictionary, if any, followed
	# by the data string in single quotes, if any. The data string is truncated to the length defined
	# by the static method maxDataLen().
	def __str__( self ):
		str = self._tagName

		if( len(self._attributes) ):
			str += " {}".format(self.attributes)

		maxLen = XMLTreeNode.maxDataLen()			# The maximum length for the dat string
		if( self._dataString ):
			str += " '{}'".format((self._dataString[:maxLen]+"..." if len(self._dataString)>=maxLen else self._dataString))

		return str

	# Full output; similar to __str__() but does not truncate the data string and empty fields are still output
	def __repr__( self ):		
		return "['{0}',{1},'{2}']".format(self.tagName,self.attributes,self.dataString )


	#
	# Static members
	#

	@staticmethod
	def maxDataLen():		# The maximum no. of characters to represent
		return 50
	
	@staticmethod
	def attrs( attributes ):
		"""Generic attribute extraction; converts an instance of xml.sax.xmlreader.AttributeImpl
		to key:value dictionary, where values are converted to bool, int or float if possible
		(else left as string).
		"""
		
		ret = {}
		for attr in attributes.getQNames():
			
			# Value of this attribute; initially a string (not the origial unicode)
			val = str( attributes.getValueByQName( attr ) )

			# Casting to bool doesn't raise exceptions, so handle this differently to floats and ints
			if val.casefold() == "true".casefold():
				ret[ str(attr) ] = True
				continue

			if val.casefold() == "false".casefold():
				ret[ str(attr) ] = False
				continue

			try:
				val = float( val )
			except ValueError:
				pass			# Not a float

			try:				# Check if the string can be converted to an integer, and if so, is its value unchanged?
				x = int( val )
				if x == val: val = x
			except ValueError:
				pass			# Not an integer

			# Use attribute name as key; if the value hasn't been converted yet (i.e. all of the exceptions above were thrown), store as string
			ret[str(attr)] = val

		return ret

