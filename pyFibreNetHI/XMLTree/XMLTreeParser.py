#
# Parses the given XML file and returns an object of type "XMLTree",
# which is a hierarchichal representation of the whole XML file, with
# methods to aid further parsing.
#
# The root node is taken to be the first encountered (after the XML preamble),
# which it should be.
#
#


# Standard imports
import xml.sax			# XML Parsing
import os				# For storing the full filename with the tree

# Local imports
from . import XMLTreeNode
from . import XMLTree

	

# 
# Parsing object.
#
class XMLTreeParser( xml.sax.ContentHandler ):

	def __init__( self ):
		"""Initialises variables."""

		super().__init__()

		# Content (including whitespace) built up incrementally as a string
		self._data = ""
		
		# The tree hierarchy will go here
		self._tree = None
		
	def tree( self ):
		"""Return the (parsed) tree."""
		return self._tree
	
	def startElement( self, tag, attributes ):
		"""Called when a star tag is encountered."""

		# Clear the data array
		self._data = ""

		# Initialise the node object
		node = XMLTreeNode( tag, attributes )

		# The first tag encountered is assumed to be the enclosing block (as it should be)
		if not self._tree:
			self._tree = XMLTree( node )
			return

		# Initialise the node object with the tag name and attributes; add to the tree (will become the open node)
		self._tree.addChildToOpenNode( node )

	# Called when an end element is encountered
	def endElement( self, tag ):
		self._tree.closeNodeWithData( self._data.strip() )
		self._data = ""		

	# Called for data between elements, which might just be whitespace (i.e. eol). Appends to growing string;
	# will be dealt with when the block is closed [i.e. the end tag is reached]
	def characters( self, data ):
		self._data += str(data)


	
	#
	# Class methods
	#
	
	@classmethod
	def parseFileAsTree( cls, fname ):	
		"""Parses the given file and returns as an instance of XMLTree."""

		# Initialise the XML parser
		state_parser = XMLTreeParser()
		p = xml.sax.make_parser()
		p.setContentHandler( state_parser )

		# Parse
		p.parse( fname )

		# Store full filename with the tree - may be useful at a later date; may fail if a file object rather than a file name
		try:
			state_parser.tree().filename = os.path.abspath( fname )
		except AttributeError:
			state_parser.tree().filename = None			
		except TypeError:
			state_parser.tree().filename = None			
		
		# Return the corresponding XMLTree object
		return state_parser.tree()

