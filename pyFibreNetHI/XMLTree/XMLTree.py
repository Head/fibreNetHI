#
# XMLTree object; essentally a linked-list of XMLTreeNode objects
# with routines for accessing the hierarchy.
#
# Nodes are deep-copied when added, so different trees constructed from
# the same nodes are distinct - altering the node of one will not affect
# the other.
#
# This means that, when referring to nodes using a node object, it has to
# be the object after adding to the tree, not before (using for instance
# the nodeAtIndex() method).
#
# Initialise with the base node, then add to the tree using one of the
# supported methods (currently: Adding/Closing open nodes, useful when
# reading straight from an XML file; adding child nodes to existing
# end-branch nodes, useful for when reconstructing the tree; others
# may be added).
#
# Can also get a subtree starting from a given node, as a copy.
#
# Methods added when needed; try 'help' for current list.
#


# Standard imports
import xml.sax			# XML Parsing
import copy				# Deep-copy nodes when they are added to the tree
import os				# For storing the full filename with the tree

# Local imports
from . import XMLTreeNode



#
# Custom exceptions
#
class AlreadyClosedBaseNode(Exception):
	def __init__(self,baseTag):
		self._baseTag = baseTag
	def __str__(self):
		return "Tried to close the open node after already closing the base node (in XMLTree object with basename '{}')".format(self._baseTag)

class BadNodeIndex(Exception):
	def __init__(self,index,baseTag):
		self._index, self._baseTag = index, baseTag
	def __str__(self):
		return "Bad node index '{0}' for XMLTree with base node tag name '{1}'".format(self._index,self._baseTag)

class CannotAddSurfaceChild(Exception):
	def __init__(self,index,baseTag):
		self._index, self._baseTag = index, baseTag
	def __str__(self):
		return "Cannot add surface child to XMLTree with base tag '{0}' with index '{1}'".format(self._baseTag,self._index)
		
class TreeDoesNotHaveNode(Exception):
	def __init__(self,node,baseTag):
		self._node, self._baseTag = node, baseTag
	def __str__(self):
		return "XMLTree with basenode '{0}' does not have the node '{1}'".format(self._baseTag,self._node)


#
# Whole XML file parsed as a tree, with each node an instance of the class XMLTreeNode
#
class XMLTree( object ):

	def __init__( self, baseNode ):
		"""Object for a whole XML file parsed as a tree, in which each node is an
		instance of XMLTreeNode. Initialise with the base node (itself an instance
		of XMLTreeNode).
		"""
	
		# Copy over the node (only one root allowed, index [], i.e. and empty list)
		self._baseNode = XMLTreeNode( baseNode.__repr__() )

		# One way to construct the tree is by opening a closing a node (corresponding to moving up and down
		# levels in the tree); initialise the persistent open-node pointer to be the base node.
		self._openNode = self._baseNode
		
		# An optional parameter for the whole tree is a filename, presumably from which it was recovered
		self._fname = None
		

	def addChildToOpenNode( self, _node ):
		"""Building the tree: At any one time have a single 'open' node; use addChildToOpenNode() to add to
		this, and closeNode() to close it (and move one level up the hierarchy). When a child is added to
		a node, it becomes the open node!
		"""

		# Copy the node and clear its parent/child links
		node = XMLTreeNode( _node.__repr__() )

		node.parentNode = self._openNode				# 2-way link between child and parent
		self._openNode.children.append( node )
		self._openNode = node							# New node becomes the open node


	def closeNodeWithData( self, data = "" ):
		"""Close the current open node, with the given data string if any.
		Raises an exception if the node is already closed.
		"""
		if self._openNode == None:
			raise AlreadyClosedBaseNode
		
		self._openNode.dataString = data
		self._openNode = ( None if self._openNode==self._baseNode else self._openNode.parentNode )
	
	def setOpenNodeToBaseNode( self ):
		self._openNode = self._baseNode
	
	def addSurfaceChild( self, index, _node ):
		"""Add a 'surface child' (i.e. assuming all parents exist) ; also assumes the index of this child
		is one plus the current existing one.
		"""

		# Copy the node and clear its parent/child links
		node = XMLTreeNode.XMLTreeNode( _node.__repr__() )

		if len(index)==0:
			raise CannotAddSurfaceChild(index,self._baseNode.tagName)

		parent = self.nodeAtIndex(index[:-1])
		if parent==None:
			raise CannotAddSurfaceChild(index,self._baseNode.tagName)

		if parent.numChildren() != index[-1]:
			raise CannotAddSurfaceChild(index,self._baseNode.tagName)
		
		node.parentNode = parent
		parent.addChild(node)
	
	def newSubTreeStartingAtNode( self, node ):
		"""Subtrees starting from the given node (or index; see below). Performs a deepcopy on all
		nodes, so this is a distinct copy of the original tree structure.
		"""

		# Is the node in this tree?
		if not self.hasNode(node):
			raise TreeDoesNotHaveNode(node,self._baseNode.tagName)

		# Initialise a new tree with the current node as the base node
		newTree = XMLTree( node )			# Copies node and clears its parent and child fields

		# And all nodes from the original one; they are all copied and have their parent/child links renewed
		def addFromNode( _node, base ):
			if not base: newTree.addChildToOpenNode( _node )
			for child in _node.children:
				addFromNode( child, False )
			if not base: newTree.closeNodeWithData( _node.dataString )				

		addFromNode( node, base = True )

		return newTree

	def newSubTreeStartingAtIndex( self, index ):
		return self.newSubTreeStartingAtNode( self.nodeAtIndex(index) )
		

	#
	# Output
	#
		
	# Simplified output; non-recoverable. A very basic tree representation with left-alignment to represent
	# the level of the tree, and ordered by blocks corresponding to a common parent node.
	def __str__( self ):
		def strForNodeAndChildren( node, padding ):
			str = node.__str__() + "\n"
			for child in node.children:
				str += padding + strForNodeAndChildren(child,padding+"|\t")
			return str

		return strForNodeAndChildren( self._baseNode, "\t" )

	# Full output; recoverable. Needs to be kept in sync with the class method 'recoverFromFile()'
	def __repr__( self ):
		
		# Need: All links; all indices and their corresponding node object representations
		indexNodeList = []
		for index in self.allIndices():
			indexNodeList.append( [ index, repr(self.nodeAtIndex(index)) ] )
	
		return repr( { "nodes":indexNodeList, "links":self.allLinks() } )

	#
	# Properties
	#
	@property
	def filename( self ):
		return self._fname
	@filename.setter
	def filename( self, value ):
		self._fname = value
	
	#
	# Tree traversal; using unique index lists (i,j,k,...) or arbitrary length, where each integer refers to the
	# i'th child of that node.
	#
	
	def baseNode( self ):
		"""Returns the base node."""
		return self._baseNode

	def baseTag( self ):
		"""Returns the tag name of the base node."""
		return self._baseNode.tagName
	
	def nodeAtIndex( self, index ):
		"""Return node at the given location; one of two synonyms."""
		return self.nodeAtLocation(index)

	def nodeAtLocation( self, index ):
		"""Return node at the given location; one of two synonyms."""

		# Check node indices have length
		try:
			len(index)
		except TypeError:
			raise BadNodeIndex(index,self._baseNode.tagName)

		# if index is empty, return the base node
		if len(index)==0: return self._baseNode
		
		# Walk through the tree
		node = self._baseNode		# Starting node
		for i in index:
			if i<0 or i>=len(node): raise BadNodeIndex(index,self._baseNode.tagName)
			node = node.child(i)
		
		return node

	def allIndices( self ):
		"""Returns a list of location indices of all nodes with the given tag name; case sensitive.
		Call with tag==None to return all indices (can also call self.allIndices() ).
		"""
		return self.locationsOfNodesWithTag(None)

	def locationsOfNodesWithTag( self, tag ):
		indices = []
		
		def checkTag( node, indices, currIndex ):
			if tag==None or node.tagName == tag:
				indices.append( currIndex )
			for i in range(node.numChildren()):
				checkTag(node.child(i),indices,currIndex+[i])

		checkTag( self._baseNode,indices,[] )
		
		return indices
	
	def nodesWithTag( self, tag ):
		"""Returns a list of nodes with the given tag name; case sensitive."""
		return [ self.nodeAtLocation(index) for index in self.locationsOfNodesWithTag(tag) ]
	
	def indexOfNode( self, node ):
		"""Returns with the index corresponding to the given node."""
		for index in self.allIndices():
			if self.nodeAtIndex(index) == node:
				return index
		return None
	
	def treeDepth( self ):
		"""Returns with the depth of the tree = maximum length of all indices = max. level index + 1."""
		return max( [len(index) for index in self.allIndices()] )
	
	def hasNode( self, node ):
		"""Returns 'true' if the passed node is in the current tree."""
		for index in self.allIndices():
			if self.nodeAtIndex(index) == node:
				return True
		return False
	
	def allLinks( self ):
		"""Returns list of all nodes and their corresponding children, all as indices. Primarily intended
		to aid in saving and recovery of the tree structure without recourse to pointers. The return format
		is a list of 2-entry lists of (parent,child) index pairings (note that the root node index is an
		empty list).
		"""
		links = []
		
		for nodeIndex in self.allIndices():
			node = self.nodeAtIndex(nodeIndex)
			for child in node.children:
				links.append( [nodeIndex,self.indexOfNode(child)] )
		
		return links
	

	#
	# Class methods
	#
	
	@classmethod
	def recoverFromFile( cls, fname ):
		"""Recover from file (assumed saved using __repr__(), and therefore needs to be kept in sync
		with repr).
		"""

		try:
			# Recover tree; should be a dictionary
			treeDict = eval( open(fname,'r').read() )
			
			# The base node should be the only one with a zero-length index list
			base = [t[1] for t in treeDict["nodes"] if len(t[0])==0 ][0]
			tree = XMLTree( XMLTreeNode.XMLTreeNode(eval(base)) )
			
			# Add subsequent nodes one level at a time
			maxLevel = max( [len(t[0]) for t in treeDict["nodes"]] )
			for level in range(1,maxLevel+1):
				nodesThisLevel = [ t for t in treeDict["nodes"] if len(t[0])==level ]
				
				# Add as a branch. Not very robust; assumes children ordered the same way in __repr__ as here
				for node in nodesThisLevel:
					tree.addSurfaceChild( node[0], XMLTreeNode.XMLTreeNode(eval(node[1])) )

		except IOError as err:
			print( "I/O error when trying to open XMLTree from file '{0}': {1}".format(fname,err) )
		except KeyError as key:
			print( "Data recovered from '{0}' lacks the required key {1}".format(fname,key) )

		# Store full filename with the tree - may be useful at a later date
		tree.filename = os.path.abspath( fname )
		
		return tree		
		
			


	