#
# Runs a (diluted) triangular lattice under simple shear and returns
# the complex shear modulus as a function of frequency.
#


#
# Imports.
#
import sys
import runGenParamsBase				# Parent class.


#
# Class definition.
#
class runShearTriSpring( runGenParamsBase.runGenParamsBase ):

	def __init__( self ):
		"""Runs a (diluted, either isotropic or anisotropic) triangular lattice under simple shear,
		and returns the complex shear modulus as a function of frequency."""
		pass

	# Short description that is displayed at the start of the the command line help message.
	def cmdLineDescripion( self ):
		return "Performs simple shear on a triangular spring lattice with bond dilution."

	#
	# Get (and check if necessary) parameters parsed from the command line.
	#
	def _parameterArgs( self, parser ):
		"""Parses all arguments specific to this class, after calling equivalent parent method."""

		super()._parameterArgs( parser )

		# Lattice and spring  constants.
		parser.add_argument( "-a", help="lattice spacing; defaults to 1", type=float, default=1 )
		parser.add_argument( "-k", help="spring constant; defaults to 1", type=float, default=1 )

		# Dilution. Can be isotropic or anisotropic but not mixed.
		parser.add_argument( "-p", "--dilution", help="(isotropic) dilution parameter; defaults to 1 (i.e. no dilution)", type=float, default=1 )
		parser.add_argument( "--pE_W", help="dilution for East-West oriented springs; defaults to 1 (i.e. no dilution)", type=float, default=1 )
		parser.add_argument( "--pNW_SE", help="dilution for NW-SE oriented springs; defaults to 1 (i.e. no dilution)", type=float, default=1 )
		parser.add_argument( "--pNE_SW", help="dilution for NE-SW oriented springs; defaults to 1 (i.e. no dilution)", type=float, default=1 )

	def checkRemainingArgs( self ):
		"""Checks arguments specific to this class, after calling equivalent parent method."""

		super().checkRemainingArgs()

		# Shorthand.
		args = self._args

		# Check lattice spacing is positive.
		if args.a <= 0:
			print( f"Bad lattice spacing '{args.a}'; must be positive." )
			sys.exit(-1)
		
		# Check spring constant is positive.
		if args.k <= 0:
			print( f"Bad spring constant '{args.k}'; must be positive." )
			sys.exit(-1)

		# Check all dilution parameters are in the range [0,1].
		if args.dilution<0 or args.dilution>1:
			print( f"Bad (isotropic) dilution parameter '{args.dilution}'; must be in the range [0,1]." )
			sys.exit( -1 )

		if args.pE_W<0 or args.pE_W>1:
			print( f"Bad East-West dilution parameter '{args.pE_W}'; must be in the range [0,1]." )
			sys.exit( -1 )

		if args.pNW_SE<0 or args.pNW_SE>1:
			print( f"Bad NW-SE dilution parameter '{args.pNW_SE}'; must be in the range [0,1]." )
			sys.exit( -1 )

		if args.pNE_SW<0 or args.pNE_SW>1:
			print( f"Bad NE-SW dilution parameter '{args.pNE_SW}'; must be in the range [0,1]." )
			sys.exit( -1 )
		
		# Check at most one of isotropic or anisotropic dilutions have been specified.
		if args.dilution<1 and ( args.pE_W<1 or args.pNW_SE<1 or args.pNE_SW<1 ):
			print( f"Bad combination of dilution parameters; cannot specify both isotropic and anisotropic dilutions." )
			sys.exit( -1 )
		
	#
	# Components of the parameters file.
	#
	def _paramsFileDriving( self, runDir ):
		"""Only one type of driving allowed by this sub-class."""
		return '\t<Parameter driving="simple shear" />\n'

	def _paramsNetwork( self, runDir, networkType=None ):
		"""Always a triangular spring network, but other parameters specified from the command line."""

		# Shorthand
		args = self._args

		# Parameter lines.
		if networkType:
			text = f'\t<Parameter networkType="{networkType}" />\n'
		else:
			text = '\t<Parameter networkType="triangular spring" />\n'

		text += f'\t<Parameter latticeSpacing="{args.a}" />\n'
		text += f'\t<Parameter springConstant="{args.k}" />\n'

		# Isotropic dilution (have already checked at most one of isotropic or anisotropic dilutions specified).
		if args.dilution < 1: text += f'\t<Parameter dilutionParam="{args.dilution}" />\n'

		# If any of the anisotropic dilutions are non-zero, output all 3, as expected by the C++ code (at time of writing).
		if args.pE_W<1 or args.pNW_SE<1 or args.pNE_SW<1:
			text += f'\t<Parameter dilution_E_W  ="{args.pE_W}" />\n'
			text += f'\t<Parameter dilution_NW_SE="{args.pNW_SE}" />\n'
			text += f'\t<Parameter dilution_NE_SW="{args.pNE_SW}" />\n'

		return text

#
# Calling from command line.
#
if __name__ == "__main__":
	runShearTriSpring().fullCycle()

