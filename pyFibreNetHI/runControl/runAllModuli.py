#
# Splits each run into 3 separate runs with different applied strains, potentially allowing
# all of the 6 elastic coefficients for a 2D material with no symmetries to be extracted.
#
# Important: Some of these strains change volume, so will not work with an explicit fluid as
# currently written. Even the interpretation of the 'drag only' case (no explicit fluid) would
# require careful consideration.
#


#
# Imports.
#
import sys
import os
import runShearTriSpring			# Parent class.


#
# Class definition.
#
class runAllModuli( runShearTriSpring.runShearTriSpring ):

	def __init__( self ):
		"""Splits each run into 3 separate runs with different applied strains, potentially allowing
		all of the 6 elastic coefficients for a 2D material (with no symmetries) to be extracted.

		Important: Some of these strains change volume, so will not work with an explicit fluid as
		currently written. Even the interpretation of the 'drag only' case (no explicit fluid) would
		require careful consideration of the results, but is allowed by this script."""
		pass

	# Short description that is displayed at the start of the the command line help message.
	def cmdLineDescripion( self ):
		return "Performs each run in triplicate with different applied strains; assumes no explicit fluid"

	#
	# Parameters.
	#
	def checkRemainingArgs( self ):
		"""Check there is no explicit fluid, after calling parent method."""

		super().checkRemainingArgs()

		if self._args.fluid != "drag only":
			print( f"Cannot have explicit fluid type '{self._args.fluid}', as not all applied strains preserve volume." )
			sys.exit( -1 )

	#
	# Run directories.
	#
	def determineRunDirs( self ):
		"Sets up triplicate run directories for each different applied strain."

		# Get the run indices as supplied.
		runsRange = self._getRunIndices()			# Can be 'None'
		
		# Store all run directories, with paths from the current work directory, separately to the common path.
		self._runDirs = []
		for suffix in self.__class__.strainSuffices():

			if runsRange == None:
				self._runDirs.append( f"{self._args.root}_{suffix}" )
			else:
				self._runDirs.extend( [ os.path.join(self._args.root,f"run{run}_{suffix}") for run in runsRange ] )

	# Determines the run index from the run dir name.
	def _runIndexFromDir( self, dir ):
		"""Determines the run index from the run dir name. Assumes base part of dir name is 'run' followed by the
		index, followed by the only or first underscore. This needs to be matched to the suffices. Returns -1 if
		it was not possible to extract an index, for instance if the specified runs was 'None'."""

		# Sanity check, in case the suffices are altered but forgot to change this method.
		# Start from rightmost underscore in case the base name also has an underscore. This will
		# need to be updated if path names with more than one underscore were generated in self.determineRunDirs().
		underLoc = dir.rfind("_")
		if underLoc == -1:
			print( f"Could not determine run index from directory name '{dir}'; has the format of the suffices been altered?" )
			sys.exit( -1 )

		# Count back from the underscore until finding first non-digit.
		firstDig = underLoc
		while dir[firstDig-1].isdigit():
			firstDig -= 1

		# This matches runBase::_runIndexFromDir()
		try:
			return int( dir[firstDig:underLoc] )
		except:
			return -1


	#
	# Driving for each run.
	#

	def _paramsFileDriving( self, runDir ):
		"""Sets an arbitrary strain depending on the suffice of the run directory name."""

		# Extract suffix from the given list, in as general a way as possible.
		suffix = None
		for s in self.__class__.strainSuffices():
			if runDir.endswith( s ):
				suffix = s
				break
		
		if not suffix:
			print( f"Could not determine strain for run directory '{runDir}'; unrecognised suffix." )
			sys.exit( -1 )

		# Generate the lines. As it happens, the suffices can be mapped directly to the strain, but
		# this may need to be generalised in future.
		pText  =  '\t<Parameter driving="arbitrary strain" />\n'

		# For shear, use a symmetric strain tensor, to close the gap on associated theory.
		if suffix == "xy":
			pText += '\t<Parameter gamma_xy="0.5" />\n'
			pText += '\t<Parameter gamma_yx="0.5" />\n'
		else:
			pText += f'\t<Parameter gamma_{suffix}="1.0" />\n'

		return pText


	#
	# Class methods.
	#
	@classmethod
	def strainSuffices( cls ):
		"""Returns a list of suffices used for the different directions of applied strain.
		Should match that in plotControl.plotAllModuli, including the order."""
		return ["xy","xx","yy"]

#
# If called from the command line.
#
if __name__ == "__main__":
	runAllModuli().fullCycle()
