#
# Abstract base class that extends runBase to generate parameters files on-the-fly,
# rather than use an existing. As there are too many parameters to specify from the
# command line, this must be subclassed with more specific classes for which the majority
# of the parameters are pre-defined.
#


#
# Imports.
#

# Standard imports.
import os
import math
import sys
from abc import abstractmethod

# Parent class.
import runBase


#
# Main class definition.
#
class runGenParamsBase( runBase.runBase ):

	def __init__( self ):
		"""Abstract base class that extends runBase to generate parameters files on-the-fly,
			rather than use an existing. As there are too many parameters to specify from the
			command line, this must be subclassed with more specific classes for which the majority
			of the parameters are pre-defined.
			
			The following methods must be overriden to return text for the lines of the parameter
			file relating to certain components (all take run directory name as an argument):
				_paramsFileDriving()			- 	Driving type.
				_paramsNetwork()				-	Network type and any related driving.
			The following may be overriden, in which case the parent method would normally be called.
				_paramsGeometry()				-	Box width/height (X/Y) handled here.
				_paramsFluid()					-	Fluid type, viscosity, and any other required parameters.
				_paramsSolver()					-	Solver used for each frequency.
				_paramsCoupling()				-	Usually just a damping coefficient.
			Try to keep human readable as par as posssible, i.e. prepend all lines with tabs.
			"""
		
		# Initialise persistent variables.
		self._X = None
		self._Y = None


	#
	# Additional parameter parsing with no parameters file.
	#
	def _parameterArgs( self, parser ):
		"""No parameter file is required for subclasses of this class; instead, parameters themselves are defined,
		specified partly here (for generic ones) and partly by the concrete child class."""

		# Geometry, always parsed by this class. Height defaults to 'triangle' (handled by C++ code).
		parser.add_argument( "-X", help="system width; required", type=float, required=True )
		parser.add_argument( "-Y", help="system height; defaults to 'triangle' (i.e. width*\\sqrt{3}/2)" )
		parser.add_argument( "--jiggle", help="node 'jiggling' amplitude; defaults to zero = no jiggling.", type=float, default=0 )

		# Fluid. Stokes requires cell size as well.
		parser.add_argument( "--fluid", help="select fluid type; defaults to 'drag only'", choices=["Stokes","drag only"], default="drag only" )
		parser.add_argument( "--visc" , help="fluid viscosity; defaults to 1", type=float, default=1 )
		parser.add_argument( "--cell" , help="fluid mesh cell size; defaults to 0.83", type=float, default=0.83 )

		# Coupling between network and fluid.
		parser.add_argument( "--damp", help="damping coefficient; defaults to 10", type=float, default=10 )

		# Solver. The argparse module handles this for us.
		parser.add_argument( "--solver", help="select solver type; defaults to 'sparse direct'",
							choices=["sparse direct","dense direct"], default="sparse direct" )

	def checkRemainingArgs( self ):
		"""No need to check the parameters file exista. Other parameters are checked here instead."""

		# Sanity check for width (better to catch errors here rather than when the executable is launched).
		self._X = self._args.X
		if self._X <= 0.0:
			print( f"Bad width parameter '{self._X}'; must be positive." )
			sys.exit(-1)

		# Get height. If not specified, will output 'triangle' to the parameters file.
		if self._args.Y:
			try:
				self._Y = float(self._args.Y)
				if self._Y <= 0.0:
					print( f"Bad height parameter '{self._Y}'; must be positive." )
					sys.exit(-1 )
			except ValueError as err:
				print( f"Could not parse '{self._args.Y}' as a (floating point) height; error was: {err}." )
				sys.exit(-1)
		
		# Make sure viscosity and cell-size are positive.
		if self._args.visc <= 0.0:
			print( f"Bad viscosity parameter '{self._args.visc}'; must be positive." )
			sys.exit(-1)
		
		if self._args.cell <= 0.0:
			print( f"Bad cell size paramter '{self._args.cell}'; must be positive." )
			sys.exit(-1)
		
		# Make sure the damping coefficient is positive.
		if self._args.damp <= 0.0:
			print( f"Bad damping coefficient '{self._args.damp}'; must be positive." )
			sys.exit(-1)

		# Make sure the jiggle amplitude is non-negative.
		if self._args.jiggle < 0.0:
			print( f"Bad jiggle amplitude parameter '{self._args.jiggle}'; must be non-negative." )
			sys.exit(-1)
			
			
	def _setParametersFile( self, runDir ):
		"""Creates and saves the parameters file for a single run on the given directory.
		Exits with 'sys.exit(-1)' for any failure."""

		#
		# Generate the text for the parameters file. Try to keep human-readable.
		#
		paramFileText = '<?xml version="1.0"?>\n<Parameters>\n'

		# System geometry. Optionally overridden.
		paramFileText += '\n<!-- System geometry -->\n'
		paramFileText += self._paramsGeometry( runDir )

		# System driving. Must be provided by a concrete derived class.
		paramFileText += '\n<!-- Driving -->\n'
		paramFileText += self._paramsFileDriving( runDir )

		# Network specification. Must be provided by a concrete derived class.
		paramFileText += '\n<!-- Network -->\n'
		paramFileText += self._paramsNetwork( runDir )

		# Fluid specification. Can be overriden, but is normally fully handled by this class.
		paramFileText += '\n<!-- Fluid -->\n'
		paramFileText += self._paramsFluid( runDir )

		# Coupling.
		paramFileText += '\n<!-- Coupling -->\n'
		paramFileText += self._paramsCoupling( runDir )

		# Solver. Can be overriden, but is normally fully handled by this class.
		paramFileText += '\n<!-- Solver -->\n'
		paramFileText += self._paramsSolver( runDir )

		# Final line.
		paramFileText += "\n</Parameters>\n"

		#
		# Write to file.
		#
		try:
			paramFileName = os.path.join( runDir, runGenParamsBase.paramFileName() )
			with open(paramFileName,'w') as pOut:
				print( paramFileText.strip(), file=pOut )
		except IOError as err:
			print( f"Could not save parameters file to '{paramFileName}'; error was: {err}." )
			sys.exit(-1)

	#
	# Components of the parameter file.
	#

	# Must be overriden.
	@abstractmethod
	def _paramsFileDriving( self, runDir ):
		"""Returns a string containing the line(s) in the parameter file that specify the driving.
		Must be provided by a concrete subclass. Prepend all lines with a single tab for human readability."""
		pass

	@abstractmethod
	def _paramsNetwork( self, runDir ):
		"""Returns a string containing all line in the parameter file that specify the network.
		Must be provided by a concrete subclass. Prepend all lines with a single tab for human readability."""
		pass

	# May be overriden as normal.
	def _paramsGeometry( self, runDir ):
		""""System geometry. Box width (X) and height (Y) handled here."""

		pText = f'\t<Parameter X="{self._X}" />\n'

		if self._args.Y:
			pText += f'\t<Parameter Y="{self._Y}" />\n'
		else:
			pText += '\t<Parameter Y="triangle" />\n'

		if self._args.jiggle:
			pText += f'\t<Parameter jiggleAmplitude="{self._args.jiggle}"/>\n'

		return pText

	def _paramsFluid( self, runDir ):
		""""Fluid type, viscosity, and cell size if required. Normally all handled here but may be overriden."""

		pText  = f'\t<Parameter fluidType="{self._args.fluid}" />\n'
		pText += f'\t<Parameter viscosity="{self._args.visc}" />\n'

		if self._args.fluid != "drag only":
			pText += f'\t<Parameter cellSize="{self._args.cell}" />\n'

		return pText

	def _paramsSolver( self, runDir ):
		"""Main solver. Normally handled here but may be overriden."""
		return f'\t<Parameter solver="{self._args.solver}" />\n'
	
	def _paramsCoupling( self, runDir ):
		"""Coupling, normally just a coupling coefficient, and normally handled here."""
		return f'\t<Parameter dampCoefficient="{self._args.damp}" />\n'

