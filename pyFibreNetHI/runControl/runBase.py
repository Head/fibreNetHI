#
# Base class for initialising and executing a series of runs, using a provided parameters file.
# Can be subclassed for more specific problems / partameter file generation etc.
#


#
# Imports.
#
import argparse
import sys
import math
import os
import shutil
import subprocess
import time


#
# Class definition.
#
class runBase( object ):

	# Constructor.
	def __init__( self ):
		"""Initialises the base class for controlling fibreNetHI runs and presenting results."""
		
		# Initialise persistent variables.
		self._runDirs = None
		self._paramsFilePath = None
		self._mainScript = None

	def cmdLineDescripion( cls ):
		"""A short description of the project that is displayed in the command line help message."""
		return "Initialises a series of identical runs with a given parameters file."

	# Full cycle: create and launch, if requested.
	def fullCycle( self ):
		"""Parses command line arguments, creates run directories, and (if requested) launches the executable(s)."""

		self.parseCommandLineArguments()
		self.setUpRunDirectories()
		self.createRunScript()

		if self._args.launch == "sh":
			self.launchShell()
		
		if self._args.launch == "nohup":
			self.launchNohup()
		
		if self._args.launch == "qsub":
			self.launchQueue()


	#
	# Parsing command line arguments.
	#

	# Main method. Call this before attempting to create run directories.
	def parseCommandLineArguments( self ):
		"""Parses command line arguments. Arguments common to any run (e.g. frequency range)
		parsed here; anything specific to certain types of driving and/or system needs to be
		supplemented by child classes."""

		parser = argparse.ArgumentParser( description=self.cmdLineDescripion() )

		#
		# Baseline command line arguments.
		#

		# Positional, required.
		parser.add_argument( "root"  , help="root directory containing all sub-directories and runs" )
		parser.add_argument( "range" , help="run indices as a Python expression (normally in quotes); 'None' for no run subdirs" )

		# Frequency arguments; only certain combinations allowed (see below).
		parser.add_argument( "--omega" , "-w", help="single frequency \\omega", type=float )
		parser.add_argument( "--begin" , "-b", help="start (higher) frequency of the sweep", type=float )
		parser.add_argument( "--end"   , "-e", help="end (lower) frequency of the sweep", type=float )
		parser.add_argument( "--factor", "-f", help="step factor in sweep", type=float )
		parser.add_argument( "--static", "-z", help="(start with) static problem", action="store_true" )

		# What to do with the script - nothing, launch interactively with sh, asynchronously with nohup, or submit to a queue (which has extra options).
		parser.add_argument( "--launch", help="select launch option; defaults to 'none'", choices=["none","sh","nohup","qsub"], default="none" )

		# Options for queuing on ARC4 in UoL.
		parser.add_argument( "--vmem", help="ARC: size of virtual memory in gigabytes", type=int, default=1 )
		parser.add_argument( "--wall", help="ARC: wall clock time in hours; max. 48", type=int, default=48 )

		# Other optional arguments.
		parser.add_argument( "--archive", help="archive any save files prior to deletion",  action="store_true" )
			# No option to fix seed currently, as this is really only meant for debugging.

		# Class-specific arguments that can be overriden by subclasses.
		self._parameterArgs( parser )

		# Perform the parsing.
		self._args = parser.parse_args()

		# Check frequency arguments; will bail ['sys.exit(-1)'] for bad combinations.
		self.checkFrequencyArgs()

		# Check any other arguments, not related to the frequency.
		self.checkRemainingArgs()

	# Adding parameters, potentially overriden by subclasses.
	def _parameterArgs( self, parser ):
		"""Adds an argument for a compulsory parameters file. Can be overridden, fpr instance if parsameters
		files are created during initialisation."""
		parser.add_argument(
				"--params" ,
				"-p",
				help="path to parameters file; defaults to 'parameters.xml' on local directory",
				type=str,
				default="parameters.xml"
			)

	# Check the various combinations of frequencies.
	def checkFrequencyArgs( self ):
		"""Checks the parsed arguments relating to frequency for sense. This largely follows the C++ code,
		but by catching here rather when executing, it should hopefully be less confusing to the user.
		Exits for a bad combination."""

		# Shorthand.
		args = self._args

		# Selected both static and a single frequency.
		if args.static and args.omega!=None and args.omega>0:
			print( "Cannot specify a single frequency and static; try one or the other." )
			sys.exit(-1)
		
		# Cannot have a single frequency and a range.
		if args.omega!=None and (args.begin!=None or args.end!=None):
			print( "Cannot specify a single frequency and a range simultaneously." )
			sys.exit( -1 )

		# If one range parameter set, all 3 must be.
		numRange = ( 1 if args.begin else 0 ) + ( 1 if args.end else 0 ) + ( 1 if args.factor else 0 )
		if numRange>0 and numRange<3:
			print( "If one range parameter (begin,end,factor) is specified, all 3 must be." )
			sys.exit( -1 )
		
		# If all 3 range parameters set, cannot have a single frequency (although static is allowed).
		if numRange==3 and args.omega:
			print( "Cannot specify both range and a single frequency" )
			sys.exit( -1 )

		# If no range parameters set, must have a single frequency (or 'static', which is a single frequency of zero).
		if numRange==0 and not ( args.omega or args.static ):
			print( "Must specify a frequency range or a single frequecy (or 'static')." )
			sys.exit( -1 )

		# Check frequencies.
		if numRange==3:

			if math.isclose( args.begin, args.end ):
				print( "Bad range parameters: 'begin' and 'end' must be (numerically) different." )
				sys.exit( -1 )
			
			if math.isclose( args.factor, 1 ):
				print( "Bad range parameters: 'factor' must be (numerically) different from 1." )
				sys.exit( -1 )

			if args.begin > args.end and args.factor > 1:
				print( "Bad range parameters: 'begin' greater than 'end' but factor greater than 1." )
				sys.exit( -1 )
				
			if args.begin < args.end and args.factor < 1:
				print( "Bad range parameters: 'begin' less than 'end' but factor less than 1." )
				sys.exit( -1 )
	
	# Check other arguments.
	def checkRemainingArgs( self ):

		# Must check the parameters file exists.
		if not os.path.isfile( self._args.params ):
			print( f"Could not find parameters file '{self._args.params}'; does not exist or is not a file." )
			sys.exit( -1 )
		
		# Store persistently, overwriting the default 'None'.
		self._paramsFilePath = self._args.params

		# Check ARC queing options are feasible.
		if self._args.vmem <= 0:
			print( f"Bad virtual memory size '{self._args.vmem}'; must be positive." )
			sys.exit( -1 )

		if self._args.wall<=0 or self._args.wall>48:
			print( f"Bad wall clock time '{self._args.wall}'; must be positive and no more than 48 hours to run on ARC." )
			sys.exit( -1 )
		

	#
	# Methods relating to setting up the directories.
	#

	# Main method; call this first.
	def setUpRunDirectories( self ):
		"""Attempts to create and initialise all of the run directories.
		Calls sys.exit(-1) after printing an error message for any failure."""

		# Gets names and stores them in a persistent list.
		self.determineRunDirs()

		# Try to create the directories.
		self.createRunDirs()

		# Move all of the required files into each directory.
		self.populateRunDirs()

	def _getRunIndices( self ):
		"""Returns a list of integer run indices or None. Prints and error message and quits ['sys.exit(-1)']
		for any situation in which this was not possible."""

		try:
			runsRange = eval(self._args.range)

			# See if it is at least iterable (unless it is 'None').
			if runsRange != None:
				for runIndex in runsRange:

					# Also make sure each run index is a non-negative integer.
					if not isinstance(runIndex,int):
						print( f"Error: All run indices must be integers (found one that was not: '{runIndex}')." )
						sys.exit(-1)
					
					if runIndex<0:
						print( f"Error: All run indices must be non-negative (found ont that was negative: '{runIndex}')." )
						sys.exit(-1)

		except SyntaxError as err:
			print( f"Could not parse '{self._args.range}' as a valid Python expression; error was: {err}." )
			sys.exit( -1 )
		except NameError as err:
			print( f"Could not parse '{self._args.range}'; error was: {err}" )
			sys.exit(-1)
		except TypeError as err:
			print( f"Could not iterate over the range parameter '{self._args.range}'; error was: {err}." )
			sys.exit( -1 )
		
		return runsRange

	# Determines run directory names.
	def determineRunDirs( self ):
		"Determines the names for the run directories, and stores them as a persistent list."

		# Check the given range expression is a list of non-negative integers, or None.
		# Will fail ['sys.exit(-1)'] with message if could not extract a list of integers, for any reason.
		runsRange = self._getRunIndices()
		
		# Store all run directories, with paths from the current work directory, separately to the common path.
		if runsRange == None:
			self._runDirs = [ self._args.root ]
		else:
			self._runDirs = [ os.path.join(self._args.root,f"run{run}") for run in runsRange ]

	# Create run directories that are assumed to have already been determined.
	def createRunDirs( self ):
		"""Attempts to create all of the run directories specified in 'determineRunDirs()', which should
		be called prior to calling this method. Will bail [sys.exit(-1)] for failure, including if one of
		the new run directories already exists, and will make some attempt to 'clear up' any new directories
		that were created prior to finding the fault."""

		# Sanity check.
		if self._runDirs == None:
			print( "Cannot create run directories; names are yet to be determined." )
			sys.exit( -1 )
		
		# Create each, one by one. Breaks from loop after the first failure.
		# Also keep track of all that were created as they may need to be destroyed in the event of one failing. 
		newDirs = []
		for runDir in self._runDirs:
			try:
				os.makedirs( runDir )
				newDirs.append( runDir )
			except FileExistsError as err:
				print( f"Failed to create run directory '{runDir}'; full error was: {err}."  )
				break

		# Try to destroy any that were created.
		if len(newDirs)>0 and len(newDirs)<len(self._runDirs):
			print( f"Will attempt to delete all run directories that were successfully created." )

			for newDir in newDirs:
				try:
					os.rmdir( newDir )
					print( f" - successfully removed '{newDir}'." )
				except Exception as err:
					print( f" - failed to remove '{newDir}': {err}." )
		
		# Do not continue operations if there were any failures.
		if len(newDirs) != len(self._runDirs ):
			sys.exit( -1 )				# Already given an error message.

	# Populate the directories with the executable and the parameters files (minimum).
	def populateRunDirs( self ):
		"""Populates the run directories will all of the required files - the executable, the shell
		script, and the parameters file."""

		# Initialise seed; increment by one each run to ensure independence, whatever time they are launched.
		seed = int( time.time() )		# Current time in (floating point) seconds since UNIX epoch.

		for runDir in self._runDirs:

			# Sanity check: Make sure the directory has been created, presumably by calling 'createRunDirs()'.
			if not os.path.isdir( runDir ):
				print( f"Error: Could not find run directory '{runDir}'; why was it not just created?" )
				sys.exit(-1)

			# Copy the executable.
			try:
				shutil.copy( runBase.executable(), runDir )
			except FileNotFoundError as err:
				print( f"Could not find file; check executable path name. Full error was: {err}." )
				sys.exit( -1 )
			except Exception as err:
				print( f"Failed to copy executable to '{runDir}': {err}." )
				sys.exit( -1 )
			
			# Path to the parameters file, or are we creating one? - depends on subclass.
			self._setParametersFile( runDir )

			# Generate the shell script. Ensure different seed for each run.
			runIndex = self._runIndexFromDir( runDir )
			runScriptText = self._runScript( runDir, seed+runIndex )		# runIndex actually -1 for single runs, but that won't cause problems here.

			# Save shell script to file.
			shellPath = os.path.join( runDir, runBase.shellScriptName() )
			try:
				with open( shellPath, 'w' ) as outFile:
					print( runScriptText.strip(), file=outFile )
			except IOError as err:
				print( f"Could not write shell script to '{runDir}'; error was: {err}." )
				sys.exit(-1)

	# Determines the run index from the run dir name.
	def _runIndexFromDir( self, dir ):
		"""Determines the run index from the run dir name. Assumes base part of dir name is 'run' followed by the index.
		Returns '-1' if it was not possible to extract a run index, such as if the range of runs was actually 'None'."""
		
		try:
			return int( os.path.basename(dir)[3:] )
		except:
			return -1

	# Set the parameters file for a single run.
	def _setParametersFile( self, runDir ):
		"""Sets the parameters file for a single run on the given directory. Exits with 'sys.exit(-1)'
		for any failure. Can be overridden to e.g. create a parameters file in-place."""

		if self._paramsFilePath:
			try:
				shutil.copy( self._paramsFilePath, os.path.join(runDir,runBase.paramFileName() ) )
			except FileNotFoundError as err:
				print( f"Could not find parameters file '{self._paramsFilePath}'; check path name. Full error was: {err}." )
				sys.exit( -1 )
			except Exception as err:
				print( f"Failed to copy parameters file '{self._paramsFilePath}' to '{runDir}': {err}." )
				sys.exit( -1 )
		else:
			print( "Failed; no parameters file specified." )		# Should never reach here, unless this method
			sys.exit( -1 )											# was meant to be overriden but never was ... ?

	# Returns text for the run script for the given run directory.
	def _runScript( self, runDir, seed ):
		"""Returns formatted text, ready to be written to file, for the shell script that is placed in
		each run directory, and controls execution and finalising for that one run."""

		# Local shorthand.
		args = self._args

		# Executable. Prepend spaces to any arguments that come after the executable name.
		script = f"./{runBase.executable()}"

		# Fix seed to ensure each run is independent; e.g. for when queuing on a cluster, when runs
		# might start within the same second.
		script += f" -s {seed}"

		# Frequency arguments. A single frequecy value is handled separately to a frequency range.
		if args.static and args.begin==None:
			script += f" --static"
		else:
			if args.omega:
				script += f" -w {args.omega}"
			else:
				script += f" -b {args.begin} -e {args.end} -f {args.factor}"
				if args.static: script += " --static"

		# Other arguments to the executable.
#		script += f" -p {runBase.paramFileName}"	# Currently redundant as parameters filename is fixed to the default.
		script += f" --save"						# Will always save output files; may delete afterwards.

		# End of executable line.
		script += "\n"

		# Line to archive files, if requested.
		if args.archive:
			script += runBase.archivingScriptLine() + "\n"
		
		# Always delete output files (may have just been archived).
		script += "rm -f save_????.out\n"

		# Also delete the executable, if only to save disk space.
		script += f"rm -f {runBase.executable()}\n"
			
		return script

	#
	# Generate run/submit scripts.
	#

	# Creates the main run script that calls all of the single-run scripts.
	# By default, this is placed on the directory common to all run directories,
	# but is intended to the called from the current work directory from which
	# this Python module is called.
	def createRunScript( self ):

		# Place the main run script on the common path for a group of runs, or locally for a single run.
		self._mainScript = os.path.join(
			( self._args.root if self._getRunIndices() else "" ),
			runBase.mainScriptName()
		)

		# Generate script as a string.
		script = ""

		# Preamble for submitting to ARC (UoL).
		if self._args.launch == "qsub":
			script +=  "#$ -cwd -V\n"
			script += f"#$ -l h_rt={self._args.wall}:00:00\n"
			script += f"#$ -pe smp 1\n"				# C++11 code is currently serial only.
			script += f"#$ -l h_vmem={self._args.vmem}G\n"

		# Shell commands to move to and from all run subdirectories, launching the local scripts.
		cwd = os.getcwd()
		for runDir in self._runDirs:
			script += f"cd {runDir}\n"
			script += f"sh {runBase.shellScriptName()}\n"
			script += f"cd {cwd}\n"

		# Save the main script to file.
		try:
			with open( self._mainScript, 'w' ) as fileOut:
				print( script.strip(), file=fileOut )
		except IOError as err:
			print( f"Could not save main script file to {self._mainScript}; error was: {err}." )
			sys.exit( -1 )


	#
	# Launching.
	#

	# Launches the main shell script interactively; will pause this Python script.
	def launchShell( self, shellCmd="sh" ):

		# Check the shell script exists.
		if not os.path.isfile( self._mainScript ):
			print( f"Unable to launch runs; could not find main shell script '{self._mainScript}'." )

		# Call the shell interactively.
		subprocess.call( f"{shellCmd} {self._mainScript}", shell=True )

	# Launches asynchronously using nohup.
	def launchNohup( self, shellCmd="sh", outFile="nohup.out" ):

		# Check the shell script exists.
		if not os.path.isfile( self._mainScript ):
			print( f"Unable to launch runs; could not find main shell script '{self._mainScript}'." )

		# Output file in the same directory as the main script. Should match createRunScript().
		outPath = os.path.join(
				( self._args.root if self._getRunIndices() else "" ),
				outFile
		)
	
		# Call the shell asynchronously; output to file. Tiemout immediately.
		try:
			subprocess.run( f"nohup {shellCmd} {self._mainScript} > {outPath}", shell=True, timeout=1 )
		except subprocess.TimeoutExpired:
			pass

	# Submits the main shell script to a queue.
	def launchQueue( self, subCmd="qsub" ):

		# Check the shell script exists.
		if not os.path.isfile( self._mainScript ):
			print( f"Unable to launch runs; could not find main shell script '{self._mainScript}'." )

		# Call the shell.
		subprocess.call( f"{subCmd} {self._mainScript}", shell=True )


	#
	# Class methods.
	#
	@classmethod
	def executable( cls ):
		"""Executable name, with path if necessary."""
		return "fibreNetHI"
	
	@classmethod
	def paramFileName( cls ):
		"""The name of the parameters file as used for each run (as opposed to the original file it
		it may have been copied from). Use the (current) default name expected by the C++ code,
		but could in principle allow this to be varied."""
		return "parameters.xml"
	
	@classmethod
	def archivingScriptLine( cls ):
		"""The line in the shell script used to archive all save files. Also includes the filename.
		Do not add an additional end-of-line character here; will be added when inserted into the script."""
		return "tar czf saves.tar.gz save_????.out"

	@classmethod
	def shellScriptName( cls ):
		"""The name of the shell script used to control a single run."""
		return "run.sh"
	
	@classmethod
	def mainScriptName( cls ):
		"""The name of the shell script file used to launch all runs."""
		return "run_all.sh"


#
# Calling from command line.
#
if __name__ == "__main__":
	runBase().fullCycle()

