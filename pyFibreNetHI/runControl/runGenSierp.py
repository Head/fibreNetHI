#
# Specialises the all-moduli runs to (potentially) anisotropic networks generated
# by the generalised Sierpinski triangle procedural generation algorithm.
#


#
# Imports.
#
import sys
import runAllModuli


#
# Main class definition.
#
class runGenSierp( runAllModuli.runAllModuli ):

	def __init__( self ):
		"""Generates networks according to the generalised Sierpinski triangle algorithm, and runs
		under different applied strains to allow extraction of all independent moduli. Data extraction
		is performed using the 'dataAllModuli' class; there is no specialisation to this geometry.
		
		In addition to all of the parameters handled by runAllModuli, also need to specify:
		 - maxFractalLevel
		 - includeMajorEdges (true/false; defaults to false)
		 - omitTriangle (L, M, T or R)
		 
		 Note that this script does not check the overall system dimensions correspond to a number of nodes
		 that is the same power of 2 in both directions. Such geometries will currently cause the C++ to exit
		 as soon as it is launched.
		 """
		pass

	# Short description that is displayed at the start of the the command line help message.
	def cmdLineDescripion( self ):
		return "Generates a triangular network based on the generalised Sierpinski triangle, and applies different strains to extract all moduli"

	#
	# Additional parameters specific to this class.
	#
	def _parameterArgs( self, parser ):

		# Parse all other parameters.
		super()._parameterArgs( parser )

		# Parameters specific to the Sierpinski triangle. Also includes two that are relevant to the (normal) Sierpinksi triangle,
		# but as this would not be in a subclass derived from 'runAllModuli' and therefore lie along a separate branch, are included here.
		parser.add_argument( "--maxFrac", help="maximum fractal level", type=int, required=True )
		parser.add_argument( "--majorEdge", help="flag to include major edges; not included if not set", action="store_true" )
		parser.add_argument( "--omitTri", help="triangle to omit, one of 4 choices for 'top', 'middle', 'left', 'right'", choices=["T","M","L","R"], required=True )

	def checkRemainingArgs( self ):
		"""Check the maximum fractal level is positive. The other new parameters are handled by the argparse module.
		Whether or not the geometry is suiatble (i.e. number of nodes the same power of 2 in both directions) is not
		checked here, but will be flagged by the C++ code on launch."""

		super().checkRemainingArgs()

		# Max. fractal level must be positive.
		if self._args.maxFrac <= 0:
			print( f"Bad maxkmum fractal level {self._args.maxFrac}; must be positive." )
			sys.exit(-1)
		
	#
	# Parameter file generation.
	#
	def _paramsNetwork( self, runDir ):
		"""Returns a string containing all line in the parameter file that specify this generalised Sierpinski network.
		Must be provided by a concrete subclass. Prepend all lines with a single tab for human readability."""

		# Most parameters handled by parent class, but must also alter the network type.
		pText = super()._paramsNetwork( runDir, "triangular spring gen Sierp" )

		pText += f'\t<Parameter maxFractalLevel="{self._args.maxFrac}"/>\n'
		pText += f'\t<Parameter omitTriangle="{self._args.omitTri}"/>\n'

		if self._args.majorEdge:
			pText += '\t<Parameter includeMajorEdges="true"/>\n'

		return pText


#
# If called from the command line.
#
if __name__ == "__main__":
	runGenSierp().fullCycle()
