#
# Reads in frequency sweep data from 'table.dat', and plots, with no assumptions on data.
# Matches with the 'runBase' in that it should be usable for any scenario.
#


#
# Imports
#

# Standard imports.
import numpy as np
import os
import sys
import collections

# Local imports. Not relative as want to call this class directly.
from pyFibreNetHI.XMLTree import XMLTreeParser


#
# Class for a single table, averaged over multiple runs.
# 
class dataBase:

	def __init__( self, dir, verbose=False, paramsFileName="parameters.xml" ):
		"""Parses the output from the fibreNetHI code, run averaging with standard errors where relevant.
		Initialise with the directory, which is assumed to be either a single directory containing at least
		the table.dat file and the parameters file (which is also parsed); or, a root directoty up to but
		not including a series of 'run?' subdirectories consecutively numbered from zero."""
		
		# Store arguments.
		self._verbose = verbose
		self._dir     = dir

		#
		# Parse the tables on all run directories.
		#

		# Are there run subdirectories, or is there just one run on the local directory?
		multipleRuns = os.path.isdir( os.path.join(self._dir,self.__class__.firstRunDir()) )

		# Try to read in all of the 'run?' subdirs, with the option for a subclass to generalise run dir names.
		runIndex = -1							# Increment by one each time, starting at zero.
		tables = collections.OrderedDict()		# All tables.
		paramsFile = None						# Fill with path to first parameters file found.

		moreTables = True
		while moreTables:
			runIndex += 1

			# Get all table files and table indices for this run.
			tableFiles = self._tableFilesForRun( self._dir, ( runIndex if multipleRuns else None ) )

			for tIndex, tDir in tableFiles.items():

				# Get the table path for this table.
				tPath = os.path.join( tDir, "table.dat" )

				# Assume non-existence of the table signifies that there are no more runs.
				if not os.path.isfile( tPath ):
					moreTables = False
					break							

				# If not yet set, get the path to the parameters file now.
				if not paramsFile:
					pFileName = os.path.join( tDir, paramsFileName )

					if not os.path.isfile( pFileName ):
						print( f"Attempted to extract parameters from '{pFileName}' but could not find file; will try next run (if any)." )
					else:
						paramsFile = pFileName

				# Verbose output.
				if self._verbose: print( "Parsing file '{}'".format(tPath) )

				#
				# Extract a single table.
				#

				# Initialise the table. Will need to extract the column headings first of all.
				table = collections.OrderedDict()

				# Read in each line of the table file, appending values to the corresponding lists.
				for line in open(tPath,'r').readlines():
					if not line.strip(): continue						# Skip any whitespace-only lines.

					# Is the first line the headings? - assume the first is the frequency.
					if line.strip()[0]=="w":

						if len(table)!=0:
							print( "Error: Two rows of column headings?" )
							return

						for key in line.split():
							table[key] = []

						continue
				
					# Parse a single row of values.
					for i, key in enumerate(table.keys()):
						table[key].append( float(line.split()[i]) )

				# Add this run's table to the full list.
				tables[tIndex] = table

			# If there was only one run, quit now.
			if not multipleRuns:
				moreTables = False
				break

		# Store the total number of runs and tables persistently. May differ due to subclasses.
		self._numTables = len(tables)
		self._numRuns   = runIndex

		# If no tables, bail now.
		if self._numTables==0:
			print( f"Could not parse any tables starting from '{dir}' using expected directory naming system for this class." )
			sys.exit( -1 )

		#
		# Sanity checks.
		#
		
		# Check all tables had the same frequencies.
		for tIndex in range(1,self._numTables):
			try:
				if len(tables[0]["w"]) != len(tables[tIndex]["w"]):
					raise ValueError("Different numbers of table rows between at least two tables.")

				for row in range(len(tables[0]["w"])):
					if tables[0]["w"][row] != tables[tIndex]["w"][row]:
						print( "Values for row {0}: {1} (run 0) and {2} (run {3})".format(row,tables[0]["w"][row],tables[run]["w"][row],run) )
						raise ValueError("Different frequency or frequencies between at least two tables.")
			except ValueError as err:
				print( "Cannot combine table files for {0}; reason: {1}".format(self._dir,err) )
				exit(-1)

		#
		# Try to extract parameters from the parameters file for the first table.
		#
		if paramsFile == None:
			print( "Could not find a parameters file in any of the run directories." )
			sys.exit(-1)

		try:
			paramsTree = XMLTreeParser.parseFileAsTree( paramsFile )
		except ValueError as err:
			print( "Could not parse parameters file '{0}':{1}.".format(paramsFile,err) )
			sys.exit(-1)
		
		# Get the actual parameters. Add additional parameters as needed.
		for paramNode in paramsTree.nodesWithTag("Parameter"):
			if "dilutionParam"   in paramNode.attributes: self.p    = paramNode.attributes["dilutionParam"  ]
			if "latticeSpacing"  in paramNode.attributes: self.a    = paramNode.attributes["latticeSpacing" ]
			if "springConstant"  in paramNode.attributes: self.k    = paramNode.attributes["springConstant" ]
			if "dampCoefficient" in paramNode.attributes: self.zeta = paramNode.attributes["dampCoefficient"]
			if "viscosity"       in paramNode.attributes: self.eta  = paramNode.attributes["viscosity"      ]
			if "X"               in paramNode.attributes: self.X    = paramNode.attributes["X"              ]
			if "Y"               in paramNode.attributes: self.Y    = paramNode.attributes["Y"              ]

			if "driving"        in paramNode.attributes: self.driving        = paramNode.attributes["driving"       ]
			if "networkType"    in paramNode.attributes: self.networkType    = paramNode.attributes["networkType"   ]
			if "stripThickness" in paramNode.attributes: self.stripThickness = paramNode.attributes["stripThickness"]
			if "fluidType"      in paramNode.attributes: self.fluidType      = paramNode.attributes["fluidType"     ]

			if "P_x" in paramNode.attributes: self.P_x = paramNode.attributes["P_x"]
			if "P_y" in paramNode.attributes: self.P_y = paramNode.attributes["P_y"]

			if "maxFractalLevel"     in paramNode.attributes: self.maxFractalLevel     = paramNode.attributes["maxFractalLevel"]
			if "addRandomSpringProb" in paramNode.attributes: self.addRandomSpringProb = paramNode.attributes["addRandomSpringProb"]
	
			if "correlationStrength" in paramNode.attributes: self.correlationStrength = paramNode.attributes["correlationStrength"]
			if "siteOccupation"      in paramNode.attributes: self.siteOccupation      = paramNode.attributes["siteOccupation"]

		#
		# Initialise data arrays and raw data to means and standard errors.
		#
		self._means, self._errors, numFreqs  = {}, {}, None
		for index in tables:

			for label in tables[index]:

				# All labels (including frequency) have errors and means (errors normally zero for frequencies but still stored for consistency).
				self._means [label] = []
				self._errors[label] = []

				# Get the number of frequencies (assumed label 'w').
				if label=="w":
					numFreqs = len( tables[index]["w"] )
	
			break		# Use first table only.
	
		# Sanity check.
		if not numFreqs:
			print( "Failed to get any frquency values from the first table file." )
			return

		# Loop over all table rows (use frequency as key).
		for wIndex in range(numFreqs):

			# For each row, get mean and error for each column across all runs.
			for col in self._means.keys():

				acrossRuns =  [ table[col][wIndex] for table in tables.values() ]
				mean, error = dataBase.stderr( acrossRuns )

				self._means [col].append( mean  )
				self._errors[col].append( error )
		
		# Add any additional columns specific to a subclass.
		self._additionalColumns( tables )

		#
		# Convert all means and error arrays to numpy arrays.	
		#
		for key in self._means.keys():
			self._means [key] = np.array( self._means [key] )	
			self._errors[key] = np.array( self._errors[key] )
			
	#
	# Accessors
	#
	def numRuns( self ):
		"""The number of runs, each of which contained one or more tables."""
		return self._numRuns

	def numTables( self ):
		"""The number of tables (not runs, which may be different)."""
		return self._numTables

	def numRows( self ):
		"""The number of rows in the table, which should be the number of frequencies."""
		return len( self._means["w"] )

	def dir( self ):
		"""The root directory name, up to but not including the 'run?' subdirs."""
		return self._dir

	def labels( self ):
		""""All plottable labels."""
		return self._means.keys()

	# Adds labels and columns specific to a certain class.
	def _additionalColumns( self, tables ):
		"""Use the provided tables ordered dictionary to extract additional table columns, and add them to the
		list of 'means' and 'errors' with the same label."""
		pass

	# Return the requested data label as a numpy array.
	def means( self, label ):
		if label.endswith("'"):
			return np.array( self._means[label+"_net"] + self._means[label+"_fl"] )
		else:
			return np.array( self._means[label] )

	def errors( self, label ):
		if label.endswith("'"):
			return np.array( self._errors[label+"_net"] + self._errors[label+"_fl"] )
		else:
			return np.array( self._errors[label] )

	#
	# Support for the context manager.
	#
	def __enter__( self ):
		return self

	def __exit__( self, exceptionType, exceptionValue, traceBack ):
		return

	#
	# Class methods.
	#
	@classmethod
	def firstRunDir( cls ):
		"""First directory name when there are multiple runs. Assume start from index 0 always."""
		return "run0"
	
	@classmethod
	def _tableFilesForRun( cls, root, runIndex ):
		"""Returns a dictionary of table indices (keys) and directory names (values) for the given run index, for
		paths expected to contain a table file. The actual table file name will be added to the end.
		If the runIndex is 'None', there were no run-subdirectories. """
		if runIndex == None:
			return { 0: root }
		else:
			return { runIndex: os.path.join(root,f"run{runIndex}") }
	

	#
	# Static methods
	#
	@staticmethod
	def stderr( data ):
		"""Returns a tuple consisting of the mean and error of the passed list of scalars; if a list of numpy arrays
		is passed, will return an array of means and errors."""
		n       = len(data)
		sumVals = sum( data )
		sumSqrd = sum( [d*d for d in data] )

		if n<=1:
			return [ sumVals, 0. ]
		else:
			var = n*(sumSqrd/n-(sumVals/n)**2)/(n-1)		# Estimator for the population variance; factor n/(n-1) so unbiased.

			try:
				if var < 0: var = 0							# Can get a negative variance due to numerical errors.
			except ValueError:								# Assume this is because this is a numpy array.
				var = np.where( var<0, 0, var )

			return [ sumVals/n, np.sqrt(var/n) ]			# np.sqrt() also works on scalars.
			
