#
# This is the old script that plotted data, and has now been replaced by 'plot.py', which uses
# the class 'plotBase' in the plotControl submodule.
#
# It is included here for legacy reasons but is now entirely stand-alone.
#


#
# Imports
#


# Standard imports.
import math
import matplotlib.pyplot as plt
import argparse
import numpy as np
import os
import sys
import collections

# Local imports.
import pyFibreNetHI

#
# Class for a single table
# 
class fibreNetHITable:

	def __init__( self, dir, verbose=False, paramsFileName="parameters.xml" ):
		"""Parses the output from the fibreNetHI code, run averaging with standard errors where relevant.
		Initialise with the directory, which is assumed to be either a single directory containing at least
		the table.dat file and tjh parameter sfile file (which is also parsed); or, a root directoty up to but
		not including a series of 'run?' subdirectories consecutively numbered from zero."""
		
		# Store arguments.
		self._verbose = verbose
		self._dir     = dir

		#
		# Parse the tables on all run directories.
		#
		
		# Check directory exists
		if not os.path.isdir(self._dir):
			print( "'{}' is not a directory; bailing.".format(dir) )
			sys.exit(-1)

		# Are there run subdirectories, or is there just one run on the local directory?
		multipleRuns = os.path.isdir( os.path.join(self._dir,"run0") )

		# Try to read in all of the 'run?' subdirs
		fileIndex = 0
		tables    = []
		while True:

			# Get the filename, assuming the convention "<dir>/run0", "<dir>/run1" etc.
			if multipleRuns:
				fname = os.path.join( os.path.join(self._dir,"run{}".format(fileIndex),"table.dat") )
			else:
				fname = os.path.join( os.path.join(self._dir,"table.dat") )

			if not os.path.isfile(fname): break		# Exceeded maximum number of runs most likely.

			if self._verbose: print( "Parsing file '{}'".format(fname) )

			# Initialise the table. Will need to extract the column headings first of all.
			table = collections.OrderedDict()

			# Set to 'True' for old-style tables.
			oldStyleTables = False

			# Read in each line of the table file, appending values to the corresponding lists.
			for line in open(fname,'r').readlines():
				if not line.strip(): continue						# Skip any whitespace-only lines.

				# Is the first line the headings? - assume the first is the frequency.
				if line.strip()[0]=="w":

					if len(table)!=0:
						print( "Error: Two rows of column headings?" )
						return

					for key in line.split():
						table[key] = []

					continue

				# If still here and have no table columns, assume old-style table files.
				if len(table)==0:
					oldStyleTables = True
					for key in ["w","G'_net","G''_net","G'_fl","G''_fl","NA_net","NA_fl","DC"]:
						table[key] = []
					if fileIndex==0 and self._verbose:
						print( " - treating {} (and any 'run' subdirs) as old-style tables.".format(fname) )
				
				# Parse a single row of values.
				if not oldStyleTables:
					for i, key in enumerate(table.keys()):
						table[key].append( float(line.split()[i]) )
				
				# Legacy: Parse a single row from the old-style table.
				if oldStyleTables:

					# Even old-style tables varied in length.
					w, Gp, Gpp, Gnet_p, Gnet_pp, netNA, flNA, DC, netComp, flComp = [ float(str) for str in line.split()[:10] ]

					table["w"].append( w )

					table["G'_net" ].append( Gnet_p  )
					table["G''_net"].append( Gnet_pp )

					table["G'_fl" ].append( Gp  - Gnet_p  )
					table["G''_fl"].append( Gpp - Gnet_pp )

					table["NA_net"].append( netNA )
					table["NA_fl" ].append( flNA  )
					table["DC"    ].append( DC    )

			# Add this run's table to the full list.
			tables.append( table )

			# If there was only one run, quit now.
			if not multipleRuns: break

			# Try the next run index.
			fileIndex += 1

		# Store the total number of runs persistently.
		self._numRuns = len(tables)

		#
		# Sanity checks.
		#
		
		# Check all files had the same frequencies.
		for run in range(1,self._numRuns):
			try:
				if len(tables[0]["w"]) != len(tables[run]["w"]):
					raise ValueError("Different numbers of table rows between at least two tables.")

				for row in range(len(tables[0]["w"])):
					if tables[0]["w"][row] != tables[run]["w"][row]:
						print( "Values for row {0}: {1} (run 0) and {2} (run {3})".format(row,tables[0]["w"][row],tables[run]["w"][row],run) )
						raise ValueError("Different frequency or frequencies between at least two tables.")
			except ValueError as err:
				print( "Cannot combine table files for {0}; reason: {1}".format(self._dir,err) )
				exit(-1)

		#
		# Try to extract parameters from the parameters file for the first run.
		#
		try:
			if multipleRuns:
				paramsFile = os.path.join( os.path.join(self._dir,"run0",paramsFileName) ) 
			else:
				paramsFile = os.path.join( os.path.join(self._dir,paramsFileName) ) 

			paramsTree = pyFibreNetHI.XMLTreeParser.parseFileAsTree( paramsFile )
		except ValueError as err:
			print( "Could not parse parameters file '{0}':{1}.".format(paramsFile,err) )
			sys.exit(-1)
		
		# Get the actual parameters
		for paramNode in paramsTree.nodesWithTag("Parameter"):

			if "dilutionParam"   in paramNode.attributes: self.p    = paramNode.attributes["dilutionParam"  ]
			if "latticeSpacing"  in paramNode.attributes: self.a    = paramNode.attributes["latticeSpacing" ]
			if "springConstant"  in paramNode.attributes: self.k    = paramNode.attributes["springConstant" ]
			if "dampCoefficient" in paramNode.attributes: self.zeta = paramNode.attributes["dampCoefficient"]
			if "viscosity"       in paramNode.attributes: self.eta  = paramNode.attributes["viscosity"      ]
			if "X"               in paramNode.attributes: self.X    = paramNode.attributes["X"              ]
			if "Y"               in paramNode.attributes: self.Y    = paramNode.attributes["Y"              ]

			if "driving"        in paramNode.attributes: self.driving        = paramNode.attributes["driving"       ]
			if "networkType"    in paramNode.attributes: self.networkType    = paramNode.attributes["networkType"   ]
			if "stripThickness" in paramNode.attributes: self.stripThickness = paramNode.attributes["stripThickness"]
			if "fluidType"      in paramNode.attributes: self.fluidType      = paramNode.attributes["fluidType"     ]

			if "P_x" in paramNode.attributes: self.P_x = paramNode.attributes["P_x"]
			if "P_y" in paramNode.attributes: self.P_y = paramNode.attributes["P_y"]
	
		#
		# Initialise data arrays and raw data to means and standard errors.
		#
		self._means, self._errors,  = {}, {}
		for label in tables[0].keys():
			self._means [label] = []
			self._errors[label] = []

		# Loop over all table rows (use frequency as key).
		for w in range(len(tables[0]["w"])):

			# For each row, get mean and error for each column across all runs.
			for column in tables[0]:
				mean, error = fibreNetHITable.stderr( [ table[column][w] for table in tables ] )
				self._means [column].append( mean  )
				self._errors[column].append( error )

		# Convert to numpy arrays.	
		for key in self._means.keys():
			self._means [key] = np.array( self._means [key] )	
			self._errors[key] = np.array( self._errors[key] )
			
	#
	# Accessors
	#
	def __len__( self ):
		"""The number of runs."""
		return self._numRuns

	def numRows( self ):
		"""The number of rows in the table."""
		return len( self._means["w"] )

	def dir( self ):
		"""The root directory name, up to but not including the 'run?' subdirs."""
		return self._dir

	# Return the requested data label as a np array.
	def means( self, label ):
		if label.endswith("'"):
			return np.array( self._means[label+"_net"] + self._means[label+"_fl"] )
		else:
			return np.array( self._means[label] )

	def errors( self, label ):
		if label.endswith("'"):
			return np.array( self._errors[label+"_net"] + self._errors[label+"_fl"] )
		else:
			return np.array( self._errors[label] )

	#
	# Support for the context manager.
	#
	def __enter__( self ):
		return self

	def __exit__( self, exceptionType, exceptionValue, traceBack ):
		return
	
	
	
	#
	# Static methods
	#
	@staticmethod
	def stderr( data ):
		n       = len(data)
		sumVals = sum( data )
		sumSqrd = sum( [d*d for d in data] )

		if n<=1:
			return [ sumVals, 0.0 ]
		else:
			var = n*(sumSqrd/n-(sumVals/n)**2)/(n-1)		# Estimator for the population variance; factor n/(n-1) so unbiased
			if var < 0.0: var = 0.0							# Can get a negative variance due to numerical errors
			return [ sumVals/n, math.sqrt(var/n) ]
			


#
# If called from the command line
#
if __name__ == "__main__":

	#
	# Command line arguments. Dirs to plot, all assumed to have a number of 'run?' subdirs.
	#
	parser = argparse.ArgumentParser( description="Plot table data from fibreNetHI" )

	parser.add_argument( "dirs", help="directories not including 'run?'", nargs="*", default=['.'] )
	parser.add_argument( "--verbose", help="Verbose output", action="store_true" )

	parser.add_argument( "modulus", help="Modulus label to plot", nargs="?", type=str, default="G" )
	parser.add_argument( "--net", help="Only plot network contributions", action="store_true" )
	parser.add_argument( "--fl", help="Only plot fluid contributions", action="store_true" )

	parser.add_argument( "--netNA", help="Overlay plot of network non-affinity (if any)", action="store_true" ) 
	parser.add_argument( "--flNA", help="Overlay plot of fluid non-affinity (if any)", action="store_true" )
	parser.add_argument( "--DC", help="Overlay plot of decoupling measure (if any)", action="store_true" ) 
	parser.add_argument( "--flux", help="Overlap plot of interfacial flux (if any)", action="store_true" )

	parser.add_argument( "--fmoms", help="Plot the force moments instead of moduli", action="store_true" )

	args = parser.parse_args()



	# Parse the data files into objects.
	objects = [ fibreNetHITable(dir,verbose=args.verbose) for dir in args.dirs ]

	#
	# Force momemnts are plotted separately to other quantities.
	#
	if args.fmoms:

		# Only single runs allowed for the force moment plots (too much data otherwise).
		if len(objects)>1:
			print( "This script currently only supports single data sets for displaying force moments." )
			sys.exit(-1)
		
		# Shorthand for the only set of runs.
		obj = objects[0]
		
		# Start from the first moment and increment as long as their is data in the table.
		# Note that the zero'th moment is not currently output by the C++ code, so start labels at 1.
		mom = 0
		while True:
			mom += 1

			clr = ["r","g","b","c","m","y"][mom%6]		# Colours for different moments.
			label = "f^{}".format(mom)					# Label by moment only. 

			try:
				plt.errorbar( obj.means("w"), obj.means("f{}_real".format(mom)), obj.errors("f{}_real".format(mom)), color=clr, ls="-", label=label )
				plt.errorbar( obj.means("w"), obj.means("f{}_imag".format(mom)), obj.errors("f{}_imag".format(mom)), color=clr, ls="--" )

			except KeyError:
				break

		# Finalise the plot.
		plt.xlabel( r"$\omega$" )
		plt.ylabel( "Moment" )
		plt.semilogx()				# Moments can be zero, and odd moments can be negative.
		plt.legend()
		plt.show()

		# Do not contiue to the remainder of the script, which is the plotting moduli.
		sys.exit(-1)

	#
	# Plot the requested modulus, and overlay any other quantities requested.
	#

	# Sanity check.
	if args.net and args.fl:
		print( "Cannot select both network and fluid; select neither to plot the sum." )
		sys.exit(-1)

	# Set up the axes, with the potential for shared x-axes with another quantity.
	fig, axes1 = plt.subplots()

	# Guidelines first, using the first data set for the frequencies.
	w = objects[0].means("w")
	axes1.plot( w, objects[0].eta*w, "k--" )

	# What moduli to plot, storage and loss moduli.
	storageStr = args.modulus + "'"  + ( "_net" if args.net else "" ) + ( "_fl" if args.fl else "" )
	lossStr    = args.modulus + "''" + ( "_net" if args.net else "" ) + ( "_fl" if args.fl else "" )

	for i, obj in enumerate(objects):
		clr = ["r","g","b","c","m","y"][i%6]

		# Plot shear moduli.
		axes1.errorbar( obj.means("w"), obj.means(storageStr), obj.errors(storageStr), color=clr, fmt="-" , label="{0}".format(obj.dir()) )
		axes1.errorbar( obj.means("w"), obj.means(lossStr   ), obj.errors(lossStr   ), color=clr, fmt="--" )
		

	#
	# Finalise axes1, for the moduli.
	#
	axes1.loglog()
	axes1.set_xlabel( r"$\omega$" )
	axes1.legend()

	#
	# Optionally overlay another measure using the right-hand axes.
	#
	if args.netNA + args.flNA + args.DC + args.flux >= 1:

		# Can only plot one at a time.
		if args.netNA + args.flNA + args.DC + args.flux > 1:
			print( "Can only plot one of the optional metrics (NA/flux etc.)." )
			sys.exit()

		# Quantity to plot.
		quant = None
		if args.netNA   : quant = "NA_net"
		if args.flNA    : quant = "NA_fl"
		if args.DC      : quant = "DC"
		if args.flux	: quant = "flux"
		
		# Plot it using the right-hand axes.
		axes2 = axes1.twinx()

		# Plot the quantity - if it exists.
		try:
			for i, obj in enumerate(objects):
				clr = ["r","g","b","c","m","y"][i%6]
				axes2.errorbar( w, obj.means(quant), obj.errors(quant), color=clr, fmt=":" )

			print( "Plotting {} with dotted line; right-hand axes.".format(quant) )
			axes2.loglog()

		except KeyError:
			print( "Could not find quantity '{}' in the table data.".format(quant) )

	
	#
	# Display.
	#
	plt.show()



