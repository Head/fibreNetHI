#
# A triangular spring network restricted to a "strip"-like horizontal region in the middle of the system.
# Largely uses networkTriSpring methods, from which it derives.
#

#
# Imports
#

# Parent class.
from . import networkTriSpring


class networkTriSpringStrip( networkTriSpring.networkTriSpring ):

    def __init__( self, X, Y, file, driving, verbose=False ):
        """The parent class should do all that is required."""
        super().__init__(X,Y,file,driving,verbose)

    # No wrapping in the y-direction.
    def _globalIndex( self, i, j ):
        while i <  0         : i += self._numX
        while i >= self._numX: i -= self._numX

        return j*self._numX + i
	
    def _wrappedPosn( self, index, x0, y0 ):
        x, y = self._posns[index]
		
        while x - x0 >   self._X/2: x -= self._X
        while x - x0 < - self._X/2: x += self._X	
		
        return np.array( [x,y] )

    def _wrapDisp( self, u ):

        while u[0] < - self._X/2: u[0] += self._X
        while u[0] >   self._X/2: u[0] -= self._X

        return u


