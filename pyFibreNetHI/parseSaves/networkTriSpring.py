#
# Parsing, visualisation and analysis of a triangular spring network.
#
# Initialise with a file object already pointing to the first line of data
# (i.e. NOT including the identifier line 'networkTriSpring')
#


#
# Imports
#

# Standard imports
import numpy as np
import math
import matplotlib.pyplot as plt

# Project imports, including the abstract base
from . import networkBase


class networkTriSpring( networkBase.networkBase ):

	def __init__( self, X, Y, file, driving, verbose=False ):
		"""Call with the box dimensions, and a file object that has already parsed the network
		identification line (so the next line to be read will be the first output by networkTriSpring::).
		Fills network and node data as supplied by the save file.
		"""
		
		# Call the parent class
		super().__init__(X,Y,driving,verbose)
		
		# First (i.e. next) line gives the network dimensions in terms of nodes, and the actual number of nodes.
		self._numX, self._numY, self._numNodes = [ int(str) for str in file.readline().split() ]
		if self._verbose:
			print( "Network nodal dimensions: {0} x {1}".format(self._numX,self._numY) )
			print( "Number of nodes in file: {}".format(self._numNodes) )
		
		# The next line gives the parameters. Note anistropic dilutions are expected, but they may be equal (=isotropic).
		self._a, self._k, self._p_E_W, self._p_NW_SE, self._p_NE_SW = [ float(str) for str in file.readline().split() ]
		
		# Create np arrays for the node positions, connections, and solutions (complex amplitudes)
		self._posns    = np.zeros( shape=(self._numNodes,2), dtype="float64"    )
		self._disps    = np.zeros( shape=(self._numNodes,2), dtype="complex128" )
		self._conns    = np.zeros( shape=(self._numNodes,6), dtype="int32"      )
		self._tensions = np.zeros( shape=(self._numNodes,6), dtype="float64"    )

		# Have one line per node from now on. Expect some nodes (e.g. those that are totally disconnected,
		# and/or not in the reduced map) to not be output, so need to be flexible here.
		for n in range(self._numNodes):
		
			# Extract per-node data.
			colStr = file.readline().split()		# Columns for this node, each as strings

			# Check the global node index matches (could remove this redundancy? - but is a nice check)
			if n != int(colStr[0]):
				print( "Error reading per-node data: Index did not match" )
				return
			
			# Undeformed position
			self._posns[n] = [ float(col) for col in colStr[1:3] ]
			
			# The 6 connections (nb. some may be -1, meaning no connection)
			self._conns[n] = [ int(col) for col in colStr[3:9] ]

			# The 6 tensions, or 0.0 if there is no connection.
			self._tensions[n] = [ float(col) for col in colStr[9:15] ]
			
			# The displacements may be {x.real y.real} (for the static solution), {x.real x.imag y.real y.imag}
			# (for a dynamic solution), or not output at all for an initial-state only save.
			if len(colStr)==19:
				self._disps[n][0] = float(colStr[15]) + 1j * float(colStr[16])
				self._disps[n][1] = float(colStr[17]) + 1j * float(colStr[18])
			else:
				if len(colStr)==17:
					self._disps[n][0] = float(colStr[15]) + 0j
					self._disps[n][1] = float(colStr[16]) + 0j
				else:
					self._disps[n][0] = 0.0 + 0.0j
					self._disps[n][1] = 0.0 + 0.0j

	#
	#MARK: Properties
	#
	
	@property
	def a( self ):
		return self._a
	
	@property
	def k( self ):
		return self._k

	@property
	def p( self ):
		"""Check dilution really is isotropic before returning a value."""
		if self._p_E_W != self._p_NW_SE or self._p_E_W != self._p_NE_SW:
			print( "Attempted to access the (isotropic) dilution parameter p for an anisotropic network." )
			return None

		return self._p

	@property
	def p_E_W( self ):
		return self._p_E_W
	
	@property
	def p_NW_SE( self ):
		return self._p_NW_SE
	
	@property
	def p_NE_SE( self ):
		return self._p_NE_SW

	#
	#MARK: Accessors
	#

	# Node properites.
	def numNodes( self ):
		"""The total number of nodes."""
		return self._numNodes
	
	def nodePosn( self, n ):
		"""The position of node n."""
		if n<0 or n>=self._numNodes:
			print( "Bad node index '{0}' - should be less than {1}.".format(n,self._numNodes) )
		else:
			return self._posns[n]

	def nodeDisp( self, n ):
		"""The displacement of node n."""
		if n<0 or n>=self._numNodes:
			print( "Bad node index '{0}' - should be less than {1}.".format(n,self._numNodes) )
		else:
			return self._disps[n]

	def nodeSeparation( self, n1, n2 ):
		"""The distance between nodes n1 and n2, taking into account periodic boundary conditions."""
		if n1<0 or n2<0 or n1>=self._numNodes or n2>=self._numNodes:
			print( "At least one of the node indices {0}, {1} is out-of-range.".format(n1,n2) )
		else:
			dx = self._wrapDisp( self._posns[n1] - self._posns[n2] )
			return np.sqrt( np.sum(dx*dx) )

	def numConnections( self, n ):
		"""Returns the number of connections to the given node."""
		if n<0 or n>=self._numNodes:
			print( "Bad node index '{0}' - should be less than {1}.".format(n,self._numNodes) )
		else:
			k = 0
			while k<6 and self._conns[n,k]!=-1:
				k += 1
			return k

	def connectedNode( self, n, conn ):
		"""Returns the index of the node found by traversing the given connection number.
		Returns -1 if no such connection exists for this node, including a bad node/connection index."""

		# Sanity checks.
		if n<0 or n>=self._numNodes or conn<0 or conn>=6:
			return -1

		return self._conns[n,conn]


	#
	#MARK: Methods
	#

	# Relative factor to displacements to ensure all periodic copies are displayed.
	def _perBCFac( self ):
		return 0.2
	
	# Gets the (i,j) nodes indices from the global index; returns as a tuple
	def _splitGlobal( self, n ):
		return ( int(n%self._numX), int(n/self._numX) )

	# Coordinates of given index wrapped to periodic BCs centred on the given coordinate.
	def _wrappedPosn( self, index, x0, y0 ):
		x, y = self._posns[index]
		
		while x - x0 >   self._X/2: x -= self._X
		while x - x0 < - self._X/2: x += self._X	

		while y - y0 >   self._Y/2: y -= self._Y
		while y - y0 < - self._Y/2: y += self._Y
		
		return np.array( [x,y] )

	# Wrap a difference vector (centred on the origin), passed as either a numpy array or a list.
	def _wrapDisp( self, u ):

		while u[0] < - self._X/2: u[0] += self._X
		while u[0] >   self._X/2: u[0] -= self._X

		while u[1] < - self._Y/2: u[1] += self._Y
		while u[1] >   self._Y/2: u[1] -= self._Y

		return u

	# Plots a single node displacement, passed as ux and uy assumed to be real.
	def _plotDisp( self, posn, ux, uy, maxMag, color, zorder ):

		# Shorthands.		
		calib = 1.0/maxMag
		pBC   = self._perBCFac()
		X     = np.array( [self._X,self._Y] )

		# Plot periodic copies within a pre-defined range.
		for offset in ( [0,0], [X[0],0], [-X[0],0], [0,X[1]], [0,-X[1]], [X[0],X[1]], [X[0],-X[1]], [-X[0],X[1]], [-X[0],-X[1]] ):

			x = posn[0] + offset[0]
			y = posn[1] + offset[1]

			pBC = 0.0

			if x>(1+pBC)*X[0] or x<-pBC*X[0] or y>(1+pBC)*X[1] or y<-pBC*X[1]: continue

			# Arrow if long enough, else a line segment
			if math.sqrt(ux**2+uy**2) > 0.1*maxMag:
				plt.arrow(
					x, y, calib*ux, calib*uy,
					width=0.05, length_includes_head=True, color=color, zorder=zorder
				)
			else:
				plt.plot(
					[x,x+calib*ux], [y,y+calib*uy],
					color=color, linewidth=2, zorder=zorder
				)

	# Takes a complex number and returns another complex with both real and imaginary components non-negative.
	def _imReMag( self, z ):
		return abs(z.real) + 1j*abs(z.imag)
	
	def addToPlot_OpenGL( self ):
		"""Returns the information required to display the network in OpenGL, but calls not rendering routines here.
		Much of this is the same as the matplotlib version, but without the periodic copies."""

		# Initialise the dictionary that will be used to return all of the required information about objects to be displayed.
		objects = { "nodes":[], "springs":[] }

		# Shorthands.
		X = np.array( [self._X,self._Y] )						# To accelerate applying periodic BCs

		#
		# Nodes.
		#
		for n in range(self._numNodes):				# Only plot nodes with at least 1 connection.
			if self._conns[n,0] != -1:
				objects["nodes"].append( [self._posns[n,0],0,self._posns[n,1]] )

		#
		# Springs.
		#
		maxMagTension = 0.0
		for n in range(self._numNodes):
			for k in range(6):
				maxMagTension = max( maxMagTension, abs(self._tensions[n,k]) )

		for n1 in range(self._numNodes):
			x1, y1 = self._posns[n1]

			# Loop over all possible connections; max. 6 for a triangular lattice.
			for k in range(6):
				n2 = self._conns[n1,k]
				if n2==-1: continue									# -1 means 'no connection'
				
				# Relative vector from n1 to n2, taking into account periodic BCs
				dx = ( self._posns[n2] - self._posns[n1] + 0.5*X ) % X - 0.5*X

				# Line start and end.
				start = [ x1      , 0, y1       ]
				end   = [ x1+dx[0], 0, y1+dx[1] ]

				# Colour and thickness if it was possible to scale to the maximum tension.
				if maxMagTension>1e-4:
					lineWidth = 3*abs(self._tensions[n1,k])/maxMagTension + 0.2
					colour    = ( (1,0,0) if self._tensions[n1,k]>0.0 else (0,0,1) )
				else:
					lineWidth = 1.0
					colour    = (0.5,0.5,0.5)				# Grey if also displaying displacements.

				# Add to the springs field in the return object.
				objects["springs"].append( [start,end,colour,lineWidth] )

		return objects


	def addToPlot_matplotlib( self, pub=False, init=False ):
		"""Adds network state in terms of matplotlib function calls; does not 'show()'.
		Plots the springs first, and then the nodes (so the nodes go on top).
		pub=True for publication-suitable format."""
		
		# Shorthand for the factor (relative to axes) to plot periodic copies.
		pBC = self._perBCFac()

		# Node size ('markersize' in matplotlib); needs to be relative to the system size.
		nodeSize = min(self._X,self._Y) / 10.0

		# To scale the colouring for spring tensions, fing the maximum tension/compression first of all.
		# Note non-existent springs are stored as zero tension, so don't need to check for them here.
		maxMagTension = 0.0
		for n in range(self._numNodes):
			for k in range(6):
				maxMagTension = max( maxMagTension, abs(self._tensions[n,k]) )

		# Plot the springs as lots of lines, using colour if only the initial state is being displayed.
		# Note this is a full double loop, to help with drawing the springs across the periodic boundaries.
		X = np.array( [self._X,self._Y] )						# To accelerate applying periodic BCs
		for n1 in range(self._numNodes):
			x1, y1 = self._posns[n1]

			# Loop over all possible connections; max. 6 for a triangular lattice.
			for k in range(6):
				n2 = self._conns[n1,k]
				if n2==-1: continue									# -1 means 'no connection'
				
				# Colour for tension/compression, thickness for magnitude; only if initial state and the magnitude
				# is not essentially zero (can happen w/o prestress because of f.p. errors across the boundaries).
				if init and maxMagTension>1e-4:

					# Check for very low tensions that are probably zero. Can happen when m==n.
#					if abs(self._tensions[n1,k])/maxMagTension < 1e-7: print( abs(self._tensions[n1,k])/maxMagTension )

					lineWidth = 3*abs(self._tensions[n1,k])/maxMagTension + 0.2
					colour    = ( "r" if self._tensions[n1,k]>0.0 else "b" )
				else:
					lineWidth = 1.0
					colour    = "0.5"				# Grey if also displaying displacements.

				# Relative vector from n1 to n2, taking into account periodic BCs
				dx = ( self._posns[n2] - self._posns[n1] + 0.5*X ) % X - 0.5*X

				# Need to draw a line from node n1 to node n2
				plt.plot( [x1,x1+dx[0]], [y1,y1+dx[1]], color=colour, zorder=0, linewidth=lineWidth )

				# Also the nearest periodic copies. Try to minimise the number redraw with "pBC" as this can be expensive.
				for offset in ( [X[0],0], [-X[0],0], [0,X[1]], [0,-X[1]], [X[0],X[1]], [X[0],-X[1]], [-X[0],X[1]], [-X[0],-X[1]] ):
					x = x1 + offset[0]
					y = y1 + offset[1]

					if x>(1+pBC)*X[0] or x<-pBC*X[0] or y>(1+pBC)*X[1] or y<-pBC*X[1]: continue

					plt.plot( [x,x+dx[0]], [y,y+dx[1]], color=colour, zorder=0, linewidth=lineWidth )


		# Plot the nodes as points. Wrap to periodic boundaries. Not if the initial state.
#		plt.plot( (self._posns%X)[:,0], (self._posns%X)[:,1], "ko", markersize=nodeSize, zorder=10 )		# Plot all nodes in the save file.
		if not init:
			for n in range(self._numNodes):				# Only plot nodes with at least 1 connection.
				if self._conns[n,0] != -1:
					plt.plot( [self._posns[n,0]], [self._posns[n,1]], "ko", markersize=nodeSize, zorder=10 )

		# Plot the solution as line segments,or arrows if they are long enough.
		# Don't bother with the out-of-phase component for publication plots. Also don't display if only the
		# initial network state was requested.
		if not init:
			maxMag = 0.0
			for n in range(self._numNodes):
				mag_real = math.sqrt( self._disps[n][0].real**2 + self._disps[n][1].real**2 )
				mag_imag = math.sqrt( self._disps[n][0].imag**2 + self._disps[n][1].imag**2 )
				maxMag = max( maxMag, mag_real, mag_imag )

			if maxMag:
				for n in range(self._numNodes):
					if self._conns[n,0]==-1: continue		# Don't plot displacements for disconnected nodes.

					self._plotDisp( self._posns[n], self._disps[n][0].real, self._disps[n][1].real, maxMag, ("k" if pub else "r"), 100 )
					if not pub:
						self._plotDisp( self._posns[n], self._disps[n][0].imag, self._disps[n][1].imag, maxMag, "g", 90 )
			else:
				print( "Not plotting displacements; max. magnitude zero." )
			
	
	def __str__( self ):
		"""Basic info about the network."""
		return "Triangular spring network with nodal dimensions: {0} x {1}".format(self._numX,self._numY)

	