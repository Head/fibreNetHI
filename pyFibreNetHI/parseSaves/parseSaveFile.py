#
# Parses files output by the fibreNetHI (C++) code.
# Can be called from the command line - see code at end of this file.
#

#
# Imports
#

# Standard imports
import matplotlib.pyplot as plt
import io
import tarfile

# Project imports
from . import networkNone
from . import networkTriSpring
from . import networkTriSpringStrip
from . import fluidDragOnly
from . import fluidStokes
from . import drivingBase


class parseSaveFile:

	def __init__( self, fnameOrReader, verbose=False ):
		"""Call with the filename of the file output by the C++ code."""
	
		# Initialise the parameters and modules
		self._X, self._Y, self._omega = None, None, None
		self._network = None
		self._fluid   = None
		self._verbose = verbose
		
		# Convert filename to readable object; if fails, assume it was a reader (e.g. an open file) in the first place.
		try:
			file = open( fnameOrReader, 'r' )
		except TypeError:
			file = io.TextIOWrapper( fnameOrReader )
	
		#	
		# Parse the file line by line
		#

		# The first line should start with 'fibreNetHI', otherwise this isn't a save file
		line = file.readline().strip()

		if not line.startswith( "fibreNetHI" ):
			print( "File '{}' does not appear to be an output solution file from fibreNetHI.".format(fname) )
			return
		if self._verbose:
			print( "Reading fibreNetHI file with first line: '{}'".format(line) )

		# The next line should be the box dimensions
		self._X, self._Y = [ float(str) for str in file.readline().split() ]
		if self._verbose:
			print( "Box dimensions: {0} x {1}".format(self._X,self._Y) )
		
		# The next line is the frequency corresponding to the solution that follows; if negative, convergence
		# was not achieved (currently just warn but could bail). Zero denotes a static solution.
		self._omega = float( file.readline().strip() )
		if self._omega==-1:
			print( "No frequency specified; only have initial state." )
		else:
			if self._omega<0.0:
				print( "WARNING: Negative frequency. This solution did not converge!" )
			else:
				if self._verbose:
					print( "Omega = {0}".format(self._omega) )
		
		# The next line specifies the type of shear applied.
		self._driving = drivingBase.drivingBase(self._X,self._Y,self._omega,file.readline().strip())

		# The next line specifies the type of network. Needs to be updated as more networks are devised.
		line = file.readline().strip()

		if( line=="networkNone" ):
			self._network = networkNone.networkNone(self._X,self._Y,file,self._driving,self._verbose)
		if( line=="networkTriSpringStrip" ):
			self._network = networkTriSpringStrip.networkTriSpringStrip(self._X,self._Y,file,self._driving,self._verbose)
		if(
			line=="networkTriSpring"
			or line=="networkTriSpringGenSierp"
			or line=="networkTriSpringSierpinski"
			or line=="networkTriSpringCorrelated"
		):
			self._network = networkTriSpring.networkTriSpring(self._X,self._Y,file,self._driving,self._verbose)

		if self._network == None:
			print( "Network descriptor '{}' not understood; is this a new network type?".format(line) )
		
		# The next line specifies the type of fluid. Absent for a static solution with omega=0.
		if self._omega > 0.0:
			line = file.readline().strip()

			if( line=="fluidDragOnly" ):
				self._fluid = fluidDragOnly.fluidDragOnly(self._X,self._Y,file,self._driving,self._verbose)
			
			if( line=="fluidStokes" ):
				self._fluid = fluidStokes.fluidStokes(self._X,self._Y,file,self._driving,self._verbose)

			if self._fluid == None:
				print( "Fluid descriptor '{}' not understood; is this a new fluid type>".format(line) )

		
	def preparePlot_matplotlib( self, network=True, fluid=True, pub=False, init=False ):
		"""Prepares the plot using matplotlib.pyplot function calls, but does not call 'show()'.
		If pub=True, will try to choose options and colours suitable for publication."""

		# Add the network.
		if network and self._network: self._network.addToPlot_matplotlib( pub, init )

		# Add the fluid; not if only the initial state is to be displayed.
		if fluid and self._fluid and not init: self._fluid.addToPlot_matplotlib( pub )

		# Truncate to the box dimensions
		plt.xlim( 0.0, self._X )
		plt.ylim( 0.0, self._Y )
		
	def __str__( self ):
		"""Prints basic info about the combined network and fluid objects."""

		str  = "fibreNetHI save file\n"

		str += "- \\omega = {0}\n".format(self._omega)
		str += "- box dimensions: {0} x {1}\n".format(self._X,self._Y)

		str += "- {}\n".format( self._network.__str__() )
		str += "- {}\n".format( self._fluid  .__str__() )
			
		return str.rstrip("\n")

	def omega( self ):
		"""The driving frequency."""
		return self._omega

	def boxDims( self ):
		"""The box dimensions as a duple."""
		return ( self._X, self._Y )

	def network( self ):
		"""The network object."""
		return self._network
	
	def fluid( self ):
		"""The fluid object."""
		return self._fluid

	def networkCompression( self ):
		"""Returns the modulus of the relative change in network area, so zero means no change in area and
		a positive value means the area changed locally. Returned value is complex (real=in-phase etc.)."""
		return self._network.compression()
	
	def fluidCompression( self ):
		"""Returns the modulus (of real and imaginary components separately) of the relative change in fluid
		mesh cells, so a positive value means there were local changes."""
		return self._fluid.compression( self._omega )

	#
	#MARK Class methods
	#
	
	# Extract multiple save files from an archive.
	@classmethod
	def fromArchive( cls, archiveName ):
		
		arc = tarfile.open( archiveName )
		return [ parseSaveFile( arc.extractfile(savefile.name) ) for savefile in arc.getmembers() ]
		
