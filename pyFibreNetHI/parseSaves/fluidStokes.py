#
# Parsing, visualisation and post-run analysis of fluid flow fields solved
# using the Stoke's equations with coupling at network nodes.
#

#
# Imports
#

# Standard imports
import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

# Project imports
from . import fluidBase


class fluidStokes( fluidBase.fluidBase ):

	def __init__( self, X, Y, file, driving, verbose=False ):
		"""Call with the box dimensions, and a file object that has already parsed the network
		identification line (so the next line to be read will be the first non-trivial output
		by fluidStokes).
		"""
		
		# Call the parent class, which will store the box dimensions in self._X and self._Y
		# the viscosity in self._eta, and the drag coefficient in self._zeta.
		super().__init__(X,Y,file,driving,verbose)
	
		# Get the number of mesh cells in each direction, and calculate the corresponding cell size
		self._numX, self._numY = [ int(str) for str in file.readline().split() ]
		self._dx,   self._dy   = [ self._X/self._numX, self._Y/self._numY ]

		# Create np arrays for the velocity and pressure meshes
		self._P = np.zeros( shape=(self._numX,self._numY  ), dtype="complex128" )
		self._v = np.zeros( shape=(self._numX,self._numY,2), dtype="complex128" )

		# Get the pressure mesh, output 'as visualised' (i.e. outer loop y, inner loop x)
		for j in range(self._numY):
			colStr = file.readline().split()		# Columns for this node, each as strings

			# Quick sanity check			
			if len(colStr) != self._numX:
				print( "Malformed save file; pressure mesh width does not match numX" )
				return
			
			for i in range(self._numX):
				real, imag = [ float(val) for val in colStr[i].strip("()").split(",") ]
				self._P[i,j] = real + 1j*imag

		# Same for the velocity mesh, which was output as x-cpts in one block, then y-cpts
		for j in range(self._numY):
			for i, col in enumerate( file.readline().split() ):
				real, imag = [ float(val) for val in col.strip("()").split(",") ]
				self._v[i,j,0] = real + 1j*imag

		for j in range(self._numY):
			for i, col in enumerate( file.readline().split() ):
				real, imag = [ float(val) for val in col.strip("()").split(",") ]
				self._v[i,j,1] = real + 1j*imag
		
	# Plots a single vector, passed as ux and uy assumed to be real.
	def _plotDisp( self, posn, ux, uy, maxMag, color, zorder ):
		calib = 1.0/maxMag
		
		# Arrow if long enough, else a line segment
		if math.sqrt(ux**2+uy**2) > 0.1*maxMag:
			plt.arrow(
				posn[0], posn[1], calib*ux, calib*uy,
				width=0.05, length_includes_head=True, color=color, zorder=zorder
			)
		else:
			plt.plot(
				[posn[0],posn[0]+calib*ux], [posn[1],posn[1]+calib*uy],
				color=color, linewidth=2, zorder=100
			)

	def __str__( self ):
		"""Returns identification string."""
		return "Stokes fluid"
	
	def velocityAt( self, x, y ):
		"""Returns the complex 2-vector for the velocity interpolated at the given point."""
		
		# x-coordinate first. This follows the C++ code.
		i     = int  ( x/self._dx )
		red_x = float( x - i*self._dx ) / self._dx
		
		if i==self._numX: i = 0
		ip1 = ( i+1 ) % self._numX
		
		# Sim. for the y-coord.
		j     = int  ( y/self._dy )
		red_y = float( y - j*self._dy ) / self._dy
		
		if j==self._numY: j = 0
		jp1 = ( j+1 ) % self._numY

		# Bilinear interpolation.
		v_00 = self._v[i  ,j  ]
		v_01 = self._v[i  ,jp1]
		v_10 = self._v[ip1,j  ]
		v_11 = self._v[ip1,jp1]
		
		# Periodicity; add offset to the x-velocities if the upper two nodes are in the periodic copy.
		# The need for the 0.5 here relates to the centre being at (X/2,Y/2) - again, can generalise 
		# when necessary.
		perBC = 0.5 * self._driving.velocityPeriodicOffset( (ip1==0), (jp1==0) )

		# Return with the interpolated velocity. perBC cancels in final term.
		return v_00 + red_x*(v_10-v_00+perBC) + red_y*(v_01-v_00+perBC) + red_x*red_y*(v_11+v_00-v_01-v_10)


	def addToPlot_matplotlib( self, pub=False ):
		"""Adds fluid state in terms of matplotlib function calls; does not call 'show()'.
		pub=True for publication-suitable format."""
		
		# Options for what to display.
		plot_P = False
		plot_v = True

		# Pressure is displayed as rectangles, centred on the mesh node which was already half-shifted
		# from the velocity mesh - handily this means we can just plot rectangles flush to the boundaries.
		if plot_P:

			# Find maximum magnitude of pressure.
			maxP = np.max( np.abs( self._P ) )
			minP = np.min( np.abs( self._P ) )
			if maxP <= minP:
				print( "WARNING: Pressure plot will not work (maxP <= minP)" )
								
			for i in range(self._numX):
				for j in range(self._numY):

					if abs(self._P[i,j]):			# Can only plot if non-zero.
						delta = ( abs(self._P[i,j]) - minP ) / ( maxP - minP )
						colour = "#0000{:02x}".format(int(255*delta))
					else:
						colour = "k"

					plt.gca().add_patch( patches.Rectangle((i*self._dx,j*self._dy),self._dx,self._dy,color=colour,zorder=0) )

		# Velocity field; use blue/green arrows.
		if plot_v:

			# First get the maximum magnitude of all velocities. Should really be evaluating all of these
			# at the midpoints of cells, as per the actual plotting of arrows (below), but this should be
			# faster and it probably won't make much difference.
			maxMag = 0.0
			for i in range(self._numX):
				for j in range(self._numY):
					mag_real = math.sqrt( self._v[i,j,0].real**2 + self._v[i,j,1].real**2 )
					mag_imag = math.sqrt( self._v[i,j,0].imag**2 + self._v[i,j,1].imag**2 )
					maxMag = max( maxMag, mag_real, mag_imag )

			# Now plot real and imaginary components, colour-coded. Don't bother with the in-phase component
			# for publications as barely visible anyway.
			if maxMag:
				for i in range(self._numX):
					for j in range(self._numY):

						# Take mid-points between cells, which means interpolating from the given vectors at corners.
						x, y = (i+0.5)*self._dx, (j+0.5)*self._dy
						vel = self.velocityAt( x, y )

						if not pub:
							self._plotDisp( (x,y), vel[0].real, vel[1].real, maxMag, "g", 50 )

						self._plotDisp( (x,y), vel[0].imag, vel[1].imag, maxMag, ( "gray" if pub else "b" ), 40 )
			else:
				print( "Not plotting fluid velocities; max. magnitude zero." )

	def compression( self, omega ):
		"""Returns relative changes in area for each fluid mesh cell. The modulus of the relative change in 
		area of each fluid mech cell is calculated, for real and imaginary components."""
		
		# Loop over all mesh cells.
		dA = 0.0
		for j in range(self._numY):
			for i in range(self._numX):
				
				# Indicies of adjacent nodes, taking into account periodicity.
				ip1 = ( i + 1 ) % self._numX
				jp1 = ( j + 1 ) % self._numY
				
				# For convenience, the four nodal velocities in the same notation as the notes.
				v1_x, v1_y = self._v[i  ,jp1,0], self._v[i  ,jp1,1]
				v2_x, v2_y = self._v[ip1,jp1,0], self._v[ip1,jp1,1]
				v3_x, v3_y = self._v[i  ,j  ,0], self._v[i  ,j  ,1]
				v4_x, v4_y = self._v[ip1,j  ,0], self._v[ip1,j  ,1]

				# The Lees-Edwards BCs are not included as they will not affect the result - since it would amount the shifting
				# the node(s) at the top of the rectangle 'to the right', it is just a shear that does not affect the area.

				# Change in area. This is the linearised expression.
				cell_dA = 0.5*self._dy*( v2_x + v4_x - v1_x - v3_x ) + 0.5*self._dx*( v1_y + v2_y - v3_y - v4_y )
				
				# Add a non-negative local change in area to the running total.
				dA += abs(cell_dA)
		
		# Return normalised to omega and the original area.
		return dA / ( omega * self._X * self._Y )
		



			  