#
# Base class for the driving types. Currently this also handles all recognised drivings, but
# this could be broken down into the same class hierarchy as the C++ code in the future
# if necessary.
#


#
# Imports.
#
import sys
import numpy as np


class drivingBase:

	def __init__( self, X, Y, omega, drivLine ):
		"""Initialise with the box X and Y dimensions, and the line in the save file for the driving."""

		# Store system size and frequency.
		self._X = X
		self._Y = Y
		self._omega = omega

		# Initialise persistent variables for the strain tensor and the pressure gradient.
		self._strainTensor = [[0,0],[0,0]]			# Real 2x2 tensor.
		self._pressureGdt  = [0,0]					# Complex 2-vector.

		# Legacy: Old code only output the label, so check which save file format we have.
		if len( drivLine.split() )==1:

			# Only 3 driving types used the old format.
			if drivLine not in ("drivingSimpleShear","drivingExtensionalShear","drivingPressureGdt"):
				sys.exit( "Driving '{}' not recognised.".format(self._type) )

			# Set strain and pressure gradient based on the type.
			if drivLine == "drivingSimpleShear":
				self._strainTensor[0][1] = 1
			
			if drivLine == "drivingExtensionalShear":
				self._strainTensor[0][0] = 1
				self._strainTensor[1][1] = -1
			
			# Nothing currently to be done for the pressure gradient (only) driving.

		else:

			cpts = drivLine.split()
			if len(cpts) != 9:
				sys.exit( "Could not read driving line in save file - unexpected number of elements (and not legacy)." )

			# The label - cpts[0] - may be e.g. drivingArbStrain, but it no longer matters here.

			self._strainTensor[0][0] = float(cpts[1])
			self._strainTensor[1][0] = float(cpts[2])
			self._strainTensor[0][1] = float(cpts[3])
			self._strainTensor[1][1] = float(cpts[4])

			self._pressureGdt[0] = complex( float(cpts[5]), float(cpts[6]) )
			self._pressureGdt[1] = complex( float(cpts[7]), float(cpts[8]) )


	def velocityPeriodicOffset( self, horizontalBdy, verticalBdy ):
		"""Returns a complex 2-vector of the shift across the corresponding boundaries, which are 'True' or 'False'."""

		# Should match the C++ code.
		xShift  = 0
		if horizontalBdy: xShift += self._X * self._strainTensor[0][0]
		if verticalBdy  : xShift += self._Y * self._strainTensor[0][1]

		yShift  = 0j
		if horizontalBdy: yShift += self._X * self._strainTensor[1][0]
		if verticalBdy  : yShift += self._Y * self._strainTensor[1][1]

		return np.array( [ 1j * xShift, 1j * yShift ] )

