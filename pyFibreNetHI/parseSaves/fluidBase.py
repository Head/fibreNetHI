#
# Abstract base for the fluid classes.
#

# Imports
from abc import ABCMeta, abstractmethod


class fluidBase( metaclass=ABCMeta ):

	def __init__( self, X, Y, file, driving, verbose=False ):
	
		# Store the box dimensions and options.
		self._X = X
		self._Y = Y
		self._driving = driving
		self._verbose = verbose
		
		# Extract the viscosity and drag coefficient, and store.
		nextLineCols = file.readline().strip().split()
		self._eta  = float( nextLineCols[0] )
		self._zeta = float( nextLineCols[1] )

	def __str__( self ):
		return "<fluid base>"

	@property
	def zeta( self ):
		return self._zeta
	
	@abstractmethod
	def addToPlot_matplotlib( self, pub=False ):
		"""Adds visual representation of the fluid to a matplotlib display. pub=True for publication-suitable format."""
		pass

	@abstractmethod
	def velocityAt( self, x, y, omega ):
		"""Returns the complex 2-vector for the velocity interpolated at the given point."""
		pass
	
	@abstractmethod
	def compression( self, omega ):
		"""Returns relative changes in area for each fluid mesh cell."""
		pass

