#
# Parsing and analysis of a drag-only fluid (no visualisation to speak of)
#
# Initialise with a file object already pointing to the first line of data
# (i.e. NOT including the identifier line 'fluidDragOnly')
#


#
# Imports
#

# Standard imports
import numpy
import matplotlib.pyplot as plt

# Project imports, including the abstract base
from . import fluidBase


class fluidDragOnly( fluidBase.fluidBase ):

	def __init__( self, X, Y, file, driving, verbose=False ):
		"""Call with the box dimensions, and a file object that has already parsed the network
		identification line (so the next line to be read will be the first non-trivial output
		by fluidDragOnly).
		"""
		
		# Call the parent class, which will also extract the viscosity into self._eta.
		super().__init__(X,Y,file,driving,verbose)
		
		# Currently no other information output

	def __str__( self ):
		"""Returns basic info as a string."""
		return "Drag only"

	def addToPlot_matplotlib( self, pub ):
		"""Adds fluid state in terms of matplotlib function calls; does not 'show()'."""
		pass
		
	def velocityAt( self, x, y, omega ):
		"""Returns the complex 2-vector for the velocity interpolated at the given point."""
		return [ 1j*omega*(y-self._Y/2), 0j ]  

	def compression( self, omega ):
		"""Returns relative changes in area for each fluid mesh cell."""
		return 0.0

	