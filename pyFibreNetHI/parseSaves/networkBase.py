#
# Abstract base for the network classes.
#

# Imports
from abc import ABCMeta, abstractmethod


class networkBase( metaclass=ABCMeta ):

	def __init__( self, X, Y, driving, verbose=False ):
		self._X = X
		self._Y = Y
		self._driving = driving
		self._verbose = verbose

	def __str__( self ):
		return "<base method>"

	@abstractmethod
	def addToPlot_matplotlib( self, pub=False, init=False ):
		"""Adds visualisation to matplotlib.pylot"""
		pass

	@abstractmethod
	def addToPlot_OpenGL( self, pub=False, init=False ):
		"""Adds visualisation using OpenGL methods."""