#
# Parsing of the no-network case, which obviously doesn't require much implementation.
#


# The abstract base
from . import networkBase


class networkNone( networkBase.networkBase ):

	def __init__( self, X, Y, file, driving, verbose=False ):
		super().__init__(X,Y,driving,verbose)

	def __str__( self ):
		return "No network."

	def addToPlot( self ):
		return
