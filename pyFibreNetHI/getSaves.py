#
# Parses files output by the fibreNetHI (C++) code.
# Can be called from the command line - see code at end of this file.
#

#
# Imports
#

# Standard imports
import matplotlib.pyplot as plt
import argparse

# Project imports.
import pyFibreNetHI.parseSaves.parseSaveFile


#
# If called from the command line
#
if __name__ == "__main__":

	#
	# Command line arguments. The save state to analyse/display.
	#
	parser = argparse.ArgumentParser( description="Display and/or analyse a single saved state" )
	parser.add_argument( "file", help="filename for saved state(s)" )
	parser.add_argument( "-p", "--pub" , help="Plot suitable for publication", action="store_true" )
	parser.add_argument( "-i", "--init", help="Display initial network state only", action="store_true" )
	parser.add_argument( "-s", "--save", help="Save as a pdf file; else will display to screen", action="store_true" )
	args = parser.parse_args()

	# If looks like an archive file, extract in bulk and do ... something. Otherwise, display the single save file.
	if args.file.endswith( ".tar.gz" ):

		print( "Attempting to extract save files from archive '{}' ...".format(args.file) )
		saves = pyFibreNetHI.parseSaves.parseSaveFile.parseSaveFile.fromArchive( args.file )

		# Display network/fluid compression for each frequency.
		# for save in saves:
		# 	print( "{0:10.5g} {1:10.5g} {2:10.5g}".format(save.omega(),save.networkCompression(),save.fluidCompression()) )

		# Display magnitude of the real and imaginary components of the displacements.
		for save in saves:
			N, w, magR, magI = save._network._numNodes, save.omega(), 0.0, 0.0
			for ux, uy in save._network._disps - saves[-1]._network._disps:
				magR += 0.5*( abs(ux.real) + abs(uy.real) )
				magI += 0.5*( abs(ux.imag) + abs(uy.imag) )
			print( "{0:10.5g} {1:10.5g} {2:10.5g}".format(w,w**0.0*magR/N,w**0.0*magI/N) )

	else:

		obj = pyFibreNetHI.parseSaves.parseSaveFile.parseSaveFile( args.file )
		obj.preparePlot_matplotlib( pub=args.pub, init=args.init )

		# Remove ticks and tick labels.
		plt.xticks([])
		plt.yticks([])

		# Ensure correct aspect ratio.
		plt.gca().set_aspect( "equal" )

		# Display or save.
		if args.save:
			plt.savefig( "save.pdf", bbox_inches="tight", transparent=True )
		else:
			plt.show()

