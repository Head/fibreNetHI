#
# Essentially a convenience wrapper for 'runBase' in the runControl sub-module,
# which is the base runControl object that requires a parameters file.
#
# Call from the command line with '-h'/'--help' to see options.
#
import runControl

if __name__ == "__main__":
	runControl.runBase().fullCycle()