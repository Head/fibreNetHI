#
# Makefile for the fibreNetHI code for the Centos Desktop machines.
# Requires the intel compiler module to have been loaded.
#

CXX = icpc -O3
CXXFLAGS := -Wall -std=c++0x -Wno-unknown-pragmas

SOURCES = $(wildcard src/*)
HEADERS = $(wildcard inc/*)
OBJECTS = $(patsubst src/%.cpp,obj/%.o,$(wildcard src/*.cpp))

INCLUDES = -Iinc -I/usr/include/libxml2
LIBDIRS  = 
LDLIBS   = -lxml2

# Next lines required for the sparse direct solve option.
CXXFLAGS := $(CXXFLAGS) -DSUPERLU
LIBDIRS := $(LIBDIRS) -L SuperLU_5.2.1/lib -L SuperLU_5.2.1/CBLAS
LDLIBS := $(LDLIBS) -lsuperlu -lblas
INCLUDES := $(INCLUDES) -I SuperLU_5.2.1/SRC

# Default
.PHONY: all
all: reset
all: fibreNetHI

# Compile main code; if called directly, will not debug or remove output/object files
.PHONY: fibreNetHI
fibreNetHI: $(OBJECTS)
	$(CXX) $(CXXFLAGS) $(OBJECTS) $(INCLUDES) $(LIBDIRS) main.cpp $(LDLIBS) -o fibreNetHI

# Automatic dependency generation uses .d files on the object-code subdir
-include $(OBJECTS:.o=.d)

# Compile paired .cpp/.h files; place dependency files on object-code subdir
obj/%.o: src/%.cpp inc/%.h
	$(CXX) $(CXXFLAGS) $(INCLUDES) -c -o $@ $<
	-@$(CXX) $(CXXFLAGS) $(INCLUDES) -MF"$(@:.o=.d)" -MG -MM -MP -MT"$@" $<

# Clear output files, either from from main or viewer
.PHONY: reset
reset:
	rm -f log.out
	rm -f save_????.out
	rm -f *.dat

# Clean intermediaries from compilation (object and dependency files) and executables
.PHONY: clean
clean: reset
clean:
	rm -f fibreNetHI
	rm -rf obj/*.o
	rm -rf obj/*.d

